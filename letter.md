
Hello, If you looking for fontend developer with  big expirience with FabricJS - I could be the best match.
I worked for different projects using this library and i know it very well.  I can rewrite any module from original library. 
Your job seems to be very interesting for me!
I could use AngularJS or ReactJS in the development on the front-end and NodeJS/expressJS on the back-end.

My projects included drag and drop, move , resize and many more features. for example, different shapes, text objects, curved text, multiple slides, clipping areas, image frames, custom filters, image editing tools (selection and drawing tools like in photoshop) etc.  
And also backend rendering to PNG/SVG and PDF.
My skype id is "den.ponomarev". 
I will be glad to talk to you, look forward to hearing from you soon
Denis.

You could check some examples of my work:
Card editor for SimplySay.
https://www.simplysay.sg/
Babybook editor:
https://www.bejaboo.com/
https://youtu.be/YYft1rU6Ues 
Product customizer:
https://stitchcounts.com/ 
https://youtu.be/6aw_VfhzaSU

tools for Image and video annotation servicesfor Computer Vision:
https://microwork.io/




If you are happy to work with i am ready to make a POC for you.
I could include the following to you:
- all in-the-box features of FabricJS (drag`n`drop, moving/resizing etc)
- shapes with custom handlers,
- curved text,
- 3D transformations
- multiple slides, 
- image decorations and filters from 3rd libraries such as Caman.js
- image editing tools (selection and drawing tools like in photoshop)
 and more
what would you like to see the most?