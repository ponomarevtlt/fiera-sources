//Editor Plugin. Allow to work with many other plugins.
new fabric.Editor({
    lang: toolbox.lang,
    id: "outlay-tool",
    //fonts plugin. allow to load fonts from your server or from Googlefonts
    "fonts": [
      "Arial",
      "Arial Black",
      "Times New Roman",
      "Comic Sans MS",
      "Courier New",
      "Georgia",
      "Impact",
      "Lucida Console",
      "Oswald",
      "Tahoma",
      "Tangerine",
      "Papyrus",
      "Poppins",
      "Roboto",
      "Raleway",
      "Oxygen",
      "Kaushan Script",
      "Open Sans",
      "PT Sans",
      "Patrick Hand SC",
      "Ubuntu"
    ],
    //default grid size
    gridSize: 41.6,
    //auto save plugin. allow to save your data to LocalStorage or to the webserver
    save: function () {...},
    autoSave: true,
    canvasContainer: "outlay-canvas",
    eventListeners: {/*Custom Events Handlers*/},
    //Toolbars plugin. Could work with most of FabricjS basic properties
    toolbars: {
      editor: {
        container: "application-menu"
      },
      canvas: {
        container: "slide-menu"
      },
      objects: {
        container: "object-menu"
      },
    },
    tools: {
      Editor: [
        "clearObjects",
        "undo",
        "redo",
        "grid"
      ],
      Canvas: [
        "addText",
        "backgroundColor",
        "uploadBackgroundImage",
        "uploadOverlayImage",
      ] ,
      Object: [
        "moveUp",
        "moveDown",
        "moveLeft",
        "moveRight",
        "order",
        "duplicate",
        "remove"
      ],
      Photo: [
        "rotateLeft",
        "rotateRight",
        "cropZoomOut",
        "cropZoom",
        "cropZoomIn",
        "cropEnd"
      ],
      Image: [
        "rotateLeft",
        "rotateRight",
        "*"
      ],
      Emoji: [
        "*"
      ],
      Clipart: [
        "*"
      ],
      Frame: [
        "rotateLeft",
        "rotateRight",
        "filters",
        "frame",
        "crop",
        "stretch",
        "*"
      ],
      Textbox: [
        "textAlign",
        "bold",
        "italic",
        "underline",
        "font",
        "fill",
        "*"
      ]
    },
    //customizinh tools from Toolbar plugin.
    actions: {
      Editor: {
        clearObjects: {
          title: "@canvas.clearObjects", //multilanguage
          className: "clip-icon clear_All"
        },
      }
    }
    history: true,
    //Prototypes plugin. allow to modify objects behavior
    prototypes: {
      Canvas: {
        "loader": "canvas-loader",
        "backgroundColor": "#FFFFFF",
        "backgroundPosition": "fill",
        "overlayPosition": "fill",
        "stretchable": true,
        "allowTouchScrolling": true,
        "animated": true,
        "preserveObjectStacking": true,
        "controlsAboveOverlay": true,
        "gridColor": "#cccccc",
        "droppable": true,
        "frontObjectsSelectionPriority": true,
        "deafultText": "Add some text here",
        "defaultTextType": "textbox"
      },
      //allow to create new classes easily
      "CanvasLoader": {
        "position": "center",
        "prototype": "group",
        "objects": [
          {
            "type": "text",
            "originX": "center",
            "text": "loading your design, just a moment...",
            "top": 25,
            "fontSize": 22,
            "color": "rgba(84, 84, 84, 1)"
          },
          {
            "originX": "center",
            "originY": "center",
            "type": "animation",
            "animation": {
              "size": 5,
              "angle": 0,
              "speed": 0.3,
              "fade": 0.5
            }
          }
        ]
      },
      Object ...
      Rect ...
    },
    //Allow to work with Projects with multiple slides.
    slides: [
      <fabric.Canvas Object>,
      <fabric.Canvas Object>
    ],
    //Gallery plugin. allow drag/drop photos from panel to the canvas.
    galleries: {
      photo: {
        container: "photos-gallery",
        elements: toolbox.galleries.photos,
        mapper: function (val) {
          return {
            preview: "image",
            type: "frame",
            thumbnail: toolbox.paths.photoThumbnail + val,
            photo: toolbox.paths.photoWeb + val
          }
        },
        activateOptions: function (data) {
          //add photo on canvas by clicking on the element
          return this.application.canvas.getPhotoPlaceholder(data);
        },
        itemClass: "prevClose",
        removeButtonTemplate: '<span class="fa fa-close closeIMG">',
        onRemove: function (data) {},
        removable: true
      }
    },
    onResize: function (containerSize, canvasSize) {...},
    onReady: function () {}
    onLoad: function () {}
  });
};