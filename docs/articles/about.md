# What all this about?

Set of extensions for FabricJS library.

Allows to create complex FabricjS functionality with a short configuration. 

Core modules are 
 - **Application** - Class which helps to Initialize FabricJS canvas and its behavior. Could be configured in a very different way. 
 - **Slide** - extension for **fabric.Canvas**.
 - **Object**  - extension for **fabric.Object**.
 
Includes a set of extension with folowing features
 - new Shapes
    - mask rectangle
    - draggable mark
 - new Brushes
    - Bucket Brush
    - Pen Brush
    - Pensil Brush
    - Points Brush
    - Rectangle Brush
 - extended functionality. 
    - custom cursors
    - custom controls
    - event listeners
    - object prototypes
    - console debugging
