# fabric.Object

Every object on canvas have new attributes which could be defined in Object prototypes.

### attribute: responsiveBorders

width of the object borders is always == strokeWidth idepends on canvas zoom

```
responsiveBorders:boolean
```

### attribute: type
type of the loaded objects by default(if not specified).
We could not store the type of the objects if all of them have the same type

```
type: "mask-rectangle"
```

### attribute: cornerSize

the size of the visible part of the object control points
```
cornerSize: 8,
```

### attribute: cornerAreaSize

the size of the invisible part of the object control points

```
cornerAreaSize: 20
```

