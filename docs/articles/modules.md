# Modules

Extensions coulb have very different roles. 
Extension could add and overwrite some methods , setter functions, properties , event listeners of fabric class prototypes
 
 ```javascript
 
fabric.util.object.extend(fabric.Object.prototype, {
  property: value,
  setProperty: function(val){},
  eventListeners: fabric.util.object.extendArraysObject(fabric.Object.prototype.eventListeners, {
    added: function () {/* do something */}
  })
})

 ```
 or define new fabric Object Classes.
 
 ```javascript
 
fabric.NewObjectClass = fabric.util.createClass(fabric.Object,{
    type: "new-object-class",
    //properties and methods
});
fabric.NewObjectClass.fromObject = function(options){
    return new fabric.NewObjectClass(options);
};


```
