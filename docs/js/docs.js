var $helpTitle = $(".help-title");
var $helpContent = $(".help-content");

function useMarkDown (el,_content) {

  var md = MarkDownIt({
    //html:         false,
    highlight: function (str, lang) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(lang, str).value;
        } catch (__) {}
      }
      return ''; // use external default escaping
    }
  });
  // var hljs = re?quire('highlight.js'); // https://highlightjs.org/

  el.html(md.render(_content));
}

$.getJSON("js/tree.json", function (data) {

  $('#tree').treeview({
    expandIcon: 'fa fa-plus',
    collapseIcon: 'fa fa-minus',
    emptyIcon: 'glyphicon',
    levels: 1,
    data: data,
    onNodeSelected: function (event, data) {
      $helpTitle.empty()
      $helpContent.empty()

      if(data.article){
        createHelp("articles/" + data.article );
      }else{
        $helpTitle.append($("<h2>").text(data.text));
        useMarkDown($helpContent,data.content);
      }
    }
  });
  $('#tree').treeview('expandAll', { levels: 1, silent: true });
});


function createHelp(helpFile) {

  $helpContent.addClass("markdown-body");

  if(/^.*\.(json|dox)$/.test(helpFile)) {
    $.getJSON(helpFile, function (data) {

      data.forEach(function (item, index) {
        if (!item.code) {
          item.code = "";
        }

        var $line = $("<div>").html(item.description.full);

        if(!$line.find("h1,h2,h3").length){

          if(item.ctx){
            var $h3 = $("<h3>").html(item.ctx.type + ": " + item.ctx.name);
            $helpContent.append($h3);
          }
        }
        $helpContent.append($line);






        var example = _.findWhere(item.tags, {type: "example"});
        if (example) {
          var $pre = $("<pre class='language-javascript'>");
          if (example.string) {
            $pre.html(example.string);
          } else {
            if(item.code[item.code.length -1 ] == "," ){
              item.code = item.code.substr(0,item.code.length - 1);
            }
            $pre.html(item.code);
          }
          hljs.highlightBlock($pre[0]);
          $helpContent.append($pre);
        }


      });
      var title = $helpContent.find("h1");
      $helpTitle.append(title);
    });
  }

  if(false && /^.*\.(json|dox)$/.test(helpFile)){
    $.getJSON(helpFile,function(data){
      var $fullPre = $("<pre class='language-javascript'>");
      // $helpContent.append($fullPre);
      data.forEach(function(item,index){
        if(!item.code){
          item.code = "";
        }
        var lines = item.code.split("\n");
        for(var i = lines.length ; i--;) {
          if(lines[i].trim().length == 0){
            lines.splice(i,1);
          }
        }

        if(_.findWhere(item.tags,{type: "collapsed"})){
          var _ident = lines[0].length - lines[0].trim().length;
          var tail = "";
          for(var i = 1 ; i < lines.length ; i++) {
            if (lines[i].length - lines[i].trim().length < _ident) {
              tail = lines.splice(i).join("\n")
              break;
            }
          }
          var _code = lines.join("\n");

          var $spoilerTrigger = $("<div class='spoiler-trigger'>");
          var $spoiler = $("<div class='spoiler'>").text(_code.substr(_code.indexOf("{")  ,_code.lastIndexOf("}")  ));
          $fullPre.append(
            "/**/,",
            _code.substr(0,_code.indexOf("{") ),
            $spoilerTrigger,
            $spoiler,
            _code.substr(_code.lastIndexOf("}") + 1),
            "\n",
            tail
          );
        }else{
          var _code = lines.join("\n");
          $fullPre.append(
            "/**/,",
            _code
          );
        }
      });

      // $fullPre[0].textContent = "";
      hljs.highlightBlock($fullPre[0]);

      var lines = $fullPre.html().split('<span class="hljs-comment">/**/</span>,');
      lines.splice(0,1);

      for(var i = 0; i < lines.length ; i++) {
        var item = data[i];


        item.tags.forEach(function(tag){
          item.tags[tag.type] = tag;
        })
        var $line = $("<div class='dox-line'>");
        var $left = $("<div class='dox-left-column'>{description.full}</div>".format(item));
        var $code = $("<pre>").html(lines[i]);
        $line.append($left, $code);
        $helpContent.append($line);

        if(item.tags.collapsed){
          $code.find(".spoiler-trigger").append($code.find(".spoiler")).click(function(){
            $(this).toggleClass("collapsed")
          })
        }


        if(item.tags.module){
          var $popup = $("<div class='popup'>@module: {tags.module.string}</div>".format(item));
          $($code.children()[0]).addClass("xxx").prepend($popup);
        }
      }


      // hljs.highlightBlock($fullPre[0] );
    });
  }

  if(/^.*\.(md)$/.test(helpFile)) {

    $.get(helpFile, function (data) {
      useMarkDown($helpContent , data)
      var title = $helpContent.find("h1");
      $helpTitle.empty().append(title);
    });
  }

  if(/^.*\.(html)$/.test(helpFile)) {

    $helpContent.load(helpFile, function () {
    });
  }

}

createHelp("articles/about.md");
