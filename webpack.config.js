const path    = require('path'),
      fs      = require('fs'),
      argv    = require('minimist')(process.argv.slice(2)),
      webpack = require('webpack');

webpack.UglifyJsPlugin  = require("uglifyjs-webpack-plugin");

const dateString = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

let PROD =  argv.prod || argv.p,
    BABEL = PROD ||argv.babel || argv.b,
    COPY = argv.copy || argv.c || false,
    WATCH = argv.watch || argv.w || false;

function babel(EntryPoint){
  if(BABEL){
    return ['babel-polyfill',EntryPoint]
  }else{
    return EntryPoint
  }
}

let config = {
  mode: PROD ? "production" : "development",
  plugins: [
    new webpack.ProvidePlugin({}),
    new webpack.BannerPlugin({
      banner: `Fiera.js. Set of tool for FabricJS library.\n@author Denis Ponomarev <ponomarevtlt@gmail.com>\n@date ${dateString}`
    }),
    new webpack.DefinePlugin({
      DEVELOPMENT: JSON.stringify(!PROD)
    })
  ],
  entry:  {
    fiera: './src/fiera.js'
    // fiera: babel('./src/fiera.js')
    // faneo: babel('./src/faneo.js')
  },
  output: {
    // publicPath:     "./../objects/",
    path:           path.resolve(__dirname, "./dist"),
    libraryTarget: 'umd',
    globalObject: "(typeof window !== 'undefined' ? window : this)",
    filename:   PROD ?  "[name].min.js" : "[name].dev.js"
  },
  devtool:      PROD ? false : 'source-map',
  target: "web",
// node: {
//   fs: 'empty',
//   "canvas": "empty",
//   "jsdom": "empty",
//   "xmldom": "empty",
//   "pdfkit": "empty",
// },
  externals: [
//  nodeExternals(),
    {
      /*fabricJS for NodeJS Extrernals*/
        "jsdom/lib/jsdom/living/generated/utils": "jsdom/lib/jsdom/living/generated/utils",
        "jsdom/lib/jsdom/browser/resources/resource-loader": "jsdom/lib/jsdom/browser/resources/resource-loader",
        "jsdom/lib/jsdom/utils": "jsdom/lib/jsdom/utils",
        "canvas": "canvas",
        "request": "request",
        "jsdom": "jsdom",
        "xmldom": "xmldom",
      /*fabricjs object  for all project files*/
        "caman": "Caman",
        // "jquery": "jQuery",
      /*writing PDF */
        "path": "path",
        "fs": "fs",
        "stream": "stream",   //stream.Writable < pdf
        "pdfkit": "pdfkit"
    },
  ],
  optimization : {
    minimizer: !PROD ? [] : [
      new webpack.UglifyJsPlugin({
        sourceMap: false
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use:{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: 'style-loader', // creates style nodes from JS strings
          },
          {
            loader: 'css-loader', // translates CSS into CommonJS
          },
          {
            loader: 'less-loader', // compiles Less to CSS
          },
        ],
      },
    ]
  }
};

function npmls(globally) {
  const childProcess = require('child_process');

  let command = 'npm list --depth=0';
  if (globally) command += " -g";
  let stdOut = childProcess.execSync(command, {encoding: 'utf8'});
  let result = {};
  let rows = stdOut.split('\n').slice(1);
  rows.forEach(value => {
    if (!value) return;
    let module = value.replace(/^(\+--|`--) /g, "");
    let i = module.lastIndexOf("@");
    result[module.substr(0, i)] = module.substr(i + 1)
  });
  return result;
}

function copyFiles(){
  const PostCompile = require('post-compile-webpack-plugin');
  const {loadJsonSync} = require("./src/util/data.js");
  let destinationArray = loadJsonSync("dest.json");

  config.plugins.push(new PostCompile(() => {
    for(let filePair of destinationArray){
      let fileName = filePair[0], destPath = filePair[1];
      let originalFile = path.resolve(config.output.path,fileName);

      let destinationFile = path.resolve(destPath , fileName);
      fs.copyFile(originalFile, destinationFile, (err) => {
        if (err) throw err;
        console.log(`${originalFile} was copied to ${destinationFile}`);
      });
      if(config.devtool){
        let originalMapFile = originalFile + ".map",
          destinationMapFile = destinationFile + ".map";

        fs.copyFile(originalMapFile, destinationMapFile, (err) => {
          if (err) throw err;
          console.log(`${originalMapFile} was copied to ${destinationMapFile}`);
        });
      }
    }
  }))
}


//созраняем конфигурацию вебпака и список утановленных модулей. отладочная информация на случай ошибок
function collectInfo(){
  let plugins = config.plugins;
  const {beautyStringify} = require("./src/util/data.js");

  config.plugins = {};
  for(let plugin of plugins){
    config.plugins[plugin.constructor.name] = JSON.parse(JSON.stringify(plugin));
  }

  let optimizationMinimizer = config.optimization.minimizer;
  config.optimization.minimizer = {};
  for(let plugin of optimizationMinimizer){
    config.optimization.minimizer[plugin.constructor.name] = JSON.parse(JSON.stringify(plugin));
  }

  let infoFile = `
    const COMPILE_INFO = {
      webpack: ${beautyStringify(config)},
      locals: ${JSON.stringify(npmls(), null, "\t")},
      globals: ${JSON.stringify(npmls(true), null, "\t")},
      date: "${dateString}"
    }
  `;
  fs.writeFileSync('dist/info.js', infoFile);

  config.plugins = plugins;
  config.optimization.minimizer = optimizationMinimizer;
}


if(!PROD) {
  collectInfo();
}

if(COPY){
  copyFiles();
}

if(WATCH) {
  config.watch = true;
}

module.exports = config;