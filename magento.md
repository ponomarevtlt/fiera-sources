Create a Platform like Fotojet using Magento
Web Development Posted Aug 14, 2019
 We want to create a platform like Fotojet to create collage. The user would be able to add images, drag and drop them, add a background, add effects to images (brightness, sharpness, grayscale, sepia, add text, clipart), create a PDF and place an order. 

If the user pays, he would also be able to download a high resolution PDF otherwise a low resolution. The order would be received by the admin and he would also receive the high resolution PDF for printing and order execution.

We want to use a magento 2 store for this. There are various extensions available for collage drag and drop feature, though I do not think it's very difficult to build either. You can check
https://www.fmeextensions.com/magento-photo-painting-custom-size.html

You can also use several git solutions available like:
https://redgoose-dev.github.io/react-photo-layout-editor/


For adding effects to images we would integrate a photo editing script in the web platform:

https://codecanyon.net/item/pixie-image-editor/10721475

We would need the option of adjusting the brightness, color and saturation level as in Fotojet

Please bid only if you are an expert with magento 2 and have experience of integrating javascript scripts in magento

Attached are the detailed user stories for the platform less