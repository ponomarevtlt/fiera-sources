
    const COMPILE_INFO = {
      webpack: {
  mode: "development",
  plugins: {
    ProvidePlugin: {
      definitions: {}
    },
    BannerPlugin: {
      options: {
        banner: "Fiera.js. Set of tool for FabricJS library.\n@author Denis Ponomarev <ponomarevtlt@gmail.com>\n@date 2019-09-06 01:25:29"
      }
    },
    DefinePlugin: {
      definitions: {
        DEVELOPMENT: "true"
      }
    }
  },
  entry: {
    fiera: "./src/fiera.js"
  },
  output: {
    path: "D:\\WORK\\fiera\\dist",
    libraryTarget: "umd",
    globalObject: "(typeof window !== 'undefined' ? window : this)",
    filename: "[name].dev.js"
  },
  devtool: "source-map",
  target: "web",
  externals: [
    {
      "jsdom/lib/jsdom/living/generated/utils": "jsdom/lib/jsdom/living/generated/utils",
      "jsdom/lib/jsdom/browser/resources/resource-loader": "jsdom/lib/jsdom/browser/resources/resource-loader",
      "jsdom/lib/jsdom/utils": "jsdom/lib/jsdom/utils",
      canvas: "canvas",
      request: "request",
      jsdom: "jsdom",
      xmldom: "xmldom",
      caman: "Caman",
      path: "path",
      fs: "fs",
      stream: "stream",
      pdfkit: "pdfkit"
    }
  ],
  optimization: {
    minimizer: {}
  },
  module: {
    rules: [
      {
        test: {},
        exclude: {},
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: {},
        loader: "json-loader"
      },
      {
        test: {},
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "less-loader"
          }
        ]
      }
    ]
  }
},
      locals: {
	"@babel/cli": "7.5.5",
	"@babel/core": "7.5.5",
	"@babel/preset-env": "7.2.3",
	"babel-loader": "8.0.4",
	"babel-polyfill": "6.26.0",
	"base64-loader": "1.0.0",
	"body-parser": "1.19.0",
	"canvas": "2.5.0",
	"css-loader": "3.0.0",
	"esm": "3.2.25",
	"express": "4.17.1",
	"fontkit": "1.8.0",
	"image-type": "4.1.0",
	"jasmine": "3.4.0",
	"jasmine-node": "3.0.0",
	"jsdom": "15.1.0",
	"json-loader": "0.5.7",
	"less": "3.9.0",
	"less-loader": "5.0.0",
	"minimist": "1.2.0",
	"opentype.js": "1.1.0",
	"pdfkit": "0.10.0",
	"pngjs": "3.4.0",
	"post-compile-webpack-plugin": "0.1.2",
	"style-loader": "0.23.1",
	"uglifyjs-webpack-plugin": "2.0.1",
	"webpack": "4.28.2"
},
      globals: {
	"@angular/cli": "7.3.9",
	"express": "4.16.3",
	"glyphhanger": "3.2.0",
	"install": "0.12.2",
	"jasmine": "3.4.0",
	"less": "3.8.1",
	"node-sass": "4.11.0",
	"sass": "1.15.3",
	"style-loader": "0.23.1",
	"typings": "2.1.1",
	"uglifyjs": "2.4.11",
	"webpack": "4.28.2",
	"webpack-cli": "3.1.0",
	"windows-build-tools": "5.0.0",
	"yarn": "1.9.4"
},
      date: "2019-09-06 01:25:29"
    }
  