module.exports = {
  mode: "development",
  entry:  {
    fiera: ["./fiera.dev.js"]//'babel-polyfill',
  },
  output: {
    path: require('path').resolve(__dirname, "./"),
    filename:  "fiera.babel.js"
  },
  externals: [
    {
      "jsdom/lib/jsdom/living/generated/utils": "jsdom/lib/jsdom/living/generated/utils",
      "jsdom/lib/jsdom/browser/resources/resource-loader": "jsdom/lib/jsdom/browser/resources/resource-loader",
      "jsdom/lib/jsdom/utils": "jsdom/lib/jsdom/utils",
      "canvas": "canvas",
      "request": "request",
      "jsdom": "jsdom",
      "xmldom": "xmldom",
      "caman": "Caman",
      "path": "path",
      "fs": "fs",
      "stream": "stream",
      "pdfkit": "pdfkit"
    },
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use:{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};