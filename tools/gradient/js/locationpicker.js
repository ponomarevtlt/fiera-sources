/*
    ui.anglepicker
*/

$.widget("ui.locationpicker", $.ui.mouse, {
  widgetEventPrefix: "location",
  _init: function() {
    this._mouseInit();
    this.pointer = $('<span class="ui-locationpicker-pointer"></span>');
    this.pointer.append('<span class="ui-locationpicker-dot"></span>');

    this.element.addClass("ui-locationpicker");
    this.element.append(this.pointer);

    this.setLocation(this.options.value);
  },
  _propagate: function(name, event) {
    this._trigger(name, event, this.ui());
  },
  _create: function() {

  },
  destroy: function() {
    this._mouseDestroy();
    this.element.removeClass("ui-locationpicker");
    this.pointer.remove();
  },
  _mouseCapture: function(event) {
    var myOffset = this.element.offset();
    this.width = this.element.width();
    this.height = this.element.height();

    this.startOffset = {
      x: myOffset.left,
      y: myOffset.top
    };
    if (!this.element.is("ui-locationpicker-dragging")) {
      this.setLocationFromEvent(event);
      this._propagate("change", event);
    }
    return true;
  },
  _mouseStart: function(event) {
    this.element.addClass("ui-locationpicker-dragging");
    this.setLocationFromEvent(event);
    this._propagate("start", event);
  },
  _mouseStop: function(event) {
    this.element.removeClass("ui-locationpicker-dragging");
    this._propagate("stop", event);
  },
  _mouseDrag: function(event) {
    this.setLocationFromEvent(event);
    this._propagate("change", event);
  },
  _setOption: function(key, value) {

    this._super(key, value);
  },

  ui: function() {
    return {
      element: this.element,
      value: this.options.value
    };
  },
  instance: function() {
    return this;
  },
  value: function(newValue) {

    if (!arguments.length) {
      return this.options.value;
    }

    var oldValue = this.options.value;
    this.setLocation(newValue);

    if (oldValue !== this.options.value) {
      this._propagate("change");
    }

    return this;
  },
  drawLocation: function() {
    this.pointer.css({
      'left': this.options.x *this.width,
      'top': this.options.y *this.height,
    });
  },
  setLocation: function(x,y) {
    this.options.x = x;
    this.options.y = y;
    this.drawLocation();
  },
  setLocationFromEvent: function(event) {
    var y = (event.pageY - this.startOffset.y)/this.height ;
    var x = (event.pageX - this.startOffset.x )/this.width;


    var snap = this.options.snap;
    if (!event.shiftKey) {
      if(x < snap){
        x = 0;
      }else if (x > 0.5 - snap && x < 0.5 + snap){
        x = 0.5;
      }else if(x > 1 - snap){
        x =1 ;
      }
      if(y < snap){
        y = 0;
      }else if (y > 0.5 - snap && y < 0.5 + snap){
        y = 0.5;
      }else if(y > 1 - snap){
        y =1 ;
      }
    }
    this.setLocation(x,y);
  },
  options: {
    distance: 1,
    delay: 1,
    snap: 0.1,
    min: 0,
    x: 0.5,
    y: 0.5
  }
});