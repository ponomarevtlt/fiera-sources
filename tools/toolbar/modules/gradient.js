import Toolbar from "../toolbar.core";

require('./../../gradient/js/jquery-ui.js');
// require('./../../gradient/js/colorFunctions');
require('./../../gradient/js/anglepicker');
require('./../../gradient/js/locationpicker');
require('./../../gradient/js/gradient.js');
require('./../lib/gradX.js');

Toolbar.prototype.colorpickerOptions = {
  position: "right bottom"
};

Toolbar.types.Gradient = class GradientToolbarButton  extends Toolbar.Tool {
  template(){
    return `<div class="btn button-menu-trigger button-{id} {className}" id="{toolbar.id}-button-{id}">
            <img data-if="icon" data-src="icon">
            <span data-include="svg" data-if="svg"></span>
          </div>
          <div class="submenu fiera-toolbar-submenu-popup" transclude>
            <input type="text">
          </div>`
  }
  render () {
    this.processing = true;
    this.input.gradX("value",this.value);
    $("#anglepicker").anglepicker("value", 50);
    this.processing = false;
  }
  post () {
    this.$item.addClass("toggled");
    this.toggleByButton();
    this.input = this.transclude.find("input");
    this.processing = true;
    this.input.gradX({
      colorpicker: function(options){
        options.element.minicolors({
          theme:"fiera",
          defaultValue: options.color,
          control:      'hue',
          format:       'rgb',
          keywords:     '',
          inline:       true,
          letterCase:   'lowercase',
          opacity:      true,
          change : options.callback
        });
      },
      change: (gradientOptions)=> {
        if(this.processing)return;
        this.setValue(gradientOptions);
      },
    });

    $("#anglepicker").anglepicker({
      start: function(e, ui) {

      },
      change: function(e, ui) {
        $("#label").text(ui.value)
      },
      stop: function(e, ui) {

      },
      value: 90
    });

    this.processing = false;
  }
};
Toolbar.types.Gradient.prototype.className = "object-menu-menu";
Toolbar.types.Gradient.prototype.type = "gradient";