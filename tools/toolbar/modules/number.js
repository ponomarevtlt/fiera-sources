import Toolbar from "../toolbar.core";
require('./../lib/number.js');

Toolbar.types.Number = class NumberToolbarButton extends Toolbar.Tool {
  onchange (e) {
    this.setValue(parseFloat($(e.target).val()));
  }
  template() {
    return `<input type="text" step="{step}" min="{min}" max="{max}" value="{value}" data-onchange="onchange(event)">`;
  }
  render() {
    let val = this.getValue();
    if(this.fixed){
      let multiplier = Math.pow(10, this.fixed);
      val = Math.round(val * multiplier) / multiplier;
    }
    this.$item.find("input")
      .val(val);
  }
  post() {
    this.$item.find("input")
      .bfhnumber({
        min: this.min,
        max: this.max,
        zeros: this.zeros,
        keyboard: true,
        buttons: true,
        wrap: false
      })
  }
};
Toolbar.types.Number.prototype.fixed = 0;
Toolbar.types.Number.prototype.zeros = false;
Toolbar.types.Number.prototype.min = 0;
Toolbar.types.Number.prototype.max = 9999;
Toolbar.types.Number.prototype.type = "number";