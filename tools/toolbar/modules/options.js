const utils = require('./../../../src/util/util.js');
utils.compile = require('./../../../src/util/compile.js');

import Toolbar from "../toolbar.core";

class OptionsToolbarButton  extends Toolbar.Tool {
  updateIcon(){
    this.selected = null;
    for(let item of this.menu) {
      if (item.option === this.value) {
        this.selected = item;
      }
    }

    const selOption = this.$item.find('.btn-selected-option');
    selOption.empty();

    if(this.selected){
      const $el = $(`<span class="btn button-{id} {selected.className}">
        <img data-if="selected.icon" data-src="selected.icon">
        <span data-include="selected.svg" data-if="selected.svg"/>`.format(this));

      selOption.append($el);
      utils.compile.compileElement($el, this);
    }
  }
  render (){
    this.updateIcon();
    for(let item of this.menu){
      item.render();
    }
  }
  template() {
    return `<div class="btn-selected-option"></div>
      <div class="submenu {subMenuClass}" style="display: none;" transclude/>`
  }
  post () {
    if (this.hovered) {
      this.$item.addClass("hovered");
      this.toggleByHover()
    }else { //if (this.toggled) {
      this.$item.addClass("toggled");
      this.toggleByButton()
    }
    this.updateIcon();
  }
}
OptionsToolbarButton.prototype.className = "object-menu-menu items-column";
OptionsToolbarButton.prototype.subMenuClass = "fiera-toolbar-submenu-popup";
OptionsToolbarButton.prototype.position = "";
OptionsToolbarButton.prototype.type = "options";


// Toolbar.types.Option = OptionToolbarButton;
Toolbar.types.Options = OptionsToolbarButton;