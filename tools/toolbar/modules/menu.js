
import Toolbar from "../toolbar.core";

class MenuToolbarButton  extends Toolbar.Tool {
  template(){
    return `<label data-if="title" for="{toolbar.id}-button-{id}">{title}</label>
          <div class="btn button-menu-trigger button-{id} {className}" id="{toolbar.id}-button-{id}">
            <img data-if="icon" data-src="icon">
            <span data-include="svg" data-if="svg"></span>
          </div>
          <div class="submenu {subMenuClass}" transclude/>`
  }
  post () {
    if (this.hovered) {
      this.$item.addClass("hovered");
      this.toggleByHover()
    }else { //if (this.toggled) {
      this.$item.addClass("toggled");
      this.toggleByButton()
    }
  }
}
MenuToolbarButton.prototype.buttonClassName = "button-menu-trigger";
MenuToolbarButton.prototype.type = "menu";
MenuToolbarButton.prototype.subMenuClass = "fiera-toolbar-submenu-popup";
Toolbar.types.Menu = MenuToolbarButton;