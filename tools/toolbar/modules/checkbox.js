import Toolbar from "../toolbar.core";

 class CheckboxToolbarButton  extends Toolbar.Tool {
  template() {
    return `<div class="btn button-{id} {className}"/>
              <img data-if="icon" data-src="icon">
              <span data-include="svg" data-if="svg"/>
              <span data-if="title" class="option-title">{title}</span>`
  }
  post() {
    this.$item.click(() => {

      //Radio Button
      if(this.option){
        this.setValue(this.option);
      }
      //Check Box
      else{
        this.setValue(!this.value);
      }

      //Radio button inside  a group , redraw the group icon
      if(this.parent  &&this.parent.render){
        this.parent.render();
      }

      this.render();
    })
  }
  render() {
    let value = this.getValue();
    if(this.option ? value === this.option : value ){
      this.$item.addClass("checked");
    }else{
      this.$item.removeClass("checked");
    }
  }
};
CheckboxToolbarButton.prototype.type = "checkbox";
Toolbar.types.Checkbox = CheckboxToolbarButton;