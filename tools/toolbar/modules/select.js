
import Toolbar from "../toolbar.core";
require('./../lib/select.js');

Toolbar.types.Select = class SelectToolbarButton  extends Toolbar.Tool {
  onchange (e,model) {
    this.setValue(e.params.data.id,model);
  }
  template() {
    return `<label data-if="!inline"  for="{toolbar.id}-tool-{id}" class="btn button-menu-trigger button-{id} {className}" id="{toolbar.id}-button-{id}">
              <img data-if="icon" data-src="icon"/>
              <span data-include="svg" data-if="svg"/>
            </label>
            <div data-if="!inline" class="submenu fiera-toolbar-submenu-popup" transclude>
                <select id="{toolbar.id}-tool-{id}"/>
            </div>
            <select data-if="inline" id="{toolbar.id}-tool-{id}"/>`
  }
  post() {
    if(!this.inline){
      this.$item.addClass("toggled");
      this.toggleByButton();
    }
    const selectEl = this.$item.find("select");

    this.setSelectOptions(this.staticOptions || []);

    let s2 =  selectEl.data("select2");

    selectEl.on("select2:closing", (e) =>{
      s2.__closed = true;
      setTimeout(function(){
        delete s2.__closed;
      },100)
    })
    .on("select2:select", (e) =>{
      this.onchange(e, this.options);
    })
    .on("mouseup",function(e){
      e.stopPropagation();
    })
    .on("click",function(e){
      e.stopPropagation();
    })
    .on("click",function(e){
      if(s2.__closed)return;
      if(s2.isOpen()){
        s2.close()
      }else{
        s2.open();
      }
    });

    this.toolbar.on("destroy",() => {
      selectEl.dpSelect("destroy");
    });
  }
  setSelectOptions (data) {
    data = [].concat(data);
    //should add tag, not option
    if(!data.filter((el)=>{return el.id === this.value}).length){
      data.push({
        id: this.value,
        text: this.value
      });
    }

    const selectEl = this.$item.find("select");
    selectEl.dpSelect(fabric._.extend({
      //default options
      width: 167,
      minimumResultsForSearch: Infinity,
      dropdownParent: this.toolbar.container.relativeParent(),
      data: data,
    },
    this.dropdown,
    {
      templateSelection: (state, container) => {
        return this.dropdown.templateSelection.call(this, state, container);
      },
      templateResult: (state, container) => {
        return this.dropdown.templateResult.call(this, state, container);
      }
    }));
  }
  render () {

    const selectEl = this.$item.find("select");
    if(!this.staticOptions){
      this.setSelectOptions(this.options)
    }
    selectEl.dpSelect("val",[this.value]);
  }
};

Toolbar.types.Select.prototype.inline = false;
Toolbar.types.Select.prototype.type = "select";
Toolbar.types.Select.prototype.itemClassName = "";

$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        $(this).siblings('select').select2('open');
    }
});
