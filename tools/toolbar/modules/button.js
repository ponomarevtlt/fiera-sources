import Toolbar from "../toolbar.core";

class ButtonToolbarButton extends Toolbar.Tool {
  template() {
    return `<div class="btn button-{id} {className} {toolbar.buttonsClassName}"  data-onclick="callAction()">
              <img data-if="icon" data-src="icon">
              <span data-include="svg" data-if="svg"></span>
              <span data-if="toolbar.buttonsTitle" class="button-title">{title}</span>`
  }
};
ButtonToolbarButton.prototype.type = "button";
Toolbar.types.Button = ButtonToolbarButton;