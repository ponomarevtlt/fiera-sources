fabric.selectColorsTool = function () {
  return {
    className: 'button-colorwheel',
    type: 'effect',
    visible: function () {
      return !!this.logoSvg;
    },
    observe: "task:changed",
    effectTpl:
      '<div id="select-colors-color-list"></div>' +
      '<div id="select-colors-colorwheel"></div>' +
      //'<div class="btn btn-circle button-eyedropper" id="select-colors-eyedropper"></div>' +
      '<div class="btn btn-circle button-bolt" id="select-colors-action-button"></div>' +
      '<input id="select-colors-threshold" type="range" min="1" max="255">',
    actionParameters: function (el, data) {
      var app = this;
      $(app.canvas.wrapperEl).click(function (e) {
        var data = app.canvas.wrapperEl.get
      });

      var initialThreshold = 55;
      var colors = app.logoSvg.extractColors();
      var _keys = Object.keys(colors);

      for (var i = 0; i < _keys.length - 1; i++) {
        for (var j = _keys.length; j-- > i + 1;) {
          var color1 = new fabric.Color(_keys[i])._source;
          var color2 = new fabric.Color(_keys[j])._source;
          if (MagicWand.difference(color1, color2) < initialThreshold) {
            delete colors[_keys[j]];
            _keys.splice(j);
          }
        }
      }


      var els = [],
        values = [],
        currentIndex = 0,
        threshold = [],
        minicolors = el.find("#select-colors-colorwheel"),
        list = el.find("#select-colors-color-list"),
        actionBtn = el.find('#select-colors-action-button'),
        thresholdEl = el.find('#select-colors-threshold');
      //eyedropperBtn = el.find('#select-colors-eyedropper');

      var _keys = Object.keys(colors);

      for (var i in _keys) {
        if (_keys[i][0] !== '#') {
          _keys[i] = '#' + new fabric.Color(_keys[i]).toHex();
        }
      }

      function eyedrop(e) {
        var ctx = app.canvas.lowerCanvasEl.getContext("2d");
        var data = ctx.getImageData(e.offsetX, e.offsetY, 1, 1);
        data.data[3] /= 255;
        var color = "rgba(" + data.data.join(",") + ")";
        setColor(color);
        e.stopPropagation();
      }

      // eyedropperBtn.click(function(){
      app.canvas.interactive = false;
      app.canvas.setCursor(app.canvas.eyedropperCursor);
      $(app.canvas.upperCanvasEl).on("click", eyedrop);
      //   });


      actionBtn.click(function () {
        data.action(values, threshold)
      });

      function setColor(value, opacity) {
        values[currentIndex] = value;
        els[currentIndex].css('background-color', value);
      }

      function setValue(index) {
        currentIndex = index;
        minicolors.minicolors('settings', {
          defaultValue: _keys[currentIndex]
        });
        thresholdEl.val(threshold[index]);
        minicolors.minicolors('value', _keys[index]);
      }


      for (var i in _keys) {
        els[i] = (function (color) {
          values.push(color);
          threshold.push(initialThreshold);

          var $color = $("<span>").css({'background-color': color});

          var $rm = $("<i>").addClass('fa fa-trash');

          $rm.click(function () {
            var index = _keys.indexOf(color);
            _keys.splice(index, 1);
            els.splice(index, 1);
            threshold.splice(index, 1);
            values.splice(index, 1);
            $color.remove();
          });

          $color.click(function () {
            setValue(_keys.indexOf(color))
          });

          $color.append($rm);
          list.append($color);
          return $color;
        })(_keys[i]);
      }

      color = _keys[currentIndex];

      thresholdEl.change(function () {
        threshold[currentIndex] = parseInt(thresholdEl.val());

        app.canvas.pathfinder.colorSelection([values[currentIndex]], [threshold[currentIndex]]);
        app.canvas.pathfinder.render();
        app.canvas.freeDrawingBrush.enable();
      });
      thresholdEl.val(threshold[currentIndex]);

      minicolors.minicolors({
        change: setColor,
        format: 'hex',
        control: 'saturation',
        inline: true
      });

      setValue(0);


      return function () {
        $(app.canvas.upperCanvasEl).off("click", eyedrop);
        app.canvas.interactive = true;
      }
    },
    title: 'removeBackground',
    action: function (colors, threshold) {
      this.canvas.pathfinder.colorSelection(colors, threshold);
      this.canvas.pathfinder.render();
      this.canvas.freeDrawingBrush.enable();
    }
  }
}
