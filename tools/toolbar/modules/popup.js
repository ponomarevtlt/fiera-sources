import Toolbar from "../toolbar.core";

Toolbar.types.Popup = class PopupToolbarButton  extends Toolbar.Tool {
  // scope (data) {
  //   return {
  //     buttonsTitle:  this.toolbar.buttonsTitle || false,
  //     isParameters: !!data.actionParameters,
  //     buttonsClassName: (this.toolbar.buttonsClassName || '')
  //     // effectClassName: data
  //   }
  // }
  template(){
    return  `<div class="btn button-{id} {className} {toolbar.buttonsClassName}">
            <span data-if="toolbar.buttonsTitle" class="button-title">{title}</span>
            </div>
            <div class="submenu {subMenuClass}" transclude/>`
  }
  post (data) {
    var $tpl;
    var foo = function () {
      if (data.effectTpl) {
        $tpl = $(data.effectTpl);
        data.transclude.html($tpl);
      }
      if (data.actionParametersId) {
        $tpl = $("#" + data.actionParametersId).clone();
        data.transclude.html($tpl);
      }
      return data.actionParameters.call(this.target, data.transclude, data, this.options);
    };
    this.toggleByButton(data.item, data.transclude, foo ,data);
  }
};
Toolbar.types.Popup.prototype.type = "popup";

