import Toolbar from "../toolbar.core";
import "./../lib/rangeslider";

class RangeToolbarButton  extends Toolbar.Tool {
  onchange (e) {
    this.setValue(parseFloat($(e.target).val()));
  }
  template() {
    return `<input type="range" step="{step}" min="{min}" max="{max}" value="{value}" >`
    // data-onchange="onchange(event)"
  }
  render() {
    // function round(value, precision) {
    //   let multiplier = Math.pow(10, precision || 0);
    //   return Math.round(value * multiplier) / multiplier;
    // }

    let slider =  this.$item.find("input");
    // if(round(this.value,2) !== round(+slider.val(),2)){
    slider.val(this.value).change();
    // }
  }
  post(){





    var slider = this.$item.find('input[type="range"]');
    slider.rangeslider({
      polyfill: false,
      min: this.min,          // Number , 0
      max: this.max,          // Number, 100
      step: this.step,         // Number, 1
      value: this.value,        // Number, center of slider
      onSlide: (position, value) =>{
        this.setValue(value);
      }
    });
  }
}
RangeToolbarButton.prototype.min = 0;
RangeToolbarButton.prototype.max = 1;
RangeToolbarButton.prototype.step = 0.1;
RangeToolbarButton.prototype.type = "range";
Toolbar.types.Range = RangeToolbarButton;


// // if(false){
//   Toolbar.on("range:created",function(event){
//
//
//
//   });
// }