import Toolbar from "../toolbar.core";
require('./../lib/minicolors.js');

Toolbar.prototype.colorpickerOptions = {
  position: "right bottom"
};

Toolbar.types.Color = class ColorToolbarButton  extends Toolbar.Tool {
  template(){
    return `
          <div class="btn button-menu-trigger button-{id} {className}" id="{toolbar.id}-button-{id}">
            <img data-if="icon" data-src="icon">
            <span data-include="svg" data-if="svg"></span>
          </div>
          <div class="submenu fiera-toolbar-submenu-popup" transclude>
            <input type="text">
          </div>`
  }
  render () {
    this.processing = true;
    this.input.minicolors("value",this.value);
    this.processing = false;
  }
  post () {
    this.$item.addClass("toggled");
    this.toggleByButton();
    this.input = this.transclude.find("input");
    this.input.minicolors( Toolbar._.extend({
      theme:"fiera",
      // value:        data.value.get() ,//|| data.value.get(),
      defaultValue: /*data.value.defaultValue ||*/ this.value,
      control:      'hue',
      format:       'hex',
      keywords:     '',
      inline:       true,
      letterCase:   'lowercase',
      opacity:      true,
      position:     "right bottom",
      swatches:     "#fff|#000|#f00|#0f0|#00f|#ff0|#0ff".split('|'),
      text:         false,
      change :(value, opacity) => {
        if(this.processing)return;
        this.setValue(value);
      }
    },this.colorpicker));
  }
};
//todo remove this
Toolbar.types.Color.prototype.className = "object-menu-menu";
Toolbar.types.Color.prototype.type = "color";