const argv    = require('minimist')(process.argv.slice(2));

module.exports = {
  watch: argv.watch || argv.w || false,
  mode: "production",
  performance: { hints: false },
  optimization: {
    minimize: false
  },
  entry:  "./toolbar.build.js",
  output: {
    // path:           path.resolve(__dirname, "./"),
    libraryTarget: 'umd',
    globalObject: "(typeof window !== 'undefined' ? window : this)",
    filename: "./../../../dist/toolbar.dev.js"
  },
  devtool: 'source-map',
  target: "web"
};