const utils = require('./../../src/util/util.js');
const _ = require("./../../src/plugins/underscore");
const compileUtils = require('./../../src/util/compile.js');
const stringUtils = require('./../../src/util/string.js');

stringUtils.capitalize = function (string, firstLetterOnly) {
  return string.charAt(0).toUpperCase() + (firstLetterOnly ? string.slice(1) : string.slice(1).toLowerCase());
};

function Toolbar(data) {
  //todo
  this.id = data.id || "toolbar" + new Date().getTime();
  this.editor = data.editor;
  this.layout = data.layout;
  this.doNotUpdate = data.doNotUpdate;

  if (data.lang) {
    this.lang = data.lang;
  }
  this.buttons = [];

  this.options = data.options || {
    title: false
  };

  this.options.buttons = this.options.buttons || {
    className: "",
    title: false
  };


  this.container = $("<div>");
  this.container.attr("id",this.id);

  if(data.container) {
    let outerContainer;
    if (data.container.constructor === String) {
      outerContainer = $(document.getElementById(data.container));
    } else {
      outerContainer = data.container;
    }
    $(outerContainer).append(this.container);
  }

  this.target = data.target;

  this.buttons = _.map(data.tools, tool => {
    let button = this.createToolbarButton(tool);
    this.fire(button.type + ":created", {data: button});
    return button;
  });

  if (data.target) {
    this.setTarget(data.target);
  }
  this.initKeys();
}
Toolbar.utils = utils;
Toolbar._ = _;
Toolbar.compile = compileUtils;
Toolbar.string = stringUtils;

Toolbar.types = {};

Toolbar.prototype = {
  buttonsClassName: "btn",
  destroy () {
    this.fire("destroy");
    this.container.empty();
    this.container.hide();
    if (this.__keydownHandler) {
      $(document).off("keydown", this.__keydownHandler);
    }
  },
  createToolbarButton(options){
    if (!options.type) {
      if (options.option) {
        options.type = "checkbox";
      } else if (options.menu) {
        options.type = "menu";
      } else {
        options.type = "button";
      }
    }
    options = Toolbar.utils.deepClone(options);
    options.toolbar = this;
    let btn = new Toolbar.types[Toolbar.string.capitalize(options.type,true)](options);



    // btn.update();
    return btn;
  },
  setTarget(target) {
    let btns =  this.getAllButtons();
    if(!target){

      this.container.hide();

      for(let btn of this.buttons){
        if(btn.visible) {
          btn._disconnect();
        }
      }
      if (this.__keydownHandler) {
        $(document).off("keydown", this.__keydownHandler);
      }
      this.target = null;
      return;
    }

    this.target = target;
    if(this.doNotUpdate){
      delete this.doNotUpdate;
      this.container.hide();
      return;
    }

    for(let btn of this.buttons){
      btn.update();
    }
    this.container.show();
  },
  translate: function (text) {
    if(!this.lang){
      return text;
    }
    if (!text) return "";
    if (text[0] !== "@") return text;
    let parts = text.substr(1).split(".");
    let lang = this.lang;
    for (let part of parts) {
      lang = lang[part];
      if (!lang) {
        return text;
      }
    }
    return lang
  },
  lang: {},
  handleKeydown(e){
    for(let btn of this.getAllButtons()){
      btn.handleKeydown(e);
    }
  },
  initKeys () {
    this.__keydownHandler = this.handleKeydown.bind(this);
    $(document).on("keydown", this.__keydownHandler);

    //$(window).on("mousewheel", function (event) {
    //  for (var i in self.buttons) {
    //    var data = self.buttons[i];
    //    if (!data.mousewheel)continue;
    //    if (!data.ctrlKey || data.ctrlKey && event.ctrlKey) {
    //      if (event.deltaY > 0 && data.mousewheel == ">") {
    //        data.action.call(target, data.option || event, event)
    //      }
    //      if (event.deltaY < 0 && data.mousewheel == "<") {
    //        data.action.call(target, data.option || event, event)
    //      }
    //      event.preventDefault();
    //      event.stopPropagation();
    //      return false;
    //    }
    //  }
    //});
  },
  getAllButtons(){
    let btns = [];
    function traverse(actions) {
      actions.forEach(function (action) {
        btns.push(action);
        if (action.menu) {
          traverse(action.menu);
        }
      })
    }
    traverse(this.buttons);
    return btns;
  },
  // [Symbol.iterator]: function* (){
  //   for(let btn of this.buttons){
  //     yield btn;
  //     if (btn.menu) {
  //       for(let subBtn of btn.menu) {
  //         yield subBtn;
  //       }
  //     }
  //   }
  // }
};
utils.observable(Toolbar.prototype);

export default Toolbar;