import Toolbar from "./toolbar.core.js";
import './ToolbarButton';
import './modules/button';
import './modules/checkbox';
import './modules/key';
import './modules/label';
import './modules/number';
import './modules/menu';
import './modules/color';
import './modules/gradient';
import './modules/options';
import './modules/range';
import './modules/select';
import './modules/popup';
import './modules/tree';

if(typeof window !== "undefined"){
  window.FieraToolbar = Toolbar;
}
if (typeof exports !== 'undefined') {
  module.exports = {
    Toolbar: Toolbar
  };
}