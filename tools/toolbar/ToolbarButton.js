import Toolbar from "./toolbar.core";

class ToolbarButton {
  baseTemplate(){
    return `<div class="object-menu-item object-menu-{type} {buttonClassName}" title="{title}">`;
            //   `<label data-if="title" for="{toolbar.id}-button-{id}">
            //     {title}
            //   </label>
            //   <div class="{toolbar.buttonsClassName} button-{id} {buttonClassName}" id="{toolbar.id}-button-{id}">
            //     <img data-if="icon" data-src="icon">
            //     <span data-if="svg" data-include="svg" ></span>
            //   </div>
            // </div>`

  }
  constructor(options) {
    this.toolbar = options.toolbar;
    delete options.toolbar;

    if(options.value){
      this._value = options.value;
      delete options.value;
    }

    if (!this._value && !options.set) {
      switch (options.type) {
        case "color":
        case "text":
        case "number":
        case "range":
        case "label":
        case "select":
        case "checkbox":
          this._value = options.id ;
      }
    }

    if(this._value){
      this._value = Toolbar.string.camelize(this._value);
      this.min = "min" + Toolbar.string.capitalize(this._value, true);
      this.max = "max" + Toolbar.string.capitalize(this._value, true);
    }
    for (let i in options) {
      this[i] = Toolbar.utils.deepClone(options[i]);
    }
    this.id = options.id;

    this.initialize

    if (!this.action) {
      if (this.type === "button" || this.type === "key") {
        if(this._value){
          this.action = "set" +  Toolbar.string.capitalize(this._value, true);;
        }
        else{
          this.action = this.id;
        }
      }
    }

    if (this.title) {
      this.title = this.toolbar.translate(this.title);
    }

    if (this.template) {
      this.$item = $(this.baseTemplate().format(this));
      this.$button = this.$item.find("> div");
      this.$label = this.$item.find("> label");
      this.$content = $(this.template().format(this)).appendTo(this.$item);


      let transclude = this.$item.find("[transclude]");
      this.transclude = transclude.length ? transclude : null;
      Toolbar.compile.compileElement(this.$item, this);

      if (this.parent) {
        this.parent.transclude.append(this.$item);
      }
      else{
        this.toolbar.container.append(this.$item);
      }
    }
    if (this.post) {
      this.post.call(this);
    }
    this.setKeyboard();

    if (this.menu && this.menu.constructor === Function) {
      this.menu = this.menu.bind(target);
    }

    if (this.menu) {
      for (let i in this.menu) {
        this.menu[i].parent = this;
        this.menu[i] = this.toolbar.createToolbarButton(this.menu[i]);
      }
    }
  }
  /**
   *
   * @param data
   * @param callback function(eventData)
   */
  useObservable( callback) {
    if (this.observe.constructor === String) {
      this.target && this.target.on(this.observe, callback);
    } else {
      this.observe( callback);
    }
    this.toolbar.on("destroy", function () {
      if (this.destroy) {
        this.destroy( callback);
      }
      else {
        this.target.off(this.observe, callback);
      }
    })
  }
  setKeyboard () {

    if (this.keyboard !== false && (this.key || this.keyCode || this.shiftKey || this.altKey || this.ctrlKey || this.metaKey)) {
      if (this.key && this.key.length === 1) {
        this.ctrlKey = true;
      }
      if (this.keyCode && this.keyCode.constructor === String) {
        this.keyCode = this.keyCode.toUpperCase().charCodeAt(0);
        this.key = String.fromCharCode(this.keyCode);
      }
      this.keyboard = true;

      let string = "";
      if (this.ctrlKey) string += "Ctrl + ";
      if (this.altKey) string += "Alt + ";
      if (this.shiftKey) string += "Shift + ";
      if (this.key) string += this.key;

      this.title += " (" + string + ")";
    }
  }
  getTargetValue (property, useToolbarTarget) {
    let target;

    if(useToolbarTarget){
      target = this.toolbar.target;
    }else{
      target = this.target;
    }
    if(property === undefined){
      return null;
    }
    if (property.constructor === Function) {
      return this.args ? property.apply(this, [target].concat(this.args)) : property.call(this,target);
    }
    if (property.constructor === String) {
      let negative = false;
      if(property.startsWith("not ")){
          property = property.substr(4);
          negative = true;
      }
      let value = fabric.util.compile.evalInContext(property,target);
      let _result;
      if (value && value.constructor === Function) {
        _result = this.args ? value.call(target,this.args) : value.call(target);
      } else {
        _result = this.args ? value[this.args] : value;
      }
      if(negative){
        return !!(_result ^ negative)
      }else{
        return _result;
      }
    }
    return property;
  }
  callAction() {
    if (!this.enabled) return;
    let _action = this.action.constructor === String ? this.target[this.action] : this.action;
    return this.option ? _action.call(this.target, this.option) : _action.call(this.target);
  }
  handleKeydown(e) {
    if (!this.keyboard || (!this.enabled) || this.hidden) {
      return;
    }
    if (
      (this.keyCode === e.keyCode || this.key === e.key) &&
      (this.ctrlKey === undefined || this.ctrlKey === e.ctrlKey) &&
      (this.altKey === undefined || this.altKey === e.altKey) &&
      (this.shiftKey === undefined || this.shiftKey === e.shiftKey) &&
      (this.metaKey === undefined || this.metaKey === e.metaKey)) {

      e.preventDefault();
      e.stopPropagation();
      this.callAction();
    }
  }
  createInput ($item, data, type) {
    let $input = $("<input>")
      .attr("type", type)
      .attr("min", data.min.call(data.target))
      .attr("max", data.max.call(data.target));

    $input.val(data.value);
    if (data.observe) {

      this.useObservable( function (val) {
        $input.val(data.get.call(data.target));
      })
    }
    $input.change(function (e) {
      data.set.call(data.target, parseFloat($input.val()))
    });
    $item.append($input);
  }
  toggleByHover ( foo, data) {
    let $item = this.$item;
    let $toggleElement = this.transclude;
    let onClose;

    $toggleElement.hide();
    $item.mouseout(function () {
      $toggleElement.hide();
      onClose && onClose();
    });
    $item.mouseover(function () {
      $toggleElement.show();
      onClose = foo && foo();
    });

    $item.click(function () {
      if (data && data.immediately) {
        data.action();
      }
    });
  }
  toggleMenu(){
    return this.opened ? this.hideMenu() : this.showMenu();
  }
  hideMenu(){
    this.transclude.hide();
    this.onClose && this.onClose();
    $(document).off("click", this.onBlur);
    this.opened = false;
  }
  showMenu(){

    let offset = this.$item.offset();

    let layout = this.toolbar.layout || this.toolbar.container.offsetParent();
    let parentWidth = layout.width();


    if(offset.left > parentWidth/ 2){
      this.transclude.addClass("popup-right").removeClass("popup-left");
    }else{
      this.transclude.removeClass("popup-right").addClass("popup-left");
    }

    this.opened = true;
    this.transclude.show();
    $(document).on("click", this.onBlur);
    this.onOpen && this.onOpen();
    if (this.toggleMenuOptions && this.toggleMenuOptions.immediately) {
      this.toggleMenuOptions.action();
    }
  }
  blur(e){
    let _parents = $(e.target).parents();
    for (let i in _parents) {
      if (_parents[i] === this.$item[0]) {
        return false;
      }
    }
    if (this.transclude.css("display") !== "none") {
      this.hideMenu();
    }
  }
  toggleByButton (foo, data) {
    this.toggleMenuOptions = data;
    this.onOpen = foo;
    this.onBlur = this.blur.bind(this);
    this.opened = false;

    this.transclude.hide()
      .click((e) =>{
        e.stopPropagation();
      });

    this.$item.click(()=> this.toggleMenu())
  }
  observeCallback() {
    if (this._visible) {
      if (!this.visible) {
        this.$item.hide();
        this.hidden = true;
        return;
      } else {
        this.$item.show();
        this.hidden = false;
      }
    }
    if (this._enabled) {
      if (!this.enabled) {
        this.$item.attr("disabled", true);
      } else {
        this.$item.removeAttr("disabled");
      }
    }

    if (this.type === "options") {
      let _val = this.value;
      $("[name=" + this.id + "]").prop("checked", false);
      $("[name=" + this.id + "][value=" + _val + "]").prop("checked", true);
    }
    if(this.render){
      this.render();
    }
  }
  _disconnect(){
    if (this.observe) {
      if (this.destroy) {
        this.destroy.call(this.target, this._observeCallback);
      }
      else {
        this.target.off(this.observe, this._observeCallback);
      }
    }

    if(this.menu){
      for(let btn of this.menu){
        if(btn.visible){
          btn._disconnect();
        }
      }
    }
  }
  update(){
    if (!this.template)return;

    // if (!this.target) {
    //   this.$item.hide();
    //   return;
    // }

    if (!this.visible) {
      this.$item.hide();
      return;
    }else{
      this.$item.show();
    }

    if (this.active) {
      this.$item.addClass("active");
    }else{
      this.$item.removeClass("active");
    }

    if (!this.enabled) {
      this.$item.attr("disabled", true);
    }else{
      this.$item.removeAttr("disabled");
    }

    if (this.observe && this.target) {
      this._observeCallback = this.observeCallback.bind(this);

      if (this.observe.constructor === String) {
        this.target && this.target.on(this.observe, this._observeCallback);
      } else {
        this.observe.call( this.target, this._observeCallback);
      }
    }

    if(this.menu){
      for(let btn of this.menu){
        btn.update();
      }
    }

    if(this.render){
      this.render();
    }
  }
  getValue(value) {
    if(!this.target){
      return;
    }
    if(this.get){
      return this.getTargetValue(this.get);
    }
    else{
      let foo = "get" +  Toolbar.string.capitalize(this._value, true);
      if(this.target[foo]){
        return this.getTargetValue(foo);
      }
      else{
        return this.getTargetValue(this._value);
      }
    }
  }
  setValue(value) {
    if(this.set){
      this.set.apply(this.target, arguments);
    }
    else{
      let foo = "set" +  Toolbar.string.capitalize(this._value, true);
      if(this.target[foo]){
        this.target[foo](value)
      }
      else{
        this.target[this._value] = value
      }
    }
  }
  get value() {
    return this.getValue.apply(this,arguments)
  }
  set value(value) {
    return this.setValue.apply(this,arguments)
  }
  get max() {
    return this.getTargetValue(this._max)
  }
  set max(value) {
    this._max = value;
  }
  get min() {
    return this.getTargetValue(this._min)
  }
  set min(value) {
    this._min = value;
  }
  get step() {
    return this.getTargetValue(this._step)
  }
  set step(value) {
    this._step = value;
  }
  get options() {
    return this.getTargetValue(this._options)
  }
  set options(value) {
    this._options = value;
  }
  get active() {
    return this.getTargetValue(this._active)
  }
  set active(value) {
    this._active = value;
  }
  get visible() {
    return this.getTargetValue(this._visible,true)
  }
  set visible(value) {
    this._visible = value;
  }
  get enabled() {
    return this.getTargetValue(this._enabled)
  }
  set enabled(value) {
    this._enabled = value;
  }
  getTarget() {
    let _target;
    if (this._target) {
      if (this._target.constructor === Function) {
        _target = this._target.call(this.toolbar.target);
      }
      else if (this._target.constructor === String) {
        _target = this.toolbar.target[this._target];
      }
      else {
        _target = this._target;
      }
    } else {
      _target = this.toolbar.target;
    }
    return _target ;//|| {application: this.toolbar.editor};
  }
  get target() {
    return this.getTarget();
  }
  set target(value) {
    this._target = value;
  }
}

Toolbar._.extend(ToolbarButton.prototype,{
  buttonClassName: "",
  className: "",
  svg  :    false,
  icon  :   false,
  type      :"none",
  option    :"",
  action    :undefined,
  _value     :undefined,
  //tool is :tive if target property appplied to Object. for example, for "Bold" button will be "active" if "bold" property set to tru
  _active:  false,
  _visible: true,
  _enabled: true,
  // _set       :undefined,     //function or peoperty name
  // _get       :undefined,
  _min       :undefined,
  _max       :undefined,

  keyboard  :undefined,
  key       :undefined,
  shiftKey  :undefined,
  altKey    :undefined,
  ctrlKey   :undefined,
  metaKey   :undefined,
  title     :undefined,
});

Toolbar.Tool = ToolbarButton;
export default ToolbarButton;