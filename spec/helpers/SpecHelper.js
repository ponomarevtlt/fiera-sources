beforeEach(function () {
  jasmine.addMatchers({
    toBeRgb: function () {
      return {
        compare: function (image, expected) {

          let canvas = fabric.util.createCanvasElement(),
              context = canvas.getContext("2d");
          canvas.width = image.width;
          canvas.height = image.height;
          context.drawImage(image,0,0);
          let imageData = context.getImageData(0,0,1,1);
          return {
            pass: imageData.data[0] === expected
          }
        }
      };
    }
  });
});
