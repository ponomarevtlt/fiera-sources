
//fabric.require("SkewObject",["ShapeMixin"], function() {



    fabric.SkewObject = {

          drawControls: function (ctx, shape, offset) {
            if (!this.hasControls) {
              return this;
            }
            this.drawBoundsControls( ctx);
            this.drawShapeControls(ctx);
          },
        initSkewPoints: function () {
            //fabric._.extend(this, fabric.ShapeMixin);
            this._initPoints();
            this.extraControls = this.extraControls || {};
            this.extraControls.skewTR = {x: this.width - 5, y: 0};
            this.extraControls.skewBL = {x: 5, y: this.height};

            this._corner_actions = this._corner_actions || {};
            this._corner_actions.skewTR = "skew";
            this._corner_actions.skewBL = "skew";

            this._controlsVisibility.skewTR = true;
            this._controlsVisibility.skewBL = true;
            //fabric._.extend(this, fabric.SkewObject);
        },

        _performSkewAction: function (e, transform, pointer) {
            //this.extraControls.curve
            //this.extraControls.curve.y = transform.point.y;

            if (transform.corner == "skewBL") {
                //_points[order].x = _point.x;
                this.skewX = Math.atan2(transform.point.x, this.height) / Math.PI * 180;
            }
            if (transform.corner == "skewTR") {
                //_points[order].y = _point.y;
                this.skewY = Math.atan2(transform.point.y, this.width) / Math.PI * 180;
            }
        }
    };
