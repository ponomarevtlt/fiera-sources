

fabric._.extend(fabric.Polyline.prototype,{
  controlsButtonsEnabled: false,
  _create_buttons_controls: function(controls){

    if(!this.controlsButtonsEnabled)return;

    var pts = this.points,
      _last = pts.length - 1;
    if(this.__magnet_point && this.__magnet_point.id[0] != "x"){
      var _id = this.__magnet_point.id;
      var _ender_area = (_id == "e1" || _id == "e2");
      var _ender_points = _ender_area || (_id == "p1" || _id == "p" + pts.length);
      var _offset = _ender_points ? 15 : 10;
      this._xbuttons_point = _ender_area ? (_id[1] == "1" ? 0 : _last) : _id.substr(1) - 1;
      this._xbuttons_curve = _id[0] == "c";
      if(this._xbuttons_curve){
        controls.push({
          x: this.__magnet_point.x + _offset,
          y: this.__magnet_point.y - _offset,
          size : 16,
          intransformable: true,
          styleFunction: this._drawInsertNodeButton,
          id: "x1"
        });
      }
      controls.push({
        x: this.__magnet_point.x + _offset,
        y: this.__magnet_point.y + _offset,
        size : 16,
        intransformable: true,
        styleFunction: this._drawRemoveNodeButton,
        id: "x2"
      });
    }else{
      delete this._xbuttons_point;
      delete this._xbuttons_curve;
    }
  },
  _drawInsertNodeButton: function (control, ctx, methodName, left, top, size, stroke) {
    ctx.save();
    ctx.fillStyle = "green";
    ctx.beginPath();
    var _size = size / 4;
    ctx.arc(left + size/2 , top + size/2 , _size * 2 , 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.strokeStyle = "white";
    ctx.translate(Math.floor(size/2),Math.floor(size/2));
    ctx.translate(0.5,0.5 );
    ctx.moveTo(left - _size , top );
    ctx.lineTo(left + _size , top );
    ctx.strokeStyle = "white";
    ctx.moveTo(left  , top + _size );
    ctx.lineTo(left  , top - _size );
    ctx.stroke();
    ctx.restore();
  },
  _drawRemoveNodeButton: function (control, ctx, methodName, left, top, size, stroke) {
    ctx.save();
    ctx.fillStyle = "red";
    ctx.beginPath();
    var _size = size / 4;
    ctx.arc(left + size/2 , top + size/2 , _size * 2 , 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.strokeStyle = "white";
    ctx.translate(size/2,size/2);
    ctx.moveTo(left - _size , top - _size);
    ctx.lineTo(left + _size , top + _size);
    ctx.strokeStyle = "white";
    ctx.moveTo(left - _size , top + _size );
    ctx.lineTo(left + _size , top - _size );
    ctx.stroke();
    ctx.restore();
  },
  _performAddAction: function (e, transform, pointer) {
    this.isMoving = false;
    var p1 = this.points[this._xbuttons_point];
    var p2 = this.points[this._xbuttons_point + 1];
    if(p1.curve){
      var new_point1 = p1.curve.get(0.25);
      var new_point2 = p1.curve.get(0.75);

      this.points.splice(this._xbuttons_point + 1,0,{
        x: p1.c.x,
        y: p1.c.y,
        c: new_point2
      })
      p1.c = new_point1;
      this._makeCurveByIndex(this._xbuttons_point);
      this._makeCurveByIndex(this._xbuttons_point + 1);
    }else{
      var new_point1 = {x : (p2.x - p1.x) / 4, y: (p2.y - p1.y) / 4};
      var new_point2 = {x : (p2.x - p1.x) / 4*3, y: (p2.y - p1.y) / 4*3};
    }
    delete this.canvas._currentTransform;
    // delete transform.action;
    // delete transform.corner;
  },
  _performRemoveAction: function (e, transform, pointer) {
    if(this.points.length == 2){
      return this.remove();
    }

    var _curvepointer = this._xbuttons_curve,
      pIndex1 = this._xbuttons_point;

    if(this.points[pIndex1 - 1 ]  && this.points[pIndex1 + 1]){
      this.points[pIndex1 - 1 ].c = this.points[pIndex1];
      this.points.splice(pIndex1,1);
      this._makeCurveByIndex(pIndex1 - 1);
    }else{
      this.points.splice(pIndex1,1);
    }
    this.isMoving = false;
    this.setSize();
    this.canvas.renderAll();
    delete this.canvas._currentTransform;
    // delete transform.action;
    // delete transform.corner;
  }
});
