window.$ = window.jQuery;


$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });

  for(var i in o){
    if(i.indexOf("[]") != -1){
      o[i.replace("[]","")] = o[i];
      delete o[i];
    }
  }
  return o;
};
