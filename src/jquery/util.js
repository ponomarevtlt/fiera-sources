
$.fn.extend({
	relativeParent: function(  ) {
      var regex = /(static|relative|absolute)/;
      var relativeParent = this.parents().filter( function() {
        var parent = $( this );
        return regex.test( parent.css( "position" ) );
      }).eq( 0 );

    return !relativeParent.length ? $( this[ 0 ].ownerDocument || document ) : relativeParent;
  },
	scrollParent: function( includeHidden ) {
		var position = this.css( "position" ),
			excludeStaticParent = position === "absolute",
			overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
			scrollParent = this.parents().filter( function() {
				var parent = $( this );
				if ( excludeStaticParent && parent.css( "position" ) === "static" ) {
					return false;
				}
				return overflowRegex.test( parent.css( "overflow" ) + parent.css( "overflow-y" ) + parent.css( "overflow-x" ) );
			}).eq( 0 );

		return position === "fixed" || !scrollParent.length ? $( this[ 0 ].ownerDocument || document ) : scrollParent;
	},
  eachSelf : function(selector,foo){
      this.find(selector).each(foo);
      if(this.is(selector)){
        foo.call(this[0]);
      }
  }

});

$.fn.appendText = function(text) {
  return this.each(function() {
    var textNode = document.createTextNode(text);
    $(this).append(textNode);
  });
};
