const PDFKit = require('./pdfkit-node');


const fs = require('fs');
const imageType = require('image-type');
const { PNG } = require('pngjs');
const SVGtoPDF = require('./pdfkit.svg');

require('./pdf.common');

const { Writable } = require('stream');
class WritableBufferStream extends Writable {
  constructor(options) {
    super(options);
    this._chunks = [];
  }
  _write(chunk, enc, callback) {
    this._chunks.push(chunk);
    return callback(null);
  }
  _destroy(err, callback) {
    this._chunks = null;
    return callback(null);
  }
  toBuffer() {
    return Buffer.concat(this._chunks);
  }
}

let PDFDocument = fabric.PDFDocument = PDFKit.PDFDocument;

PDFDocument.prototype.svg = function(svg, x, y, options) {
  if(!options)options = {};
  if(!options.fontCallback){
    options.fontCallback = function (family) {return family;};
  }
  return SVGtoPDF(this, svg, x, y, options);
};
/**
 *
 * @param src
 * @param label
 * @returns {*}
 */
PDFKit.PDFImage.open = function (src, label) {
  let buffer;

  if (Buffer.isBuffer(src)) {
    buffer = src;
  } else if (src instanceof ArrayBuffer) {
    buffer = Buffer.from(new Uint8Array(src));
  } else {
    let match;
    if (match = /^data:.+;base64,(.*)$/.exec(src)) {
      buffer = Buffer.from(match[1], 'base64');
    } else {
      buffer = fs.readFileSync(src);
      if (!buffer) {
        return;
      }
    }
  }

  const { mime } = imageType(buffer);

  switch (mime) {
    // if (buffer[0] === 0xff && buffer[1] === 0xd8)
    case 'image/jpeg':
      return new PDFKit.JPEGImage(buffer, label);
    // if (buffer[0] === 0x89 && buffer.toString('ascii', 1, 4) === 'PNG')
    case 'image/png':
      const png = PNG.sync.read(buffer);
      //fixed PDFKit problems with SRGB palette.
      if (png.palette) {
        buffer = PNG.sync.write(png, { interlace: false });
      }
      //fixed PDFKit problems with interlaced PNG files.
       if (png.interlace) {
        buffer = PNG.sync.write(png, { interlace: false });
      }
      return new PDFKit.PNGImage(buffer, label);
    default:
      throw new Error('Unsupported image format :' + mime);
  }
};



fabric.PDFDocument.prototype.toBuffer = function(callback) {
  return new Promise(resolve => {
    let stream = new WritableBufferStream();
    this.pipe(stream);
    stream.on('finish', () => {
      let buffer = stream.toBuffer();
      callback && callback(buffer);
      resolve(buffer)
    });
  })
};

fabric.util.object.extend(fabric.PDFDocument.prototype, {
  toFile (filename,callback) {
    const fs = require('fs');
    return new Promise((resolve, reject) => {
      filename = fabric.util.data.resolvePath(filename);
      let file = fs.createWriteStream(filename);
      file.on("finish", () => {
        callback && callback(filename);
        resolve(filename);
      });
      file.on("error", reject);
      this.pipe(file);
    });
  }
});

