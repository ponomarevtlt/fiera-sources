window.PDFDocument = require('./pdfkit-web.js');
let blobStream = require('blob-stream');
const SVGtoPDF = require('./pdfkit.svg.js');
const {BlobStream} = require('./browser-stream.js');
import fs from 'fs'

PDFDocument.prototype.blobURL = function (callback) {
  var stream = this.pipe(blobStream());
  stream.on('finish', function() {
    callback(stream.toBlobURL('application/pdf'));
  });
};

PDFDocument.prototype.toBlob = function (callback) {
  const stream = new BlobStream();
  this.pipe(stream);
  stream.on('finish', function () {
    callback(stream.toBlob('application/pdf'));
  });
  this.end();
};

function registerBinaryFiles(ctx) {
  ctx.keys().forEach(key => {
    // extracts "./" from beginning of the key
    fs.writeFileSync(key.substring(2), ctx(key))
  });
}

function registerAFMFonts(ctx) {
  ctx.keys().forEach(key => {
    const match = key.match(/([^/]*\.afm$)/)
    if (match) {
      // afm files must be stored on data path
      fs.writeFileSync(`data/${match[0]}`, ctx(key))
    }
  });
}


PDFDocument.prototype.svg = function(svg, x, y, options) {
  return SVGtoPDF(this, svg, x, y, options);
};

// registerBinaryFiles(require.context('./assets', true));

// is good practice to register only required fonts to avoid the bundle size increase
registerAFMFonts(require.context('./afm', false, /Helvetica.*\.afm$/));
// registerAFMFonts(require.context('pdfkit/js/afm', false, /Courier.*\.afm$/));
// registerAFMFonts(require.context('pdfkit/js/afm', false, /Times.*\.afm$/));

