'use strict';
// require('./../modules/canvas.events');

fabric.util.object.extend(fabric.Canvas.prototype, {
  setDroppable(val){
    if(!this.droppable && val){
      this.droppable = true;
      this.initDragAndDrop();
    }
  },
  accepts: {},
  droppable: false, //{}
  isAccepts (accepts, data) {

    for (var i in accepts) {
      if (accepts[i].constructor == Array) {
        if (accepts[i].indexOf(data[i]) == -1)return false;
      } else {
        if(accepts[i] == "*")continue;
        if (accepts[i] != data[i]) return false;
      }
    }
    return true;
    //return (this.supportedTypes == "*" || this.supportedTypes.indexOf(type)!= -1)
  },
  drop (data) {
    this.setData(data);

  },
  handleDrop(e) {
    this.file = e.dataTransfer.files[0];
    const reader = new FileReader();

    reader.onload = (imgFile) => {
      console.log(imgFile)
      const data = imgFile.target["result"];
      fabric.Image.fromURL(data, (img) => {
        let oImg = img.set({
          left: 0,
          top: 0,
          angle: 0
        }).scale(1);
        this.canvas.add(oImg).renderAll();
        var a = this.canvas.setActiveObject(oImg);
        var dataURL = this.canvas.toDataURL({format: 'png', quality: 0.8});
      });
    };
    reader.readAsDataURL(this.file);
    return false;
  },
  //
// <div
//   (dragover)="false"
//   (dragend)="false"
//   (drop)="handleDrop($event)">
//   <canvas id="canvas" class="canvas" width="500" height="500">
//   </canvas>
// </div>
  dropAction(e){
    this.fire("before:drop",e);

    e.target = this.findTarget(e.originalEvent);
    if (e.target && !this.isDrawingMode) {
      // To unify the behavior, the object's double click event does not fire on drawing mode.
      e.target.fire('object:drop', e);
    }

    if (e.target && e.target.accepts && this.isAccepts(e.target.accepts, e.data)) {
        if(e.target.deactivate){
          e.target.deactivate();
        }
        e.target.setData(e.data);
    } else {
      if (this.isAccepts(this.accepts, e.data)) {
        this.setData(e.data);
      }
    }
  },
  initDragAndDrop () {
    this.on("drop", options => {

      let e = options.e;
      if(!e.dataTransfer && !e.data){
        return false;
      }

      let _zoom = this.getZoom(),
        _x = (e.x - e.offsetX)/ _zoom,
        _y = (e.y - e.offsetY)/ _zoom,
        _w,
        _h,
        _scaleX = 1,
        _scaleY = 1,
        _stroke = 0;

      // e.x /= _zoom;
      // e.y /= _zoom;
      // e.offsetX /= _zoom;
      // e.offsetY /= _zoom;

      if(e.dataTransfer && e.dataTransfer.files[0]){
        let file = e.dataTransfer.files[0];
        e.stopPropagation();
        e.preventDefault();

        fabric.util.readImageFile(file, img => {
          e.data = {
            height: img.height,
            src: img.src,
            thumbnail: false,
            type: "photo",
            width: img.width
          };
        });
        
        this.application.uploadAction(file);
      } else if(e.data) {
        _w = e.helper.width() / _zoom;  // (e.data.width || e.width  );
        _h = e.helper.height() / _zoom; //(e.data.height || e.height );

        if (e.data.scaleX) {
          _scaleX = e.data.scaleX / this.viewportTransform[0]
        } else {
          _w /= this.viewportTransform[0]
        }

        if (e.data.scaleY) {
          _scaleY = e.data.scaleY / this.viewportTransform[3];
        } else {
          _h /= this.viewportTransform[3]
        }

        _stroke = e.data.strokeWidth || 0;
        if (e.data.left) {
          _x += (e.data.left + _stroke) / this.viewportTransform[0];
        }
        if (e.data.top) {
          _y += (e.data.top + _stroke) / this.viewportTransform[3];
        }

        e.data = fabric._.extend({}, e.data, {
          scaleX: _scaleX,
          scaleY: _scaleY,
          left: _x,
          top: _y,
          width: _w,
          height: _h,
          originalWidth: e.data.width,
          originalHeight: e.data.height
        });

        this.dropAction(e);
      }
    });

    this.on("mouse:dragenter", function (e) {
      if (e.target && e.target.accepts && this.isAccepts(e.target.accepts, e.data)) {
        e.target.activate();
        e.e.helper.css("cursor", "alias");
        this.setCursor("alias");
      } else {
        e.e.helper.css("cursor", "not-allowed");
        this.setCursor("not-allowed");
      }
    });
    this.on("mouse:dragleave", function (e) {
      if (e.target && e.target.accepts) {
        if (this.isAccepts(e.target.accepts, e.data)) {
          this._activated = false;
          e.target.deactivate()
        }
        e.e.helper.css("cursor", "pointer");
      }
    });
  }
});
