
fabric._.extend(fabric.Canvas.prototype,{
  /**
   * ['PencilBrush','RectangleBrush']
   */
  activeDrawingTools: [],
  /**
   *
     {
      BrushClassName: {
        className: 'brush className',
        title: 'brush title'
      }
    }
   */
  drawingTools: {},
  insertDrawingTool: false,
  actions: fabric._.extend(fabric.Canvas.prototype.actions,{
    drawingTool: {
      title: 'drawing-tool',
      type: 'options',
      value: 'freeDrawingBrush',
      menu: function(){
        var _tools = {};
        for(var i in this.activeDrawingTools){
          var _tool = this.activeDrawingTools[i];
          _tools[_tool] = fabric._.extend({option: _tool},this.drawingTools[_tool]);
        }
        return _tools;
      }
    }
  }),
  freeDrawingBrush: 'PencilBrush',
  drawingColor: [0,0,0,255],
  _onMouseDownInDrawingMode: function(e) {
    this._isCurrentlyDrawing = true;
    if(!this.freeDrawingBrush.target){
      this.discardActiveObject(e).renderAll();
    }
    if (this.clipTo) {
      fabric.util.clipContext(this, this.contextTop);
    }
    var pointer = this.getPointer(e);
    this.freeDrawingBrush.onMouseDown(pointer);
    this._handleEvent(e, 'down');
  },
  getFreeDrawingBrush: function() {
    if(!this.freeDrawingBrush){
      return "Selection";
    }
    return  fabric.util.string.capitalize(fabric.util.string.camelize(this.freeDrawingBrush.type),true);
  },
  setFreeDrawingBrush: function(brush) {

    if(brush == 'Selection'){
      this.isDrawingMode = false;
      this.freeDrawingBrush = "";
      return;
    }
    var className = fabric.util.string.capitalize(fabric.util.string.camelize(brush),true);
    if(this["__" + className]){
      this.freeDrawingBrush = this["__" + className] ;
    }else{
      this.freeDrawingBrush = this["__" + className] = new  fabric[className](this);
    }
    this.fire("brush:changed",{brush: this.freeDrawingBrush});
  },
  drawZoomedArea : function(ctx,left, top ,width, height , pointer ) {

    width = width || 90;
    height = height || 90;

    ctx.save();
    ctx.translate(left || 0, top || 0);

    ctx.fillStyle = 'black';
    ctx.strokeStyle = "#fff";
    ctx.strokeWidth = 1;
    ctx.setLineDash([2, 2]);
    ctx.drawImage(this.backgroundImage._element,Math.floor(pointer.x) - 4, Math.floor(pointer.y) - 4 , 9 , 9, 0,0 , width, width );
    ctx.strokeRect(0,0 , width, width);
    ctx.strokeRect(40 , 40 , 10, 10);
    ctx.restore();
  }
});

fabric.Canvas.prototype.drawingTools.Selection = {
  className: 'fa fa-mouse-pointer',
  title: 'Selection'
};
fabric.Canvas.prototype.activeDrawingTools.push("Selection");
