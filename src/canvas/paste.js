
fabric._.extend(fabric.Canvas.prototype, {
  cutObjects (){

  },
  copyObjects (){

  },
  pasteObjects (){

  },
  actions: fabric._.extend(fabric.Canvas.prototype.actions, {
    cutObjects: {
      title: "@canvas.cutObjects",
      className: 'fa fa-cut broken'
    },
    copyObjects: {
      title: "@canvas.copyObjects",
      className: 'fa fa-copy broken'
    },
    pasteObjects: {
      title: "@canvas.pasteObjects",
      className: 'fa fa-paste broken'
    }
  })
});