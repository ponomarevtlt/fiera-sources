fabric._.extend(fabric.Canvas.prototype, {
  eventListeners: fabric.util.extendArraysObject(fabric.Canvas.prototype.eventListeners, {
    "loading:begin": function(e) {
      this._applyTemplate(e.options)
    }
  })
});

fabric._.extend(fabric.StaticCanvas.prototype, {
  ____store_template: function(propertiesToInclude, _data) {
    if (this.template) {
      for (let i in _data.template) {
        if (JSON.stringify(_data[i]) === JSON.stringify(_data.template[i])) {
          delete _data[i];
        }
      }
      return this.template.id;
    }
    return null;
  },
  eventListeners: fabric.util.extendArraysObject(fabric.StaticCanvas.prototype.eventListeners, {
    "loading:begin": function(e) {
      this._applyTemplate(e.options)
    }
  }),

  _applyTemplate: function(options){
    let val = options.template;
    if(val){
      let tpl = this.getTemplate(val);
      fabric._.extend(options,tpl.data);
      if(tpl.data.width){
        options.originalWidth = tpl.data.width;
      }
      if(tpl.data.height){
        options.originalHeight = tpl.data.height;
      }
    }
  },
  /*tTemplate: function(template){

    this.template = template;
    if(!template)return;

    this.setWidth(this.slideWidth || template.width);
    this.setHeight(this.slideHeight || template.height);
    this.originalHeight = this.height;
    this.originalWidth = this.width;

    this.set(fabric._.rearrange(template,["areas","helpers","offsets"]));

    this._update_clip_rect();
    this._update_background_overlay_image("background");
    this.fire("modified",{type: "template"});
    this.renderAll();
  },*/
  // setTemplate: function(value){
  //   this.template = value;
  // },
  //-------------------------------------------------------------------------------
  getTemplate: function(id){
    return fabric._.findWhere(this.application.templates,{id: id})
  },
  updateSlideTemplate: function(slide) {
    slide.canvas.setTemplate(slide.canvas.template);
    this.canvas.template = slide.canvas.template;
    if(this.activeSlide  == slide){
      this.canvas.setTemplate(slide.canvas.template);
    }
  },
  updateTemplate: function(template) {
    for(var i in this.slides){
      if(this.slides[i].canvas.template == template){
        this.updateSlideTemplate(this.slides[i]);
      }
    }
  }
});
