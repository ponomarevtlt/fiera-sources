
fabric.util.object.extend(fabric.Object.prototype, {
  layer: "objects"
});

fabric.Layer = {
  objects: function(layerName){
    return this._chooseObjectsToRender(layerName)
  },
  render: function(ctx,layerName){
    let objects = this._chooseObjectsToRender(layerName);
    ctx.save();
    //apply viewport transform once for all rendering process
    ctx.transform.apply(ctx, this.viewportTransform);
    this._renderObjects(ctx,objects );
    ctx.restore();
  },
  svg: function(markup,reviver,layerName){
    let objects = this._chooseObjectsToRender(layerName);
    for (let instance of objects) {
      if (instance.excludeFromExport)continue;
      this._setSVGObject(markup, instance, reviver);
    }
  }
};





let CanvasMixin = {
  renderOrder: ["shadow","background","objects","overlay","overlayObjects","controls","loader"],
  layers: {
    loader: {
      render(ctx) {
        if(this.processingProperties.length && this._loader){
          ctx.save();
          ctx.transform.apply(ctx, this.viewportTransform);
          this._loader.render(ctx);
          ctx.restore();
        }
      }
    },
    shadow: {
      render(ctx) {
        let _rect = this.backgroundRect;
        if(!_rect)return;
        _rect.left = this.viewportTransform[4];
        _rect.top = this.viewportTransform[5];
        _rect.width = this.originalWidth * this.viewportTransform[0];
        _rect.height = this.originalHeight * this.viewportTransform[0];
        // _rect.fill = object || "#ffffff";
        _rect.dirty = true;
        _rect.render(ctx);
      }
    },
    background: {
      export: true,
      render:  function(ctx){
        this._renderBackground(ctx);
      },
      svg: function(markup,reviver){
        this._setSVGBgOverlayColor(markup, 'background');
        this._setSVGBgOverlayImage(markup, 'backgroundImage', reviver);
      }
    },
    objects: {
      export: true,
      objects: true,
    },
    overlayObjects: {
      export: true,
    },
    // overlayObjects: {
    //   export: true,
    //   render: function(ctx){
    //     ctx.save();
    //     //apply viewport transform once for all rendering process
    //     ctx.transform.apply(ctx, this.viewportTransform);
    //     this._renderObjects(ctx, this._chooseObjectsToRender("overlay"));
    //     ctx.restore();
    //   },
    //   svg: function(markup,reviver){
    //     let objects = this._chooseObjectsToRender("overlay");
    //     for (let instance of objects) {
    //       if (instance.excludeFromExport)continue;
    //       this._setSVGObject(markup, instance, reviver);
    //     }
    //   }
    // },
    controls: {
      render:  function(ctx){
        // this.controlsAboveOverlay &&
        if (this.interactive) {
          this.drawControls(ctx);
        }
      }
    },
    overlay: {
      export: true,
      render:  function(ctx){
        this._renderOverlay(ctx);
      },
      svg: function(markup,reviver){
        this._setSVGBgOverlayColor(markup, 'overlay');
        this._setSVGBgOverlayImage(markup, 'overlayImage', reviver);
      }
    },
  },
  /**
   * @private
   */
  getObjectByID: function(_id){
    if(this._objects){
      for (let o of this._objects) {
        if (o.id === _id) {
          return o;
        }
      }
    }
    if ( this.layers) {
      for (let lid in this.layers) {
        if (this.layers[lid].objects) {
          if(this.layers[lid].objects !== true){
            for (let o of this.layers[lid].objects.call(this)) {
              if (o.id === _id) {
                return o;
              }
            }
          }
        }
      }
    }
    return null;
  }
};
fabric.util.object.extend(fabric.StaticCanvas.prototype,CanvasMixin);
fabric.util.object.extend(fabric.Canvas.prototype,CanvasMixin);

var SCanvasMixin = {
  _chooseObjectsToRender: function(layer) {

    let object, objsToRender, activeGroupObjects;
    let activeObjects = this.interactive ? this.getActiveObjects() : [];

    if (activeObjects.length > 0 && !this.preserveObjectStacking) {
      objsToRender = [];
      activeGroupObjects = [];
      for (let i = 0, length = this._objects.length; i < length; i++) {
        object = this._objects[i];
        if(layer && object.layer !== layer )continue;
        if (activeObjects.indexOf(object) === -1 ) {
          if(!object.hiddenActive){
            objsToRender.push(object);
          }
        }
        else {
          activeGroupObjects.push(object);
        }
      }
      if (activeObjects.length > 1) {
        this._activeObject._objects = activeGroupObjects;
      }
      objsToRender.push.apply(objsToRender, activeGroupObjects);
    }
    else {
      if(layer){
        objsToRender = [];
        for (let i = 0, length = this._objects.length; i < length; i++) {
          object = this._objects[i];
          if(layer && object.layer !== layer )continue;
          objsToRender.push(object);
        }
      }
      else{
        objsToRender = this._objects;
      }
    }
    return objsToRender;
  },
  /**
   * Renders background, objects, overlay and controls.
   * @param {CanvasRenderingContext2D} ctx
   * @param {Array} objects to render
   * @return {fabric.Canvas} instance
   * @chainable
   */
  renderCanvasLayers: function(ctx) {
    let forExport = !!ctx;
    if(!ctx){
      ctx = this.contextContainer;
    }
    if (this.isRendering) {
      fabric.util.cancelAnimFrame(this.isRendering);
      this.isRendering = 0;
    }
    this.calcViewportBoundaries();
    this.clearContext(ctx);
    this.fire('before:render');
    if (this.clipTo) {
      fabric.util.clipContext(this, ctx);
    }


    for(let layerName of this.renderOrder){
      let l = this.layers[layerName];
      if(!l || l.visible === false || (forExport && !l.export)){
        continue;
      }
      if(l  && l.render){
        l.render.call(this, ctx);
      }else{
        fabric.Layer.render.call(this, ctx, layerName);
      }
      // if(this._chooseObjectsToRender &&  i === "objects"){
      //   this._renderObjects(ctx, this._chooseObjectsToRender());
      // }else{
      //   this._renderObjects(ctx, this.layers[i].objects);
      // }
    }

    // this._renderBackgroundObjects(ctx);
    // this._renderObjects(ctx, objects);
    // this._renderOverlayObjects(ctx);
    // if (this.controlsAboveOverlay && this.interactive) {
    //   this.drawControls(ctx);
    // }
    this.fire('after:render');
  },
  _renderOverlayObjects: function(){},
  _renderBackgroundObjects: function(){},
};

fabric.util.object.extend(fabric.SStaticCanvas.prototype,SCanvasMixin);
fabric.util.object.extend(fabric.SCanvas.prototype,SCanvasMixin);