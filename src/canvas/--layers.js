
fabric._.extend(fabric.StaticCanvas.prototype, {
  defaultOptions: fabric._.extend(fabric.StaticCanvas.prototype.defaultOptions,{
    layers: ["background","objects","foreground"]
  }),
  setLayers: function(value){
    for(var layerName of value){
      this.layers[layerName] = this.createLayer();
      if(layerName === "objects"){
        this.layers[layerName].objects = this._objects;
      }
    }
  },
  layers: {},
  createLayer: function(zindex){
    return {
      objects: []
    };
    /*
    var _canvasElement = fabric.util.createCanvasElementWithSize(this);
    _canvasElement.style.left = 0;
    _canvasElement.style.top = 0;
    _canvasElement.style.position = 'absolute';
    var _ctx = _canvasElement.getContext('2d');
    _ctx.imageSmoothingEnabled = this.imageSmoothingEnabled;


    var layerAfter = null;
    for(var i in this.layers){

      if(this.layers[i].zindex > zindex && !layerAfter || layerAfter.zindex > this.layers[i].zindex){
        layerAfter  = this.layers[i];
      }
    }
    if(this.wrapperEl){
      if(layerAfter){
        this.wrapperEl.insertBefore(_canvasElement,layerAfter.canvas);
      }else{
        this.wrapperEl.appendChild(_canvasElement);
      }
    }
    return {
      transform: true,
      canvas: _canvasElement,
      context: _ctx,
      objects: []
    }*/
  },
  add: function (/*, isNewElement*/) {
    for (let obj of [...arguments]) {

      if(obj.layer) {
        var _layer = this.layers[obj.layer];
      /*  if(!_layer){
          _layer = this.layers[obj.layer] = [];
        }*/
        _layer.objects(this).push(obj)
      }else{
        this._objects.push(obj);
      }
      if (this._onObjectAdded) {
        this._onObjectAdded(obj);
      }
    }
    this.renderOnAddRemove && this.renderAll();
    return this;
  },
  remove: function () {
    var _objects = this.getObjects(),
      index;
    var objects;
    for (var i = 0, length = arguments.length; i < length; i++) {
      if (arguments[i].layer) {
        objects = arguments[i].layer;
      } else {
        objects = _objects;
      }

      index = objects.indexOf(arguments[i]);
      // only call onObjectRemoved if an object was actually removed
      if (index !== -1) {
        objects.splice(index, 1);
        this._onObjectRemoved(arguments[i]);
      }
    }

    this.renderOnAddRemove && this.renderAll();
    return this;
  },
  // renderLayer: function (layer) {
  //   layer = this.layers[layer];
  //   if(this.processing)return false;
  //   var ctx = layer.context;
  //   ctx.save();
  //   this.clearContext(ctx);
  //   this.clipTo && fabric.util.clipContext(this, ctx);
  //   layer.transform &&  ctx.transform.apply(ctx, this.viewportTransform);
  //   this._renderObjects(layer.context,layer.objects);
  //   ctx.restore();
  // },
//   renderAll: function () {
//     if(this.processing)return false;
//     if (this.selection && !this._groupSelector && !this.isDrawingMode) {
//       this.clearContext(this.contextTop);
//     }
//
//     this.fire('before:render');
//
//    // for(var i in this.layers){
//     var ctx = this.contextContainer;
//     this.clearContext(ctx);
//     ctx.save();
//     if (this.clipTo) {
//       fabric.util.clipContext(this, ctx);
//     }
//     ctx.transform.apply(ctx, this.viewportTransform);
//
//     this._renderBackground(ctx);
//
//     for(var i in this.layers){
//       if(this._chooseObjectsToRender &&  i === "objects"){
//         this._renderObjects(ctx, this._chooseObjectsToRender());
//       }else{
//         this._renderObjects(ctx, this.layers[i].objects);
//       }
//     }
//
//     this.fire('render');
//
//     if(!this.virtual){
//       if (!this.controlsAboveOverlay && this.interactive ) {
//         this.drawControls(ctx);
//       }
//       this._renderOverlay(ctx);
//     }
//
//     if (this.controlsAboveOverlay && this.interactive && !this._isCurrentlyDrawing ) {
//       if(this.borderOpacityWhenMoving || !this.isMoving){
//         this.drawControls(ctx);
//       }
//     }
// /*
//     for(var i in this.layers) {
//       var ctx = this.layers[i].context;*/
//       ctx.restore();
//
//
//     this.fire('after:render');
//   },
//   _renderBackground: function(ctx) {
//     this._renderBackgroundOrOverlay(ctx, 'background');
//   },
//   _renderOverlay: function(ctx) {
//     this._renderBackgroundOrOverlay(ctx, 'overlay');
//   }
});

