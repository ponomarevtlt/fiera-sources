/**
 *
 *
 * @example
 *
 colletion("mask-rectangle",{left: ">100"}).remove()

 colletion(fabric.MaskRectangle",{left: ">100"}).remove()

 colletion({type: "mask-rectangle" , left: ">100"}).remove()

 rectangles = this.canvas.collection(fabric.Rect,{color: 'red'});

 //makes all rectangles with attribute "color = 'red'" visible.
 rectangles.updateCollection().set({visible: true});

 */
fabric._.extend(fabric.Canvas.prototype, {
  collection: function (type, options) {
    var classPrototype;
    var _applicationPrototype;
    if (typeof type == "function") {
      classPrototype = type.prototype;
    } else if (typeof type == "string") {
      classPrototype = fabric[type].prototype
    } else if (type.type) {
      classPrototype = fabric[type.type].prototype;
      options = type;
    } else {
      options = type;
    }
    options = options || {type: classPrototype.type};

    function makeFunction(foo) {
      return function () {
        var options = arguments;
        this.forEach(function (obj) {
          foo.apply(obj, options)
        });
        return this;
      }
    }

    var collectionProto;
    var _array = this.find(options);
    _array.canvas = this;
    _array.options = options;

    if (classPrototype) {
      if (this.application) {
        _applicationPrototype = this.application.getDefaultProperties(classPrototype, {});
      }
      collectionProto = [];
      for (var i in classPrototype) {
        if (typeof classPrototype[i] === "function") {
          collectionProto[i] = classPrototype[i];
        }
      }
      for (var i in _applicationPrototype) {
        if (typeof _applicationPrototype[i] === "function") {
          collectionProto[i] = _applicationPrototype[i];
        }
      }
    } else {
      _array.forEach(function (_obj) {
        if (!collectionProto) {
          collectionProto = [];
          for (var i in _obj) {
            if (typeof _obj[i] === "function") {
              collectionProto[i] = _obj[i];
            }
          }
        } else {
          for (var i in collectionProto) {
            if (!_obj[i] || typeof _obj[i] !== "function") {
              delete collectionProto[i];
            }
          }
        }
      })
    }

    for (var i in collectionProto) {
      collectionProto[i] = makeFunction(collectionProto[i]);
    }
    collectionProto.__proto = _array.__proto__;
    _array.__proto__ = collectionProto;

    _array.setCollection = function (_arr) {
      this.length = 0;
      for (var i in _arr) {
        this.push(_arr[i]);
      }
      return this;
    };
    _array.updateCollection = function () {
      var _arr = this.canvas.find(this.options);
      this.setCollection(_arr)
      return this;
    };

    _array.filter = function () {
      var _arr = this.__proto__.filter.apply(this, arguments);
      this.setCollection(_arr)
      return this;
    };

    return _array;
  }
});
