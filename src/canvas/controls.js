var _performTransformAction_overwritten = fabric.Canvas.prototype._performTransformAction;

fabric._.extend(fabric.Canvas.prototype, {
  cursors: {

  },
  /**
   * Add custom pointers
   * @param corner
   * @param target
   * @param e
   * @private
   */
  _setCornerCursor: function (corner, target, e) {
    var _cursor = target._controls[corner].cursor;
    if(!_cursor || _cursor === "resize"){
      this.setCursor(this._getRotatedCornerCursor(corner, target, e));
    }else{
      this.setCursor(this.cursors[_cursor] || _cursor);
    }
  },
  /**
   * custom transform actions
   * @param e
   * @param transform
   * @param pointer
   * @private
   */
  _performTransformAction: function (e, transform, pointer) {

    if(!transform.action)return;
    var t = transform.target;
    var foo = t["_perform" + fabric.util.string.capitalize(transform.action) + "Action"];
    if (foo) {
      var _p = t.toLocalPoint(pointer, transform.originX, transform.originY);
      _p.x /= t.scaleX;
      _p.y /= t.scaleY;
      if (t.flipX) _p.x *= -1;
      if (t.flipY) _p.y *= -1;
      transform.point = _p;
      foo.call(t, e, transform, pointer);
      return;
    }
    _performTransformAction_overwritten.call(this, e, transform, pointer);
  },
  /**
   * custom pointers
   * @param target
   * @param corner
   * @param e
   * @returns {string}
   * @private
   */
  _getActionFromCorner: function (target, corner, e) {
    if (corner) {
      var _corner = target._controls[corner];
      return e[this.altActionKey] && _corner.altAction || _corner.action;
    }
    return 'drag';
  }
});

fabric._.extend(fabric.Object.prototype, {
  setControls: function(){
    var _controls =  this.getBoundsControls();
    this.setExtraControls && this.setExtraControls(_controls);

    for(var i in _controls){
      var _control = _controls[i];
      if(!_control.style){
        _control.style = this.cornerStyle;
      }
      if(!_control.size){
        _control.size = this.getCornerSize(i);
      }
    }
    this._controls = _controls;
  },
  /**
   * Solved the problem when it is impossible to move point when it is under another native control such as "mtr", "tl" etc.
   * Determines which corner has been clicked
   * @private
   * @param {Object} pointer The pointer indicating the mouse position
   * @return {String|Boolean} corner code (tl, tr, bl, br, etc.), or false if nothing is found
   */
  _findTargetCorner: function (pointer) {
    if (!this.hasControls || !this.active) {
      return false;
    }
    var ex = pointer.x,
      ey = pointer.y,
      xPoints,
      lines;
    this.__corner = 0;

    for (var i in this.oCoords) {
      if(!this._controls[i].visible){
        continue;
      }
      lines = this._getImageLines(this.oCoords[i].corner);
      xPoints = this._findCrossPoints({x: ex, y: ey}, lines);
      if (xPoints !== 0 && xPoints % 2 === 1) {
        this.__corner = i;
        return i;
      }
    }
    return false;
  },
  controls: {
    tl: {visible: "{hasBoundControls}",x: 0, y: 0, action: "scale"},
    tr: {visible: "{hasBoundControls}",x: "{width}", y: 0, action: "scale"},
    bl: {visible: "{hasBoundControls}",x: 0, y: "{height}", action: "scale"},
    br: {visible: "{hasBoundControls}",x: "{width}", y: "{height}", action: "scale"},
    ml: {visible: "{hasBoundControls}&&!{lockUniScaling}" , x: 0,             y: "{height}/2", action:   "scaleX" , altAction: "skewY"},
    mr: {visible: "{hasBoundControls}&&!{lockUniScaling}" , x: "{width}",     y: "{height}/2", action:   "scaleX" , altAction: "skewY"},
    mt: {visible: "{hasBoundControls}&&!{lockUniScaling}" , x: "{width}/2",   y: 0,            action:   "scaleY" , altAction: "skewX"},
    mb: {visible: "{hasBoundControls}&&!{lockUniScaling}" , x: "{width}/2",   y: "{height}",   action:   "scaleY" , altAction: "skewX"},
    mtr:{visible: "{hasRotatingPoint}", x: "{width}/2",   y: "-{rotatingPointOffset}/{zoom}/{scaleY}", action: "rotate", cursor: "rotationCursor"},
  },
  hasBoundControls: true,
  getBoundsControls: function(){
    // if(!this.hasBoundsControls)return {};
    var zoom = this.canvas.viewportTransform[0];

    //todo eval тяжелая функция
    var _replace = function (val){
      if(val && val.constructor == String){
        val = val.replace(/\{zoom\}/g,zoom)
           .replace(/\{([^}]*)\}/g,function(a,b){
             return "this." +b
           });
        val = eval(val);
      }
      return val;
    }.bind(this);

    var controls = {};
    for(var i in this.controls){
      var control = this.controls[i];

      var _visible = (control.visible === undefined || _replace(control.visible));

      // if(_visible){
        var _newControl = fabric._.clone(control);
        delete _newControl.visible;
        _newControl.x = _replace(_newControl.x);
        _newControl.y = _replace(_newControl.y);
        _newControl.visible = _visible;
        controls[i] = _newControl;
      // }
    }

    // this._getControlsFunction = eval("return ")
    // //
    // var controls = {
    //   tl: {x: 0, y: 0, action: "scale"},
    //   tr: {x: this.width, y: 0, action: "scale"},
    //   bl: {x: 0, y: this.height, action: "scale"},
    //   br: {x: this.width, y: this.height, action: "scale"}
    // };
    // if(!this.lockUniScaling) {
    //   fabric._.extend(controls,{
    //     ml: {x: 0,            y: this.height/2, action:   "scaleX" , altAction: "skewY"},
    //     mr: {x: this.width,   y: this.height/2, action:   "scaleX" , altAction: "skewY"},
    //     mt: {x: this.width/2, y: 0,             action:   "scale"  , altAction: "skewX"},
    //     mb: {x: this.width/2, y: this.height,   action:   "scale"  , altAction: "skewX"},
    //   })
    // }
    // if(this.hasRotatingPoint){
    //   fabric._.extend(controls, {
    //     mtr: this.hasRotatingPoint && {x: this.width / 2, y: -this.rotatingPointOffset / zoom, action: "rotate", cursor: "rotationCursor"},
    //     mtr2: {x: this.width / 2, y: this.height + this.rotatingPointOffset / zoom, action: "rotate", cursor: "rotationCursor"}
    //   });
    // };
    // return target._corner_actions[corner.substr(1)] || target._default_corner_action;
    return controls;
  },
  /**
   * Sets corner position coordinates based on current angle, width and height
   * See https://github.com/kangax/fabric.js/wiki/When-to-call-setCoords
   * @return {fabric.Object} thisArg
   * @chainable
   */
  setCoords: function () {
    if(!this.canvas)return;

    var i, _p,
      zoom = this.canvas.viewportTransform[0],
      theta = fabric.util.degreesToRadians(this.angle),
      vpt = this.getViewportTransform(),
      dim = this._calculateCurrentDimensions(),
      currentWidth = dim.x, currentHeight = dim.y;

    // If width is negative, make postive. Fixes path selection issue
    if (currentWidth < 0) {
      currentWidth = Math.abs(currentWidth);
    }

    var sinTh = Math.sin(theta),
      cosTh = Math.cos(theta),
      _angle = currentWidth > 0 ? Math.atan(currentHeight / currentWidth) : 0,
      _hypotenuse = (currentWidth / Math.cos(_angle)) / 2,
      offsetX = Math.cos(_angle + theta) * _hypotenuse,
      offsetY = Math.sin(_angle + theta) * _hypotenuse,
      coords = fabric.util.transformPoint(this.getCenterPoint(), vpt);
    var TL = {x: coords.x - offsetX, y: coords.y - offsetY};

    this.setControls();
    this.oCoords = {};

    for (i in this._controls) {
      _p = {x: this._controls[i].x * this.scaleX, y: this._controls[i].y * this.scaleY};
      // _c = this.flipX ? (this.flipY ? "br" : "tr") : (this.flipY ? "bl" : "tl");
      this.oCoords[i] = {
        x: TL.x + ((-sinTh * _p.y + cosTh * _p.x) * (this.flipX ? -1 : 1)) * zoom,
        y: TL.y + (( cosTh * _p.y + sinTh * _p.x) * (this.flipY ? -1 : 1)) * zoom
      }
    }

    this._setCornerCoords && this._setCornerCoords();
    return this;
  },
  drawControls: function (ctx) {

    if (!this.hasControls) {
      return this;
    }

    var transformMatrix = this._calcDimensionsTransformMatrix(this.skewX, this.skewY, false);
    var wh = this._calculateCurrentDimensions(),
      width = wh.x,
      height = wh.y,
      zoom = this.canvas.viewportTransform[0],
      methodName = this.transparentCorners ? 'stroke' : 'fill',
      i, corner, _p, left, top, _x, _y, stroke, scaleOffset;

    ctx.save();
    ctx.lineWidth = 1;
    var _corner_alpha =  this.isMoving ? this.borderOpacityWhenMoving : 1;
    ctx.strokeStyle = ctx.fillStyle = this.cornerColor;

    for (var cornerName in this._controls) {
      corner = this._controls[cornerName];
      if(!corner.visible || corner.intransformable && this.canvas._currentTransform){
        continue;
      }
      _p = fabric.util.transformPoint(corner, transformMatrix);
      left = (this.flipX ? ( width - _p.x) : _p.x) * zoom;
      top = (this.flipY ? (height - _p.y) : _p.y) * zoom;
      stroke = !this.transparentCorners && this.cornerStrokeColor;

      var _foo = corner.styleFunction || this.cornerStyles[corner.style];
      if(corner.size) {
        ctx.globalAlpha = _corner_alpha;
        _foo.call(this, cornerName, ctx, methodName, left - (width + corner.size) / 2, top - (height + corner.size) / 2, corner.size, stroke);
      }
      if(corner.area){
        ctx.globalAlpha = 0.008;
        // ctx.globalAlpha = 0.3;
        this.cornerStyles.circle.call(this, cornerName, ctx, methodName, left -(width + corner.area) / 2 , top -(height + corner.area) / 2, corner.area, stroke);

        ctx.globalAlpha = 1;
      }
    }

    // if (this.extraControls) {
    //   for (i in this.extraControls) {
    //     _p = fabric.util.transformPoint(this.extraControls[i], transformMatrix);
    //     scaleOffset = this.cornerSize;
    //     left = -(width + scaleOffset) / 2;
    //     top = -(height + scaleOffset) / 2;
    //     _x = left + (this.flipX ? ( width - _p.x) : _p.x) * zoom;
    //     _y = top + (this.flipY ? (height - _p.y) : _p.y) * zoom;
    //     this._drawControl( i, ctx, methodName, _x, _y);
    //   }
    // }

    this.drawControlsInterface && this.drawControlsInterface(ctx);
    this.drawBorders && this.drawBorders(ctx);
    ctx.restore();

    return this;
  },

  cornerStyles: {

    symbols:function (control, ctx, methodName, left, top,size,stroke) {

        var iconOffset = 4,
          iconSize = size - iconOffset * 2,
          offset = size * .5,
          offsetCorner = this.offsetCorner,
          dotSize = 4,
          icon = false;

        // offset = offsetCorner = 0;

        if (this.isControlVisible(control)) {

          var wh = this._calculateCurrentDimensions(),
            width = wh.x,
            height = wh.y;

          if (control == 'br' || control == 'mtr' || control == 'tl' || control == 'bl' || control == 'ml' || control == 'mr' || control == 'mb' || control == 'mt') {
            switch (control) {

              case 'tl':
                //copy
                left = left - offset + offsetCorner;
                top = top - offset + offsetCorner;
                icon = this.__editorMode || this.copyable ? String.fromCharCode('0xe942') : false;
                break;
              case 'mtr':
                // rotate
                var rotateRight = this.__imageEditor ? 0 : width / 2;
                left = left + rotateRight + offset - offsetCorner;
                top = top - offset + offsetCorner;
                icon = (this.__editorMode || this.rotatable) && !this.uploadZone ? String.fromCharCode('0xe923') : false;
                break;
              case 'br':
                // resize
                left = left + offset - offsetCorner;
                top = top + offset - offsetCorner;
                icon = (this.__editorMode || this.resizable) && this.type !== 'textbox' ? String.fromCharCode('0xe922') : false;
                break;
              case 'bl':
                //remove
                left = left - offset + offsetCorner;
                top = top + offset - offsetCorner;
                icon = this.__editorMode || this.removable ? String.fromCharCode('0xe926') : false;
                break;
            }
          }

          this.transparentCorners || ctx.clearRect(left, top, size, size);

          var extraLeftOffset = control == 'mt' || control == 'mb' ? 5 : 0;
          ctx.fillStyle = this.cornerColor;

          if (!this.__imageEditor) {

            if (control == 'ml' || control == 'mr' || control == 'mt' || control == 'mb') {
              ctx.beginPath();
              left += dotSize * 3;
              top += dotSize * 3;
              ctx.arc(left, top, dotSize, 0, 2 * Math.PI);
              ctx.fillStyle = this.cornerIconColor;
              ctx.fill();
            }
            else if(icon) {
              ctx.fillRect(left, top, size, size);
              ctx.font = iconSize + 'px FontFPD';
              ctx.fillStyle = this.cornerIconColor;
              ctx.textAlign = 'left';
              ctx.textBaseline = 'top';
              ctx.fillText(icon, left + iconOffset + extraLeftOffset, top + iconOffset);
            }

          }
          else {
            ctx.fillRect(left, top, size, size);
          }

        }
    },
    arc: function (control, ctx, methodName, left, top, size, stroke) {
      ctx.save();
      ctx.beginPath();
      ctx.translate(left, top);

      if (control[0] !== "m") {
        ctx.translate(size, 0);
        ctx.rotate(Math.PI / 2);
      }
      switch (control) {
        case "tr":
          ctx.arc(size / 2, size / 2, size / 2, 2 * Math.PI, 0.5 * Math.PI, true);
          ctx.lineTo(size / 2, size / 2);
          break;
        case "br":
          ctx.arc(size / 2, size / 2, size / 2, 0.5 * Math.PI,  Math.PI, true);
          ctx.lineTo(size / 2, size / 2);
          break;
        case "bl":
          ctx.arc(size / 2, size / 2, size / 2, Math.PI, 1.5 * Math.PI, true);
          ctx.lineTo(size / 2, size / 2);
          break;
        case "tl":
          ctx.arc(size / 2, size / 2, size / 2, 1.5 * Math.PI, 2 * Math.PI, true);
          ctx.lineTo(size / 2, size / 2);
          break;
        case "mt":
          ctx.arc(size / 2, size / 2, size / 2, 0, Math.PI, true);
          break;
        case "mb":
          ctx.arc(size / 2, size / 2, size / 2, 0, Math.PI, false);
          break;
        case "ml":
          ctx.translate(0, size);
          ctx.rotate(-Math.PI / 2);
          ctx.arc(size / 2, size / 2, size / 2, 0, Math.PI, true);
          break;
        case "mr":
          ctx.translate( -1, size);
          ctx.rotate(-Math.PI / 2);
          ctx.arc(size / 2, size / 2, size / 2, 0, Math.PI, false);
          break;
        default:
          ctx.arc(size / 2, size / 2, size / 2, 0, 2 * Math.PI, false);
      }
      // ctx.moveTo(0, 0);
      // ctx.lineTo(size, 0);
      // ctx.lineTo(size, size / 2);
      //
      // if (control[0] == "m") {
      //   ctx.lineTo(0, size / 2);
      // } else {
      //   ctx.lineTo(size / 2, size / 2);
      //   ctx.lineTo(size / 2, size);
      //   ctx.lineTo(0, size);
      // }

      // if (control !== "mtr") {
      //   if (control[0] !== "m") {
      //     ctx.arc(size / 2, size / 2, size / 2, 0, 1.5 * Math.PI, false);
      //     ctx.lineTo(size / 2, size / 2)
      //   }
      // }
      ctx.closePath();
      ctx[methodName]();
      if (stroke) {
        ctx.stroke();
      }
      ctx.restore();
    },
    frame: function (control, ctx, methodName, left, top, size, stroke) {
      if (control == "mtr") {
        // this.transparentCorners || ctx.clearRect(left, top, size, size);
        ctx[methodName + 'Rect'](left, top, size, size);
        if (stroke) {
          ctx.strokeRect(left, top, size, size);
        }
        return;
      }

      ctx.save();
      ctx.beginPath();
      ctx.translate(left, top);
      switch (control) {
        case "tr":
          ctx.translate(size, 0);
          ctx.rotate(Math.PI / 2);
          break;
        case "br":
          ctx.translate(size, size);
          ctx.rotate(Math.PI);
          break;
        case "bl":
          ctx.translate(0, size);
          ctx.rotate(-Math.PI / 2);
          break;
        case "mr":
          ctx.translate(size, 0);
          ctx.rotate(Math.PI / 2);
          break;
        case "ml":
          ctx.translate(size / 2, 0);
          ctx.rotate(Math.PI / 2);
          break;
        case "mb":
          ctx.translate(0, size / 2);
          break;
      }
      ctx.moveTo(0, 0);
      ctx.lineTo(size, 0);
      ctx.lineTo(size, size / 2);

      if (control[0] == "m") {
        ctx.lineTo(0, size / 2);
      } else {
        ctx.lineTo(size / 2, size / 2);
        ctx.lineTo(size / 2, size);
        ctx.lineTo(0, size);
      }
      ctx.closePath();
      ctx[methodName]();
      if (stroke) {
        ctx.stroke();
      }
      ctx.restore();
    },
    circle: function (control, ctx, methodName, left, top, size, stroke) {
      ctx.beginPath();
      ctx.arc(left + size / 2, top + size / 2, size / 2, 0, 2 * Math.PI, false);
      ctx[methodName]();
      if (stroke) {
        ctx.stroke();
      }
    },
    rect: function (control, ctx, methodName, left, top, size, stroke) {
      this.transparentCorners && ctx.clearRect(left, top, size, size);
      ctx[methodName + 'Rect'](left, top, size, size);
      if (stroke) {
        ctx.strokeRect(left, top, size, size);
      }
    }
  },
  resizableEdge: false,
  /**
   * Sets the coordinates of the draggable boxes in the corners of
   * the image used to scale/rotate it.
   * @private
   */
  _setCornerCoords: function() {
    var coords = this.oCoords,
      newTheta = fabric.util.degreesToRadians(45 - this.angle),
      cornerHypotenuse,
      cosHalfOffset,
      sinHalfOffset ,
      x, y;

    for (var point in coords) {
      var size = this._controls[point].area || this._controls[point].size;
      cornerHypotenuse = size * 0.707106;
      cosHalfOffset = cornerHypotenuse * Math.cos(newTheta);
      sinHalfOffset = cornerHypotenuse * Math.sin(newTheta);

      var _corners = {tl: point, tr: point, bl: point, br: point};

      if(this.resizableEdge){
        switch(point){
          case "ml":
            _corners = {tl: "tl", tr: "tl", bl: "bl", br: "bl"};
            break;
          case "mt":
            _corners = {tl: "tl", tr: "tr", bl: "tl", br: "tr"};
            break;
          case "mr":
            _corners = {tl: "tr", tr: "tr", bl: "br", br: "br"};
            break;
          case "mb":
            _corners = {tl: "bl", tr: "br", bl: "bl", br: "br"};
            break;
        }
      }

      coords[point].corner = {
        tl: {
          x: coords[_corners.tl].x - sinHalfOffset,
          y: coords[_corners.tl].y - cosHalfOffset
        },
        tr: {
          x: coords[_corners.tr].x + cosHalfOffset,
          y: coords[_corners.tr].y - sinHalfOffset
        },
        bl: {
          x: coords[_corners.bl].x - cosHalfOffset,
          y: coords[_corners.bl].y + sinHalfOffset
        },
        br: {
          x: coords[_corners.br].x + sinHalfOffset,
          y: coords[_corners.br].y + cosHalfOffset
        }
      };
    }
  },
  getCornerAreaSize: function(control){
    if(this.cornerAreaSize){
      return this.cornerAreaSize;
    }
    return this.cornerSize[control] || this.cornerSize.all || this.cornerSize
  },
  getCornerSize: function(control){
    return this.cornerSize[control] || this.cornerSize.all || this.cornerSize
  },
  /**
   * @private
   */
  _drawControl: function (control, ctx, methodName, left, top) {
    if (!this.isControlVisible(control)) {
      return;
    }
    var _cornerStyle = this.cornerStyle[control] || this.cornerStyle.all || this.cornerStyle;
    var size = this.getCornerSize(control);
    var stroke = !this.transparentCorners && this.cornerStrokeColor;
    this.cornerStyles[_cornerStyle].call(this, control, ctx, methodName, left, top, size, stroke);
  }
  // _getControlsVisibility: function() {
  //   if (!this._controlsVisibility) {
  //     this._controlsVisibility = {
  //       tl: true,
  //       tr: true,
  //       br: true,
  //       bl: true,
  //       ml: true,
  //       mt: true,
  //       mr: true,
  //       mb: true,
  //       mtr: true,
  //       p: true
  //     };
  //   }
  //   return this._controlsVisibility;
  // },
  // isControlVisible: function(controlName) {
  //   var _mtr = controlName === 'mtr';
  //   var _cv = this._getControlsVisibility();
  //   var _middle = !_mtr && controlName[0] == "m";
  //   var _corner = controlName === 'tl' || controlName === 'tr' || controlName === 'bl' || controlName === 'br';
  //
  //   if (!this.hasBoundsControls && (_mtr || _corner || _middle))return false;
  //   if (!this.hasRotatingPoint && _mtr )return false;
  //   if (this.lockUniScaling && _middle)return false;
  //
  //   if(_cv[controlName]) return true;
  //
  //   if(!_corner && !_middle && !_mtr){
  //     return _cv.p
  //   }
  //
  //   return false;
  // }
});
