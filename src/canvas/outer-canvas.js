
var addListener = fabric.util.addListener,
    removeListener = fabric.util.removeListener,
    addEventOptions = { passive: false };

fabric._.extend(fabric.Canvas.prototype, {
  updateOuterCanvasContainer(){
    this._onOuterCanvasResize();
  },
  proxyEvents(functor, eventjsFunctor){
    let canvas = this.outerCanvas;

    functor(canvas, 'mousedown', this._onMouseDown);
    functor(canvas, 'mousemove', this._onMouseMove, addEventOptions);
    functor(canvas, 'mouseout', this._onMouseOut);
    functor(canvas, 'mouseenter', this._onMouseEnter);
    functor(canvas, 'wheel', this._onMouseWheel);
    functor(canvas, 'contextmenu', this._onContextMenu);
    functor(canvas, 'dblclick', this._onDoubleClick);
    functor(canvas, 'touchstart', this._onMouseDown, addEventOptions);
    functor(canvas, 'touchmove', this._onMouseMove, addEventOptions);
    functor(canvas, 'dragover', this._onDragOver);
    functor(canvas, 'dragenter', this._onDragEnter);
    functor(canvas, 'dragleave', this._onDragLeave);
    functor(canvas, 'drop', this._onDrop);
    if (typeof eventjs !== 'undefined' && eventjsFunctor in eventjs) {
      eventjs[eventjsFunctor](canvas, 'gesture', this._onGesture);
      eventjs[eventjsFunctor](canvas, 'drag', this._onDrag);
      eventjs[eventjsFunctor](canvas, 'orientation', this._onOrientationChange);
      eventjs[eventjsFunctor](canvas, 'shake', this._onShake);
      eventjs[eventjsFunctor](canvas, 'longpress', this._onLongPress);
    }
  },
  setOuterCanvasContainer(id){
    this.outerCanvas = fabric.util.createCanvasElement();
    this.outerCanvas.style.cssText = "position: fixed; z-index: 0;";
    this._outerCtx = this.outerCanvas.getContext("2d");

    let _parent = document.getElementById(id);
    _parent.append(this.outerCanvas);
    this._outerCanvasContainer = _parent;
    fabric.window.addEventListener('resize', this._onOuterCanvasResize.bind(this));
    this._onOuterCanvasResize();

    this._checkScrollWidth();
    this._onBecomeVisible(this._outerCanvasContainer,this._onOuterCanvasResize.bind(this))

    this.proxyEvents(addListener, 'add');
    return this;
  },
  _onBecomeVisible(el,callback){
    if($(el).is(':visible')){
      return callback();
    }
    let interval = setInterval(function () {
        if ($(el).is(':visible')) {
          callback();
          clearInterval(interval);
        }
      },
      100 // 0.1 second (wait time between checks)
    );
  },
  //todo
  _checkScrollWidth(){
    let scrollDiv = document.createElement("div");
    scrollDiv.className = "scrollbar-measure";
    document.body.appendChild(scrollDiv);
// Get the scrollbar width
    this._scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    // console.warn(scrollbarWidth); // Mac:  15
// Delete the DIV
    document.body.removeChild(scrollDiv);
    this._scrollbarWidth = 0;
  },
  drawControls: function(ctx) {

    if(this.outerCanvas) {
      this._outerCtx.clearRect(0, 0, this.outerCanvas.width, this.outerCanvas.height);
    }
    let activeObject = this._activeObject;
    if (activeObject) {
      activeObject._renderControls(ctx);
      if(this.outerCanvas){
        this._outerCtx.save();
        this._outerCtx.translate(this._innerPosition.left,this._innerPosition.top);
        activeObject._renderControls(this._outerCtx);
        this._outerCtx.restore();
      }
    }
  },
  renderTopLayer(ctx) {
    if (this.isDrawingMode && this._isCurrentlyDrawing) {
      this.freeDrawingBrush && this.freeDrawingBrush._render();
      this.contextTopDirty = true;
    }
    if(this.outerCanvas) {
      this._outerCtx.clearRect(0, 0, this.outerCanvas.width, this.outerCanvas.height);
    }
    // we render the top context - last object
    if (this.selection && this._groupSelector) {
      this._drawSelection(ctx);
      this.contextTopDirty = true;
      if(this.outerCanvas) {
        this._outerCtx.save();
        this._outerCtx.translate(this._innerPosition.left, this._innerPosition.top);
        this._drawSelection(this._outerCtx);
        this._outerCtx.restore();
      }
    }
  },
  _onOuterCanvasResize(){
    //todo check scriolltop scrolleft
    //a = this.getScrollContainer().scrollTop

    let parent = this._outerCanvasContainer;
    this.outerCanvas.width  = $(parent).width() - this._scrollbarWidth;
    this.outerCanvas.height = $(parent).height() - this._scrollbarWidth;
    let _innerPosition = $(this.wrapperEl).absolutePosition();
    let _outerPosition = $(this._outerCanvasContainer).absolutePosition();
    this._innerPosition = {left: _innerPosition.left - _outerPosition.left,top: _innerPosition.top - _outerPosition.top}
    this.drawControls(this.contextContainer);
  },
  outerCanvas: null
});