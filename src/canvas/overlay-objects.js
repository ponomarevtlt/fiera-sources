
fabric._.extend(fabric.StaticCanvas.prototype, {
  setOverlayObjects(objects, callback) {
    for (let objectOptions of objects) {
      objectOptions.layer = "overlay";
    }

    this.createObjects(objects,function(){
      for(var i in this._objects){
        this._objects[i].setCoords();
      }
      callback();
    });
  }
});
