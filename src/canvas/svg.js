fabric.util.svgMediaRoot = "";

fabric._.extend(fabric.Editor.prototype, {
  toSVG: function(options,reviver) {

    let slide = this.slides[0];
    if(this.slides.length === 1){
      return slide.toSVG(options,reviver);
    }

    let _w = slide.originalWidth || slide.width,
        _h = slide.originalHeight || slide.height;

    if(slide.originalWidth){
      this.svgViewportTransformation = false;
    }

    options || (options = { });

    let markup = [];
    if(!slide.suppressPreamble){
      slide._setSVGPreamble(markup, options);
    }
    slide._setSVGHeader(markup, options);

    markup.push("<pageSet>");

    for(let i in this.slides){
      markup.push("<page>");
      markup.push(this.slides[i].getSVGBody(options, reviver));
      markup.push("</page>");
    }

    markup.push("</pageSet>");

    markup.push('</svg>');

    if(this.originalWidth){
      this.width = _w;
      this.height = _h;
    }
    return markup.join('');

  }
});
fabric._.extend(fabric.StaticCanvas.prototype, {
  /**
   * @private
   */
  _setSVGHeader: function(markup, options) {
    var width = options.width || this.width,
      height = options.height || this.height,
      vpt, viewBox = 'viewBox="0 0 ' + this.width + ' ' + this.height + '" ',
      NUM_FRACTION_DIGITS = fabric.Object.NUM_FRACTION_DIGITS;

    if (options.viewBox) {
      viewBox = 'viewBox="' +
        options.viewBox.x + ' ' +
        options.viewBox.y + ' ' +
        options.viewBox.width + ' ' +
        options.viewBox.height + '" ';
    }
    else {
      if (this.svgViewportTransformation) {
        vpt = this.viewportTransform;
        viewBox = 'viewBox="' +
          toFixed(-vpt[4] / vpt[0], NUM_FRACTION_DIGITS) + ' ' +
          toFixed(-vpt[5] / vpt[3], NUM_FRACTION_DIGITS) + ' ' +
          toFixed(this.width / vpt[0], NUM_FRACTION_DIGITS) + ' ' +
          toFixed(this.height / vpt[3], NUM_FRACTION_DIGITS) + '" ';
      }
    }

    var creatorString = `Created with Fabric.js ${fabric.version}`;
    markup.push(`<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
            version="1.1" xml:space="preserve" width="${width}" height="${height}" ${viewBox}>
        <desc>${creatorString}</desc>
        <defs>
          ${this.createSVGFontFacesMarkup()}
          ${this.createSVGRefElementsMarkup()}
          ${this.createSVGClipPathMarkup(options)}
        </defs>`
    );
  },
  _toSVG_overwritten: fabric.StaticCanvas.prototype.toSVG,
  suppressPreamble: true,
  toSVG: function(options, reviver) {
    let _w = this.width, _h = this.height;
    if(this.originalWidth){
      this.svgViewportTransformation = false;
      this.width = this.originalWidth;
      this.height = this.originalHeight;
    }

    options || (options = { });

    let markup = [];

    if(!this.suppressPreamble){
      this._setSVGPreamble(markup, options);
    }
    this._setSVGHeader(markup, options);
    markup.push(this.getSVGBody(options, reviver));

    markup.push('</svg>');

    if(this.originalWidth){
      this.width = _w;
      this.height = _h;
    }
    return markup.join('');
  },
  _setSVGLayers(markup, reviver){

    for(let layerName of this.renderOrder){
      let l = this.layers[layerName];
      if(l && l.svg){
        l.svg.call(this, markup, reviver);
      }else{
        fabric.Layer.svg.call(this, markup, reviver, layerName);
      }
    }
  },
  getSVGBody(options,reviver){

    let _w = this.width,
        _h = this.height;

    if(this.originalWidth){
      this.width = this.originalWidth;
      this.height = this.originalHeight;
    }
    options = options || {};

    let scaleX = options.scaleX || (options.width ? options.width / this.width : 1),
        scaleY = options.scaleY || (options.height ? options.height / this.height : 1),
        _l = (options.left || 0),
        _t = (options.top || 0) ,
        angle = (options.angle || 0) ,
        clipPath = "";

    if(options.clipPath){
      clipPath = `clip-path: url(#${options.clipPath});`;
    }

    let transform = "";
    if(angle !== 0){
      transform += `rotate(${angle}) `;

      if(angle == -180 || anglr == 180){
          transform += `translate(-${this.width} -${this.height}) `;

      }
    }
    let transleteTransform = "";
    if(_l !== 0 || _t !== 0){
        transleteTransform += `translate(${_l} ${_t}) `;
    }
    let scaleTransform = "";
    if(scaleX !==1  || scaleY !== 1){
        scaleTransform += `scale(${scaleX} ${scaleY}) `;
    }

      let markup = [`<g transform="${scaleTransform} ${transleteTransform}"><g transform="${transform}" style="${clipPath}" id="${options.clipPath}">`];

    this._setSVGLayers(markup, reviver);

    markup.push('</g></g>');

    if(this.originalWidth){
      this.width = _w;
      this.height = _h;
    }
    return markup.join("");
  }
});

fabric.util.object.extend(fabric.Object.prototype, {
  _createBaseSVGMarkup: function (objectMarkup, options) {
    options = options || {};
    var shadowInfo = options.withShadow ? 'style="' + this.getSvgFilter() + '" ' : '',
      absoluteClipPath = this.clipPath && this.clipPath.absolutePositioned;

    if (this.clipPath) {
      this.clipPath.clipPathId = 'CLIPPATH_' + fabric.Object.__uid++;
    }

    let commonPieces = [
      options.noStyle ? '' : 'style="' + this.getSvgStyles() + '" ',  //styleInfo
      this.strokeUniform ? 'vector-effect="non-scaling-stroke" ' : '', //vectorEffect
      options.noStyle ? '' : this.addPaintOrder(), ' ',
      options.additionalTransform ? 'transform="' + options.additionalTransform + '" ' : '',
    ].join('');
    // insert commons in the markup, style and svgCommons
    objectMarkup[objectMarkup.indexOf('COMMON_PARTS')] = commonPieces;

    let markup = `
      <g ${this.getSvgTransform(false)} ${!absoluteClipPath ? shadowInfo + this.getSvgCommons() : ''}>
          ${this.fill && this.fill.toLive && this.fill.toSVG(this) || ""}
          ${this.stroke && this.stroke.toLive && this.stroke.toSVG(this) || ""}
          ${this.shadow && this.shadow.toSVG(this) || ""}
          ${this.clipPath && `<clipPath id="${this.clipPath.clipPathId}" >${this.clipPath.toClipPathSVG(options.reviver)}</clipPath>` || ""}
          ${objectMarkup.join('')}
      </g>`;

    if (absoluteClipPath) {
      markup = `<g ${shadowInfo} ${this.getSvgCommons()} >${markup}</g>`;
    }

    return options.reviver ? options.reviver(markup) : markup;
  }
});

fabric.StaticCanvas.prototype.inlineSVG = false;

