fabric.Editor.prototype.lang = {
  "editor": {
    "history": "history",
    "save": "save project",
    "addSlide": "add slide",
    "toggleGrid": "toggle grid"
  },
  "canvas": {
    "uploadImage": "uploadImage",
    "zoom": "zoom",
    "zoomOut": "zoomOut",
    "zoomIn": "zoomIn",
    "clear": "clear",
    "clearObjects": "Clear All",
    "setBackgroundColor": "Set Background Color",
    "addText": "Add Text",
    "removeSlide": "remove slide",
    "duplicateSlide": "duplicate slide"
  },
  "history": {
    "redo": "redo",
    "undo": "undo",
    "list": "list"
  },
  "group": {
    "remove": "remove group",
    "duplicate": "duplicate group",
    "groupSelectedElements": "group Selected Elements",
    "ungroup": "ungroup"
  },
  "image": {
    "source": "set image source",
    "crop": "crop",
    "cropZoomIn": "Zoom In",
    "cropZoomOut": "Zoom Out",
    "cropEnd": "crop end",
    "filters": "filters",
    "frames": "frames"
  },
  "text": {
    "fill": "fill",
    "bold": "bold",
    "italic": "italic",
    "align": "text align",
    "alignCenter": "align center",
    "alignLeft": "align left",
    "alignRight": "align right",
    "alignJustify": "align justify",
    "alignMiddle":  "align vertical middle",
    "alignTop":     "align vertical top",
    "alignBottom":  "align vertical bottom",
    "verticalAlign": "text align vertical",
    "fontFamily": "font family",
    "fontSize": "font size",
    "overline": "overline",
    "underline": "underline",
    "linethrough": "linethrough",
    "backgroundColor": "backgroundColor",
    "textBackgroundColor": "textBackgroundColor",
    "textStyle": "text style",
    "enterEditing": "enterEditing"
  },
  "object": {
    "centerAndZoomOut": "centerAndZoomOut",
    "left": "X coordinate",
    "top": "Y coordinate",
    "position": "set object position",
    "dimensions": "set object dimensions",
    "center": "center",
    "fill": "fill",
    "width": "width",
    "height": "height",
    "flip": "flip",
    "flop": "flop",
    "order": "z-index",
    "bringForward": "bring forward",
    "sendBackwards": "send backwards",
    "bringToFront": "bring to front",
    "sendToBack": "send to back",
    "remove": "remove",
    "duplicate": "duplicate",
    "rotateLeft": "rotate Left",
    "rotateRight": "rotate Right",
    "stretch": "stretch"
  }
};