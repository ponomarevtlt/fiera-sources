fabric._.extend(fabric.Editor.prototype, {
  /**
   * move slide to another position
   * @param slide
   * @param newPosition
   */
  moveSlide: function (slide,newPosition) {
    var _s = this.slides;

    var _curPos = _s.indexOf(slide);
    newPosition = parseInt(newPosition);

    if (_curPos < newPosition) {
      _s.splice(_curPos, 1);
      _s.splice(newPosition,0, slide);
    } else {
      _s.splice(_curPos, 1);
      _s.splice(newPosition, 0, slide);
    }
  },
  /**
   * replace slide
   * @param slide
   * @param newPosition - position of the second slide
   */
  replaceSlide: function (slide,newPosition) {
    var _s = this.slides;
    var _replacedSlide = _s[newPosition];
    var _curPos = _s.indexOf(slide);
    if (_curPos < newPosition) {
      _s.splice(newPosition, 1, slide);
      _s.splice(_curPos, 1, _replacedSlide);
    } else {
      _s.splice(_curPos, 1, _replacedSlide);
      _s.splice(newPosition, 1, slide);
    }
  },
  setActiveSlideByIndex: function(index){
    this.setActiveSlide(this.slides[index])
  },
  setActiveSlideById: function(id){
    this.setActiveSlide(  fabric._.findWhere(this.slides,{id: id}));
  },
  duplicateSlide: function (slideData) {
    slideData = slideData.toObject();
    var _slide = this.addSlide(slideData);
    _slide.load(_slide.object);
  },
  nextSlide: function () {
    var i = this.slides.indexOf(this.activeSlide);
    if (i < this.slides.length - 1) {
      this.setActiveSlide(i + 1);
    }
  },
  prevSlide: function () {
    var i = this.slides.indexOf(this.activeSlide);
    if (i > 0) {
      this.setActiveSlide(i - 1);
    }
  },
  gotoSlide: function (slide) {
    this.setActiveSlide(slide - 1);
  },
  nextSlideAvailable: function () {
    var i = this.slides.indexOf(this.activeSlide);
    return i < this.slides.length - 1
  },
  prevSlideAvailable: function () {
    var i = this.slides.indexOf(this.activeSlide);
    return i > 0
  },
  createSlidesListElement: function(slide){
    let $element = fabric.util.createCanvasElement();
    $element.setAttribute("height",140);
    fabric.util.createThumb(slide,$element);

    slide.$slideElement = $("<div>")
      .addClass("slide")
      .click(() => this.setActiveSlide(slide))
      .append($element)
      .append($("<span>").html(slide.title));


    if(slide.description){
      slide.$slideElement.attr("title",slide.description)
    }

    slide.on({
      "modified": function () {
        this.renderAll();
      },
      "removed": function () {
        slide.$slideElement.remove();
      }
    });
    this.$slideListInner.append(slide.$slideElement);
  },
  createSlidesList: function ($container) {
    this.$slideList = this.$slidesList = $("<div>").addClass("slides-list");
    this.$slideListInner = $("<div>").addClass("slides-list-inner");
    this.$slideList.append(this.$slideListInner);

    $container.append(this.$slidesList);

    this.slides.forEach(slide => this.createSlidesListElement(slide));

    this.on("slide:created",e => {
      this.createSlidesListElement(e.target);
    });

    return {
      list: this.$slideList
    }

    //this.canvas.on( "dimensions:modified" ,this.updateThumbs.bind(this))
    //this.updateThumbs();
  },
  updateThumbs: function(){
    this.slides.forEach(slide => {
      slide.canvas.setDimensions({width: this.canvas.width, height: this.canvas.height})
    })
  }
});

fabric._.extend(fabric.Canvas.prototype, {
  title: "New Slide",
  unique: false,
  required: false,
  // stateProperties: ["unique","required"],
  removeSlide: function(){
    this.application.removeSlide(this)
  },
  duplicateSlide: function(){
    this.application.duplicateSlide(this)
  },
  actions: fabric._.extend(fabric.Canvas.prototype.actions, {
    removeSlide: {
      title: "@canvas.removeSlide",
      className: 'fa fa-trash-o'
    },
    duplicateSlide: {
      title: "@canvas.duplicateSlide",
      className: 'fa fa-clone'
    }
  })
  // eventListeners: fabric.util.extendArraysObject(fabric.Canvas.prototype.eventListeners, {
  //   "modified" : function(e){
  //     if(this.canvas){
  //       this.canvas.canvas.set(e.states.modified);
  //       this.canvas.fire("modified");
  //     }
  //   },
  //   "object:modified" : function(){
  //     if(this.canvas){
  //       this.canvas.fire("modified");
  //     }
  //   }
  // })
});
