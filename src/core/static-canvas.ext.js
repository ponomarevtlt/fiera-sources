
let CanvasMixin = {
  cacheProperties: [],
  objects: null,
  processingProperties: null,
  /**
   * @values  "svg" | "canvas
   */
  canvasType: "canvas",
  hasStateChanged: fabric.Object.prototype.hasStateChanged,
  setEventListeners: function(val){
    this.on(val);
  },
  find: function (options) {
    if (typeof options === "string"){
      options = {
        type: options
      }
    }
    return fabric._.where(this._objects,options);
  },
  onResize: function(){
    let _scale = Math.min(1,800 /this.width );
    // this.setZoom(_scale);
    this.setDimensions({width: this.width,height: this.height});
  },
  getCenter: function (el) {
    return {
      top: (this.originalHeight  || this.getHeight()) / 2,
      left: (this.originalWidth || this.getWidth()) / 2
    };
  },


  setOriginalSize: function (w, h) {
    this.originalWidth = h ? w : (w.naturalWidth || w.width);
    this.originalHeight = h ? h : (w.naturalHeight || w.height);
    return this;
  },
  setOriginalWidth: function (value) {
    this.originalWidth = value;
    if(!this.stretchable){
      this.setWidth(value);
    }
  },
  setOriginalHeight: function (value) {
    this.originalHeight = value;
    if(!this.stretchable){
      this.setHeight(value);
    }
  },

};
fabric._.extend(fabric.StaticCanvas.prototype,CanvasMixin);
fabric._.extend(fabric.Canvas.prototype,CanvasMixin);
