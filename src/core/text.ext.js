fabric.util.object.extend(fabric.Textbox.prototype, {
  getStylePosition(index){
    var loc = this.get2DCursorLocation(index);
    if (this._styleMap && !this.isWrapping) {
      var map = this._styleMap[loc.lineIndex];
      if (!map) {
        return null;
      }
      loc.lineIndex = map.line;
      loc.charIndex = map.offset + loc.charIndex;
    }
    return loc;
  }
});

fabric.util.object.extend(fabric.Text.prototype, {
  getStylePosition(index){
    return this.get2DCursorLocation(index);
  },
  store_textLines(){
    return this.textLines.map(line => line.length);
  },
  setTextLines (val){
    // console.log("text lines",val,this.textLines);
  },
  /**
   * Check if characters in a text have a value for a property
   * whose value matches the textbox's value for that property.  If so,
   * the character-level property is deleted.  If the character
   * has no other properties, then it is also deleted.  Finally,
   * if the line containing that character has no other characters
   * then it also is deleted.
   *
   * @param {string} property The property to compare between characters and text.
   */
  cleanStyle: function (property) {
    if (!this.styles || !property || property === '') {
      return false;
    }
    var obj = this.styles, stylesCount = 0, letterCount, stylePropertyValue,
      allStyleObjectPropertiesMatch = true, graphemeCount = 0, styleObject;
    // eslint-disable-next-line
    for (var p1 in obj) {
      letterCount = 0;
      // eslint-disable-next-line
      for (var p2 in obj[p1]) {
        var styleObject = obj[p1][p2],
          stylePropertyHasBeenSet = styleObject.hasOwnProperty(property);

        stylesCount++;

        if (stylePropertyHasBeenSet) {
          if (!stylePropertyValue) {
            stylePropertyValue = styleObject[property];
          }
          else if (styleObject[property] !== stylePropertyValue) {
            allStyleObjectPropertiesMatch = false;
          }

          if (styleObject[property] === this[property]) {
            delete styleObject[property];
          }
        }
        else {
          allStyleObjectPropertiesMatch = false;
        }

        if (Object.keys(styleObject).length !== 0) {
          letterCount++;
        }
        else {
          delete obj[p1][p2];
        }
      }

      if (letterCount === 0) {
        delete obj[p1];
      }
    }
    // if every grapheme has the same style set then
    // delete those styles and set it on the parent
    for (var i = 0; i < this._textLines.length; i++) {
      graphemeCount += this._textLines[i].length;
    }

    if (allStyleObjectPropertiesMatch && stylesCount === graphemeCount) {

      //edited:  visceroid
      if (stylePropertyValue !== undefined) {
        this[property] = stylePropertyValue;
      }
      this.removeStyle(property);
    }
  }
});

fabric.TextMixin = {
  toObject: fabric.Object.prototype.toObject,
  accepts: {
    role: "fontFamily"
  },
  setData: function (data) {
    if (data.role === "fontFamily") {
      this.setFontFamily(data.fontFamily)
    }
  },
  getStyle: function (styleName) {
    return (this.getSelectionStyles && this.isEditing)
      ? (this.getStyleAtPosition(this.selectionStart)[styleName] || this[styleName])
      : (this[styleName] === undefined ? this['__' + styleName] : this[styleName]);
  },
  getPattern: function (url) {
    var _fill = this.getStyle('fill ');
    return _fill && _fill.source;
  },
  setPattern: function (url) {
    if (!url) {
      this.setStyle('fill');
    } else {
      // var _texture = _.findWhere(this.project.textures, {id: url});
      var _this = this;
      fabric.util.loadImage(url, function (img) {
        _this.setStyle('fill', new fabric.Pattern({
          source: img,
          repeat: 'repeat'
        }));
      }, this, this.crossOrigin); //todo
    }
  },
  /* getOpacity: function () {
    return this.getStyle('opacity') * 100;
  },
  setOpacity: function (value) {
    this.setStyle('opacity', parseInt(value, 10) / 100);
  },*/
  getRadius: function () {
    return this.get('radius');
  },
  setShadow: function (options) {
    return this.setProperty('shadow', options ? new fabric.Shadow(options) : null);
  },
  setRadius: function (value) {
    this.setProperty('radius', value);
  },
  getSpacing: function () {
    return this.get('spacing');
  },
  setSpacing: function (value) {
    this.setProperty('spacing', value);
  },
  getReverted: function () {
    return this.get('reverted');
  },
  setReverted: function (value) {
    this.setProperty('reverted', value);
  },
  getFill: function () {
    return this.getStyle('fill');
  },
  setFill: function (value) {
    this.setStyle('fill', value);
  },
  getText: function () {
    return this.get('text');
  },
  setText: function (value) {
    this.setProperty('text', "" + value);
  },
  getTextAlign: function () {
    return this.get('textAlign');
  },
  setTextAlign: function (value) {
    this.setProperty('textAlign', value.toLowerCase());
  },
  getFontFamily: function () {
    return this.get('fontFamily');
  },
  setStyles: function(val){
    this.styles = val || {};
  },
  setFontFamily: function (value) {
    this.setStyle('fontFamily', value);
  },
  getStyles: function () {
    return {
      fill: this.fill,
      fontSize: this.fontSize,
      textBackgroundColor: this.textBackgroundColor,
      fontFamily: this.fontFamily,
      fontWeight: this.fontWeight,
      fontStyle: this.fontStyle,
      stroke: this.stroke,
      strokeWidth: this.strokeWidth
    };
  },
  getBgColor: function () {
    return this.get('backgroundColor');
  },
  setBgColor: function (value) {
    this.setProperty('backgroundColor', value);
  },
  getTextBgColor: function () {
    return this.get('textBackgroundColor');
  },
  setTextBgColor: function (value) {
    this.setProperty('textBackgroundColor', value);
  },
  getStroke: function () {
    return this.getStyle('stroke');
  },
  setStroke: function (value) {
    this.setStyle('stroke', value);
  },
  getStrokeWidth: function () {
    return this.getStyle('strokeWidth');
  },
  setStrokeWidth: function (value) {
    this.setStyle('strokeWidth', parseInt(value, 10));
  },
  decreaseFontSize: function () {
    this.setStyle('fontSize', parseInt(this.getStyle('fontSize')) - 1);
  },
  increaseFontSize: function () {
    this.setStyle('fontSize', parseInt(this.getStyle('fontSize')) + 1);
  },
  getFontSize: function () {
    return this.getStyle('fontSize');
  },
  setFontSize: function (value) {
    this.setStyle('fontSize', parseInt(value, 10));
  },
  minFontSize: 2,
  maxFontSize: 250,
  minLineHeight: 2,
  maxLineHeight: 200,
  maxStrokeWidth: function () {
    return Math.ceil(this.getFontSize() / 10);
  },
  getLineHeight: function () {
    return this.getStyle('lineHeight');
  },
  setLineHeight: function (value) {
    this.setStyle('lineHeight', parseFloat(value, 10));
  },
  getBold: function () {
    return this.getStyle('fontWeight') === "bold";
  },
  setBold: function (value) {
    this.setStyle('fontWeight', value ? 'bold' : 'normal');
  },
  getItalic: function () {
    return this.getStyle('fontStyle') === 'italic';
  },
  setItalic: function (value) {
    this.setStyle('fontStyle', value ? 'italic' : 'normal');
  },
  getUnderline: function () {
    return this.getStyle('underline');
  },
  setUnderline: function (value) {
    this.setStyle('underline', value );
  },
  getLinethrough: function () {
    return this.getStyle('linethrough');
  },
  setLinethrough: function (value) {
    this.setStyle('linethrough', value);
  },
  getOverline: function () {
    return this.getStyle('overline');
  },
  setOverline: function (value) {
    this.setStyle('overline', value);
  },
  addText: function (text, options) {
    let match = this.text.match(/\n/g);
    let lineIndex = match && match.length || 0;
    let charIndex = this.text.length - this.text.lastIndexOf("\n") - 1;

    if (!this.styles[lineIndex]) {
      this.styles[lineIndex] = {}
    }

    if (!this.styles[lineIndex][charIndex]) {
      this.styles[lineIndex][charIndex] = {}
    }
    fabric.util.object.extend(this.styles[lineIndex][charIndex], options);
    this.text += text;
    // this.styles;
  },
  _checkModifiedText: function (prop, value) {
    if (this.isEditing) {
      var isTextChanged = (this._textBeforeEdit !== this.text);
      if (isTextChanged) {
        this.canvas.fire("object:modified", {target: this});
      }
    }
  },
  setProperty: function (prop, value) {
    if (this.canvas && this.canvas.stateful) {
      this._checkModifiedText();
      this.saveState();
    }

    this[prop] = value;

    this.fire("modified", {});
    if (this.canvas) {
      this.canvas.fire("object:modified", {target: this});
      this.canvas.renderAll();
    }
    this._textBeforeEdit = this.text;
  },
  _removeStyle: function (styleName) {
    for(let row in this.styles) {
      for (let index in this.styles[row]) {
        delete this.styles[row][index][styleName]
      }
    }
  },
  _removeStyleAt: function (propertyToRemove,index) {
    var loc = this.getStylePosition(index);
    if (!this._getLineStyle(loc.lineIndex) || !this._getStyleDeclaration(loc.lineIndex, loc.charIndex)) {
      return;
    }
    let style = this.styles[loc.lineIndex][loc.charIndex];
    delete style[propertyToRemove];
    if(!Object.keys(style).length ){
      delete this.styles[loc.lineIndex][loc.charIndex];
      if(!this.styles[loc.lineIndex].length ){
        delete this.styles[loc.lineIndex];
      }
    }
  },
  _modifyObjectStyleProperty (styleName,value){
    let count = 0;
    for(let row in this.styles) {
      for (let index in this.styles[row]) {
        if(this.styles[row][index] === undefined || this.styles[row][index][styleName] === value){
          count++;
        }else{
          return;
        }
      }
    }
    if(count === this.text.length){
      this._removeStyle(styleName);
      this[styleName] = value;
    }
  },
  setStyle: function (styleName, value) {
    if (this.canvas && this.canvas.stateful) {
      this._checkModifiedText();
      this.saveState();
    }

    let _old = this.getStyles();

    _old.styles = this.styles;
    if (this.setSelectionStyles && this.isEditing && this.selectionStart !== this.selectionEnd) {

      if (value === undefined || this[styleName] === value){
        for (let i = this.selectionStart; i < this.selectionEnd; i++) {
          this._removeStyleAt(styleName,i);
        }
        this._forceClearCache = true;
      }else{
        this.setSelectionStyles({[styleName]: value}, this.selectionStart, this.selectionEnd);
      }
      this._modifyObjectStyleProperty(styleName,value);
      this.setCoords();
    }
    else {
      if (value !== undefined) {
        this._removeStyle(styleName);
        this[styleName] = value;
      } else {
        delete this[styleName];
      }
      if (!this._textLines && this.ready) {
        this._clearCache();
        this._splitText();
      }
      // this.cleanStyle(styleName);
    }

    this.setCoords();

    if (this.caching) {
      this.dirty = true;
    }

    this.fire("modified", {});

    if (this.canvas) {
      this.canvas.fire("object:modified", {target: this});
      this.canvas.renderAll();
    }
  },
  generateTextStyle: function () {
    return {
      'font-style': this.isItalic() ? 'italic' : 'normal',
      'font-weight': this.isBold() ? 700 : 400,
      'text-decoration': (this.isLinethrough() ? 'line-through ' : '') +
      (this.isOverline() ? 'overline ' : '') +
      (this.isUnderline() ? 'underline ' : '')
    }
  }
};

fabric.util.object.extend(fabric.Text.prototype, fabric.TextMixin);
fabric.util.object.extend(fabric.IText.prototype, fabric.TextMixin);
fabric.util.object.extend(fabric.Textbox.prototype, fabric.TextMixin);
fabric.util.object.extend(fabric.IText.prototype, {
  // stateProperties: fabric.IText.prototype.stateProperties.concat(["styles"]),
  store_styles: function () {
    if (!Object.keys(this.styles).length) return null;
    let _styles = {};
    let _is_not_empty = false;
    for (let row in this.styles) {
      if (Object.keys(this.styles[row]).length) {
        var _row_empty = true;
        for (let char in this.styles[row]) {
          if (Object.keys(this.styles[row][char]).length) {
            if (_row_empty) {
              _styles[row] = {};
              _row_empty = false;
            }
            _styles[row][char] = fabric.util.object.clone(this.styles[row][char]);
          }
        }
        if (!_row_empty) {
          _is_not_empty = true;
        }
      }
    }
    return _is_not_empty && _styles || null;
  },
  initHiddenTextarea_native: fabric.IText.prototype.initHiddenTextarea,
  initHiddenTextarea: function () {
    this.initHiddenTextarea_native();
    this.hiddenTextarea.style.width = "9999px";
    this.hiddenTextarea.style["margin-left"] = "-9999px";
  },
  /**
   * Exits from editing state
   * @return {fabric.IText} thisArg
   * @chainable
   */
  /* exitEditing: function() {

     var isTextChanged = (this._textBeforeEdit !== this.text);
     this.selected = false;
     this.isEditing = false;
     this.selectable = true;

     this.selectionEnd = this.selectionStart;

     if (this.hiddenTextarea) {
       this.hiddenTextarea.blur && this.hiddenTextarea.blur();
       this.canvas && this.hiddenTextarea.parentNode.removeChild(this.hiddenTextarea);
       this.hiddenTextarea = null;
     }

     this.abortCursorAnimation();
     this._restoreEditingProps();
     this._currentCursorOpacity = 0;

     this.fire('editing:exited');
     isTextChanged && this.fire('modified');
     if (this.canvas) {
       this.canvas.off('mouse:move', this.mouseMoveHandler);
       this.canvas.fire('text:editing:exited', { target: this });
       isTextChanged && this.canvas.fire('object:modified', { target: this });
       // this.canvas.fireModifiedIfChanged(this);
     }
     return this;
   },*/
});