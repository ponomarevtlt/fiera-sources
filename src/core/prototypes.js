/**
 * # Prototypes

 Prototypes allows to define **prototypes** property in Application configuration.

 ```javascript
 Application ({
  prototypes: {
    ClassName: options
  }
 })
 ```

 If prototype is defined every new Object created by application will have this properties by default.

 ```javascript
 NewClass: {
    $super: "ParentClass",
    type: "new-class",
    \/*other properties and methods*\/
   }
 ```

 if property **type** of Object class is defined then every object be default will have this type.

 ```javascript
 Object: {
    type: "rectangle"
   }
 ...
 //rectangle will be created
 fabric.createObject({width:100, height: 100})
 ```
 */
fabric._.extend(fabric.Editor.prototype, {
  /**
   * default prototypes propertes for objects
   */
  /*prototypes: {
    Object: {
      includeDefaultValues: false
    },
    Canvas: {
      includeDefaultValues: false
    }
  },*/
  optionsOrder: fabric.util.a.build(fabric.Editor.prototype.optionsOrder).find("canvasContainer").before("prototypes").array,
  getDefaultProperties: function(stringTypeOrPrototype){
    let klassname, proto;
    if(stringTypeOrPrototype.constructor === String){
      klassname = fabric.util.string.capitalize(fabric.util.string.camelize(stringTypeOrPrototype),true);
      let _klass = this.getKlass(klassname);
      proto = _klass.prototype;
    }else{
      proto = stringTypeOrPrototype;
      klassname = fabric.util.string.capitalize(fabric.util.string.camelize(proto.type),true);
    }
    let _protoProperties = proto && proto.__proto__ && proto.__proto__.type && this.getDefaultProperties(proto.__proto__) || {};
    let _defaultProperties =  klassname && this.prototypes && fabric._.clone(this.prototypes[klassname]) || {};

    fabric._.extend(_protoProperties,_defaultProperties);
    return _protoProperties;
  },

  getSyncKlass: function(type){
    let klassName = fabric.util.string.camelize(type.charAt(0).toUpperCase() + type.slice(1));
    return this.klasses["S" + klassName]|| fabric["S" + klassName]  || this.klasses[klassName] || fabric[klassName]
  },
  getKlass: function(type){
    let klassName = fabric.util.string.camelize(type.charAt(0).toUpperCase() + type.slice(1));
    return /*this.klasses["S" + klassName]|| fabric["S" + klassName]  ||*/ this.klasses[klassName] || fabric[klassName]
  },
  store_prototypes () {
    if(!this._prototypes)return;
    return this._prototypes;
  },
  setPrototypes: function (prototypes) {
    this._prototypes = prototypes;
    let _prototypes = fabric.util.deepExtend({},prototypes);
    this.prototypes = _prototypes;
    if(_prototypes.eventListeners){
      _prototypes.eventListeners.$extend = 'array';
    }
    for (let klassName in _prototypes) {
      let _proto = _prototypes[klassName];
      for (let prop in _proto) {
        let value = _proto[prop];
        if(prop[0] === "+"){
          delete _proto[prop];
          prop = prop.substr(1);
          let protoValue = fabric[klassName].prototype[prop];
          if(protoValue.constructor === Array){
            if(value.constructor === Array){
              _proto[prop] = protoValue.concat(value)
            }else{
              _proto[prop] = protoValue.concat([value])
            }
          }
        }
        if (value && value["$extend"]) {
          let _extend = value["$extend"];
          let protoValue = fabric[klassName].prototype[prop];
          if( _extend === "array"){
            _proto[prop] = fabric.util.extendArraysObject(protoValue,value);
          } else if( _extend === "deep"){
            _proto[prop] = fabric.util.deepExtend(protoValue,value);
          }else{
            _proto[prop] = fabric._.extend(protoValue,value);
          }
          delete value["$extend"];
        }
      }
      if(_proto["prototype"]){
        let _superklass = this.getSyncKlass(_proto["prototype"]);
        if(!_superklass){
          console.warn(`class ${_proto["prototype"]} doent exists `);
          continue;
        }
        delete _proto["prototype"];
        _proto.type = fabric.util.string.toDashed(klassName);

        // let _fromObject = _proto.fromObject || _superklass.fromObject ;
        delete _proto.fromObject;
        let _klass = this.klasses[klassName] = fabric.util.createClass(_superklass, _proto);
        // _klass.fromObject = _fromObject.bind(_klass);
      }
    }
    if (_prototypes.Editor) {
      fabric.util.deepExtend(this, _prototypes.Editor);
    }
    if (this.actions && this.actions.constructor === Function) {
      this.actions = this.actions.call(this)
    }
  },
  _populateWithDefaultProperties: function(target,options){
    if(!target.disableDefaultProperties){
      fabric._.defaults(options, this.getDefaultProperties(target, options));
      for (let key in options) {
        let value = options[key];
        if (key[0] === "+") {
          let _key = key.substr(1);
          let _arr = target.get(_key);
          if (_arr instanceof Array) {
            _arr = _arr.slice().concat(value);
          } else {
            _arr = fabric._.extend({}, _arr, value);
          }
          options[_key] = _arr;
          delete options[key];
        }
      }
    }
  },
  eventListeners: fabric.util.extendArraysObject(fabric.Editor.prototype.eventListeners, {
    "created": function (e) {
      if(e.options.prototypes && e.options.prototypes.Editor){
        fabric._.defaults(e.options,e.options.prototypes.Editor)
      }
    },
    "entity:load": function (e) {
      fabric._.defaults(e.options,this.getDefaultProperties(e.options.type));
    }
  })
});

fabric.on({
  "entity:created": function (e) {
    if (e.target.application) {
      e.target.application._populateWithDefaultProperties(e.target, e.options);
      delete e.options.application;
    }
  }
});