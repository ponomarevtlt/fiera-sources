

fabric._.extend(fabric.util, {
  createCanvasElement: function (a,b) {
    let canvas = fabric.document.createElement('canvas');
    if(a && b){
      canvas.width = a;
      canvas.height = b;
    }
    else if(a){
      canvas.width = a.width;
      canvas.height = a.height;
    }
    return canvas;
  },

  /**
   * Creates image element (works on client and node)
   * @static
   * @memberOf fabric.util
   * @return {HTMLImageElement} HTML image element
   */
  createImage: function() {
    // if(fabric.isLikelyNode){
    //   const { Image } = require('canvas');
    //   return new Image();
    // }else{
      let img = fabric.document.createElement('img');
      img.crossOrigin = 'Anonymous';
      return img;
    // }
  }
});


fabric._.extend(fabric.Editor.prototype, {
  setUtils: function (utilsOptions) {

    if (!utilsOptions) {
      return;
    }
    fabric._.extend(this.util || {}, utilsOptions);

    if (this.options['util']) {
      fabric._.extend(fabric.util, this.options['util']);
    }
    if (this.options['fabric']) {
      fabric._.extend(fabric, this.options['fabric']);
    }
    delete this.options['fabric'];
    delete this.options['util'];
  }
});