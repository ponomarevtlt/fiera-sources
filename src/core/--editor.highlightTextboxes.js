/**
 * @author Denis Ponomarev
 * Highlight textboxes
 */
fabric._.extend(fabric.Editor.prototype, {
  highlightTextboxes: false,
  actions: fabric._.extend(fabric.Canvas.prototype.actions,{
    highlightTextboxes: {
      title: "@editor.highlightTextboxes",
      className: "fa fa-ticket-alt broken",
      type: "checkbox",
      value: "highlightTextboxes"
    }
  })
});