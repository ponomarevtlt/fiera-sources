
/**
 ### attribute: backgroundImageProperties

 default attributes for background image

 ### freeDrawingBrush

 default active drawing brush

 ```
 freeDrawingBrush: "PaintBucketBrush" | "PaintPenBrush" | "PencilBrush"
 ```

 ### attribute: onSlideLoaded

 onSlideLoaded calls as a callback for load fucntion

 ### attribute: backgroundPosition
 ```
 backgroundPosition: 'manual' | 'resize' | 'contain' | 'cover' | 'center'
 ```

 - manual - background will ne not scaled and put at left top corner
 - resize - canvas will be resized according to image size
 - contain - will be scaled to contain canvas size
 - cover - will be scaled to cover all canvas size
 - center - backogrund will be not scaled but put in the middle

 ### method: setInteractiveMode

 switch between drawing and hand( moving cunvas by mouse) modes

 ```javascript
 canvas.setInteractiveMode( mode : "hand" | "mixed") : void
 ```

 ### drawingColor

 drawing color using by brushes

 */

fabric.Canvas.fromJson = function(url,callback , element){
  fabric.util.data.loadJson(url,function(data){
    new fabric.Canvas(element,data,callback)
  })
};


fabric._.extend(fabric.Canvas.prototype, {
  stateful: true,
  optionsOrder: ["originalWidth","originalHeight","width","height","*"],
  originalState: {},
  stateProperties: [],
  editingObject: null,
  fitIndex: 0.8,
  originalWidth: 0,
  originalHeight: 0,
  /**
   * Select object which is over the selected one
   */
  frontObjectsSelectionPriority: false,
  canvasType: "canvas",
  /**
   * initialized width of the canvas
   */
  width: 160 ,
  /**
   * initialized height of the canvas
   */
  height: 90,
  /**
   * output quality
   */
  dotsPerUnit: 1,
  scale: 1,
  loaded: false,
  /**
   * allow user to interact with canvas
   */
  interactive: true,

  thumbSize: {
    width: 50,
    height: 100
  },
  contextTopImageSmoothingEnabled: true,

  pageBackground: false,
  /**
   * required to show video
   */
  animated: false,
  //TODO BUGS IF DISABLED
  preserveObjectStacking: true,
  getOriginalSize: function () {
    return {
      width: this.originalWidth,
      height: this.originalHeight
    }
  },

  /**
   * Method that determines what object we are clicking on
   * the skipGroup parameter is for internal use, is needed for shift+click action
   * @param {Event} e mouse event
   * @param {Boolean} skipGroup when true, activeGroup is skipped and only objects are traversed through
   */
  ____findTarget: function (e, skipGroup) {
    if (this.skipTargetFind) {
      return;
    }
    let ignoreZoom = true,
      pointer = this.getPointer(e, ignoreZoom),
      activeGroup = this.getActiveGroup(),
      activeObject = this.getActiveObject(),
      activeTarget, activeTargetSubs;
    // first check current group (if one exists)
    // active group does not check sub targets like normal groups.
    // if active group just exits.
    this.targets = [];
    if (activeGroup && !skipGroup && activeGroup === this._searchPossibleTargets([activeGroup], pointer)) {
      this._fireOverOutEvents(activeGroup, e);
      return activeGroup;
    }
    // if we hit the corner of an activeObject, let's return that.
    if (activeObject && activeObject._findTargetCorner(pointer)) {
      this._fireOverOutEvents(activeObject, e);
      return activeObject;
    }

    if (!this.frontObjectsSelectionPriority && activeObject && activeObject === this._searchPossibleTargets([activeObject], pointer)) {
      if (!this.preserveObjectStacking) {
        this._fireOverOutEvents(activeObject, e);
        return activeObject;
      }
      else {
        activeTarget = activeObject;
        activeTargetSubs = this.targets;
        this.targets = [];
      }
    }

    let target = this._searchPossibleTargets(this._objects, pointer);
    if (e[this.altSelectionKey] && target && activeTarget && target !== activeTarget) {
      target = activeTarget;
      this.targets = activeTargetSubs;
    }
    this._fireOverOutEvents(target, e);
    return target;
  },
  setInteractive (value) {
    this.interactive = value;
  },
  setContextTopImageSmoothingEnabled () {
    let ctx = this.contextTop;
    if(ctx.imageSmoothingEnabled){
      ctx.imageSmoothingEnabled = this.contextTopImageSmoothingEnabled;
      return;
    }
    ctx.webkitImageSmoothingEnabled = this.contextTopImageSmoothingEnabled;
    ctx.mozImageSmoothingEnabled    = this.contextTopImageSmoothingEnabled;
    ctx.msImageSmoothingEnabled     = this.contextTopImageSmoothingEnabled;
    ctx.oImageSmoothingEnabled      = this.contextTopImageSmoothingEnabled;
  },
  _onMouseUpInDrawingMode: function(e) {
    this._isCurrentlyDrawing = false;
    if (this.clipTo) {
      this.contextTop.restore();
    }
    let pointer = this.getPointer(e);
    this.freeDrawingBrush.onMouseUp(pointer);
    this._handleEvent(e, 'up');
  },
  _clearObjects: function(){
    this.discardActiveObject();

    let _removedObjects = this._objects.filter(object => (!object.permanent));
    this._objects = this._objects.filter(object => (object.permanent));

    if (this._hasITextHandlers) {
      this.off('mouse:up', this._mouseUpITextHandler);
      this._iTextInstances = null;
      this._hasITextHandlers = false;
    }
    return _removedObjects;
  },
  clearObjects: function(){
    let _oldObjects = this._clearObjects();
    this.clearContext(this.contextContainer);
    this.fire('canvas:cleared',{objects: _oldObjects});
    this.renderAll();
    return this;
  },
  setAnimated: function(val){
    this.animated = val;

    let canvas = this;

    let render = function render() {
      canvas.renderAll();
      fabric.util.requestAnimFrame(render);
    };

    if(val){
      fabric.util.requestAnimFrame(render);
    }
  },
  // eventListeners: {
  //   "modified loading:begin draw:after object:modified canvas:cleared object:added object:removed group:removed canvas:created" : function(){
  //     this.dirty = true;
  //     console.log("dirty");
  //   }
  // },
  create: function () {
    this.created = true;
    this._initInteractive();
    this._createCacheCanvas();
  }
});