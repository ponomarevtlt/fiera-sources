
var StateMixin = {
  _saveStateOverwritten: fabric.Object.prototype.saveState,
  /**
   * Saves state of an object
   * @param {Object} [options] Object with additional `stateProperties` array to include when saving state
   * @return {fabric.Object} thisArg
   */
  saveState(options) {
    let history = this.application ? this.application.history : this.history;
    if(history && history.processing)return;
    let _includeDefaultValues = this.includeDefaultValues;
    if(!_includeDefaultValues){
      this.includeDefaultValues = true;
    }

    var property = "originalState";

    if(!options && this.canvas && this.canvas._currentTransform){
      options = ["left","top","width","height","scaleX","scaleY","skewX","skewY"];
    }
    if(!options){
      this[property] = this.toObject();
    }else if(options.length) {
      this[property] = this.getProperties(options);
    }else{
      this._saveStateOverwritten(options);
    }
    if(!_includeDefaultValues){
      this.includeDefaultValues = false;
    }
    return this;
  },

  getModifiedStates() {

    let original= this.originalState;
    let modified= this.getProperties(Object.keys(this.originalState));

    let _counter = 0;
    let states = {
      original: {},
      modified: {}
    };

    for (let prop in original) {
      if (original[prop] !== modified[prop]) {
        if (original[prop] instanceof Object) {
          if (JSON.stringify(original[prop]) === JSON.stringify(modified[prop])) {
            continue;
          }
        }
        states.original[prop] = original[prop];
        states.modified[prop] = modified[prop];
        _counter++;
      }
    }
    return _counter && states;
  }
};
fabric.util.object.extend(fabric.Canvas.prototype, fabric.StateMixin);
fabric.util.object.extend(fabric.StaticCanvas.prototype, StateMixin);
fabric.util.object.extend(fabric.Object.prototype,StateMixin);

var CanvasStatesMixin = {
  // toObjectSync: function (propertiesToInclude) {
  //   propertiesToInclude = (propertiesToInclude || []).concat(this.storeProperties);
  //
  //   let _data = {
  //     objects: this.getObjects(),
  //     width: this.originalWidth || this.width,
  //     height: this.originalHeight || this.height,
  //   };
  //   fabric.util.populateWithProperties(this, _data, propertiesToInclude);
  //
  //
  //   this.fire("before:object", {object: _data});
  //
  //   return _data;
  // },
  includeDefaultValues: false,
  store_width: function () {
    return this.originalWidth || this.width;
  },
  store_height: function () {
    return this.originalHeight || this.height;
  },
  toObject: fabric.Object.prototype.toObject
};
fabric.util.object.extend(fabric.StaticCanvas.prototype, CanvasStatesMixin);
fabric.util.object.extend(fabric.Canvas.prototype, CanvasStatesMixin);
