

fabric._.extend(fabric.Object.prototype, {
  hasBoundsControls: true,
  stroke: "transparent",
  minStrokeWidth: 0,
  movementDelta: 1,
  eventListeners: {},
  //--------------------------------------------------------------------------------------------------------------------
  // Event Listeners
  // //--------------------------------------------------------------------------------------------------------------------
  //
  // initEventListeners: function(){
  //
  //     if(!this.__eventListeners){
  //         this.__eventListeners = {};
  //     }
  //     for (let event in this.eventListeners) {
  //         if(!this.__eventListeners[event]){
  //             this.__eventListeners[event] = [];
  //         }
  //         this.__eventListeners[event] = this.__eventListeners[event].concat (this.eventListeners[event]);
  //     }
  // },
  setEventListeners(val){
    this.on(val);
  },
  // setAngle(angle) {
  //   this.angle = angle;
  // },
  id: null,
  store_id(){
    if(this.id && this.id.constructor === String){
      return this.id;
    }
    else{
      return null;
    }
  },
  setId(id){
    this.id = id;
    if(id.constructor === String){
      if(typeof window !== "undefined" && !window[id]){
        window[id] = this;
      }
    }
  },
  disable () {
    this.set({
      selectable: false,
      evented: false,
      hasControls: false,
      lockMovementX: true,
      lockMovementY: true
    });
  },
  _initEntity (options) {
    if(options.canvas && !options.application){
      options.application = options.canvas.application;
    }
    this.application = options.application;
    fabric.fire("entity:created", {target: this, options: options});
  },
  add (canvas) {
    canvas.add(this);
  },
  stepRotating() {
    let b = this.angle, a = 45 * parseInt(b / 45);
    5 > b - a ? this.setAngle(a) : 40 < b - a && this.setAngle(a + 45);
  },
  onTop () {
    return this.canvas._objects.indexOf(this) === this.canvas._objects.length - 1;
  },
  onBottom () {
    return this.canvas._objects.indexOf(this) === 0;
  },
  flop () {
    this.flipY = !this.flipY;
    this.canvas.renderAll();
  },
  flip () {
    this.flipX = !this.flipX;
    this.canvas.renderAll();
  },
  duplicate() {
    let _object = this.storeObject();
    _object.active = true;
    _object.left+=10;
    _object.top+=10;
    let _clone = /*this.cloneSync && this.cloneSync() || */this.canvas.createObject(_object);
    return _clone;
  },
  _normalizeAngle:function(){
    if(this.angle < 0){
      this.angle += 360;
    }else if(this.angle > 360){
      this.angle %= 360;
    }
  },
  maxStrokeWidth(){
    return Math.min(this.width,this.height) / 2;
  },
  moveUp(){
    this.top -= this.movementDelta;
    this.canvas.renderAll();
  },
  moveDown(){
    this.top += this.movementDelta;
    this.canvas.renderAll();
  },
  moveLeft(){
    this.left -= this.movementDelta;
    this.canvas.renderAll();
  },
  moveRight(){
    this.left += this.movementDelta;
    this.canvas.renderAll();
  },
  removeFromCanvas (){
    //todo remove this line
    if(this.canvas)
      this.canvas.remove(this)
  }
});

let ALIASING_LIMIT = 2;
/**
 * Return the dimension and the zoom level needed to create a cache canvas
 * big enough to host the object to be cached.
 * @private
 * @param {Object} dim.x width of object to be cached
 * @param {Object} dim.y height of object to be cached
 * @return {Object}.width width of canvas
 * @return {Object}.height height of canvas
 * @return {Object}.zoomX zoomX zoom value to unscale the canvas before drawing cache
 * @return {Object}.zoomY zoomY zoom value to unscale the canvas before drawing cache
 */
fabric.Object.prototype._getCacheCanvasDimensions = function() {
  let zoom = this.canvas && this.canvas.getZoom() || 1,
    objectScale = this.getObjectScaling(),
    retina = this.canvas && this.canvas._isRetinaScaling() ? fabric.devicePixelRatio : 1,
    dim = this._getNonTransformedDimensions(),
    zoomX = Math.abs(objectScale.scaleX * zoom * retina),
    zoomY = Math.abs(objectScale.scaleY * zoom * retina),
    width = Math.abs(dim.x * zoomX),
    height = Math.abs(dim.y * zoomY);
  return {
    // for sure this ALIASING_LIMIT is slightly crating problem
    // in situation in wich the cache canvas gets an upper limit
    width: width + ALIASING_LIMIT,
    height: height + ALIASING_LIMIT,
    zoomX: zoomX,
    zoomY: zoomY,
    x: dim.x,
    y: dim.y
  };
};

fabric.util.object.extend(fabric.Image.prototype, {

  _createLoader(){
    let _processing;
    if(this.canvas){
      _processing = this.canvas.processing;
      this.canvas.processing = true;
    }
    this.canvas.processing = true;
    this._loader = this.canvas.createObject(this.loader,{
      statefullCache: true,
      originX: "center",
      originY: "center",
      stored: false,
      selectable: false,
      evented: false,
      hasControls: false
    });

    this._loader.set({
      relativeLeft: 0,
      relativeTop: 0,
      relative: this
    });

    if(this.canvas) {
      this.canvas.processing = _processing;
    }
  },
  _setElementOverwritten: fabric.Image.prototype.setElement,
  /**
   * Sets source of an image
   * @param {String} src Source string (URL)
   * @param {Function} [callback] Callback is invoked when image has been loaded (and all filters have been applied)
   * @param {Object} [options] Options object
   * @return {fabric.Image} thisArg
   * @chainable
   */
  setSrc: function (src, callback) {
    this._src = src;
    this.loaded = false;
    //restore Content Position on image change
    if(this.application && this.fitting === "manual"){
      this.fitting = this.application.getDefaultProperties(this.type).fitting || this.__proto__.fitting;
    }
    if(this.loader) {
      if(this.canvas){
        this._createLoader();
      }
      else{
        this.on("added",()=> {
          if(!this.loaded){
            this._createLoader();
          }
        });
      }
    }
    if(this.thumbnailSourceRoot && !this.thumbnail){
      this.setThumbnail(src);
    }
    if(this.sourceRoot){
      this.src = fabric.util.getURL(src, this.sourceRoot);
    }else{
      this.src = src;
    }
    fabric.util.loadImage(this.src, function (img) {
      this.loaded = true;
      let _processing;
      if(this.canvas){
        _processing = this.canvas.processing;
        this.canvas.processing = true;
      }
      if(this._loader){
        // let a = this.canvas._objects.length;
        this._loader.removeFromCanvas();
        // let b = this.canvas._objects.length;
        // console.log(b - a);
      }

      this.setElement(img);
      if(this.canvas) {
        this.canvas.processing = _processing;
      }
      callback && callback(this);
    }, this, this.crossOrigin);
    return this;
  },
  store_clipPath: function () {
    if(!this.clipPath){
      return;
    }
    return this.clipPath.storeObject();
  },
  store_src: function () {
    let src;
    if (this._edited) {
      src = this._element.src;
    } else {
      src =  this._src || this.src || this._original_src || this._originalElement && this._originalElement.src || this._element && this._element.src;
    }
    if (!src) return null;

    let sourceRoot = this.sourceRoot || fabric.util.mediaRoot;
    if (src.indexOf(sourceRoot) === 0) {
      src = src.replace(sourceRoot, "");
    }
    return src;
  },
  toObject: fabric.Object.prototype.toObject,
  _setSrcOverwritten: fabric.Image.prototype.setSrc,
});










