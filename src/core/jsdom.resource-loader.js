
if(fabric.isLikelyNode){
  //quick image loader fix
  let loader = require('jsdom/lib/jsdom/browser/resources/resource-loader');
  loader.prototype._fetchURL = loader.prototype.fetch;
  loader.prototype.fetch = function(urlString, options){
    if(fabric.isLikelyNode){
      if(!/^(file|data|http|https)\:\/\//.test(urlString)){
        urlString = "file://" + require("path").resolve(urlString);
      }
    }
    return loader.prototype._fetchURL(urlString, options)
  };
}