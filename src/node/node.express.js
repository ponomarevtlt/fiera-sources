
const path        = require('path'),
  fs          = require('fs'),
  express     = require('express'),
  request     = require('request'),
  bodyParser  = require('body-parser');
//font upload
const fontkit  = require('fontkit');
// const formidable  = require('formidable');



fabric.express = {
  initialize: function(config){

    const app = express();
    let dataLimit = config.dataLimit || "10mb";

    app.all('/*', function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
    });

    app.use(bodyParser.json({limit: dataLimit, extended: true}));
    app.use(bodyParser.urlencoded({limit: dataLimit, extended: true}));

    if (config.routes) {
      for (let route in config.routes) {
        app.use(route, express.static(config.routes[route]));
      }
    }

    if(config.fontsDirectoryURL){
      app.use(config.fontsDirectoryURL,  express.static(fabric.node.directories.fonts));
    }

    if(config.mediaDirectoryURL){
      app.use(config.mediaDirectoryURL,  express.static(fabric.node.directories.media));
    }

    if(config.exportDirectoryURL) {
      app.use(config.exportDirectoryURL, express.static(fabric.node.directories.export));
    }

    if(config.uploadDirectoryURL) {
      app.use(config.uploadDirectoryURL, express.static(fabric.node.directories.upload));
    }

    config.frames && app.get(config.frames,  function frames (req, res) {
      res.status(200).json(fabric.node.frames);
    });

    config.svg && app.post(config.svg, fabric.request.svg);

    config.pdf && app.post(config.pdf,fabric.request.pdf);

    config.proxy && app.get(config.proxy , fabric.express.proxy);

    config.fontsStyles && app.get(config.fontsStyles, fabric.express.fontsStyles);

    config.fontsList && app.get(config.fontsList , fabric.express.fontsList);

    config.lang && app.get(config.lang, fabric.express.lang);

    config.galleries && app.get(config.galleries, fabric.express.galleries);

    config.create && app.post(config.create, fabric.express.create);

    config.list && app.post(config.list,fabric.express.list);

    config.data && app.get(config.data, fabric.express.data);

    config.save && app.post(config.save, fabric.express.save);

    if(config.uploadFont){
      app.post(config.uploadFont,fabric.express.uploadFont);
    }

    if(config.uploadImage){
      const multer  = require('multer');

      let storage = multer.diskStorage({
          destination: (req, file, callback)=> {
            callback(null, fabric.node.directories.upload);
          },
          filename: (req, file, callback) => {
            let filename = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
            console.log("uploading " + filename);
            callback(null, filename);
          }
        }),
        upload = multer({
          storage: storage,
          fileFilter: (req, file, callback) => {
            // console.log(file.mimetype);
            if (!file.mimetype.startsWith('image/')) {
              return callback('Only images are allowed', null);
            }
            callback(null, true);
          }
        }).single('file');

      app.post(config.uploadImage, upload, (req, res)  =>{
        res.status(200).json({
          url: config.uploadDirectoryURL + res.req.file.filename
        });
      });
    }


    let port = process.env.PORT ? process.env.PORT : config.port || 80;
    app.set('port', port);
    app.listen(port, () => {
      require("dns").lookup(require("os").hostname(), (err, hostname, fam)=> {
        console.log(`listening at http://${hostname}:${port}`);
      })
    });

    return app;
  },
  lang (req, res) {
    let lang;
    if(req.query.language){
      lang = fabric.node.lang[req.query.language];
    }else{
      lang = fabric.node.lang;
    }
    res.status(200).json(lang);
  },
  galleries (req, res) {
    let galleries = fabric.node.galleries;
    res.status(200).json(galleries);
  },
  fontsList (req, res) {
    let fonts = Object.keys(fabric.fonts.info.locals);
    res.status(200).json(fonts);
  },
  fontsStyles (req, res) {
    let styles = "/* Fiera.js Fonts */";
    if(req.query.families){
      styles = fabric.fonts.getFontsStyles(req.query.families.split("|"));
    }
    res.status(200).type("css").end(styles);
  },
  create (req, res) {
    let file = req.body.file;
    fs.writeFile(`${fabric.node.directories.data}${file}.json`, '',  (err) => {
      if (err) throw err;
      let data = JSON.parse(fs.readFileSync(`${fabric.node.directories.data}${file}.json`).toString());
      res.status(200).json(data);
    });
  },
  proxy (req, res) {
    let url = req.url.substr(7);
    request.get(url).pipe(res);
  },
  list (req, res) {
    fs.readdir(fabric.node.directories.data, (err, files) => {
      files = files.map(file => {
        return file.replace(/.json$/,"")
      });
      res.status(200).json(files);
    });
  },
  data (req, res)  {
    let file = req.query.file;
    let fName = path.resolve(`${fabric.node.directories.data}${file}.json`);
    let data;
    try{
      if(fs.existsSync(fName)){
        data = fabric.util.formatEditorData(fabric.util.data.load(fName));
      }else{
        data = {};
      }
      res.status(200).json(data);
    }catch(e){
      console.trace(e.message);
      res.status(400).json( {message: e.message, stack: e.stack});
    }
  },
  save (req, res) {
    let file = req.body.file;
    let fName = `${fabric.node.directories.data}${file}.json`;
    let data = req.body.data || "{}";
    let beautyData = fabric.util.data.beautyStringify(JSON.parse(data), true);
    fs.writeFile(fName, beautyData, function (err) {
      if (err) throw err;
      res.status(200).json({message: "saved"});
    });
  },
  uploadFont (req, res){

    let fontMimeTypes = {
      "application/octet-stream":     "ttf",
      "application/x-font-ttf":       "ttf",
      "application/x-font-truetype":  "ttf",
      "application/x-font-opentype":  "otf",
      "application/font-woff":        "woff",
      "application/font-woff2":       "woff2"
    };

    let form = new formidable.IncomingForm();
    //Formidable uploads to operating systems tmp dir by default
    form.uploadDir = "./media/uploads/";       //set upload directory
    form.keepExtensions = true;     //keep file extension

    form.parse(req, function(err, fields, files) {
      let file = files.file;

      if(file.size > 1000000){
        throw new Error("File is too big");
      }
      if (!file.type in Object.keys(fontMimeTypes)) {
        return callback(new Error(`type '${file.type}' is unsupported.`));
      }
      let font = fontkit.openSync(file.path);
      let filename =   font.familyName;// + "." + fontMimeTypes[file.type];

      fs.rename(file.path, './media/uploads/' + filename, function(err) {
        if (err) throw err;

        res.status(200).json({
          name: font.familyName,
          key: filename
        });
      });
    });
  }
};