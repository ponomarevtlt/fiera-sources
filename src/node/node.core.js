
const path        = require('path');

fabric.node = {

  /**
   * create NodeJS FabricJS Editor
   * @param {*} data
   */
  createEditor: function(data) {
    console.time("total time");
    return new fabric.Editor({
      frames: this.frames,
      prototypes: data.prototypes,
      slide: data.slide,
      slides: data.slides
    }).then(e => {
      console.timeEnd("total time");
      return e;
    });
  },

  getData: function(data){
    if (data.constructor === String) {
      data = fabric.util.data.load(path.resolve(data), 'json');
    }
    return fabric.util.formatEditorData(data);
  },

  initialize: function (config) {
    this.directories = config.directories;

    if (this.directories.home) {
      process.chdir(path.resolve(this.directories.home));
    }

    fabric.DPI = config.dpi;
    fabric.util.mediaRoot = this.directories.media;
    fabric.util.mediaRootUrl = this.directories.mediaURL;
    fabric.util.exportDirectory = this.directories.export;
    fabric.util.exportDirectoryURL = this.directories.exportURL;
    fabric.fonts.fontsSourceRoot = this.directories.fonts;
    fabric.fonts.fontsSourceRootURL = this.directories.fontsURL;

    if (config.debug) {
      for (let option of config.debug) {
        switch (option) {
          case "pdf":
            fabric.pdf.debug = true;
            break;
          case "node-canvas":
            fabric.util.nodeCanvasDebug = true;
            break;
          case "time":
            fabric.util.timeDebug = true;
            break;
          case "loader":
            fabric.util.loaderDebug = true;
            break;
        }
      }
    }
    this.onInitialize(config);
  },
  onInitialize: function(config){
    fabric.fonts.loadLocalFonts(config.fontsList, config.fontsFull);
    if(config.frames){
      this.frames = fabric.util.data.load(config.frames)
    }
    if(config.lang){
      this.lang = fabric.util.data.load(config.lang)
    }
    if(config.galleries){
      this.galleries = fabric.util.data.load(config.galleries)
    }
  },
  processArguments: function (args) {
    if (!args) args = process.argv.slice(2);
    const argv = require('minimist')(args);
    if (argv._.length > 0) {
      let argData = argv._[0] === "{" ? JSON.parse(argv._[0]) : argv._[0];

      let output = argv._.splice(1);

      let data = fabric.node.getData(argData);

      return fabric.node.createEditor(data).then(editor => {
        let promises = [];
        let result = {};
        for (let file of output) {
          let extension = /\.(\w+$)/.exec(file)[1];
          switch (extension) {
            case "pdf":
              promises.push(fabric.save.pdf(editor, file).then(pdf => result.pdf = pdf));
              break;
            case "svg":
              promises.push(fabric.save.svg(editor, file).then(svg => result.svg = svg));
              break;
            case "png":
              promises.push(fabric.save.png(editor, file).then(png => result.png = png));
              break;
          }
        }
        return Promise.all(promises).then(() => {
          console.log(result);
        });
      });
    }
  }
}