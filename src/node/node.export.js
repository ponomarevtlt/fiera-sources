const fs =    require('fs'),
      path =  require('path');



fabric.request = {
  pdf: function (req, res) {
    try {
      let data = fabric.node.getData(req.body);
      fabric.node.createEditor(data).then(e => {
        if (req.body.file) {

          fabric.save.pdf(e.editor, req.body.file).then(pdf => {
            res.status(200).json({url: pdf});
          });
        }else{
        fabric.response.pdf(res, e.editor)
        }
      });
    }
    catch (e) {
      console.trace(e.message);
      res.status(400).json({message: e.message, stack: e.stack});
    }
  },
  svg: function (req, res) {
    try {
      let data = fabric.node.getData(req.body);
      fabric.node.createEditor(data).then(e => {
        if (req.body.file) {
          fabric.save.svg(e.editor, req.body.file).then(svg => {
            res.status(200).json({url: svg});
          })
        }
        else{
          fabric.response.svg(res, e.editor);
        }
      });
    }
    catch (e) {
      console.trace(e.message);
      res.status(400).json({message: e.message, stack: e.stack});
    }
  }
};

fabric.save = {
  svg: function saveSVG(editor,filesPattern){
    return new Promise(resolve => {
      fabric.util.svgMediaRoot = "./public/";
      let svg = [];
      editor.slides.forEach((slide,index)  => {
        let svgData = slide.toSVG();
        let filename = (filesPattern.indexOf("*") !== -1 ? filesPattern.replace("*",index) : filesPattern + index);
        svg.push((fabric.util.exportDirectoryURL || fabric.util.exportDirectory) + filename);
        let svgStream = fs.createWriteStream(path.resolve(fabric.util.exportDirectory +filename));
        svgStream.write(svgData);
        svgStream.end();
        console.log("==SVG SAVED== " + path.resolve(fabric.util.exportDirectory +filename));
      });
      fabric.util.svgMediaRoot = "";

      resolve(svg);
    })
  },
  png: function savePNG(editor,filesPattern){
    return new Promise(resolve => {
      let png = [];

      editor.slides.forEach((slide,index) => {
        if(filesPattern){
          let filename = (filesPattern.indexOf("*") !== -1 ? filesPattern.replace("*",index) : filesPattern + index);
          png.push(filename);
          slide.saveAsPNG(filename);
          console.log("==PNG SAVED== " + path.resolve(filename));
        }else{
          png.push(slide.canvas.toDataURL());
        }
      });

      resolve(png);
    })
  },
  pdf: function savePDF(editor,file){
    return new Promise(resolve => {
      let document = editor.makeDocument();
      document.toFile(path.resolve(fabric.util.exportDirectory + file),()=> {
        resolve((fabric.util.exportDirectoryURL || fabric.util.exportDirectory) + file);
      })
    })
  }
};

fabric.response = {
  pdf:  function sendAsPDF(response,editor){
    let document = editor.makeDocument();
    response.status(200)
    response.header('Content-type', 'application/pdf');
    response.header('Content-disposition', 'attachment; filename=Untitled.pdf');
    document.pipe(response);
  },
  svg:  function sendAsSVG(response,editor,pageIndex = 0){
    fabric.util.svgMediaRoot = "./public/";
    let svgData = editor.toSVG();
    fabric.util.svgMediaRoot = "";

    response.status(200).type("svg").end(svgData);
  },
  png:  function sendAsPNG(response,editor,pageIndex){
    let stream = editor.slides[pageIndex].createPNGStream();
    response.type("png");
    stream.pipe(response);
  },
  jpg:  function sendAsJPG(response,editor,pageIndex){
    let stream = editor.slides[pageIndex].createJPEGStream();
    response.type("jpg");
    stream.pipe(response);
  },
  gif:  function sendAsGIF(response,editor) {
    let canvas = editor.slides[0];//todo
    let GIFEncoder = require('gifencoder');
    let encoder = new GIFEncoder(canvas.width * 32, canvas.height * 32);
    let stream = encoder.createReadStream();
    response.type("gif");
    stream.pipe(response);
    encoder.start();
    encoder.setRepeat(0);   // 0 for repeat, -1 for no-repeat
    encoder.setDelay(150);  // frame delay in ms
    encoder.setQuality(15); // image quality. 10 is default.

    let context = canvas.getContext("2d");
    // Add 3 frames
    encoder.addFrame(context);
    encoder.addFrame(context);
    encoder.addFrame(context);
    encoder.finish();
  },
  webp: function sendAsWEBP(response, editor) {
    let Whammy = require('node-whammy');
    let encoder = new Whammy.Video(7);
    let currentId = 0,
      time = 0,
      timeout = 20000,
      delay = 20,
      addedFrame = -1,
      totalFrames = 3,
      tmpFrames = Array.apply(null, Array(totalFrames));

    let addFrame = function addFrame(context) {
      let id = currentId++;
      canvasToWebp(context.canvas, function(webmData) {
        tmpFrames[id] = webmData;
        for(let i = addedFrame + 1; i < totalFrames; ++i) {
          if(tmpFrames[i] !== undefined) {
            encoder.add(tmpFrames[i]);
            addedFrame = i;
          }
          else {
            break;
          }
        }
      });
    };

    let checkReady = function checkReady() {
      if(totalFrames <= addedFrame + 1) {
        try {
          let output = encoder.compile(true);
          response.type('webm');
          response.send(new Buffer(output));
          console.log('Webm compilation: ' + time + 'ms');
        }
        catch(err) {
          response.send(err.toString());
        }
      }
      else if((time += delay) < timeout) {
        setTimeout(checkReady, delay);
      }
      else {
        response.send('Timeout of ' + timeout + 'ms exceed');
      }
    };


    let canvas = editor.slides[0];//todo
    let context = canvas.getContext("2d");

    // Add 3 frames
    addFrame(context);
    addFrame(context);
    addFrame(context);

    setTimeout(checkReady, delay);

  }
};

function downloadDocumentWorker(data, options,  res){
  let worker = require('child_process').fork("server/worker.js", {execArgv: [],  stdio: ['pipe','pipe','pipe', 'ipc']});
  // var options = { stdio: ['pipe','pipe','pipe','pipe'] };  // first three are stdin/out/err
  // var pipe = proc.stdio[3];
  // pipe.write(Buffer('hello'));
  ///The child can open the pipe like this:
  // var pipe = new net.Socket({ fd: 3 });
  // pipe.on('data', function(buf) {
  //   // do whatever
  // });
  worker.stdout.on('data', (data) => {process.stdout.write(`⚙${worker.pid}: ${data.toString()}`);});
  worker.stderr.on('data', (data) => {process.stdout.write(`⚙${worker.pid}: ${data.toString()}`);});
  worker.on('message', message => {
    pdfResponse(message);
  });
  worker.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
  });
  worker.send({
    data: data,
    options: options
  });
}

/**
 * обработка данных от родительского процесса
 */
function checkParentProcess(){
  let messageFromParent = null;
  if (process.send) {
    process.on('message', options => {
      messageFromParent = options;
      let data = fabric.node.getData(options.data);
      fabric.node.createEditor(data)
    });
    setTimeout(function(){
      if(!messageFromParent){
        console.log("no message from parent");
        process.exit();
      }
    },5000)
  }
}





/*canvas to MP4
https://www.npmjs.com/package/ffmpeg
https://scwu.io/rendering-canvas-to-mp4-using-nodejs/
var recorder = child_process.spawn("ffmpeg.exe", [
  "-y", "-f", "image2pipe",
  "-vcodec", "png", "-r", "60",
  "-i", "-", "-vcodec", "h264",
  "-r", "60", "output.mp4"
]);
Next, for each frame grab the canvas data and convert to a binary string.

  var url = canvas.toDataURL();
var data = atob( url.substring(url.indexOf("base64") + 7) );
Finally, we can write the data to stdin.

recorder.stdin.write(data, "binary");
When finished, we can close the stream.

recorder.stdin.end();


*/

