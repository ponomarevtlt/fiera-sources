'use strict';


/**
 * PencilBrush class
 * @class fabric.RectangleBrush
 * @extends fabric.BaseBrush
 */
fabric.RectangleBrush = fabric.util.createClass(fabric.BaseBrush, /** @lends fabric.RectangleBrush.prototype */ {
  resultObjectType: "Rect",
  type: "rectangle-brush",
  /**
   * Constructor
   * @param {fabric.Canvas} canvas
   * @return {fabric.RectangleBrush} Instance of a pencil brush
   */
  initialize: function (canvas) {
    this.callSuper('initialize', canvas);
  },

  /**
   * Inovoked on mouse down
   * @param {Object} pointer
   */
  onMouseDown: function (pointer) {
    //prepare for drawing
    delete this.p2;

    this.p1 = new fabric.Point(pointer.x, pointer.y);
    this._setBrushStyles();
    this._setShadow();
    this.canvas.contextTop.moveTo(this.p1.x, this.p1.y);
    this._render();
  },

  /**
   * Inovoked on mouse move
   * @param {Object} pointer
   */
  onMouseMove: function (pointer) {

    this.p2 = new fabric.Point(pointer.x, pointer.y);

    if(this.drawingObject){
      var _rect = this._getRect();
      this.drawingObject.set(_rect);
      this.drawingObject.fire("scaling");
    }else{

      if (this.p1.x === this.p2.x && this.p1.y === this.p2.y) {
        delete this.p2;
      } else {

        if(this.resultObjectType){
          var _rect = this._getRect();
          this.drawingObject = this._createRectangle(_rect);
        }
      }
    }
    this.canvas.renderLayer("lower");
  },

  /**
   * Invoked on mouse up
   */
  onMouseUp: function () {
    if(this.drawingObject){
      var _rect = this._getRect();
      var _rect2 = this._checkRectangle(_rect);

      this.canvas.layers.lower.objects.splice(this.canvas.layers.lower.objects.indexOf(this.drawingObject), 1);

      this.drawingObject.set(_rect2);
      this.canvas.add(this.drawingObject);
      this.canvas.setActiveObject(this.drawingObject);
      this.drawingObject.setCoords();
      delete this.drawingObject;
    }
    this.canvas.renderAll();
    // this._finalizeAndAddPath();
  },

  /**
   * Draw a smooth path on the topCanvas using quadraticCurveTo
   * @private
   */
  _render: function () {
    this.canvas.fire("before:brush:render",{target: this})
    if (!this.p2) {
      return;
    }
    var ctx = this.canvas.contextTop,
      v = this.canvas.viewportTransform,
      p1 = this.p1,
      p2 = this.p2;

    ctx.save();
    ctx.transform(v[0], v[1], v[2], v[3], v[4], v[5]);
    ctx.beginPath();
    if(this.application){
      var _klass_proto = this.application.getDefaultProperties(fabric[this.resultObjectType].prototype) || {};
      ctx.lineWidth = _klass_proto.strokeWidth;
      ctx.strokeStyle = _klass_proto.stroke;
      ctx.fillStyle = _klass_proto.fill;
    }

    var _rect = this._getRect();
    ctx.rect(_rect.left, _rect.top, _rect.width, _rect.height);

    ctx.fill();
    ctx.stroke();
    ctx.restore();
    this.canvas.fire("brush:render",{target: this})
  },
  _getRect: function () {

    var x1, x2, y1, y2;
    if (this.p1.x < this.p2.x) {
      x1 = this.p1.x, x2 = this.p2.x;
    } else {
      x2 = this.p1.x, x1 = this.p2.x;
    }
    if (this.p1.y < this.p2.y) {
      y1 = this.p1.y, y2 = this.p2.y;
    } else {
      y2 = this.p1.y, y1 = this.p2.y;
    }
    return {
      left: x1,
      top: y1,
      width: x2 - x1,
      height: y2 - y1
    };
  },
  drawingLimits: null, /*{
   left: 0,
   width: 0,
   height: 0,
   top: 0
   },*/
  minHeight: 50,
  minWidth: 50,
  _checkRectangle: function(_rect){


    var dl = this.drawingLimits;
    if(dl && dl == "backgroundImage"){
      dl= this.canvas.backgroundImage;
    }

    _rect = {
      left: _rect.left,
      top: _rect.top,
      width: _rect.width,
      height: _rect.height,
    };

    // _rect.left  -= fabric.Rect.prototype.strokeWidth / 2;
    // _rect.top   -= fabric.Rect.prototype.strokeWidth / 2;

    if(dl){
      if(_rect.left + _rect.width < dl.left ||
        _rect.top  + _rect.height < dl.top ||
        _rect.left > dl.width ||
        _rect.top > dl.height){
        return false;
      }

      if(_rect.top < dl.top){
        _rect.height +=_rect.top ;
        _rect.top = dl.top;
      }
      if(_rect.left < dl.left){
        _rect.width += _rect.left;
        _rect.left = dl.left;
      }
    }

    if(this.minWidth && _rect.width < this.minWidth){
      _rect.width = this.minWidth;
    }
    if(this.minHeight && _rect.height < this.minHeight){
      _rect.height = this.minHeight;
    }


    if(dl) {
      var _xdiff = _rect.left + _rect.width - dl.width;
      if (_xdiff > 0) {
        _rect.width -= _xdiff;
        if (this.minWidth && _rect.width < this.minWidth) {
          _rect.left -= this.minWidth - _rect.width;
          _rect.width = this.minWidth;
        }
      }
      var _ydiff = _rect.top + _rect.height - dl.height;
      if (_ydiff > 0) {
        _rect.height -= _ydiff;
        if (this.minHeight && _rect.height < this.minHeight) {
          _rect.top -= this.minHeight - _rect.height;
          _rect.height = this.minHeight;
        }
      }
    }
    return _rect;
  },
  _createRectangle: function(rect){
    this.canvas.setActiveObject(false);
    rect.active = true;
    rect.application = this.application;
    rect.canvas = this.canvas;
    var _object = fabric.util.createObject(this.resultObjectType,rect);
    this.canvas.layers.lower.objects.push(_object);
    _object.updateResponsiveBorder();
    this.canvas.renderAll();
    return _object;
  }

  /**
   * On mouseup after drawing the path on contextTop canvas
   * we use the points captured to create an new fabric rect object
   * and add it to the fabric canvas.
   */
  // _finalizeAndAddPath: function () {
  //
  //   if (!this.p2) {
  //     return false;
  //   }
  //   var ctx = this.canvas.contextTop;
  //   ctx.closePath();
  //
  //
  //   this.canvas.clearContext(this.canvas.contextTop);
  //   this._resetShadow();
  //
  //   // var _rect = this._getRect();
  //   var _rect2 = this._checkRectangle(_rect);
  //   if(_rect2){
  //     // if(this.resultObjectType){
  //     //   this._createRectangle(_rect2);
  //     // }
  //     this.canvas.fire('rect:created', {rect: _rect2, original: _rect});
  //   }else{
  //     this.canvas.fire('rect:rejected', {rect: _rect});
  //   }
  //   this.canvas.renderLayer(this.layers.lower);
  // }
});


if(!fabric.isLikelyNode) {
    fabric.Canvas.prototype.drawingTools.RectangleBrush = {
        className: 'fa fa-pencil-square',
        title: 'select-brush'
    };
    fabric.Canvas.prototype.activeDrawingTools.push("RectangleBrush");
}
