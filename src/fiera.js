
if(typeof DEVELOPMENT === "undefined" || DEVELOPMENT){
  require('./plugins/debugObjects');
}

global.fabric = require('./fabric/HEADER.js').fabric;
const NODE = fabric.isLikelyNode, BROWSER = !NODE;

//Original FabricJS modules
if(BROWSER) {
  require('./plugins/event.js');
  // require('./plugins/default-passive-events.js');
}

require('./fabric/mixins/observable.mixin.js');
require('./fabric/mixins/collection.mixin.js');
require('./fabric/mixins/shared_methods.mixin.js');
require('./fabric/util/misc.js');
require('./fabric/util/arc.js');
require('./fabric/util/lang_array.js');
require('./fabric/util/lang_object.js');
require('./fabric/util/lang_string.js');
require('./fabric/util/lang_class.js');
require('./fabric/util/dom_event.js');
require('./fabric/util/dom_style.js');
require('./fabric/util/dom_misc.js');
require('./fabric/util/dom_request.js');
require('./fabric/log.js');

require('./fabric/util/named_accessors.mixin.js');

require('./fabric/parser.js');
require('./fabric/elements_parser.js');

require('./fabric/point.class.js');
require('./fabric/intersection.class.js');
require('./fabric/color.class.js');

require('./fabric/gradient.class.js');
require('./fabric/pattern.class.js');
require('./fabric/shadow.class.js');

require('./fabric/static_canvas.class.js');

if(NODE) {
  fabric.StaticCanvas.supports = function(){return true;}
}
/**
 * freedrawing
 */
// require('./fabric/brushes/base_brush.class.js');
// require('./fabric/brushes/pencil_brush.class.js');
// require('./fabric/brushes/circle_brush.class.js');
// require('./fabric/brushes/spray_brush.class.js');
// require('./fabric/brushes/pattern_brush.class.js');


/**
 * core logic
 */
require('./fabric/canvas.class.js');
require('./fabric/mixins/canvas_events.mixin.js');
require('./fabric/mixins/canvas_grouping.mixin.js');
require('./fabric/mixins/canvas_dataurl_exporter.mixin.js');
require('./fabric/mixins/canvas_serialization.mixin.js');
require('./fabric/shapes/object.class.js');

//remove clippath from stateProperties
fabric.Object.prototype.stateProperties = (
  'top left width height scaleX scaleY flipX flipY originX originY transformMatrix ' +
  'stroke strokeWidth strokeDashArray strokeLineCap strokeDashOffset strokeLineJoin strokeMiterLimit ' +
  'angle opacity fill globalCompositeOperation shadow visible backgroundColor ' +
  'skewX skewY fillRule paintFirst strokeUniform'
).split(' ');

require('./fabric/mixins/object_origin.mixin.js');
require('./fabric/mixins/object_geometry.mixin.js');
require('./fabric/mixins/object_stacking.mixin.js');
require('./fabric/mixins/object.svg_export.js');
require('./fabric/mixins/stateful.mixin.js');

/**
 *  interaction and gestures
 */
require('./fabric/mixins/object_interactivity.mixin.js');
require('./fabric/mixins/canvas_gestures.mixin.js');

/**
 *  animation
 */
require('./fabric/util/animate.js');
require('./fabric/util/animate_color.js');
require('./fabric/util/anim_ease.js');
require('./fabric/mixins/animation.mixin.js');

/**
 *  basic classes
 */
require('./fabric/shapes/line.class.js');
require('./fabric/shapes/circle.class.js');
require('./fabric/shapes/triangle.class.js');
require('./fabric/shapes/ellipse.class.js');
require('./fabric/shapes/rect.class.js');
require('./fabric/shapes/polyline.class.js');
require('./fabric/shapes/polygon.class.js');
require('./fabric/shapes/path.class.js');
require('./fabric/shapes/group.class.js');
require('./fabric/shapes/image.class.js');
require('./fabric/shapes/active_selection.class.js');

require('./fabric/mixins/object_straightening.mixin.js');

/**
 *  image_filters
 */
require('./fabric/filters/webgl_backend.class.js');
require('./fabric/filters/2d_backend.class.js');
require('./fabric/filters/base_filter.class.js');
require('./fabric/filters/colormatrix_filter.class.js');
require('./fabric/filters/brightness_filter.class.js');
require('./fabric/filters/convolute_filter.class.js');
require('./fabric/filters/grayscale_filter.class.js');
require('./fabric/filters/invert_filter.class.js');
require('./fabric/filters/noise_filter.class.js');
require('./fabric/filters/pixelate_filter.class.js');
require('./fabric/filters/removecolor_filter.class.js');
require('./fabric/filters/filter_generator.js');
require('./fabric/filters/blendcolor_filter.class.js');
require('./fabric/filters/blendimage_filter.class.js');
require('./fabric/filters/resize_filter.class.js');
require('./fabric/filters/contrast_filter.class.js');
require('./fabric/filters/saturate_filter.class.js');
require('./fabric/filters/blur_filter.class.js');
require('./fabric/filters/gamma_filter.class.js');
require('./fabric/filters/composed_filter.class.js');
require('./fabric/filters/hue_rotation.class.js');

/**
 *  Text
 */
require('./fabric/shapes/text.class.js');
require('./fabric/mixins/text_style.mixin.js');
require('./fabric/shapes/itext.class.js');
require('./fabric/mixins/itext_behavior.mixin.js');
require('./fabric/mixins/itext_click_behavior.mixin.js');
require('./fabric/mixins/itext_key_behavior.mixin.js');
require('./fabric/mixins/itext.svg_export.js');
require('./fabric/shapes/textbox.class.js');
// require('./fabric/mixins/textbox_behavior.mixin.js');


/**************************************************************************************************************
 *
 *
 * CUSTOM MODULES
 *
 *
 *************************************************************************************************************/
fabric.util.object.extend(fabric.util,require('./util/util'));
fabric.util.object.extend(fabric.util,require('./util/size'));
require('./util/object');
fabric.util.a = require('./util/array');
fabric.util.syntax = require('./util/syntax');

fabric.util.data = require('./util/data');
fabric.util.compile = require('./util/compile');
// fabric.util.loader = require('./util/loader');
fabric.util.syntax = require("./util/syntax");
fabric.util.promise = require('./util/promise');
fabric.util.string = require('./util/string');
fabric.util.wand = require('./plugins/magicwand');

require('./core/jsdom.resource-loader.js');

require('./core/base');
require('./core/object.sync');
require('./core/scanvas.objects');
require('./core/scanvas');

require('./core/object.ext');
require('./core/fabric.states');
require('./core/editor');
require('./core/fabric.util');
require('./core/static-canvas.ext');
require('./canvas/layers');
require('./core/canvas.ext');
require('./core/text.ext');
require('./core/group.ext');
require('./core/slides');
require('./core/prototypes');
require('./core/image.ext');
require('./core/translations');

if(BROWSER) {
  require('./modules/actions');
}

// require('./modules/groups');
require('./modules/relative');
require('./modules/save');
require('./modules/fromURL');
require('./modules/object.order');
require('./modules/library');
require('./modules/facebook');

require('./modules/plugins/zoom');
require("./modules/responsiveborders.js");
require('./modules/export');

require('./canvas/thumb');
require('./canvas/background');
require('./canvas/svg');
require('./canvas/events');
require('./canvas/outer-canvas');

require('./modules/alignment');
require('./modules/shapes');
require("./modules/frames");
require('./modules/placeholders');
require('./modules/editor.debug');
// require('./modules/shirt');
// require('./modules/slide.areas');
// require('./modules/slide.drawing-tools');

require('./images/image.filters');
require('./images/sprite.class');
// require('./images/image.photoshop-tools');

require('./text/textbox.textVerticalAlign');
require('./text/textbox.maxLines');

require('./shapes/animation.class');
require('./shapes/barcode');
require('./shapes/pathGroup');
require('./shapes/path');
require('./shapes/Frame');
require('./shapes/ImageBorder');

// require('./filters/image.remove-white');

if(!fabric.isLikelyNode){
  require('./shapes/Frame.actions');
}

require('./pdf/pdf.common');

require('./fonts/fonts');

// require('./brushes/BaseBrush');
// require('./brushes/PencilBrush');
// require('./brushes/RectangleBrush');
// require('./brushes/PolygonBrush');
// require('./brushes/PaintPenBrush');
// require('./brushes/PaintBucketBrush');
// require('./brushes/PointsBrush');

// require('./shapes/polyline');
// require('./shapes/bezierText');
// require('./shapes/textbox.list');
// require('./shapes/textFrame');

if(BROWSER){
  // require('./../css/fiera.toolbar.less');
  require('./modules/target');
  require('./modules/upload');
  require('./canvas/snap');
  require('./canvas/droppable');
  require('./canvas/resizable');
  require('./canvas/paste');
  require('./canvas/objectsOrder');
  require('./modules/gallery');
  require('./modules/grid');
  require('./modules/movementLimits');
  require('./modules/undo');
  require('./modules/fabric.tabbable');
  require('./text/staticBorderColor');
  require('./canvas/interactive-mode');
  require('./canvas/stretchable');

  require('./modules/instagram');
  require('./modules/loader');
  require('./modules/toolbars');
  require('./modules/ruler');
}

require("./../dist/info.js");

//fabric.media.error = require('./media/error-button.svg')
//fabric.media.error = 'data:image/svg+xml;base64,' + require('base64-loader!./media/error-button.svg')
fabric.media.error = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==";

//global variable
exports.fabric = fabric;
