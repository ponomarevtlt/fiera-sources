{

  /*
   fabric.Image.filters.Redify = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
   type: 'Redify',
   applyTo: function (canvasEl) {
   let context = canvasEl.getContext('2d'),
   imageData = context.getImageData(0, 0,
   canvasEl.width, canvasEl.height),
   data = imageData.data;
   for (let i = 0, len = data.length; i < len; i += 4) {
   data[i + 1] = 0;
   data[i + 2] = 0;
   }
   context.putImageData(imageData, 0, 0);
   }
   });
   fabric.Image.filters.Redify.fromObject = function (object) {
   return new fabric.Image.filters.Redify(object);
   };
   */

  fabric.Image.filters.Sharpen = fabric.util.createClass(fabric.Image.filters.Convolute, {
    type: 'Sharpen',
    matrix: [
        0, -1, 0,
        -1, 5, -1,
        0, -1, 0
    ]
  });


  fabric.Image.filters.Emboss = fabric.util.createClass(fabric.Image.filters.Convolute, {
    type: 'Emboss',
    matrix: [
        1,   1,   1,
        1, 0.7,  -1,
        -1,  -1,  -1
    ]
  });


  if(fabric.Image.filters.Mask){
    fabric.Image.filters.Mask.prototype.maskFilter = true;
  }

  let prototypeOptions = {
    Brightness: {
      "brightness": {value: 100, min: 0, max: 255}
    },
    Noise: {
      "noise": {value: 100, min: 0, max: 1000}
    },
    Convolute: {
      "opaque": {value: true, type: "boolean" },
      "matrix": {value: [1, 1, 1, 1, 1, 1, 1, 1, 1], type: "matrix" }
    },
    Blur: {},
    Sharpen: {},
    Emboss: {},
    Pixelate: {
      "blocksize": {value: 4, min: 2, max: 20}
    },
   /* Multiply: {
      "color": {type: 'color', value: "#F0F"}
    },
    Mask: {
      mask: {
        type: 'image',
        value: {
          src:  "photos/explosion.png"
        }
      },
      channel: { value: 0}
    },
    Tint: {
    "color":  {type: 'color', value: "#3513B0"},
    "opacity": {value: 1, min: 0, max: 1, step: 0.1}
    },
    Blend: {
    "color": {type: 'color', value: "#3513B0"},
    "mode": {
    value: "add",
    options: [
    {value: "add", title: "Add"},
    {value: "diff", title: "Diff"},
    {value: "subtract", title: "Subtract"},
    {value: "multiply", title: "Multiply"},
    {value: "screen", title: "Screen"},
    {value: "lighten", title: "Lighten"},
    {value: "darken", title: "Darken"}
    ]
    }
    }
    */
  };
  for(let i in prototypeOptions){
    fabric.Image.filters[i].prototype.options = prototypeOptions[i];
  }

}



fabric.util.object.extend(fabric.Editor.prototype, {

  getFiltersList: function(el) {

    el = el || fabric.Image.prototype;
    let filterList = [];
    for (let i in el.availableFilters) {
      let _f = fabric.Image.filters[el.availableFilters[i]];

      let _data = {
        type: el.availableFilters[i]
      };
      if (_f.prototype.custom) {
        if (!el.customFilters) {
          continue;
        }
      }
      if (_f.prototype.maskFilter) {
        if (!el.maskFilter) {
          continue;
        }
      }
      if (_f.prototype.caman) {
        if (!el.camanFilters) {
          continue;
        }
        _data.caman = true;
      } else {
        if (!el.fabricFilters) {
          continue;
        }
      }
      if (_f.prototype.options) {
        _data.options = fabric._.clone(_f.prototype.options);
      }
      _data.text = _f.prototype.title || el.availableFilters[i];

      filterList.push(_data)
    }
    return filterList;
  }
});

fabric._.extend(fabric.Image.prototype, {
  camanFilters: false,
  fabricFilters: true,
  customFilters: false,
  maskFilter: false,
  availableFilters: [
    //fabricJS
    "BlackWhite",
    "BlendColor",
    "BlendImage",
    "Blur",
    "Brightness",
    "Brownie",
    "ColorMatrix",
    "Composed",
    "Contrast",
    // "Convolute",
    // "Emboss",
    "Gamma",
    "Grayscale",
    "HueRotation",
    "Invert",
    "Kodachrome",
    "Noise",
    "Pixelate",
    "Polaroid",
    "RemoveColor",
    "Saturation",
    "Sepia",
    // "Sharpen",
    "Technicolor",
    "Vintage"
  ],
  getFilter: function (filterName) {
    filterName = fabric.util.string.uncapitalize(filterName);
    for(let i in this.filters){
      if(fabric.util.string.uncapitalize(this.filters[i].type) === filterName){
        return this.filters[i];
      }
    }
    return false;
  },
  setFilters: function (filters) {

    this.saveState(["filters"]);

    this.filters = [];
    if(filters){
      for(let filterOptions of filters){

        //todo createObject
        let filterName = fabric.util.string.capitalize(fabric.util.string.camelize(filterOptions.type),true);

        let filter = new fabric.Image.filters[filterName](filterOptions);
        filter.image = this;
        this.filters.push(filter);
      }
    }
    if(this._originalElement){
      this.applyFilters();
    }
    if(this.canvas){
      this.canvas.fire('object:modified', {target: this});
      this.canvas.renderAll();
    }
    this.fire('modified', {} );
  },
  setFilter: function (filter) {
    this.saveState(["filters"]);

    let _old_filter = false;
    if(filter.replace){
      this.filters = [];
    }else{
      _old_filter = fabric._.findWhere(this.filters, {type: filter.type});
      _old_filter = _old_filter && _old_filter.toObject() || false;
    }

    let _type;
    let _new_filter;

    if(filter.type){
      _type = fabric.util.string.capitalize(filter.type,true);
      _new_filter = filter.options && fabric._.clone(filter.options);
    }else{
      _type = false;
      _new_filter = false;
    }

    /* this.project.history.add({
     data:   [$.extend(true, {}, this.data)],
     slide:  this.slide,
     object: this,
     redo:   filter,
     undo:   _old_filter ,
     type:   "filter",
     undoFn: function(action){
     action.object._set_filter(action.undo);
     },
     redoFn:  function(action){
     action.object._set_filter(action.redo);
     }
     });
     */
    this._set_filter(_type, _new_filter, _old_filter);

    if(this.canvas){
      this.canvas.renderAll();
      this.canvas.fire('object:modified', {target: this});
    }
    this.fire('modified', {} );
  },
  _set_filter: function (_type, _new_filter) {
    let _old_filter;

    if(_type){
      _old_filter = this.getFilter(_type);
    }

    if (_old_filter && _new_filter) {
      for (let i in _new_filter) {
        _old_filter[i] = _new_filter[i];
      }
    } else if (_old_filter && !_new_filter) {
      this.filters.splice(this.filters.indexOf(_old_filter), 1);
    }
    if (!_old_filter && _new_filter) {
      var filter = new fabric.Image.filters[_type](_new_filter);
      filter.image = this;
      this.filters.push(filter);
    }
    if(this._originalElement){
      this.applyFilters();
    }
  },



  /**
   * @override
   * Applies filters assigned to this image (from "filters" array)
   * @method applyFilters
   * @param {Function} callback Callback is invoked when all filters have been applied and new image is generated
   * @param {Array} filters to be applied
   * @param {fabric.Image} imgElement image to filter ( default to this._element )
   * @param {Boolean} forResizing
   * @return {CanvasElement} canvasEl to be drawn immediately
   * @chainable
   */
 /* applyFilters: function(callback, filters, imgElement, forResizing) {

    filters = filters || this.filters;
    imgElement = imgElement || this._originalElement;

    if (!imgElement) {
      return;
    }

    let replacement = fabric.util.createImage(),
      retinaScaling = this.canvas ? this.canvas.getRetinaScaling() : fabric.devicePixelRatio,
      minimumScale = this.minimumScaleTrigger / retinaScaling,
      _this = this, scaleX, scaleY;

    if (filters.length === 0) {
      this._element = imgElement;
      callback && callback(this);
      return imgElement;
    }

    let canvasEl = fabric.util.createCanvasElement();
    canvasEl.width = imgElement.naturalWidth || imgElement.width;  //overridden
    canvasEl.height = imgElement.naturalHeight || imgElement.height; //overridden
    canvasEl.getContext('2d').drawImage(imgElement, 0, 0, canvasEl.width, canvasEl.height);

    filters.forEach(function(filter) {
      if (!filter) {
        return;
      }
      if (forResizing) {
        scaleX = _this.scaleX < minimumScale ? _this.scaleX : 1;
        scaleY = _this.scaleY < minimumScale ? _this.scaleY : 1;
        if (scaleX * retinaScaling < 1) {
          scaleX *= retinaScaling;
        }
        if (scaleY * retinaScaling < 1) {
          scaleY *= retinaScaling;
        }
      }
      else {
        scaleX = filter.scaleX;
        scaleY = filter.scaleY;
      }
      filter.applyTo(canvasEl, scaleX, scaleY);
      if (!forResizing && filter.type === 'Resize') {
        _this.width *= filter.scaleX;
        _this.height *= filter.scaleY;
      }
    });

    replacement.width = canvasEl.width;
    replacement.height = canvasEl.height;
    if (fabric.isLikelyNode) {
      replacement.src = canvasEl.toBuffer(undefined, fabric.Image.pngCompression);
      // onload doesn't fire in some node versions, so we invoke callback manually
      _this._element = replacement;
      !forResizing && (_this._filteredEl = replacement);
      callback && callback(_this);
    }
    else {
      replacement.onload = function() {
        _this._element = replacement;
        !forResizing && (_this._filteredEl = replacement);
        callback && callback(_this);
        replacement.onload = canvasEl = null;
      };
      replacement.src = canvasEl.toDataURL('image/png');
    }
    return canvasEl;
  },*/
  actions : fabric._.extend(fabric.Image.prototype.actions, {
    filterOptions: {
      type: 'effect',
      className: 'fa fa-filter',
      title: "@image.filterOptions",
      actionParameters: function ($el, data) {
        //prototypeOptions
      }
    },
    filters: {
      title: "@image.filters",
      itemClassName: "images-selector",
      className: "fa fa-filter",
      type: "select",
      set: function (val, filtersData) {
        let options = false;
        if (val === "none") {
          val = false;
        } else {
          let _f = fabric._.findWhere(filtersData, {id: val});
          _f.enabled = !_f.enabled;
          for (let i in _f.options) {
            if ($.isNumeric(_f.options[i])) {
              _f.options[i] = parseFloat(_f.options[i]);
            }
          }
          if (_f.enabled) {
            options = {};
            for (let i in _f.options) {
              options[i] = _f.options[i].value;
            }
          }
        }
        this.setFilter({
          type: val,
          options: options,
          replace: true
        });
      },
      get: function () {
        return this.filters && this.filters.length ? fabric.util.string.capitalize(this.filters[0].type, true) : "none"
      },
      options: function () {

        let _filters = this.application.getFiltersList(this);
        for (let i in this.filters) {
          let _f = fabric._.findWhere(_filters,{type: fabric.util.string.capitalize(this.filters[i].type)})
          if(_f){
            _f.enabled = true;
          }
        }

        for (let i in _filters) {
          _filters[i].id = _filters[i].type;
        }
        return [{
          id: 'none',
          text: 'original',
          enabled: !this.filters || !this.filters.length
        }].concat(_filters);

      },
      dropdown: {
        previewWidth: 60,
        previewHeight: 60,
        templateSelection: function (state) {
          if (state.any) {
            return state.text;
          }
          return $(`<span>${state.text}</span>`);
        },
        templateResult: function (state) {
          let $el = $(`<span title=><span class="filter-preview"></span><span class="filter-name">${state.text}</span></span>`);

          if (!fabric.filterBackend) {
              fabric.filterBackend = fabric.initFilterBackend();
              fabric.isWebglSupported(fabric.textureSize);
          }

          let sourceImage = this.target._originalElement || this.target._element;

          if(sourceImage){
            let canvas = fabric.util.createCanvasElement();
            canvas.width = this.dropdown.previewWidth;
            canvas.height = this.dropdown.previewHeight;


            var ctx = canvas.getContext("2d");
            ctx.drawImage(sourceImage,0,0,this.dropdown.previewWidth,this.dropdown.previewHeight);

            let filters = [];

            if(state.id && state.id !== "none"){
              let filterName = state.id ? fabric.util.string.capitalize(state.id,true) : "none";
              filters.push({type: filterName});
            }

            let fImage = new fabric.SImage({
              element: canvas,
              filters: filters
            });

            $el.find(".filter-preview").append($(fImage._element));
          }

          return $el;
        },
      }
    }
  })
});

//
// fabric.Image.filterManager = {
//
//   //hide: function(object){
//   //
//   //},
//   show: function (object) {
//     this.activeObject = object;
//     this.fire('show', object);
//     this.on('target:changed', object)
//   }
// // };


