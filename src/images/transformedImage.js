//http://jsfiddle.net/mrbendel/6rbtde5t/1/
//http://thecodeplayer.com/walkthrough/3d-perspective-projection-canvas-javascript
//http://tulrich.com/geekstuff/canvas/perspective.html


'use strict';
require("./../mixins/bezierMixin");
require("./../mixins/cacheMixin");
require("./../util/gl");

fabric.TransformedImage = fabric.util.createClass(
  fabric.Image,
  fabric.BezierMixin,
  fabric.CacheMixin,
  {
    type: 'transformed-image',
    points:     null,
    _triangles: [],
    wireframe:  false,
    perspective: false,
    verticalSubdivisions: 7, // vertical subdivisions
    horizontalSubdivisions: 7,
    // fixWireFrame: true,
    fixWireFrame: true,
    renderBorder:false,
    curveCornerSize: 4,
    pointCornerSize: 4,
    //
    // hasBoundControls: false,
    // hasRotatingPoint: false,
    // hasTransformControls: true,
    // // editMode: "transform",
    //
    // hasShapeBorders: true,
    // hasBorders: false,


    hasBoundControls: true,
    hasRotatingPoint: true,
    hasTransformControls: false,
    extraCacheDPI: 3,
    // editMode: "transform",

    hasShapeBorders: false,
    hasBorders: true,


    switchControls: true,

    curveCursor: "pointer",
    transformCursor: "pointer",
    // specialProperties: fabric.Image.prototype.specialProperties.concat(["points","curve"] ),
    stateProperties: ["points"].concat(fabric.Image.prototype.stateProperties),
    initialize: function (el,options,callback) {
      options || ( options = {});

      fabric._.defaults(options,{
        width : el && el.width  || 100,
        height: el && el.height || 100
      });

      if(options.points){
        this.width = 0;
        this.height = 0;
      }

      this.points = options.points || [
        {x: 0, y: 0},
        {x: options.width, y: 0},
        {x: options.width, y: options.height},
        {x: 0, y: options.height}
      ];

      if(options.renderBorder !== undefined){
        this.renderBorder     = options.renderBorder;
      }
      if(options.wireframe !== undefined){
        this.wireframe     = options.wireframe;
      }
      if(options.perspective !== undefined){
        this.perspective     = options.perspective;
      }
      if(options.verticalSubdivisions !== undefined){
        this.verticalSubdivisions     = options.verticalSubdivisions;
      }
      if(options.horizontalSubdivisions !== undefined){
        this.horizontalSubdivisions     = options.horizontalSubdivisions;
      }
      this.extraControls = {};

      this._corner_actions = this._corner_actions || {};
      this._corner_actions.curve = "curve";

      this.callSuper('initialize',el, options,callback);

      this.setCurve(this.curve);

      this._initCurveOffset();
      if(this.switchControls ){
        this._switch_controls_bind = this.switchEditMode.bind(this);
        this.on('dblclick',this._switch_controls_bind);
      }

      if(options.contentOffsets){
        this._update_content_offsets(options.contentOffsets);
      }
      this.on("shape:modified",function(data){
        this.dirty = true;
        this.canvas.renderAll();
      });
      this.on("scaling",function(data){
        this.dirty = true;
        this.canvas.renderAll();
      });
      this.on("content:modified",function(data){
        if(data.bounds){
          this._initCurveOffset();
          var _b =  data.bounds || {
            minX: 0,
            maxX: this._originalElement.width,
            minY: 0,
            maxY: this._originalElement.height
          };
          this._update_content_offsets(_b);
        }
        this.dirty = true;
        this.canvas.renderAll();
      });
      this.updateBbox();
    },
    curve :  {x: 0.5, y: 0.5},
    setExtraControls: function(controls){
      if(!this.hasTransformControls)return;
      this.addPointsControls(controls);

      var _coord = fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,this.width * this.curve.x,this.height * this.curve.y,true);
      controls["c1"] = {
        action: "curve",
        visible: true,
        cursor: this.curveCursor || this.transformCursor,
        x: _coord.x,
        y: _coord.y,
        area:  this.pointCornerAreaSize || this.pointCornerSize,
        size : this.pointCornerSize || this.cornerSize,
        style: this.cornerStyle
      };
    },
    // drawControls: function (ctx, shape, offset) {
    //   if (!this.hasControls) {
    //     return this;
    //   }
    //   this.drawBoundsControls( ctx);
    // },
    // _update_curve_point: function(){
    //   this.extraControls.curve = fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,this.width * this.curve.x,this.height * this.curve.y,true);
    // },
    drawControlsInterface: function (ctx) {
      if(!this.hasShapeBorders)return;
      ctx.save();
      ctx.scale(this.scaleX,this.scaleY);
      ctx.translate(-this.width/2,-this.height/2);


      var _p = this.points,
        //   if (this.renderBorder) {
        p1 = /*new fabric.Point(*/_p[0],//.x,this.points[0].y);
        p2 = /*new fabric.Point(*/_p[1],//.x,this.points[1].y);
        p3 = /*new fabric.Point(*/_p[2],//.x,this.points[2].y);
        p4 = /*new fabric.Point(*/_p[3];//.x,this.points[3].y);



      ctx.beginPath();
      ctx.moveTo(p1.x, p1.y);

      if(p1.c){
        this.drawCurve(ctx, p1.curve);
      }else{
        ctx.lineTo(p2.x, p2.y);
      }
      ctx.lineTo(p3.x, p3.y);
      if(p3.c){
        this.drawCurve(ctx, p3.curve);
      }else{
        ctx.lineTo(p4.x, p4.y);
      }
      ctx.lineTo(p1.x, p1.y);
      ctx.stroke();
      ctx.closePath();
      ctx.restore();
    },
    setCurve: function(curve){
      curve.x = 0.5;
      this.curve = curve ||  {x: 0.5, y: 0.5};
      this.dirty = true;
      this._updateShapeCurving();
    },
    _updateShapeCurving: function () {

      var _coord = fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,this.width * this.curve.x,this.height * this.curve.y - this.height/2,true);
      this.setPoint("c1", _coord);

      var _coord = fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,this.width * this.curve.x,this.height * this.curve.y + this.height/2,true);
      this.setPoint("c3", _coord);
    },
    _performShapeAction: function (e, transform, pointer) {
      transform.corner.substr(1)
      this.setPoint(transform.corner, transform.point);
      this._updateShapeCurving();
      transform.actionPerformed = true;
      this.fire('shaping', e);
    },
    _performCurveAction: function (e, transform, pointer) {


      // transform.corner.substr(1)
      this.setCurve(fabric.util.gl.getImageCoordianateForPoint(this.points,transform.point));

      this._updateShapeCurving();

      transform.actionPerformed = true;
      this.fire('curving', e);
    },
    _initCurveOffset: function(){
      this.curving = {
        left: {x: 0, y: this.height / 2},  //  fabric.util.transformPoint({x: 0, y: this.height /2}, transformMatrix);
        middle: {x: this.width / 2, y: this.height * this.curve.y},         //   fabric.util.transformPoint({x: this.width / 2, y: _y}, transformMatrix);
        right: {x: this.width, y: this.height / 2}
      };//   fabric.util.transformPoint({x: this.width, y:  this.height /2}, transformMatrix);
    },
    setControlPoints: function () {
      this._controls = [];
      this.addPointsControls(this._controls);
      return this._controls;
    },
    // _show_pointers: function ()  {
    //
    //   // if(!this._controlsVisibility){
    //   //   this._controlsVisibility = [];
    //   // }
    //   // if(this._editing_mode == '3d'){
    //   //
    //   //   var _default_corners = {
    //   //     tl: false,
    //   //     tr: false,
    //   //     br: false,
    //   //     bl: false,
    //   //     ml: false,
    //   //     mt: false,
    //   //     mr: false,
    //   //     mb: false,
    //   //     mtr:false,
    //   //     p : true
    //   //   };
    //   // }else{
    //   //   var _default_corners = {
    //   //     tl: true,
    //   //     tr: true,
    //   //     br: true,
    //   //     bl: true,
    //   //     ml: true,
    //   //     mt: true,
    //   //     mr: true,
    //   //     mb: true,
    //   //     mtr:true,
    //   //     p : false
    //   //   };
    //   // }
    //   // for(var i in _default_corners){
    //   //   this._controlsVisibility[i] = _default_corners[i];
    //   // }
    //   // this.canvas && this.canvas.renderAll();
    // },
    // setEditMode:function(val){
    //   if(val === "transform"){
    //
    //   }
    //   if(val === "transform"){
    //
    //   }
    // },
    switchEditMode: function () {
      if(this.hasBoundControls){
        this.hasRotatingPoint = false;
        this.hasBoundControls = false;
        this.hasTransformControls = true;
        this.hasBorders = false;
        this.hasShapeBorders = true;
      }else{
        this.hasRotatingPoint = true;
        this.hasBoundControls = true;
        this.hasTransformControls = false;
        this.hasBorders = true;
        this.hasShapeBorders = false;
      }
      this.setCoords();
      this.canvas.renderAll();
    },
    normalize: function(){
      this.off('dblclick',this._switch_controls);
      delete this._switch_controls;
    },
    /**
     * @private
     * @param {CanvasRenderingctx2D} ctx ctx to render on
     */
    _render: function (ctx, noTransform) {
      ctx.save();
      this._initCurveOffset();



      var points = fabric.util.deepClone(this.points);
      var curving = fabric.util.deepClone(this.curving);
      var _scaleX = this.scaleX * this.extraCacheDPI;
      var _scaleY = this.scaleY * this.extraCacheDPI;
      for(var i in curving){
        curving[i].x *= _scaleX;
        curving[i].y *= _scaleY;
      }
      for(var i in points){
        points[i].x *= _scaleX;
        points[i].y *= _scaleY;
        if(points[i].c){
          points[i].c.x *= _scaleX;
          points[i].c.y *= _scaleY;
        }
      }

      this._triangles = fabric.util.gl.calculateGeometry(
        points,
        this._element.naturalWidth,
        this._element.naturalHeight,
        this.verticalSubdivisions,
        this.horizontalSubdivisions,
        this.perspective,
        curving
      );

      if (this._element){
        fabric.util.gl.drawElement(ctx,this._element,this._triangles,this.wireframe)
      }
      if (this.wireframe) {
        fabric.util.gl.drawTriangles(ctx,this._triangles);
      }
      if (this.fixWireFrame) {
        fabric.util.gl.fixSemiTransparentPixels(ctx);
      }

      ctx.restore();
    },
    _update_content_offsets: function(_b){

      var _omX = 0, _omY = 0;
      if(this.contentOffsets){
        _omX = this.contentOffsets.minX;
        _omY = this.contentOffsets.minY;
      }

      if(_b){
        this.points = [
          fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,_b.minX - _omX,_b.minY - _omY),
          fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,_b.maxX - _omX,_b.minY - _omY),
          fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,_b.maxX - _omX,_b.maxY - _omY),
          fabric.util.gl.getPointForImageCoordianate(this.width,this.height,this.points,this.curving,_b.minX - _omX,_b.maxY - _omY)
        ];
        this.dirty = true;
      }

      this.contentOffsets = _b;
    },
    toObject: function(propertiesToInclude) {
      var object = this.callSuper('toObject', propertiesToInclude);

      if (!this.includeDefaultValues) {
        this._removeDefaultValues(object);
      }

      var points = []
      for(var i in this.points){
        points.push(this.points[i].x);
        points.push(this.points[i].y);
      }
      object.curve = this.curve;
      object.points = points;
      return object;
    },
    update_curve_height: function () {
      if(this.extraControls.curve.y > this._original_height){
        this.height = this.extraControls.curve.y;
      }else  if(this.extraControls.curve.y < this._original_height){
        this.height = Math.max(this.extraControls.curve.y, this._original_height);
      }
    }
  });









var _TRI = fabric.TransformedImage.prototype;
fabric.TransformedImage.prototype.actions = fabric._.extend({}, fabric.Image.prototype.actions, {
  switchControls: {
    className: 'fa fa-cube ',
    title: 'toggle transform',
    action: _TRI.switchControls
  }
});

fabric.TransformedImage.fromObject = function(object,callback){

  object = fabric._.clone(object);

  function onImageLoaded(img){
    fabric.Image.prototype._initFilters.call(object, object.filters, function(filters) {
      object.filters = filters || [ ];
      fabric.Image.prototype._initFilters.call(object, object.resizeFilters, function(resizeFilters) {
        object.resizeFilters = resizeFilters || [ ];
        var instance = new fabric.TransformedImage(img, object,callback);
      });
    });
  }


  if(object.image){
    onImageLoaded(object.image);
  }else{

    fabric.util.loadImage(object.src, onImageLoaded, null, object.crossOrigin);
  }
};
