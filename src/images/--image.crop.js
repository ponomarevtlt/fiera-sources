// require("./clipTo");


var MAX_CROP_ZOOM_VALUE = 10;
var MIN_CROP_ZOOM_VALUE = 0.5;
/**
 * Cropping do not support skewX and skewY
 */
fabric._.extend(fabric.Image.prototype, {
  stateProperties: fabric.Image.prototype.stateProperties.concat(["crop","fitting"]),
  crop: null,
  eventListeners: fabric.util.extendArraysObject(fabric.Image.prototype.eventListeners, {
    "element:modified": function () {
      if(this.crop){
        this.setCrop(this.crop);
      }
    }
  }),
  store_crop: function () {
    if (!this.crop) return null;
    const DIG = fabric.Object.NUM_FRACTION_DIGITS;
    let _crop = {};

    if (this.crop.left) _crop.left = fabric.util.toFixed(this.crop.left, DIG);
    if (this.crop.top) _crop.top = fabric.util.toFixed(this.crop.top, DIG);
    if (this.crop.scaleX) _crop.scaleX = fabric.util.toFixed(this.crop.scaleX, DIG);
    if (this.crop.scaleY) _crop.scaleY = fabric.util.toFixed(this.crop.scaleY, DIG);
    if (this.crop.angle) _crop.angle = fabric.util.toFixed(this.crop.angle, DIG);
    return _crop;
  },
  setCrop: function (crop) {
    this.crop = crop;
    this.dirty = true;
    if (!crop) {
      delete this.drawEl;
      return;
    }
    if (!this._element) {
      return;
    }
    if (!this.drawEl) {
      this.drawEl = new fabric.SImage({
        element: this._element,
        originX: "center",
        originY: "center"
      });
      // this.drawEl.group = this;
    }
    this.drawEl.set({
      skewX: crop.skewX || 0,
      skewY: crop.skewY || 0,
      angle: crop.angle || 0,
      left: crop.left * this.width || 0,
      top: crop.top * this.height || 0,
      width: this.width,
      height: this.height,
      scaleX: crop.scaleX || 1,
      scaleY: crop.scaleY || 1,
    });
    if(!this.shape){
      this.setShape({
        offsets: 0
      })
    }
  },

  /**
   * Execute the drawing operation for an object on a specified context
   * @param {CanvasRenderingContext2D} ctx Context to render on
   */
  drawObject: function(ctx, forClipping) {

    if (forClipping) {
      this._setClippingProperties(ctx);
    }
    else {
      this._renderBackground(ctx);
      this._setStrokeStyles(ctx, this);
      this._setFillStyles(ctx, this);
    }
    this._render(ctx);
    this._drawClipPath(ctx);

    if(this._decoElement){
      this._renderDeco(ctx,this._decoElement);
    }
  },
  /**
   * When set to `true`, force the object to have its own cache, even if it is inside a group
   * it may be needed when your object behave in a particular way on the cache and always needs
   * its own isolated canvas to render correctly.
   * Created to be overridden
   * since 1.7.12
   * @returns false
   */
  needsItsOwnCache: function() {
    if (this.paintFirst === 'stroke' && typeof this.shadow === 'object') {
      return true;
    }
    if (this.clipPath || this.imageCropClipPath) {
      return true;
    }
    return false;
  },
  _renderFill: function (ctx) {

    let w = this.width, h = this.height,
      sW = w * this._filterScalingX, sH = h * this._filterScalingY,
      x = -w / 2, y = -h / 2,
      elementToDraw = this._element;

    if (!elementToDraw) {
      return;
    }

    if (this.isMoving === false && this.resizeFilters && this.resizeFilters.length && this._needsResize()) {
      this._lastScaleX = this.scaleX;
      this._lastScaleY = this.scaleY;
      elementToDraw = this.applyFilters(null, this.resizeFilters, this._filteredEl || this._originalElement, true);
    }
    else {
      elementToDraw = this._element;
    }

    if (this.crop) {
      this.drawEl.canvas = this.canvas;
      this.drawEl._element = this._element;
      // this.drawEl.render(ctx);

      ctx.save();
      this.drawEl.transform(ctx);
      this.drawEl.drawObject(ctx);
      ctx.restore();


    } else {

      //render SVG as Paths GROUP
      if(false && this._objects){
        // for (let object of this._objects) {
        //   object.render(ctx);
        // }
      }
      else{
        elementToDraw && ctx.drawImage(elementToDraw,
          this.cropX * this._filterScalingX,
          this.cropY * this._filterScalingY,
          elementToDraw.width,
          elementToDraw.height,
          x, y, w, h);
      }
    }

    if(this.imageCropClipPath){
      this._clipPath = this.clipPath;
      this.clipPath = this.imageCropClipPath;
      let path = this.clipPath;
      path.canvas = this.canvas;
      path.shouldCache();
      path._transformDone = true;
      path.renderCache({ forClipping: true });
      this.drawClipPathOnCache(ctx);
      this.clipPath = this._clipPath;
      delete this._clipPath;
    }

    this._stroke(ctx);
    this._renderStroke(ctx);

  },
  cropPhotoEnd: function () {

    // this.canvas.off("mouse:wheel",this.onMouseWheel);
    this.canvas.editingObject = null;
    this.canvas.off("selection:created selection:updated selection:cleared", this._endFoo);
    if (this.canvas._gridObject) {
      this.canvas._gridObject.enabled = true;
    }

    this.set({
      hasControls: true,
      evented: true,
      flipX: this._transform_element.flipX,
      flipY: this._transform_element.flipY
    });
    this._resetOrigin();

    this._cropmode = false;

    this.canvas.discardActiveObject();
    this.canvas.remove(this._transform_element);
    this.canvas.stateful = true;

    this.canvas.setActiveObject(this);
    this.canvas.renderAll();

    this.fitting = "manual";
    this.canvas.fire('object:modified', {target: this});
    this.fire('modified', {} );
  },
  updateCrop: function () {

    let I = this._transform_element,
      F = this,
      to_radians = Math.PI / 180,
      cosA = Math.cos(F.angle * to_radians),
      sinA = Math.sin(F.angle * to_radians),
      left = ((I.left - F.left) * cosA + (I.top - F.top) * sinA  /*- F.width/ 2*/) * (I.flipX ? -1 : 1),
      top = (-(I.left - F.left) * sinA + (I.top - F.top) * cosA  /*- F.height/ 2*/) * (I.flipY ? -1 : 1);

    F.setCrop({
      skewX: I.skewX,
      skewY: I.skewY,
      left: (left) / F.width / F.scaleX,
      top: (top) / F.height / F.scaleY,
      scaleX: I.scaleX / F.scaleX,
      scaleY: I.scaleY / F.scaleY,
      angle: I.angle - F.angle
    });
    this.fire('crop:modified');
  },

  _createTransformedElement: function () {

    let options;
    if (this.drawEl) {
      this.drawEl.group = this;
      this.setCrop(this.crop);
      fabric.Group.prototype.realizeTransform.call(this, this.drawEl);
      delete this.drawEl.group;

      options = {
        application: this.application,
        angle: this.drawEl.angle,
        left: this.drawEl.left,
        top: this.drawEl.top,
        width: this.drawEl.width,
        height: this.drawEl.height,
        scaleX: this.drawEl.scaleX,
        scaleY: this.drawEl.scaleY,
        skewX: this.drawEl.skewX,
        skewY: this.drawEl.skewY,
      };
      this.setCrop(this.crop)
    } else {
      options = {
        angle: this.angle,
        left: this.left,
        top: this.top,
        width: this.width,
        height: this.height,
        scaleX: this.scaleX,
        scaleY: this.scaleY,
      };
    }

    fabric._.extend(options, {
      application: this.application,
      movementLimits: this,
      movementLimitMode: "content",
      originX: "center",
      originY: "center",
      hasControls: true,

    });

    let el = fabric.util.createObject("Crop",options);
    // let el = new fabric.Crop( options);
    el.canvas = this.canvas;

    return el;
  },
  cropPhotoStart: function () {
    this._cropmode = true;
    // if(!this.canvas.processing){//hhistory transaction
    this.canvas.stateful && this.saveState();
    // }
    this.canvas.stateful = false;

    // this.canvas.discardActiveObject();
    if (this.canvas._gridObject) {
      this.canvas._gridObject.enabled = false;
    }

    this._setOriginToCenter();
    this._transform_element = this._createTransformedElement();
    this._transform_element.parent = this;
    this._transform_element.on("rotating", this._transform_element.stepRotating);

    this.canvas.add(this._transform_element);

    // this.canvas.on("mouse:wheel",this.onMouseWheel);

    //рамку двигать нельзя
    this.set({
      hasControls: false,
      evented: false
    });

    this.canvas.setActiveObject(this._transform_element);
    this.active = true;

    let _this = this;
    this._endFoo = function () {
      _this.cropPhotoEnd();
    };
    this.canvas.on("selection:created selection:updated selection:cleared", this._endFoo);

    this.fire("cropping:entered");
    this.canvas.fire('frame:cropping:entered', {target: this});
    this.canvas.editingObject = this;
    this.canvas.renderAll();
  },
  scaleContentElement(scaleX,scaleY){
    this._transform_element.set({
      scaleX: scaleX,
      scaleY: scaleY,
    });
    this.updateCrop();
    this.canvas.renderAll();
  },
  cropZoomOut: function () {
    this.scaleContentElement(
      Math.min(MAX_CROP_ZOOM_VALUE,Math.max(this._transform_element.scaleX *0.9, MIN_CROP_ZOOM_VALUE)),
      Math.min(MAX_CROP_ZOOM_VALUE,Math.max(this._transform_element.scaleY *0.9, MIN_CROP_ZOOM_VALUE))
    );
  },
  cropZoomIn: function () {
    Math.sqrt(this.scaleX * this.scaleY);

    this.scaleContentElement(
      Math.min(MAX_CROP_ZOOM_VALUE,this._transform_element.scaleX * 1.11),
      Math.min(MAX_CROP_ZOOM_VALUE,this._transform_element.scaleY * 1.11)
    );
  },
  toggleClipModeByClick: function (e) {
    if (this._is_clipping_available(e.e)) {
      if (!this._cropmode) {
        this.clipPhotoStart()
      } else {
        this.clipPhotoEnd()
      }
    }
  },
  clippingSupportedTypes: "*", // * or ["image","video",...]
  cropEnabled: true,
  _is_clipping_available: function (e) {
    if (!this.cropEnabled) return false;
    if (e && this.isPossibleTarget(e, this.button)) {
      return;
    }
    return this.element && (this.clippingSupportedTypes === "*" || this.clippingSupportedTypes.indexOf(this.element.type) !== -1);
  },
  croppingAvailable: function () {
    return this.cropEnabled && this._element && !this._cropmode;
  },
  actions: fabric._.extend(fabric.Image.prototype.actions, {
    crop: {
      title: "@image.crop",
      className: "fa fa-crop",
      action: "cropPhotoStart",
      visible: "croppingAvailable",
      observe: "cropping:entered cropping:exited element:modified"
    }
  })
});

fabric.Crop = fabric.util.createClass(fabric.SImage, {
  type: "crop",
  snappable: false,
  stored: false, // do not save cropImage objects
  tools: ["cropZoomIn","cropZoom", "cropZoomOut", "cropEnd"],
  actions: {
    cropZoom: {
      type: "range",
      className: "fa fa-search-plus ",
      title: "@image.cropZoom",
      observe: "parent.crop:modified",
      get: function () {
        let zoomValue = Math.sqrt(this.scaleX * this.scaleY);
        return Math.sqrt((zoomValue - MIN_CROP_ZOOM_VALUE)/(MAX_CROP_ZOOM_VALUE -MIN_CROP_ZOOM_VALUE));
      },
      set: function (val) {
        if(this.parent.__modifiedBy){
          return;
        }
        this.parent.__modifiedBy = "range";
        let zoomValue = (MAX_CROP_ZOOM_VALUE -MIN_CROP_ZOOM_VALUE) * Math.pow(val,2) + MIN_CROP_ZOOM_VALUE;
        this.parent.scaleContentElement(zoomValue,zoomValue);
        this.canvas.renderAll();
        delete this.parent.__modifiedBy;
      },
      min: 0,
      max: 1,
      step: 0.01
    },
    cropZoomIn: {
      className: "fa fa-search-plus ",
      title: "@image.cropZoomIn",
      action () {
        this.parent.cropZoomIn();
      }
    },
    cropZoomOut: {
      className: "fa fa-search-minus",
      title: "@image.cropZoomOut",
      action () {
        this.parent.cropZoomOut();
      }
    },
    cropEnd: {
      className: "fa fa-crop-end", //this class specified om fiera.toolbar.less
      title: "@image.cropEnd",
      action () {
        this.parent.cropPhotoEnd();
      }
    }
  },
  opacity: 0.3,
  eventListeners: fabric.util.extendArraysObject(fabric.Image.prototype.eventListeners, {
    "mousewheel"(e){
      this.parent.__modifiedBy = "wheel";
      if (e.e.deltaY < 0) {
        this.parent.cropZoomIn();
      } else {
        this.parent.cropZoomOut();
      }
      e.e.stopPropagation();
      e.e.preventDefault();
      delete this.parent.__modifiedBy;
    },
    "rotating moving scaling skewing" () {
      this.parent.__modifiedBy = "controls";
      this.parent.updateCrop();
      delete this.parent.__modifiedBy;
    }
  })
});