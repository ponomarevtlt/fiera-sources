

fabric._.extend(fabric.Image.prototype, {
  decoSourceRoot: "",
  decoThumbnailSourceRoot: "",
  optionsOrder: fabric.util.a.build(fabric.Image.prototype.optionsOrder).before("decoSourceRoot","decoThumbnailSourceRoot").array,
  setDeco: function (deco,cb) {
    if(deco && deco.constructor === String){
        deco = fabric.Image.frames[deco];
    }
    if(deco && deco.src && !deco.image){

      this._set_deco(false);
      //show low quality image
      if(this.decoThumbnailSourceRoot){
        var thumbnailSrc = fabric.util.getURL(deco.src,this.decoThumbnailSourceRoot);

        fabric.util.loadImage(thumbnailSrc, img => {
          //do not modify image if frame was changed
          if(this.frame.deco !== deco){
            return;
          }
          if(deco.image)return;
          deco.image = img;
          this._set_deco(deco);
          this.canvas && this.canvas.renderAll();
        }, this, this.crossOrigin);
      }

      deco.src = fabric.util.getURL(deco.src,this.decoSourceRoot);

      fabric.util.loadImage(deco.src, img => {
        //do not modify image if frame was changed
        if(this.frame.deco !== deco){
          return;
        }
        deco.image = img;
        this._set_deco(deco);
        this.canvas && this.canvas.renderAll();
        cb && cb();
      }, this, this.crossOrigin);
    }else{
        this._set_deco(deco);
        this.canvas && this.canvas.renderAll();
        cb && cb();
    }
  },
  _update_frame: function () {
      if(this.frame && this.frame.image && this.frame.slice) {
          let _canvas = this._decoElement._element;
          _canvas.width = this.width;
          _canvas.height = this.height;
          fabric.util.drawBorderImage(_canvas, this.frame.image, this.frame);
      }
  },
  _set_deco: function (deco) {
      if(this._decoElement){
          delete this._decoElement;
      }
      if(deco && deco.image) {

          //if (!deco["border_image"]) {
          //    img.width = this.width;
          //    img.height = this.height;
          //}

          if (deco.width && deco.slice) {
              //Canvas for NodeJS
              let canvas = typeof Canvas !== 'undefined' && new Canvas() || document.createElement("canvas");

              canvas.width = this.width;
              canvas.height = this.height;
              fabric.util.drawBorderImage(canvas, deco.image, deco);

              this._decoElement = canvas;
          } else {
              this._decoElement = deco.image;
          }
          this.canvas && this.canvas.renderAll();
      }
      this.deco = deco;
      this.dirty = true;
  }
});