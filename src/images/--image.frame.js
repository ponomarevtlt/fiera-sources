require("./../modules/frames.js");

fabric._.extend(fabric.Image.prototype, {
  frame: false,
  optionsOrder: fabric.util.a.insertBefore(fabric.Image.prototype.optionsOrder,"*","framesUnits"),
  stateProperties: fabric.Image.prototype.stateProperties.concat(["frame"]),
  store_frame: function(){
    if(!this.frame)return false;

    if(this.frame.id){
      return this.frame.id;
    }
    return this.frame;
  },
  framesUnits: "mixed",
  setFrame: function (value,callback) {
    this.saveState();

    if (value) {
      if (value.constructor === String) {
        this.frame = this.application.getFrame(value);
      } else {
        this.frame = value;
      }
      if(!this.frame.shape.units){
        this.frame.shape.units = this.framesUnits;
      }
      this.setShape(this.frame.shape,path => {
        this.imageCropClipPath = path;
      });
      this.setDeco(this.frame.deco, () => {
        callback && callback();
        this.fire('frame:modified');
      });
    } else {
      this.frame = false;
      this.setShape(false);
      this.setDeco(false);
      callback && callback();
      this.fire('frame:removed');
    }
    if(this.canvas){
      this.canvas.fire('object:modified', {target: this});
    }
    this.fire('modified');
  },
  availableFrames: false,// ["ClipPath", "PointsPath"],
  uploadShape:function(){

  },
  actions: fabric._.extend(fabric.Image.prototype.actions || {}, fabric.FramesActionsMixin)
});

