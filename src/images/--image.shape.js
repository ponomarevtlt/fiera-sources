
fabric._.extend(fabric.Image.prototype, {
  decorateBorderStyle: {
    strokeWidthFull: 0,
    strokeWidthEmpty: 2,
    strokeWidthActive: 2,
    strokeDashArray : [5,5],
    fill: "transparent",
    strokeEmpty:  "red",
    strokeFull:   "red",
    strokeActive: "green"
  },
  // _cropByFrame: function(ctx) {
  //     if (this.crop) {
  //         ctx.beginPath();
  //         ctx.rect(x, y, this.width, this.height);
  //         ctx.clip();
  //     }
  // },

  // initShape: function(options){
  //
  //   var _shapeOptions = fabric._.extend(options.shape || {} , this.shape);
  //
  //   this._fabric_shape =  new fabric.Rect(fabric._.extend({},_shapeOptions,{
  //     width:            options.width,
  //     height:           options.height,
  //     stroke:           _shapeOptions.strokeEmpty,
  //     strokeWidth:      _shapeOptions.strokeWidthEmpty
  //   }));
  // },


  updateStroke: function(){

    return;
    //todo : it is related to deco property
    let _stroke = this.shape.stroke;
    let _sw = 0;
    if((this._activated || this._clipmode )&& this.shape.strokeWidthActive){
      _sw = this.shape.strokeWidthActive
    }else if(this.element && this.shape.strokeWidthFull){
      _sw = this.shape.strokeWidthFull;
    }else if(!this.element && this.shape.strokeWidthEmpty){
      _sw = this.shape.strokeWidthEmpty;
    }
    if((this._activated || this._clipmode) && this.shape.strokeActive){
      _stroke = this.shape.strokeActive
    }else if(this.element && this.shape.strokeFull){
      _stroke = this.shape.strokeFull;
    }else if(!this.element && this.shape.strokeEmpty){
      _stroke = this.shape.strokeEmpty;
    }

    let _strokeWidth = _sw / Math.max(this._fabric_shape.scaleX,this._fabric_shape.scaleY);

    if(this.shape){
      if(this.shape.paths ){
        for(let path of this._fabric_shape.paths){
            path.setStrokeWidth(_strokeWidth);
            path.setStroke(_stroke);
        }
      }else{
        this._fabric_shape.setStrokeWidth(_strokeWidth);
        this._fabric_shape.setStroke(_stroke);
      }
    }
  },
  /**
   * объект готовится к замену фото
   */
  activate: function(){
    this._activated = true;
    this._fabric_shape.setOpacity(1);
    this.updateStroke();
    this.canvas.renderAll();
  },
  /**
   * кобъект не готовится к замену фото
   */
  deactivate: function(){
    this._activated = false;
    this.updateStroke();
    this.canvas.renderAll();
  }
});
