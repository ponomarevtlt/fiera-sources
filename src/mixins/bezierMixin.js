require("./../plugins/bezier");
require("./../mixins/shapeMixin");


var Bezier = fabric.Bezier;

fabric.BezierMixin = {
  curveCornerSize: 4,
  curveCornerAreaSize: 10,
  cornerAreaSize: 4,
  pointCornerAreaSize: 10,
  pointCornerSize: 4,
  transformCornerStyle: null,//"rect",
  curveCornerStyle: null,//"rect",
  outlineTop: 0,
  outlineBottom: 0,
  hasTransformControls: true,
  _renderBezier: function (ctx,_outline ,x,y) {
    _outline = _outline || this.outline;
    ctx.beginPath();
    var _this = this;
    // ctx.save();
    // ctx.translate(this._transalate.x, this._transalate.y);
    var doc = function (c) {
      _this.drawCurve(ctx, c);
    };
    _outline.curves.forEach(doc);

    ctx.closePath();

    this._renderFill(ctx);
    this._renderStroke(ctx);
  },
  drawBezierControls: function (ctx, p1, p2, p3, p4) {
    var pts = this.points;
    ctx.save();
    ctx.globalAlpha = this.isMoving ? this.borderOpacityWhenMoving : 1;
    ctx.strokeStyle = ctx.fillStyle = this.cornerColor;
    ctx.scale(this.canvas.viewportTransform[0], this.canvas.viewportTransform[3]);
    ctx.translate(-this.width / 2 * this.scaleX, -this.height / 2 * this.scaleY);
    ctx.beginPath();
    ctx.moveTo(p1.x * this.scaleX, p1.y * this.scaleY);
    ctx.lineTo(p2.x * this.scaleX, p2.y * this.scaleY);
    if (pts.length === 3) {
      ctx.moveTo(p2.x * this.scaleX, p2.y * this.scaleY);
      ctx.lineTo(p3.x * this.scaleX, p3.y * this.scaleY);
    }
    else {
      ctx.moveTo(p3.x * this.scaleX, p3.y * this.scaleY);
      ctx.lineTo(p4.x * this.scaleX, p4.y * this.scaleY);
    }
    ctx.closePath();
    ctx.stroke();
    ctx.scale(this.scaleX, this.scaleY);
    ctx.beginPath();
    this.drawCurve(ctx, p1.curve);
    ctx.stroke();
    ctx.restore();
  },
  drawCurve: function (ctx, curve, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    var p = curve.points, i;
    //ctx.moveTo(p[0].x + ox, p[0].y + oy);
    ctx.lineTo(p[0].x + ox, p[0].y + oy);
    if (p.length === 3) {
      ctx.quadraticCurveTo(
        p[1].x + ox, p[1].y + oy,
        p[2].x + ox, p[2].y + oy
      );
    }
    if (p.length === 4) {
      ctx.bezierCurveTo(
        p[1].x + ox, p[1].y + oy,
        p[2].x + ox, p[2].y + oy,
        p[3].x + ox, p[3].y + oy
      );
    }
  },
  drawLine: function (ctx, p1, p2, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    ctx.beginPath();
    ctx.moveTo(p1.x + ox, p1.y + oy);
    ctx.lineTo(p2.x + ox, p2.y + oy);
    ctx.stroke();
  },
  drawPoint: function (ctx, p, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    ctx.beginPath();
    ctx.arc(p.x + ox, p.y + oy, 5, 0, 2 * Math.PI);
    ctx.stroke();
  },
  drawPoints: function (ctx, points, offset) {
    offset = offset || {x: 0, y: 0};
    points.forEach(function (p) {
      this.drawCircle(ctx, p, 3, offset);
    }.bind(this));
  },
  drawArc: function (ctx, p, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    ctx.beginPath();
    ctx.moveTo(p.x + ox, p.y + oy);
    ctx.arc(p.x + ox, p.y + oy, p.r, p.s, p.e);
    ctx.lineTo(p.x + ox, p.y + oy);
    ctx.fill();
    ctx.stroke();
  },
  drawCircle: function (ctx, p, r, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    ctx.beginPath();
    ctx.arc(p.x + ox, p.y + oy, r, 0, 2 * Math.PI);
    ctx.stroke();
  },
  drawbbox: function (ctx, bbox, offset) {
    offset = offset || {x: 0, y: 0};
    var ox = offset.x;
    var oy = offset.y;
    ctx.beginPath();
    ctx.moveTo(bbox.x.min + ox, bbox.y.min + oy);
    ctx.lineTo(bbox.x.min + ox, bbox.y.max + oy);
    ctx.lineTo(bbox.x.max + ox, bbox.y.max + oy);
    ctx.lineTo(bbox.x.max + ox, bbox.y.min + oy);
    ctx.closePath();
    ctx.stroke();
  },
  drawHull: function (ctx, hull, offset) {
    ctx.beginPath();
    if (hull.length === 6) {
      ctx.moveTo(hull[0].x, hull[0].y);
      ctx.lineTo(hull[1].x, hull[1].y);
      ctx.lineTo(hull[2].x, hull[2].y);
      ctx.moveTo(hull[3].x, hull[3].y);
      ctx.lineTo(hull[4].x, hull[4].y);
    } else {
      ctx.moveTo(hull[0].x, hull[0].y);
      ctx.lineTo(hull[1].x, hull[1].y);
      ctx.lineTo(hull[2].x, hull[2].y);
      ctx.lineTo(hull[3].x, hull[3].y);
      ctx.moveTo(hull[4].x, hull[4].y);
      ctx.lineTo(hull[5].x, hull[5].y);
      ctx.lineTo(hull[6].x, hull[6].y);
      ctx.moveTo(hull[7].x, hull[7].y);
      ctx.lineTo(hull[8].x, hull[8].y);
    }
    ctx.stroke();
  },
  drawShape: function (ctx, shape, offset) {
    offset = offset || {x: 0, y: 0};
    var order = shape.forward.points.length - 1;
    ctx.beginPath();
    ctx.moveTo(offset.x + shape.startcap.points[0].x, offset.y + shape.startcap.points[0].y);
    ctx.lineTo(offset.x + shape.startcap.points[3].x, offset.y + shape.startcap.points[3].y);
    if (order === 3) {
      ctx.bezierCurveTo(
        offset.x + shape.forward.points[1].x, offset.y + shape.forward.points[1].y,
        offset.x + shape.forward.points[2].x, offset.y + shape.forward.points[2].y,
        offset.x + shape.forward.points[3].x, offset.y + shape.forward.points[3].y
      );
    } else {
      ctx.quadraticCurveTo(
        offset.x + shape.forward.points[1].x, offset.y + shape.forward.points[1].y,
        offset.x + shape.forward.points[2].x, offset.y + shape.forward.points[2].y
      );
    }
    ctx.lineTo(offset.x + shape.endcap.points[3].x, offset.y + shape.endcap.points[3].y);
    if (order === 3) {
      ctx.bezierCurveTo(
        offset.x + shape.back.points[1].x, offset.y + shape.back.points[1].y,
        offset.x + shape.back.points[2].x, offset.y + shape.back.points[2].y,
        offset.x + shape.back.points[3].x, offset.y + shape.back.points[3].y
      );
    } else {
      ctx.quadraticCurveTo(
        offset.x + shape.back.points[1].x, offset.y + shape.back.points[1].y,
        offset.x + shape.back.points[2].x, offset.y + shape.back.points[2].y
      );
    }
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
  },
  // initBezier: function () {
  //   this.curve = new Bezier(0, 0, 0, 1, 1, 1, 0, 1);
  //   this.outline = this.curve.outline(this.bezierOutline[0], this.bezierOutline[1]);
  // },
  // bezierType: "quadraticByPoints",
  // defaultShape: "arc",//line
  setPoints: function (points) {

    if(points && points[0] !== undefined && points[0].x === undefined){
      var _NEW_points = [];
      for(var i=0; i < points.length ;i+=2){
        _NEW_points.push({x: points[i] , y: points[i + 1]});
      }
      points = _NEW_points;
    }


    // if(this.defaultShape == "curve"){
    //   points = [0, 0, this.width/2, -this.width/2, this.width/2, this.width/2, this.width, 0];
    // }else if(this.defaultShape == "arc"){
    //   points = [0, 0, this.width*0.16, -this.width*0.43, this.width *(1 - 0.16), -this.width*0.43, this.width, 0];
    // }else{
    //   points = [0, 0, this.width / 3, 1, this.width / 3 * 2, 1, this.width, 0];
    // }
    var _intervals = this.closed && points.length > 1 ? points.length : points.length - 1;
    this.points = [].concat(points);
    if(this.points.length > 1 && this.bezierControls){
      for(var i = 0 ; i < _intervals; i ++ ){
        var p2 = this.points[i + 1] || this.points[0];
        var p1 = this.points[i];
        var _cp = p1.c;
        if(_cp){
          if(p1.c2){
            this._makeCubic(p1, p1.c,p1.c2, p2);
          }else{
            this._makeCurve(p1, p1.c, p2);
          }
        }else{
          _cp = {
            x: (p1.x + p2.x)/2,
            y: (p1.y + p2.y)/2
          }
        }
      }
    }
  },
  // setPoint: function (order, _point) {
  //   var _points = this.points;
  //   _points[order].x = _point.x;
  //   _points[order].y = _point.y;
  //
  //   if(order == 0 ){
  //     var _p1 = _points[1], _p2 =  _points[0];
  //   }
  //   if(order == 1 ){
  //     var _p1 = _points[0], _p2 =  _points[1];
  //   }
  //   if(_points.length == 3){
  //     if(order == 2 ){
  //       var _p1 = _points[1], _p2 =  _points[2];
  //     }
  //   }else{
  //     if(order == 2 ){
  //       var _p1 = _points[3], _p2 =  _points[2];
  //     }
  //     if(order == 3 ){
  //       var _p1 = _points[2], _p2 =  _points[3];
  //     }
  //   }
  //   if (_p1.x - _p2.x < 1) {
  //     if(_p1.x <  _p2.x){
  //       _p2.x++;
  //     }else{
  //       _p2.x--;
  //     }
  //   }
  //   if (_p1.y - _p2.y < 1) {
  //     if(_p1.y <  _p2.y){
  //       _p2.y++;
  //     }else{
  //       _p2.y--;
  //     }
  //   }
  //   this.curve.update();
  //   this.outline = this.curve.outline(this.bezierOutline[0], this.bezierOutline[1]);
  //   this.updateBbox();
  //   this.canvas.renderAll();
  // }
  setExtraControls: function(controls){
    if(this.hasTransformControls){
      this.addPointsControls(controls);
    }
  },
  addPointsControls: function(controls){

    var pts = this.points,
      _last = pts.length - 1;
    for(var i in pts){
      controls["p" + (+i + 1)] = {
        action: "shape",
        visible: true,
        x: pts[i].x,
        y: pts[i].y,
        cursor: this.transformCursor ,
        area:  this.pointCornerAreaSize || this.pointCornerSize,
        size : this.pointCornerSize || this.cornerSize,
        style: this.cornerStyle
      };

      if(this.bezierControls) {
        if (pts[i].c2) {

          controls[ "d" + (+i + 1)] ={
            action: "shape",
            visible: true,
            x: pts[i].c2.x,
            y: pts[i].c2.y,
            cursor: this.transformCursor,
            size: this.curveCornerSize ,
            area: this.curveCornerAreaSize,
            style: this.transformCornerStyle || this.cornerStyle
          }

        }

        if (pts[i].c) {
          controls["c" + (+i + 1)] = {
            action: "shape",
            visible: true,
            x: pts[i].c.x,
            y: pts[i].c.y,
            cursor: this.curveCursor || this.transformCursor,
            size: this.curveCornerSize ,
            area: this.curveCornerAreaSize,
            style: this.curveCornerStyle || this.cornerStyle
          }
        } else {
          var p2 = pts[+i + 1] || this.closed && pts[0];
          if(p2){
            controls["c" + (+i + 1)] = {
              action: "shape",
              visible: true,
              cursor: this.curveCursor || this.transformCursor,
              x: (pts[i].x + p2.x) / 2,
              y: (pts[i].y + p2.y) / 2,
              size: this.curveCornerSize,
              area: this.curveCornerAreaSize,
              style: this.curveCornerStyle || this.cornerStyle
            }
          }
        }
      }
    }
  },
  drawBezierShapeControls: function(ctx){
    for(var i = 0 ; i < this.points.length ; i++){
      if(this.points[i].c2){
        this.drawBezierControls(ctx, this.points[i],this.points[i].c,this.points[i].c2,this.points[i + 1]);
      }
    }
  },
  bezierControls: false,
  _makeCurveByIndex: function (index) {
    this._makeCurve(this.points[index],this.points[index].c,this.points[index + 1] || this.points[0])
  },
  _makeCubic: function (p1, c1,c2,p2) {
    p1.curve = new Bezier(p1.x,p1.y , c1.x ,c1.y , c2.x,c2.y , p2.x,p2.y);
    if(this.outlineTop || this.outlineBottom){
      p1.outline = p1.curve.outline(this.outlineBottom, this.outlineTop);
    }
  },
  _makeCurve: function (_curPoint, c1,_p2) {
    _curPoint.c = {x: c1.x,y : c1.y};
    _curPoint.curve = Bezier.quadraticFromPoints.apply(this,[_curPoint,c1,_p2]);
    if(this.outlineTop || this.outlineBottom){
      _curPoint.outline = _curPoint.curve.outline(this.outlineTop, this.outlineBottom);
    }
  },
  _update_curve: function (_pointIndex) {
    var _p1 = this.points[_pointIndex ];
    if(!_p1.c){
      return;
    }
    var _p2 = this.points[_pointIndex + 1] || this.points[0];

    if(_p1.c2){
      _p1.curve.points[0].x = _p1.x;
      _p1.curve.points[0].y = _p1.y;
      _p1.curve.points[1].x = _p1.c.x;
      _p1.curve.points[1].y = _p1.c.y;
      _p1.curve.points[2].x = _p1.c2.x;
      _p1.curve.points[2].y = _p1.c2.y;
      _p1.curve.points[3].x = _p2.x;
      _p1.curve.points[3].y = _p2.y;
      _p1.curve.update();
      _p1.outline = _p1.curve.outline(this.outlineBottom, this.outlineTop);
    }else{
      var bezier_pointers = Bezier.getABC(2, _p1, _p1.c, _p2 , 0.5);
      _p1.curve.points[0].x = _p1.x;
      _p1.curve.points[0].y = _p1.y;
      _p1.curve.points[1].x = bezier_pointers.A.x;
      _p1.curve.points[1].y = bezier_pointers.A.y;
      _p1.curve.points[2].x = _p2.x;
      _p1.curve.points[2].y = _p2.y;
      _p1.curve.update();
      if(this.outlineTop || this.outlineBottom) {
        _p1.outline = _p1.curve.outline(this.outlineBottom, this.outlineTop);
      }
    }
  },
  setPoint: function (order, _point) {

    var dragPointer,
      _curvepointer = order[0] == "c",
      pIndex1 = +order.substr(1) - 1,
      p1 = this.points[pIndex1],
      pIndex2 = this.points[pIndex1 + 1] ? pIndex1 + 1 : (this.closed ? 0 : -1),
      p2 = pIndex2 !== -1 && this.points[pIndex2],
      pIndex0 = this.points[pIndex1 - 1] ? pIndex1 - 1 : (this.closed ? this.points.length - 1 : -1),
      p0 = pIndex0 !== -1 && this.points[pIndex0];

    if (this.pointsLimits) {
      _point.x = Math.max(0, Math.min(_point.x, this.width));
      _point.y = Math.max(0, Math.min(_point.y, this.height));
    }
    var _curvepointer2 = order[0] == "d";
    if(_curvepointer2){
      dragPointer =  p1.c2;
    }else{
      if(_curvepointer){

        if(!p1.c){
          this._makeCurve(p1, _point, p2);
        }
        dragPointer = p1.c
      }else{
        dragPointer = p1;
      }
    }
    if(dragPointer){
      dragPointer.x = _point.x;
      dragPointer.y = _point.y;
    }
    for(var i in this.points){
      this.points[i].c && this._update_curve(+i);
    }
    this.updateBbox();
    this.fire("shape:modified");
    this.canvas && this.canvas.renderAll();
  }
};

fabric._.defaults(fabric.BezierMixin,  fabric.ShapeMixin);
