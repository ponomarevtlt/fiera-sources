module.exports = {
  map: function (data, foo) {

    if(data.constructor == Array){
      return Promise.all(data.map(foo))
        .then(function (results, error) {
          if (error) {
            return Promise.reject(error);
          }
          return Promise.resolve(results);
        })
    }

    var keys = Object.keys(data);
    var urls = Object.values(data);
    return Promise.all(urls.map(foo))
      .then(function (results, error) {
        if (error) {
          return Promise.reject(error);
        }
        var _map = {};

        for(var i = 0 ; i < results.length; i++){
          _map[keys[i]] = results[i];
        }
        return Promise.resolve(_map);
      })
  },
  wrap: function (context) {
    return function wrap(foo) {
      return function () {
        var options = Array.prototype.slice.call(arguments,1);
        if (!foo.length) {
          return new Promise(function (resolve, fail) {
            var _result = foo.call(context);
            (_result || _result === undefined) ? resolve() : fail();
          });
        } else {
          return new Promise(foo.bind(context));
        }
      }
    }
  }
};
