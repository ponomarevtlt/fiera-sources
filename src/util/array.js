
let ArrayBuilder = function(array){
  this.array = array;
  this.index = -1;
};
ArrayBuilder.prototype = {
  before(...elements){
    Array.prototype.splice.apply(this.array,[this.index === -1 ? 0: this.index,0].concat(elements));
    return this;
  },
  after(...elements){
    Array.prototype.splice.apply(this.array,[this.index === -1 ? this.array.length: this.index + 1,0].concat(elements));
    return this;
  },
  find(element){
    this.index = this.array.indexOf(element);
    return this;
  }
};

module.exports = {
  insertBefore: function(array,elementAfter,...insertedElement){
    Array.prototype.splice.apply(array,[array.indexOf(elementAfter),0].concat(insertedElement))
    return array;
  },
  build: function (array){
    return new ArrayBuilder(array && array.slice() || [])
  }
};