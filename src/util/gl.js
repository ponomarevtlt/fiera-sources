

fabric.util.gl = {

  getImageCoordianateForPoint: function(points,pointer){
    var p1 = points[0], p2 = points[1], p3 = points[2], p4 = points[3];

    function checkT(t){
      var ax = p1.x + (p2.x - p1.x) * t , ay = p1.y + (p2.y - p1.y) * t,
        bx = p4.x + (p3.x - p4.x) * t , by = p4.y + (p3.y - p4.y) * t;
      var _sign =  (bx - ax) * (pointer.y - ay) - (by - ay) * (pointer.x - ax);
      return _sign > 0;
    }

    function getX(){
      //true слева
      var element = -1;
      var i = 0;

      if(checkT(i)){
        i--;
        while(checkT(i)){
          i--;
          if(i < -5) {
            return -5;
          }
        }
        element = i;
      }else{
        i++;
        while(!checkT(i)){
          i++;
          if(i > 5){
            return 5;
          }
        }
        element = i - 1;
      }

      var precision = 0.5;
      element += precision;
      while(precision > 0.05){
        precision/=2;
        if(checkT(element)){
          element -=precision;
        }else{
          element +=precision;
        }
      }
      return element;
    }
    var t = getX();

    var ax = p1.x + (p2.x - p1.x) * t , ay = p1.y + (p2.y - p1.y) * t,
      bx = p4.x + (p3.x - p4.x) * t , by = p4.y + (p3.y - p4.y) * t;

    var  _y = (pointer.y -ay) / (by - ay) ;

    return {x: t, y : _y };
  },
  getPointForImageCoordianate: function(w,h,points,curving,x,y,doNotUseCurve){
    var curRow = y / h,
      curCol = x / w;
    var p1 = points[0],
      p2 = points[1],
      p3 = points[2],
      p4 = points[3],
      curRowX1 = p1.x + (p4.x - p1.x) * curRow,
      curRowY1 = p1.y + (p4.y - p1.y) * curRow,
      curRowX2 = p2.x + (p3.x - p2.x) * curRow,
      curRowY2 = p2.y + (p3.y - p2.y) * curRow,
      p1x = curRowX1 + (curRowX2 - curRowX1) * curCol,
      p1y = curRowY1 + (curRowY2 - curRowY1) * curCol;

    if(!doNotUseCurve){
      p1y += this._getCurveOffset(curCol,curving).y;
    }else{

      var p1 = points[0],
        p2 = points[1],
        p3 = points[2],
        p4 = points[3];

      if(p2.y == p1.y && p4.y == p3.y) {
        y = p1y;
      }
      if(p3.x == p2.x && p4.x == p1.x) {
        x = p1x;
      }
      if(curRowY2 == curRowY1) {
        curRowY1 = p1y;
      }
    }
    return {x: p1x , y : p1y};
  },
  fixSemiTransparentPixels: function(ctx){

    var imgData = ctx.getImageData(0,0,ctx.canvas.width,ctx.canvas.height);
    for(var i = 3; i < imgData.data.length; i+=4){
      if(imgData.data[i] > 0){
        imgData.data[i] = 255;
      }
    }
    ctx.putImageData(imgData,0,0);
  },
  drawTriangles: function(ctx,triangles){
    ctx.strokeStyle = "black";
    ctx.beginPath();
    var tri;
    for (var i in triangles) {
      tri = triangles[i];
      ctx.moveTo(tri.p0.x, tri.p0.y);
      ctx.lineTo(tri.p1.x, tri.p1.y);
      ctx.lineTo(tri.p2.x, tri.p2.y);
      ctx.lineTo(tri.p0.x, tri.p0.y);
    }
    ctx.stroke();
    ctx.closePath();
  },
  drawTriangle: function(ctx, im, x0, y0, x1, y1, x2, y2, sx0, sy0, sx1, sy1, sx2, sy2,stroke) {
    ctx.save();

    // Clip the output to the on-screen triangle boundaries.
    ctx.beginPath();
    ctx.moveTo(x0, y0);
    ctx.lineTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.closePath();
    if(stroke){
      ctx.stroke();//xxxxxxx for wireframe
    }
    ctx.clip();

    /*
     ctx.transform(m11, m12, m21, m22, dx, dy) sets the context transform matrix.

     The context matrix is:

     [ m11 m21 dx ]
     [ m12 m22 dy ]
     [  0   0   1 ]

     Coords are column vectors with a 1 in the z coord, so the transform is:
     x_out = m11 * x + m21 * y + dx;
     y_out = m12 * x + m22 * y + dy;

     From Maxima, these are the transform values that map the source
     coords to the dest coords:

     sy0 (x2 - x1) - sy1 x2 + sy2 x1 + (sy1 - sy2) x0
     [m11 = - -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sy1 y2 + sy0 (y1 - y2) - sy2 y1 + (sy2 - sy1) y0
     m12 = -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (x2 - x1) - sx1 x2 + sx2 x1 + (sx1 - sx2) x0
     m21 = -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx1 y2 + sx0 (y1 - y2) - sx2 y1 + (sx2 - sx1) y0
     m22 = - -----------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (sy2 x1 - sy1 x2) + sy0 (sx1 x2 - sx2 x1) + (sx2 sy1 - sx1 sy2) x0
     dx = ----------------------------------------------------------------------,
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

     sx0 (sy2 y1 - sy1 y2) + sy0 (sx1 y2 - sx2 y1) + (sx2 sy1 - sx1 sy2) y0
     dy = ----------------------------------------------------------------------]
     sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0
     */

    // TODO: eliminate common subexpressions.
    var denom = sx0 * (sy2 - sy1) - sx1 * sy2 + sx2 * sy1 + (sx1 - sx2) * sy0;
    if (denom == 0) {
      return;
    }
    var m11 = -(sy0 * (x2 - x1) - sy1 * x2 + sy2 * x1 + (sy1 - sy2) * x0) / denom;
    var m12 = (sy1 * y2 + sy0 * (y1 - y2) - sy2 * y1 + (sy2 - sy1) * y0) / denom;
    var m21 = (sx0 * (x2 - x1) - sx1 * x2 + sx2 * x1 + (sx1 - sx2) * x0) / denom;
    var m22 = -(sx1 * y2 + sx0 * (y1 - y2) - sx2 * y1 + (sx2 - sx1) * y0) / denom;
    var dx = (sx0 * (sy2 * x1 - sy1 * x2) + sy0 * (sx1 * x2 - sx2 * x1) + (sx2 * sy1 - sx1 * sy2) * x0) / denom;
    var dy = (sx0 * (sy2 * y1 - sy1 * y2) + sy0 * (sx1 * y2 - sx2 * y1) + (sx2 * sy1 - sx1 * sy2) * y0) / denom;

    ctx.transform(m11, m12, m21, m22, dx, dy);

    // Draw the whole image.  Transform and clip will map it onto the
    // correct output triangle.
    //
    // TODO: figure out if drawImage goes faster if we specify the rectangle that
    // bounds the source coords.
    ctx.drawImage(im, 0, 0);
    ctx.restore();
  },
  drawElement: function(ctx,element,triangles,stroke){

    var tri;
    for (var i in triangles) {
      tri = triangles[i];

      this.drawTriangle(ctx, element,
        +tri.p0.x, tri.p0.y,
        tri.p1.x, tri.p1.y,
        tri.p2.x, tri.p2.y,
        tri.t0.u, tri.t0.v,
        tri.t1.u, tri.t1.v,
        tri.t2.u, tri.t2.v,
        stroke);
    }
  },
  _getCurveOffsetPoint :function(t,curving) {
    var x = (1 - t) * (1 - t) * curving.left.x + 2 * (1 - t) * t * curving.middle.x + t * t * curving.right.x;
    var y = (1 - t) * (1 - t) * curving.left.y + 2 * (1 - t) * t * curving.middle.y + t * t * curving.right.y;
    return {x: x, y: y};
  },
  _getCurveOffset: function(t,curving) {
    var p = this._getCurveOffsetPoint(t,curving);
    p.y -= curving.left.y;
    return p;
  },
  calculateGeometry : function(points,imgW,imgH, subs, divs, perspective, curving) {
    // clear _triangles out
    var _triangles = [];
    // generate subdivision

    var p1 = new fabric.Point(points[0].x,points[0].y);
    var p2 = new fabric.Point(points[1].x,points[1].y);
    var p3 = new fabric.Point(points[2].x,points[2].y);
    var p4 = new fabric.Point(points[3].x,points[3].y);

    var dx1 = p4.x - p1.x;
    var dy1 = p4.y - p1.y;
    var dx2 = p3.x - p2.x;
    var dy2 = p3.y - p2.y;

    if(perspective) {
      //1/2
      var _perspectiveY = p2.distanceFrom(p3) / p1.distanceFrom(p4) ;
      var _perspectiveX = p1.distanceFrom(p2) / p4.distanceFrom(p3);
      var _totalDistanceX = 0, _totalDistanceY = 0;
      if (_perspectiveX < 1) _perspectiveX = 1 / _perspectiveX;
      if (_perspectiveY < 1) _perspectiveY = 1 / _perspectiveY;

      _perspectiveX = Math.sqrt(_perspectiveX);
      _perspectiveY = Math.sqrt(_perspectiveY);

      var lastX = 1;
      for (var sub = 0; sub < subs; ++sub) {
        _totalDistanceX += lastX;
        lastX *= _perspectiveX;
      }
      var lastY = 1;
      for (var sub = 0; sub < subs; ++sub) {
        _totalDistanceY += lastY;
        lastY *= _perspectiveY;
      }
    }
    var curRow, nextRow = 0, curRowTex, nextRowTex = 0;
    lastX = 1;
    for (var sub = 0; sub < subs; ++sub) {
      curRow = nextRow;
      curRowTex = nextRowTex;

      nextRowTex = (sub + 1) / subs;

      if(perspective){
        nextRow = curRow + lastX / _totalDistanceX;
        lastX *= _perspectiveX;
      }else{
        nextRow = nextRowTex;
      }

      var curRowX1 = p1.x + dx1 * curRow;
      var curRowY1 = p1.y + dy1 * curRow;

      var curRowX2 = p2.x + dx2 * curRow;
      var curRowY2 = p2.y + dy2 * curRow;

      var nextRowX1 = p1.x + dx1 * nextRow;
      var nextRowY1 = p1.y + dy1 * nextRow;

      var nextRowX2 = p2.x + dx2 * nextRow;
      var nextRowY2 = p2.y + dy2 * nextRow;

      var curCol, nextCol = 0, curColTex, nextColTex = 0;
      lastY = 1;
      for (var div = 0; div < divs; ++div) {

        curCol = nextCol;
        curColTex  = nextColTex;

        nextColTex = (div + 1) / divs;

        if(perspective){
          nextCol = curCol + lastY / _totalDistanceY;
          lastY *= _perspectiveY;
        }else{
          nextCol = nextColTex;
        }

        var dCurX = curRowX2 - curRowX1;
        var dCurY = curRowY2 - curRowY1;
        var dNextX = nextRowX2 - nextRowX1;
        var dNextY = nextRowY2 - nextRowY1;

        var p1x = curRowX1 + dCurX * curCol ;
        var p1y = curRowY1 + dCurY * curCol + fabric.util.gl._getCurveOffset(curCol,curving).y;

        var p2x = curRowX1 + (curRowX2 - curRowX1) * nextCol;
        var p2y = curRowY1 + (curRowY2 - curRowY1) * nextCol + fabric.util.gl._getCurveOffset(nextCol,curving).y;

        var p3x = nextRowX1 + dNextX * nextCol;
        var p3y = nextRowY1 + dNextY * nextCol  + fabric.util.gl._getCurveOffset(nextCol,curving).y;

        var p4x = nextRowX1 + dNextX * curCol;
        var p4y = nextRowY1 + dNextY * curCol  + fabric.util.gl._getCurveOffset(curCol,curving).y;

        var u1 = curColTex * imgW;
        var u2 = nextColTex * imgW;
        var v1 = curRowTex * imgH;
        var v2 = nextRowTex * imgH;

        var triangle1 = {
          p0: {x: p1x, y: p1y},
          p1: {x: p3x, y: p3y},
          p2: {x: p4x, y: p4y},
          t0: {u: u1, v: v1},
          t1: {u: u2, v: v2},
          t2: {u: u1, v: v2}
        };

        var triangle2 = {
          p0: {x: p1x , y: p1y},
          p1: {x: p2x , y: p2y},
          p2: {x: p3x , y: p3y},
          t0: {u: u1  ,v: v1},
          t1: {u: u2  ,v: v1},
          t2: {u: u2  ,v: v2}
        };

        _triangles.push(triangle1);
        _triangles.push(triangle2);
      }
    }
    return _triangles;
  }
};
