var util = {

  splitBy: function (query, delimiter) {
    let traceQueries = [];
    let r = 0,
      f = 0,
      _p_start = 0;
    if (query == "") return [];
    for (let i = 0; i < query.length; i++) {
      let c = query[i];
      if (c == "(") {
        r++;
        f = 1;
      } else if (c == ")") {
        r--;
      }
      if (r == 0 && f == 1) f = 0;
      if (delimiter.indexOf(c) != -1 && r == 0 && f == 0) {
        traceQueries.push(query.substring(_p_start, i));
        _p_start = i + 1;
      }
    };
    traceQueries.push(query.substring(_p_start));
    return traceQueries;
  }
};