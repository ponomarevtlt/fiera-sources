
 let utils = {
  load: function (url, dataType, callback_success, callback_error) {
    var fs = require("fs");
    try {
      var data = fs.readFileSync(url, 'utf8');
      data = data.replace(/^\uFEFF/, '');
      var _parsed = utils.parseData(data, dataType);
      if (_parsed.status == "error") {
        callback_error({
          status: "error",
          message: _parsed.message,
          data: _parsed.data
        });
        return;
      }
      callback_success(_parsed.data);
    } catch (e) {

      if (e.code === 'ENOENT') {
        console.log('File not found!');
      } else {
        throw e;
      }
      callback_error(data);
    }
  }
};
module.exports = utils;
