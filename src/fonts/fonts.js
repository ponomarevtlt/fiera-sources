/**
 * @name FontNotation
 * @property name {string} default file
 * @property d {string} file for bold font style
 * @property i {string} file for italic font styleset
 * @property z {string} file for bold italic font style
 */

if(!fabric.isLikelyNode){
  if(window.FontFaceObserver){
    fabric.FontFaceObserver = window.FontFaceObserver;
  }else{
    fabric.FontFaceObserver = require("./../plugins/fontfaceobserver");
  }
}
if(fabric.Editor){
  fabric.util.object.extend(fabric.Editor.prototype, {
    uploadFont(options) {
      let uploader = this.fontsUploader || this.uploader || {};
      options = options || {};
      options.accept = "font/ttf";

      if (!uploader.callback && !options.callback) {
        options.callback = function (image, data, canvas) {
          //console.log(image, data, canvas);
        }.bind(this)
      }
      if (!uploader.upload && !options.upload) {
        options.upload = function (file, data, callback) {
          fabric.util.readImageFile(file, img => {
            callback(img, options.data, options.canvas);
        });
        };
      }
      this.uploadFiles(fabric._.extend({}, uploader, options));
    }
  });


  fabric.util.object.extend(fabric.Editor.prototype, {
    postloaders: fabric.Editor.prototype.preloaders.concat([
      function(options,callback) {
        //load all fallbacks
        if(fabric.isLikelyNode){
          fabric.fonts.registerNodeCanvasFonts(this._usedFonts).then(callback);
        }else{
          callback();
        }
      }
    ]),
    preloaders: fabric.Editor.prototype.preloaders.concat([
      function(options,callback) {
        this.fonts = [];
        this._usedFonts = fabric.fonts.getUsedFonts(options.prototypes)
          .concat(fabric.fonts.getUsedFonts(options.slide))
          .concat(fabric.fonts.getUsedFonts(options.slides));

        if(fabric.isLikelyNode){
          fabric.fonts.registerNodeCanvasFonts(this._usedFonts).then(callback);
        }else{

          fabric.fonts.loadFonts(this._usedFonts, ()=> {
            callback();
        });

        }
      }
    ]),
    addUsedFonts(values){
      values.forEach(ff => this.addUsedFont(ff));
    },
    addUsedFont(valueName){
      if(this._usedFonts.indexOf(valueName) === -1){
        this._usedFonts.push(valueName);
      }
    },
    /**
     *
     * @param options
     * @param options.nativeFontsSourceRoot
     * @param options.native {FontNotation | String}
     * @param callback
     */
    setFonts: function (options, callback) {

      /*if(options.constructor === Object){
        let FO = options.fonts;
        delete options.fonts;
        if(FO) {
          let nativeFamilies = FO.native && fabric.fonts.getFontsFamilyList(FO.native) || [];
          let googleFamilies = FO._googleFamilies = FO.google && fabric.fonts.getFontsFamilyList(FO.google) || [];
          let customFamilies = FO._customFamilies = FO.custom && fabric.fonts.getFontsFamilyList(FO.custom) || [];
          this.fonts = nativeFamilies.concat(googleFamilies).concat(customFamilies).sort();
        }else {
          FO = {
            _googleFamilies: [],
            _customFamilies: [],
          };
          this.fonts = []
        }

        fabric.fonts.linkCustomFonts(FO.custom);

        let url = fabric.fonts.buildGoogleUrl(FO._googleFamilies);
        $(`<style href="${url}" rel="stylesheet"></style>`).appendTo(document.head);
        callback();
      }else{*/

      this.fonts = [].concat(options);
      let fontsToCheck = [].concat(options).concat(this._usedFonts);

      fabric.fonts.loadFonts(fontsToCheck, ()=> {
        this.updateFontsOnSlides();
      callback();
    });
    },
    updateFontsOnSlides: function () {
      fabric.util.clearFabricFontCache();
      if (!this.slides) return;
      for (let slide of this.slides) {
        let els = slide.find({type: "text"})
          .concat(slide.find({type: "textbox"}))
          .concat(slide.find({type: "i-text"}));

        for (let el of els) {
          el._forceClearCache = true;
        }
        slide.renderAll();
        for (let el of els) {
          el.dirty = true;
        }
        slide.renderAll();
      }
    },
    fonts: [
      'Arial',
      'Arial Black',
      'Comic Sans MS',
      'Courier New',
      'Georgia',
      'Impact',
      'Lucida Console',
      'Tahoma',
      'Times New Roman',
      'Geneva',
      'monospace',
      'cursive'
    ]
  });
}


fabric.fonts = {

  _formatFontVariations(font){
    for (let fvName in font.variations) {
      font.variations[fvName] = {
        src: font.variations[fvName],
        active: false
      };
    }
    font.range = new RegExp("[" + font.range + "]");
    return font;
  },
  /**
   * грузит шрифты из списка fontsToCheck
   * грузит с помозщью нескольких методов.
   * если  указаны данные о шрифтах fabric.fonts.info.locals
   * то будет использовать этот объект для построения стилей самостоятельно.
   * далее стили для остальных шрифтов попробует загрузить с сервера.
   * По умолчанию - сервер Google fonts.
   * Но так же можно указать  локальный сервер, со скриптом из fiera.express.js в виде /fonts?families=family1|family2
   */
  loadFonts (fontsToCheck,callback) {

    function waitForWebfontsTobeLoaded(fontFamilies){
      let observers = [];

      for(let family of fontFamilies ){
        for(let fvName in fabric.fonts.registry[family].variations ){
          let variation = fabric.fonts.registry[family].variations[fvName];
          let fvOptions = fabric.fonts._readFontVariantName(fvName);
          let testString = fabric.fonts._getTestStringByUnicodeRange(fabric.fonts.registry[family].range);

          variation.observer = new fabric.FontFaceObserver(family,fvOptions)
            .load(testString)
            .then((font) => {
            if(fabric.fonts.debug){
              console.log(`Font loaded: ${family} ${fvOptions.style} ${fvOptions.weight}`);
            }
          variation.active = true;
          delete variation.observer;
          fabric.util.clearFabricFontCache(family);
        });
          observers.push(variation.observer)
        }
      }

      Promise.all(observers).then(fonts => {
        callback();
    }).catch(function (err) {
        console.warn('Some font are not available:', err);
        callback();
      });
    }

    let fontsToLoad = this._excludeAvailableFonts(fontsToCheck),
      localFonts = [],
      remoteFonts = [];

    for(let i = fontsToLoad.length; i--; ){
      if(fabric.fonts.info.locals && fabric.fonts.info.locals[fontsToLoad[i]]){
        localFonts.push(fontsToLoad[i]);
      }else if(this.fontsURL){
        remoteFonts.push(fontsToLoad[i]);
      }else{
        console.warn(`font will not be loaded: ${fontsToLoad[i]}`);
      }
    }
    if(!localFonts.length && !remoteFonts.length){
      callback();
      return;
    }

    if(localFonts.length){
      let styles = this.getFontsStyles(localFonts,!!this.fontsURL);
      $(`<style id="fiera-local-fonts" rel="stylesheet">${styles}</style>`).appendTo(document.head);

      for(let ff of localFonts ){
        fabric.fonts.registry[ff] = this._formatFontVariations(fabric.fonts.info.locals[ff]);
      }
      if(!remoteFonts.length){
        waitForWebfontsTobeLoaded(localFonts);
      }
    }

    if(remoteFonts.length) {
      let url = `${this.fontsURL}${remoteFonts.join("%7C").replace(/\s/g, "+")}`;
      fabric.util.data.load(url, "text", styles => {
        $(`<style rel="stylesheet">${styles}</style>`).appendTo(document.head);

      let parsedFonts = fabric.fonts.readFontCSS(styles);
      let fontsObjects = {};
      for (let family in parsedFonts) {
        parsedFonts[family].forEach(obj => {
          if (!fontsObjects[family]) {
          fontsObjects[family] = {
            variations: {},
            range: obj.range,
            comment: obj.comment
          }
        }
        fontsObjects[family].variations[obj.weight + (obj.style === "italic" ? "i" : "")] = obj.src;
      })
      }
      for (let ff in fontsObjects) {
        fabric.fonts.registry[ff] = this._formatFontVariations(fontsObjects[ff]);
      }
      waitForWebfontsTobeLoaded(localFonts.concat(remoteFonts));
    }, (e) => {
        console.error(e);
        callback();
      });
    }

  },
  _testEl: null,
  _testDataURL: null,
  isFontAvailable(fontFamily) {

    //Times New Roman is standart serif font
    if (fontFamily === "Times New Roman") {
      return true;
    }
    let ctx;
    if(!this._testDataURL){
      this._testEl = document.createElement("canvas");
      this._testEl.width = 400;
      this._testEl.height = 50;
      ctx = this._testEl.getContext("2d");
      ctx.font = "50px serif";
      ctx.fillText("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0, 50);
      this._testDataURL = this._testEl.toDataURL();
    }else{
      ctx = this._testEl.getContext("2d");
    }
    ctx.clearRect(0, 0, this._testEl.width, this._testEl.height);
    ctx.font = "50px " + fontFamily;
    ctx.fillText("ABCDEFGHIJKLMNOPQ", 0, 50);
    let dataURL2 = this._testEl.toDataURL();
    return dataURL2 !== this._testDataURL;
  },
  _excludeAvailableFonts(fontsToCheck) {
    let fontsToLoad = [];

    for (let fontFamily of fontsToCheck) {

      if(!this.isFontAvailable(fontFamily)){
        fontsToLoad.push(fontFamily);
        if(fabric.fonts.debug){
          console.log(`${fontFamily} will be loaded`)
        }
      }
    }
    return fontsToLoad;
  },
  fontsURL: "https://fonts.googleapis.com/css?family=",
  // fontsURL: "/fonts?families=",
  // buildGoogleUrl: function(fontFamilies_,subsets,text) {
  //   let sb = [];
  //   for (let i = 0; i < fontFamilies_.length; i++) {
  //     sb.push(fontFamilies_[i].replace(/ /g, '+'));
  //   }
  //
  //   let url = 'https://fonts.googleapis.com/css?family=' + sb.join('%7C');
  //   if (subsets) {
  //     url += "&subset="+ subsets.join(",");
  //   }
  //   if (text) {
  //     url += '&text=' + encodeURIComponent(text);
  //   }
  //   return url;
  // },
  fontsSourceRoot: "/fonts/",
  getFontsStyles(families,noWarnings) {
    let stylesCSS = '';

    families.forEach(family => {
      if (fabric.fonts.info.locals && fabric.fonts.info.locals[family]) {
      fabric.fonts.readNotation(fabric.fonts.info.locals[family], (_, style, weight, file) => {
        let fontURL = `${this.fontsSourceRoot}${file}`;

      let styleName = this.getFontWeightName(weight);
      if(style === "italic"){
        if(weight === 400){
          styleName = "Italic"
        }else{
          styleName += " Italic";
        }
      }

      stylesCSS += `/* custom */
@font-face {
  font-family: '${family}';
  font-style: ${style};
  font-weight: ${weight};
  src:  local('${family} ${styleName}'), local('${family.replace(/ /g, "")}-${styleName.replace(/ /g, "")}'), url(${fontURL}) format('truetype');
}\n`;
    });
      return;
    }
    if (fabric.fonts.info.google && fabric.fonts.info.google[family]) {
      fabric.fonts.readNotation(fabric.fonts.info.google[family], (_, style, weight, file) => {
        let fontURL = `${fabric.fonts.googleFontsCdnURL}${file}`;
      let styleName = this.getFontWeightName(weight);
      if(style === "italic"){
        if(weight === 400){
          styleName = "Italic"
        }else{
          styleName += " Italic";
        }
      }

      stylesCSS += `/* google */
@font-face {
  font-family: '${family}';
  font-style: ${style};
  font-weight: ${weight};
  src:  local('${family} ${styleName}'), local('${family.replace(/ /g, "")}-${styleName.replace(/ /g, "")}'), url(${fontURL}) format('truetype');
}\n`;
    });
    }
    else if(!noWarnings){
      console.warn(`font family ${family} was not found`)
    }
  });

    return stylesCSS;
  },
  info: {
    locals: null,
    google: null
  },
  debug: false,
  getUsedFonts(data){
    if(!data)return [];
    let usedFonts = [];
    fabric.util.data.recoursive(data,(propertyName, valueName, object) => {
      if(propertyName === "fontFamily"){
      if(usedFonts.indexOf(valueName) === -1){
        usedFonts.push(valueName);
      }
    }
  });
    return usedFonts;
  },
  nativeFonts: ["Times New Roman","SimSun","Kartika","SimHei","Arial","Lucida Console","Tahoma","Arial Black","Comic Sans MS","Courier New","Georgia","Impact"],
  readFontCSS(styles){
    let CSSJSON = require("./../plugins/cssjson");
    let json = CSSJSON.toJSON(styles,{ordered: true, comments: true});
    let comment = "";
    let parsed = {};
    Object.values(json).forEach(el => {
      if(!el.value){
      comment = el.replace(/^\/\*/,"").replace(/\*\/$/,"").trim();
      return;
    }
    let obj = {};
    Object.values(el.value).forEach(prop => {
      obj[prop.name] = prop.value;
  });

    let ff = obj["font-family"].replace(/'/g,"");
    if(!parsed[ff]){
      parsed[ff] = [];
    }
    el = {
      comment: comment,
      style: obj["font-style"],
      weight:  obj["font-weight"],
      src: /url\(([^\)]+)/.exec(obj["src"])[1],
    };
    if(obj["unicode-range"]){
      el.range = obj["unicode-range"].replace(/\s/g,"").replace(/U\+([0-F]+)\-([0-F]+)/g,"U+$1-U+$2").replace(/U\+/g,"\\u");
    }
    parsed[ff].push(el)
  });
    return parsed;
  },
  registry: {},
  getFontVariation(family, weight, style){
    let regFont = fabric.fonts.registry[family];
    if(!regFont)return null;
    let variation = regFont.variations[weight + (style === "italic" ? "i" : "")];
    if(variation){
      return {style: style, weight: weight, variation: variation};
    }

    if(style !== "normal"){
      return fabric.fonts.getFontVariation(family, weight, "normal")
    }
    else if(weight !== 400){
      return fabric.fonts.getFontVariation(family, 400, "normal")
    }
    else{
      variation = regFont.variations[Object.keys(regFont.variations)[0]];
      return {style: style, weight: weight, variation: variation};
    }
  },
  _getTestStringByUnicodeRange(s){
    let re = /\u([0-F]+)/g;
    let test = "";
    let m;
    do {
      m = re.exec(s);
      if (m) {
        test += String.fromCodePoint(parseInt(m[1], 16)) ;  ///String.fromCodePoint ;.toString(16)
      }
    } while (m);
    return test;
  },
  definitions : {
    Thin: 100,
    ExtraLight:200,
    Light: 300,
    Regular: 400,
    Normal: 400,
    Medium: 500,
    SemiBold:  600,
    Bold: 700,
    ExtraBold:  800,
    Black: 900
  },
  // readNotations: function(options,functor) {
  //   options.native && options.native.forEach(fontNotation => {
  //     fabric.fonts.readNotation(fontNotation,(family,style,weight,file)=>{
  //       functor(family,style,weight,this.nativeFontsSourceRoot + file);
  //     })
  //   });
  //   options.google && options.google.forEach(fontNotation => {
  //     fabric.fonts.readNotation(fontNotation,(family,style,weight,file)=>{
  //       functor(family,style,weight,this.googleFontsSourceRoot + file);
  //     })
  //   });
  //   options.custom && options.custom.forEach(fontNotation => {
  //     fabric.fonts.readNotation(fontNotation,(family,style,weight,file)=>{
  //       functor(family,style,weight,this.customFontsSourceRoot + file);
  //     })
  //   });
  // },
  getFontWeightName (number){
    for(var name in this.definitions){
      if(this.definitions[name] === +number){
        return name;
      }
    }
    return number;
  },
  _readFontVariantName(key){
    let style = "normal";
    let weight = "400";
    key = key.toLowerCase();
    if (key.indexOf("i") !== -1) {
      key = key.replace("i", "");
      style = "italic";
    }
    if (key.indexOf("italic") !== -1) {
      key = key.replace("italic", "");
      style = "italic";
    }
    weight = key || "normal";
    if(fabric.fonts.definitions[weight]){
      weight = "" + fabric.fonts.definitions[weight];
    }
    return {style: style,weight: weight}
  },
  readNotation (fontNotation, callback) {

    let variations = {};

    if(fontNotation.constructor === String){
      let fs = require("fs");
      let filename = fontNotation;
      fontNotation = {};
      if(filename.indexOf("*") === -1){
        if(fs.existsSync(fabric.fonts.fontsSourceRoot + filename)){
          variations = {"400": filename};
        }
      }else{
        let fvSuffixes = {"400": ["","r","400"], "400i" : ["i","400i"], "700": ["b","bd","700"],  "700i": ["bi","z","700i"]};
        for(let fvName in fvSuffixes){
          for(let suffix of fvSuffixes[fvName]){
            let fvFilename = filename.replace("*",suffix);
            if(fs.existsSync(fabric.fonts.fontsSourceRoot + fvFilename)){
              variations[fvName] = fvFilename;
              break;
            }
          }
        }
      }
    }else if(fontNotation.variations){
      variations = fontNotation.variations
    }else{
      if(fontNotation.r)variations["400"] = fontNotation.r;
      if(fontNotation.b)variations["700"] = fontNotation.b;
      if(fontNotation.i)variations["400i"] = fontNotation.i;
      if(fontNotation.bi)variations["700i"] = fontNotation.bi;
    }

    for(let fvName in variations){
      let fvOptions = fabric.fonts._readFontVariantName(fvName);
      let src = variations[fvName];
      if (!/(\.\w+$)/.test(src)) {
        src += ".ttf";
      }
      callback(fontNotation.name, fvOptions.style, fvOptions.weight, src);
    }
  },

  /**
   * For loading TTF/WOFF
   * @param url  <http:location_of_your_ttf_file>
   */
  loadFontFileAsBuffer: function(url) {
    return new Promise( (resolve, fail)=> {
      let type;
    let match = (/[.]/.exec(url)) ? /[^.]+$/.exec(url) : undefined;
    type = match && match[0] || "ttf";
    let request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === 4 && request.status===200) {
        let buf;
        // if(type === 'woff' || type === 'woff2' ){
        // let WORF = require("./../plugins/worf");
        //   buf =_base64ToArrayBuffer(btoa(WORF.Converter.woffToSfnt(request.responseText)));
        // }
        buf = request.response;
        resolve(buf);
      }
    };
    request.open('GET', url, true);
    // if (type === 'woff' || type === 'woff2') {
    //   request.overrideMimeType('text/plain; charset=x-user-defined');
    // }
    request.responseType = "arraybuffer";
    request.send(null);
  })
  },
  loadBinaryFonts (fontFamilies) {
    let promises = [];
    for(let ff of fontFamilies){
      let fontVariations = fabric.fonts.registry[ff];

      for(let fv of fontVariations){
        let url = fv.src;
        if (url.indexOf('blob') !== 0 && url.indexOf('data') !== 0 && url.indexOf('://') === -1 && !url.startsWith('./')) {
          url = "./fonts/" + url;
        }
        promises.push(fabric.fonts.loadFontFileAsBuffer(url).then(buf => {
          fv.buffer = buf;
      }))
      }
    }
    return Promise.all(promises).catch(function(err) {
      console.warn('Some font are not available:', err);
    });
  },
  linkCustomFonts(fonts){
    if (fonts) {
      if (fonts.length) {
        let newStyle = document.createElement('style');
        for (let fontNotation of fonts) {
          fabric.fonts.readNotation(fontNotation, (family, style, weight, file) => {
            newStyle.appendChild(document.createTextNode(`
            @font-face {
              font-family: '${family}';
              font-style: ${style};
              font-weight: ${weight};
              src: url('${this.customFontsSourceRoot}${file}') format('truetype');
            }`
          ));
        });
        }
        document.head.appendChild(newStyle);
      }
    }
  },


  /**
   * @typedef {String} FontVariationsString
   * relative to FontsDirectory.
   * possible values: "subDirectory/fontFamily.ttf" //path to single varitaion fonts
   * possible values: "subDirectory/fontFamily*.ttf" // to multiple varitaions fonts
   * font Files could have names :
   *  fontFamily.ttf or fontFamilyr.ttf for regular font
   *  fontFamilyb.ttf for bold font
   *  fontFamilyi.ttf for italic font
   *  fontFamilybi.ttf for bold italic font
   *  fontFamily[fontWeight][i].ttf for fontswith specified weight and stylefor example, fontFamily-100.ttf or fontFamily-700i.ttf
   */

  /**
   * @typedef {object | FontVariationsString} FontNotation
   * @property {String} family
   * @property {String} category  Category from Google Fonts "handwriting" etc
   * @property {Object<String,String>} variations. keys - 100-900[i], values - url to .TTF font.
   *
   * @property b {string} file for bold font style
   * @property i {string} file for italic font styleset
   * @property bi {string} file for bold italic font style
   *
   */


  /**
   * @name FontNotation
   * @property name {string} default file
   *
   **/
  getFontsFamilyList (fontNotationsArray) {
    let _families = [];
    for (let fontNotation of fontNotationsArray) {
      if (fontNotation.constructor === String) {
        _families.push(fontNotation);
      }
      else {
        _families.push(fontNotation.name);
      }
    }
    return _families;
  }
};
//https://fontdrop.info/