
var utils = require('./../util/util.js');
utils.object = require('./../util/object.js');


/***********************************************************************************************************************
 ** DPHistory
 ***********************************************************************************************************************/


var DPHistory = function (application, initAction) {
    this.application = application;
    this.clear(initAction);
};

DPHistory.prototype.setRecords = function (records,current) {
  this.canUndo = records.length;
  this.canRedo = false;

  if(!records.length){
    records = [{
      type: 'initialized',
      id: 0,
      text : 'initialized'
    }]
  }
  this.records = records;
  this.length = this.records.length ;
  this.current = current === undefined ? records.length - 1 : current;
  this.activeAction = this.records[this.current];
  this.fire("changed",{action: this.activeAction});
  return this;
};  

DPHistory.prototype.restore = function () {
  this.setRecords(this.saved.records,this.saved.current);
  return this;
};

DPHistory.prototype.save = function () {
  this.saved = {
    current: this.current,
    records: utils.deepClone(this.records)
  };
  return this;
};

DPHistory.prototype.clear = function (initAction) {
    if (initAction) {
        initAction.id = 0;
    } else {
        initAction = {
            type: 'initialized',
            id: 0,
          text : 'initialized'
        }
    }
    this.records = [initAction];
    this.current = 0;
    this.canUndo = false;
    this.canRedo = false;
    this.activeAction = this.records[this.current];
  this.fire("changed",{action: this.activeAction});
  return this;
};


DPHistory.prototype.add = function(action){

    if (!this.enabled || this.processing) {
      return false;
    }
    action.moment = new Date().getTime();
    this.canUndo = true;
    this.canRedo = false;
    this.records.splice(this.current+ 1);
    this.records.push(action);
    this.length = this.records.length;
    action.id = this.length - 1;
    action.text = action.type || action.text;
    this.current = this.length - 1;

  this.activeAction = this.records[this.current];
  this.fire("changed",{action: action});
  return this;
};
DPHistory.prototype.disable = function(){
  this.enabled = false;
  return this;
};
DPHistory.prototype.enable = function(){
  this.enabled = true;
  return this;
};
DPHistory.prototype.undo = function(noFire){
    this.canRedo = true;
    var _action = this.records[this.current];
    this.current--;
  this.processing = true;
    _action.undo.call(this.application,_action);
  this.processing = false;
    if(this.current == 0){
        this.canUndo = false;
    }
    if(!noFire){
      this.activeAction = this.records[this.current];
      this.fire("changed",{action: _action});
    }
  return this;
};

DPHistory.prototype.goto = function(index){
    if(index == this.current)return;
    if(index < this.current){
        for(var i = this.current - index ;i--; ){
            this.undo(true);
        }
    }if(index > this.current){
        for(var i = index - this.current ;i--; ){
            this.redo(true);
        }
    }
  this.activeAction = this.records[this.current];
  this.fire("changed",{action: this.activeAction});
  return this;
};

DPHistory.prototype.redo = function(noFire){
    if(this.current == this.length - 1){
        return;
    }
  this.processing = true;
    this.canUndo = true;
    this.current++;
    var _action = this.records[this.current];

    _action.redo.call(this.application,_action);

    if(this.current == this.length - 1){
        this.canRedo = false;
    }
  this.processing = false;
  if(!noFire) {
    this.activeAction = this.records[this.current];
    this.fire("changed",{action: _action});
  }
  return this;
};
utils.observable(DPHistory.prototype);
module.exports = DPHistory;
