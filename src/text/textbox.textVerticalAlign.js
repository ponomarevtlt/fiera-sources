
fabric._.extend(fabric.Textbox.prototype, {
  stateProperties: fabric.Textbox.prototype.stateProperties.concat(["textVerticalAlign"]),
  _dimensionAffectingProps: fabric.Textbox.prototype._dimensionAffectingProps.concat(["textVerticalAlign"]),
  textVerticalAlign: false, //"top","middle","bottom"
  setTextVerticalAlign(value){
    if (this.canvas && this.canvas.stateful) {
      this.saveState();
    }
    this.textVerticalAlign = value;

    this.fire("modified", {});
    if (this.canvas) {
      this.canvas.fire("object:modified", {target: this});
      this.canvas.renderAll();
    }
  },
  _textHeight: 0,
  /**
   * @private
   * @return {Number} Top offset
   */
  _getTopOffset: function() {
    let offset = 0;
    let h2 = -this.height / 2;
    if(!this.textVerticalAlign) {
      return h2;
    }
    offset = this.height - this._textHeight;

    switch (this.textVerticalAlign) {
      case "top":
        return h2;
      case "middle":
        return h2 + offset / 2;
      case "bottom":
        return h2 + offset;
    }
  },
  /**
   * Unlike superclass's version of this function, Textbox does not update
   * its width.
   * @private
   * @override
   */
  initDimensions: function () {
    if (this.__skipDimension) {
      return;
    }
    this.isEditing && this.initDelayedCursor();
    this.clearContextTop();
    this._clearCache();
    // clear dynamicMinWidth as it will be different after we re-wrap line
    this.dynamicMinWidth = 0;
    // wrap lines
    this._styleMap = this._generateStyleMap(this._splitText());
    // if after wrapping, the width is smaller than dynamicMinWidth, change the width and re-wrap
    if (this.dynamicMinWidth > this.width) {
      this._set('width', this.dynamicMinWidth);
    }
    if (this.textAlign.indexOf('justify') !== -1) {
      // once text is measured we need to make space fatter to make justified text.
      this.enlargeSpaces();
    }
    // clear cache and re-calculate height

    this._textHeight = this.calcTextHeight();

    //@edited
    if(!this.textVerticalAlign || this.height < this._textHeight){
      this.height = this._textHeight;
    }
    this.saveState({propertySet: '_dimensionAffectingProps'});
  },

  /**
   * @private
   */
  _getSVGLeftTopOffsets: function() {
    return {
      textLeft: -this.width / 2,
      textTop: this._getTopOffset(),//edited
      lineTop: this.getHeightOfLine(0)
    };
  },
  actions: fabric._.extend(fabric.Textbox.prototype.actions,{

    textVerticalAlignMiddle: {
      title: "@text.alignMiddle",
      option: 'middle',
      value: "textVerticalAlign",
      className: 'fa fa-align-center fa-rotate-90'
    },
    textVerticalAlignTop: {
      title: "@text.alignTop",
      option: 'top',
      value: "textVerticalAlign",
      className: 'fa fa-align-left  fa-rotate-90'
    },
    textVerticalAlignBottom: {
      title: "@text.alignBottom",
      option: 'bottom',
      value: "textVerticalAlign",
      className: 'fa fa-align-right fa-rotate-90'
    },
    textVerticalAlign: {
      type: 'options',
      title: "@text.verticalAlign",
      value: "textVerticalAlign",
      menu: ["textVerticalAlignTop","textVerticalAlignMiddle","textVerticalAlignBottom"]
    }
  })
});