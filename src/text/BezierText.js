'use strict';

//для отрисовки фона
// require("./../shapes/polyline");
require("./../mixins/bezierMixin");
// require("./../mixins/cacheMixin");

//fabric.require("BezierText",["BezierMixin"], function() {
  //fabric.IText,
  fabric.BezierText = fabric.util.createClass(fabric.IText,fabric.BezierMixin, {
    type: 'bezier-text',
    bezierControls: true,
    backgroundColor: false,
    backgroundStroke: false,
    outlineTop: 0,
    outlineBottom: 0,
    // outlineTop: 40,
    // outlineBottom: 0,
    stateProperties: fabric.IText.prototype.stateProperties.concat(["points" , "backgroundStroke" ]),
    initialize: function (text, options) {
      this.ctx = fabric.util.createCanvasElement().getContext('2d');
      this.text = text;
      if(!this.outlineTop){
        options.outlineTop = options.outlineTop || options.fontSize || this.fontSize;
      }
      if(!this.outlineBottom){
        options.outlineBottom = options.outlineBottom || ((options.fontSize || this.fontSize) / 4);
      }
      options || ( options = {});
      this.callSuper('initialize',text,  options);
      //this.curve = new (Function.prototype.bind.apply(Bezier, options.points));

//todo
//      if(!this.styles || _.isEmpty(this.styles)){
//        var style = this.getCurrentCharStyle(0,0);
//        style.strokeWidth++;
//        this.styles = {0: {0: style}};
//      }
//       this.initBezier();




      options.points = options.points || [
          {
            x: 0,
            y: this.fontSize,
            c: {
              x: options.width * 0.3,
              y: this.fontSize + 0.5
            },
            c2: {
              x: options.width * 0.7,
              y: this.fontSize + 0.5
            }
          },
          {
            x: options.width,
            y: this.fontSize
          },
        ];


      if(!this.styles[0]){
        this.styles[0] = {};
      }
      if(!this.styles[0][0]){
        this.styles[0][0] = this.getCurrentCharStyle(0,0);
      }



      this.setPoints(options.points);
      this.on("added",function(){
        this.updateBbox();
      })
    },
    _getTextWidth: function(ctx){
      return this.width;
    },
    _getTextHeight: function(ctx){
      return this.height;
    },
    _getTopOffset: function(){
      return 0;
    },
    _getLeftOffset: function(){
      return 0;
    },

    _clearTextArea: function(ctx) {
      // we add 4 pixel, to be sure to do not leave any pixel out
      var width = this.width + 4, height = this.height + 4;
      ctx.clearRect(-width / 2, -height / 2, width, height);
    },
    /**
     * Renders text selection
     * @param {Array} chars Array of characters
     * @param {Object} boundaries Object with left/top/leftOffset/topOffset
     * @param {CanvasRenderingContext2D} ctx transformed context to draw on
     */
    renderSelection: function(chars, boundaries, ctx) {

      ctx.fillStyle = this.selectionColor;

      var start = this.get2DCursorLocation(this.selectionStart),
        end = this.get2DCursorLocation(this.selectionEnd),
        startLine = start.lineIndex,
        endLine = end.lineIndex;


      var _start_char_index = Math.max(start.charIndex,0);
      var _end_char_index = Math.min(end.charIndex,this.text.length - 1);



      for (var i = _start_char_index; i <= _end_char_index; i++) {


        var t, pt, nv, angle;
        t = this._middlePoints[i];
        if(t > 1)break;
        pt = this.points[parseInt(t)].curve.get(t);
        nv = this.points[parseInt(t)].curve.normal(t);
        angle = - Math.atan2(nv.x, nv.y) ;
        var charHeight = this.getCurrentCharFontSize(0, i);
        var boxWidth = this._getWidthOfChar(ctx, chars[i], 0, i);

        ctx.save();
        ctx.translate(- this.width/2, - this.height/2);
        ctx.translate(pt.x + this.curveTextOffset*nv.x ,pt.y +  this.curveTextOffset*nv.y);
        ctx.rotate(angle);
        ctx.fillRect(0, 0,
          boxWidth,
          -charHeight);

        ctx.restore();
      }
      //
      //ctx.fillRect(
      //  boundaries.left + lineOffset,
      //  boundaries.top + boundaries.topOffset,
      //  boxWidth,
      //  lineHeight);

    },
    drawControlsInterface: function (ctx) {
      if(this.hasTransformControls){
        this.drawBezierShapeControls(ctx);
      }
    },
    /**
     * @private
     * @param {String} method Method name ("fillText" or "strokeText")
     * @param {CanvasRenderingContext2D} ctx Context to render on
     * @param {String} line Text to render
     * @param {Number} left Left position of text
     * @param {Number} top Top position of text
     * @param {Number} lineIndex Index of a line in a text
     */
    _renderTextLine: function(method, ctx, line, left, top, lineIndex) {
      // lift the line by quarter of fontSize
      top -= this.fontSize * this._fontSizeFraction;

      // short-circuit
      var lineWidth = this._getLineWidth(ctx, lineIndex);
      this._renderChars(method, ctx, line, left, top, lineIndex);
      /*
       //justify
       var words = line.split(/\s+/),
       charOffset = 0,
       wordsWidth = this._getWidthOfWords(ctx, line, lineIndex, 0),
       widthDiff = this.width - wordsWidth,
       numSpaces = words.length - 1,
       spaceWidth = numSpaces > 0 ? widthDiff / numSpaces : 0,
       leftOffset = 0, word;

       for (var i = 0, len = words.length; i < len; i++) {
       while (line[charOffset] === ' ' && charOffset < line.length) {
       charOffset++;
       }
       word = words[i];
       this._renderChars(method, ctx, word, left + leftOffset, top, lineIndex, charOffset);
       leftOffset += this._getWidthOfWords(ctx, word, lineIndex, charOffset) + spaceWidth;
       charOffset += word.length;
       }*/
    },
    curveTextOffset: -3,
    //todo


    /**
     * Returns index of a character corresponding to where an object was clicked
     * @param {Event} e Event object
     * @return {Number} Index of a character
     */
    getSelectionStartFromPointer: function(e) {
      var mouseOffset = this.getLocalPointer(e),
        prevWidth = 0,
        width = 0,
        height = 0,
        charIndex = 0,
        newSelectionStart,
        line;

      mouseOffset.x /= this.scaleX;
      mouseOffset.y /= this.scaleY;

      //todo

      var p = this.points[0].curve.project(mouseOffset);

      for(var i in this._charPoints){
        if(this._charPoints[i] > p.t){
          return +i;
        }
      }
      return this.text.length ;
    },
    isEmptyStyles: function(){
      return false;
    },
    _renderChar: function(method, ctx, lineIndex, i, _char, left, top, lineHeight) {
      var charWidth, charHeight, shouldFill, shouldStroke,
        decl = this.getCurrentCharStyle(lineIndex, i + 1),
        offset, textDecoration;

      if (decl) {
        charHeight = this._getHeightOfChar(ctx, _char, lineIndex, i);
        shouldStroke = decl.stroke;
        shouldFill = decl.fill;
        textDecoration = decl.textDecoration;
      }
      else {
        charHeight = this.fontSize;
      }

      shouldStroke = (shouldStroke || this.stroke) && method === 'strokeText';
      shouldFill = (shouldFill || this.fill) && method === 'fillText';

      charWidth = this._applyCharStylesGetWidth(ctx, _char, lineIndex, i);
      textDecoration = textDecoration || this.textDecoration;

      var left = 0, top = 0;
      var t2, pt2 ,nv2 , angle2;

      //todo this.points[0].curve это некорректно!
      var _curve = this.points[0].curve;

      for(var index = 0; index < _char.length; index++){
        var charIndex = index + i - _char.length + 1;

        t2 = this._charPoints[charIndex];
        //do not render chars that not fit into the text area
        if(t2 > 1){
          break;
        }
        pt2 = _curve.get(t2);
        nv2 = _curve.normal(t2);
        angle2 = -Math.atan2(nv2.x,nv2.y) ;

        ctx.save();
        ctx.translate(pt2.x + this.curveTextOffset*nv2.x, pt2.y +  this.curveTextOffset*nv2.y);
        ctx.rotate(angle2);
        shouldFill && ctx.fillText(_char[index], 0, 0);
        shouldStroke && ctx.strokeText(_char[index], 0, 0);
        ctx.restore();
      }

      if (textDecoration || textDecoration !== '') {
        offset = this._fontSizeFraction * lineHeight / this.lineHeight;
        this._renderCharDecoration(ctx, textDecoration, left, top, offset, charWidth, charHeight);
      }

    },
    _renderCharDecoration: function (ctx){

    },
    _renderTextBoxBackground: function (ctx){
      ctx.translate(-this.width/2 ,-this.height/2);
      if (!this.textBackgroundColor && !this.textBackgroundStroke){
        return;
      }
      if (this.textBackgroundColor) {
        this.__fill = this.fill;
        this.fill = this.textBackgroundColor;
        this._setFillStyles(ctx);
        this.fill = this.__fill;
      }
      if (this.textBackgroundStroke) {
        this.__stroke = this.stroke;
        this.stroke = this.textBackgroundStroke;
        this._setStrokeStyles(ctx);
      }
      fabric.BezierPolyline.prototype._render.call(this,ctx);

      if (this.textBackgroundStroke) {
        this.stroke = this.__stroke;
      }
    },

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    _renderText: function(ctx) {
      //this._translateForTextAlign(ctx);
      this._renderTextFill(ctx);
      this._renderTextStroke(ctx);
      //this._translateForTextAlign(ctx, true);
    },
    _getPoint: function(t){
      var _normal = this.points[0].curve.normal(t);
      var info = {
        point:  this.points[0].curve.get(t),
        normal:  _normal,
        angle: -Math.atan2(_normal.x,_normal.y)
      };
      return info;
    },
    /**
     * Renders cursor
     * @param {Object} boundaries
     * @param {CanvasRenderingContext2D} ctx transformed context to draw on
     */
    renderCursor: function(boundaries, ctx) {

      var cursorLocation = this.get2DCursorLocation(),
        lineIndex = cursorLocation.lineIndex,
        charIndex = cursorLocation.charIndex,
        charHeight = this.getCurrentCharFontSize(lineIndex, charIndex);
      //leftOffset = (lineIndex === 0 && charIndex === 0)
      //  ? this._getLineLeftOffset(this._getLineWidth(ctx, lineIndex))
      //  : boundaries.leftOffset;


      var t , pt,nv , angle;
      //todo
      t = this._middlePoints[charIndex];
      var info = this._getPoint(t);
      ctx.save();
      ctx.translate(- this.width/2, - this.height/2);
      ctx.translate(info.point.x + this.curveTextOffset*info.normal.x ,info.point.y +  this.curveTextOffset*info.normal.y);
      ctx.rotate(info.angle);

      ctx.fillStyle = this.getCurrentCharColor(lineIndex, charIndex);
      ctx.globalAlpha = this.__isMousedown ? 1 : this._currentCursorOpacity;

      ctx.fillRect(0, 0,
        this.cursorWidth / this.scaleX,
        -charHeight);

      ctx.restore();

    },

    /**
     * Render Shape Object
     * @private
     * @param {CanvasRenderingctx2D} ctx ctx to render on
     */
    _render: function (ctx, noTransform){
      var x = noTransform ? this.left : -this.width / 2 ,
        y = noTransform ? this.top : -this.height / 2;

      ctx.save();

      this.clipTo && fabric.util.clipContext(this, ctx);
      ctx.translate(-x,-y);

      this._setOpacity(ctx);
      this._setShadow(ctx);
      this._setupCompositeOperation(ctx);

      this._renderTextBoxBackground(ctx);


      // this._renderTextLinesBackground(ctx);
      this._setStrokeStyles(ctx);
      this._setFillStyles(ctx);

      this._middlePoints = [];
      this._charPoints = [];
      this._charWidth = [];

      var curveWidth = this.getLength();
      var lineIndex = 0, charIndex = 0, _charWidth, textWidth = 0;
      if (this.textAlign == 'justify') {
        var _l = curveWidth / this.text.length;
        for (; charIndex < this.text.length; charIndex++) {
          this._charWidth.push(_l);
        }
      }else{
        for (; charIndex < this.text.length; charIndex++) {
          _charWidth = this._getWidthOfChar(ctx, this.text[charIndex], lineIndex, charIndex);
          this._charWidth.push(_charWidth);
          textWidth += _charWidth;
        }
      }

      var last_t  = 0;
      for(charIndex = 0; charIndex < this.text.length; charIndex++){

        //todo this.points[0] это только для кривых из двух точек
        var _cw = this._charWidth[charIndex] / 2;


        var _new_t =  this.points[0].curve.getTbyLength(_cw,0.0001,last_t);
        this._middlePoints.push((_new_t + last_t) / 2);
        last_t = _new_t;


        var _new_t =  this.points[0].curve.getTbyLength(_cw,0.0001,last_t);
        this._charPoints.push((_new_t + last_t) / 2);
        last_t = _new_t;
      }
      this._middlePoints.push(last_t);

      var t_offset;
      last_t  = Math.min(1,last_t);
      if (this.textAlign == 'right') {
        t_offset = 1 - last_t;
      }else if (this.textAlign == 'center') {
        t_offset = (1 - last_t)/ 2;
      }
      if(t_offset){
        for(var i = 0; i < this._charPoints.length; i++){
          this._charPoints[i] += t_offset;
        }
        for(var i = 0; i < this._middlePoints.length; i++){
          this._middlePoints[i] += t_offset;
        }
      }


      ctx.translate(x,y);
      // ctx.translate( -this.width / 2 ,-this.height / 2);

      ctx.textAlign = "center";
      this._renderText(ctx);
      this._renderTextDecoration(ctx);
      this.clipTo && ctx.restore();
      this.renderCursorOrSelection();


      ctx.restore();

    },
    /**
     * Returns object representation of an instance
     * @param {Array} [propertiesToInclude] Any properties that you might want to additionally include in the output
     * @return {Object} object representation of an instance
    //  */
    // toObject: function (propertiesToInclude) {
    //
    //   var _points = [];
    //   for(var i in this.points){
    //     _points.push(this.points[i].x,this.points[i].y);
    //   }
    //   var object = fabric._.extend(this.callSuper('toObject', propertiesToInclude), {
    //     points: _points,
    //     backgroundColor: this.backgroundColor,
    //     backgroundStroke: this.backgroundStroke,
    //   });
    //   if (!this.includeDefaultValues) {
    //     this._removeDefaultValues(object);
    //   }
    //   return object;
    // }
  });

  fabric.BezierText.fromObject = function (object) {
    return new fabric.BezierText(object.text,object);
  };
  fabric.util.createAccessors(fabric.BezierText);

  if(fabric.objectsLibrary){
    fabric.objectsLibrary.bezierTextCurved = {
      "title": "bezierTextCurved",
      "type":"bezier-text",
      "width": 200,
      "defaultShape": "curve",
      "text":"Add some text here"
    };
  }

  fabric.BezierText.prototype.actions = fabric._.extend({}, fabric.IText.prototype.actions, {});

  fabric.Text.prototype.curveText = function () {
    var obj = this.toObject();
    obj.rasterizedType = obj.type;
    obj.type = "bezier-text";
    obj.movementLimits = this.movementLimits;
    obj.clipTo = this.clipTo;
    obj.active = true;

    var _this = this;
    _this.canvas.createObject(obj, function () {
      _this.canvas.remove(_this);
      _this.canvas.renderAll();
    });
  };

  if(fabric.Text.prototype.actions){

    fabric.IText.prototype.actions.curveText =
      fabric.Text.prototype.actions.curveText = {
        insert: 'curveTool',
        className: 'button-easel',
        title: 'curveText',
        action: fabric.Text.prototype.curveText
      };
  }

//});

//материалы для вдохновения
//http://jsfiddle.net/ashishbhatt/XR7j6/10/
//http://jsfiddle.net/EffEPi/qpJTz/
//http://tympanus.net/Development/Arctext/
//http://jsfiddle.net/E5vnU/(function (global){

