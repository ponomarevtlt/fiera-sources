const {getTypeMap} = require('./units');

fabric.util.uploadImageMaxSize = {
    width: 400,
    height: 400
};

fabric.util.uploadImageMinSize = {
    width: 100,
    height: 100
};

fabric.util.readImageFile = function(file,callback){

    let reader = new FileReader();
    reader.onload = function (e) {
        let result = e.target.result;
        let type = result.substr(11, result.indexOf(";") - 11);

        console.log(type);

        if(type === "svg+xml"){

          let units = getTypeMap();
          let xml = jQuery.parseXML(atob(result.substr(26)));
          fabric.parseSVGDocument(xml.documentElement, function (results, _options, elements, allElements) {
            let groupElement = new fabric.Group(results);

            let canvas = fabric.util.createCanvasElement();
            canvas.width = groupElement.width;
            canvas.height = groupElement.height;
            let ctx = canvas.getContext("2d");
            for (let object of results) {
              object.render(ctx);
            }
            canvas.src = result;
            canvas.type = type;
            canvas.name = file.name;
            callback(canvas,e);

          })
        }else{

          let img = new Image();
          img.type = type;
          img.name = file.name;
          img.onload = function () {
            callback(img,e)
          };
          img.src = result;
        }
    };
    reader.readAsDataURL(file );
};

fabric.util.createUploadInput = function (options, complete, progress) {

    let input = document.createElement("input");
    input.setAttribute("type", "file");
    input.setAttribute("accept", options.accept || "image/*");
    if(options.multiple){
        input.setAttribute("multiple", options.multiple);
    }
    input.className = "hidden";

    $(input).change(function () {
        if(input.files && input.files.length){
            if(input.files.length === 1) {
                if (options.validation && options.validation(input.files[0], options.data) === false) {
                    return complete(false)
                }else{
                    if( options.upload){
                        options.upload(input.files[0],options.data,function(image){
                            progress( image,1,1);
                            complete([image]);
                        })
                    }else if( options.multipleUpload ){
                        options.multipleUpload(input.files,options.data,complete);
                    }
                }
            }else{
                if( options.multipleUpload ){
                    options.multipleUpload(input.files, options.data,complete);
                }else{
                    let _loader = fabric.util.loader(input.files.length,function(loaded){
                        complete(loaded);
                    },function(total, current, image){
                        progress && progress(image, total, current);
                    });
                    for(let i= 0 ;i < input.files.length; i++ ){

                        if (options.validation && options.validation(input.files[i], options.data) === false) {
                            _loader(false);
                        }else {
                            options.upload(input.files[i], options.data, _loader)
                        };
                    }
                }
            }
        }
    });

    fabric.util.uploadInput = input;
};

fabric.util.resizeUploadedImage = function (img, callback) {

    if (img.type === "svg+xml") {
        callback(img);
        return;
    }
    //Here we can put a restriction to upload a minim sized logo
    if (img.width < fabric.util.uploadImageMinSize.width || img.height < fabric.util.uploadImageMinSize.height) {
        alert("Logo is too small. MInimum size is " + fabric.util.uploadImageMinSize.width + "x" + fabric.util.uploadImageMinSize.height);
        callback(false);
        return;
    }

    if (img.width > fabric.util.uploadImageMaxSize.width || img.height > fabric.util.uploadImageMaxSize.height) {
        let size = fabric.util.getProportions(img, fabric.util.uploadImageMaxSize, "contain");
        let filter = new fabric.Image.filters.ResizeDP();
        let canvas = fabric.util.createCanvasElement(img);
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        filter.applyTo(canvas, size.width, size.height);
        callback(canvas);
    } else {
        callback(img);
    }
};

fabric.plugins["upload"] = {
  prototypes: {
    Editor: {
      uploadFiles: function (options){
        if(options.onLoad)         options.onLoad = options.onLoad.bind(this);
        if(options.upload)         options.upload = options.upload.bind(this);
        if(options.callback)       options.callback = options.callback.bind(this);
        if(options.validation)     options.validation = options.validation.bind(this);
        if(options.multipleUpload) options.multipleUpload = options.multipleUpload.bind(this);

        fabric.util.createUploadInput(options,
          function (images) {
            options.callback && options.callback(images , options.data, options.canvas);
          },
          function (img) {
            options.onLoad(img , options.data, options.canvas);
          });
        $(fabric.util.uploadInput).trigger('click');
      }
    },
    Canvas: {
      uploadClass: 'image',
      multipleUpload: false,
      uploadImage: function (options){
        let uploader = this.application.imagesUploader || this.application.uploader || {};

        options = options || {};
        options.canvas = this;
        if(!uploader.onLoad && !options.onLoad){
          options.onLoad = function (image , data, canvas) {
            canvas.createImageObject(image);
          }
        }
        if(!uploader.upload && !options.upload){
          options.upload = function (file, data, callback) {
            fabric.util.readImageFile(file, img => {
              callback(img, options.data, options.canvas);
            });
          };
        }
        this.application.uploadFiles( fabric._.extend({},uploader , options))
      },
      createImageObject: function (img ) {
        if (!img)return;

        this.createObject({
          position: "center",
          active: true,
          type: this.uploadClass,
          element: img,
          width: img.width,
          height: img.height,
          clipTo: this.activeArea,
          movementLimits: this.activeArea
        },el => this.fitObject(el));
        //
        // let src = img.src;
        // if(src && (src.endsWith(".svg") || src.startsWith("data:image/svg+xml"))){
        //   fabric.loadSVGFromURL(src, (els) => {
        //     let groupElement = new fabric.Group(els);
        //     img.width = groupElement.width;
        //     img.height = groupElement.height;
        //     _create(this,img);
        //     this.add(groupElement);
        //   });
        // }else{
        //   _create(this,img);
        // }
      },
      actions: {
        uploadImage: {
          className:  'fa fa-upload',
          key: 'u',
          title: '@canvas.uploadImage',
          action: function () {
            this.uploadImage({data: "default"});
          }
        }
      }
    }
  }
};