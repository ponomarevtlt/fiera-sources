fabric._.extend(fabric.Editor.prototype, {
  setFrames: function (frames) {
    let output = {};
    fabric._.forEach(frames, (frame) => {
      let type = fabric.util.string.capitalize(fabric.util.string.camelize(frame.id), true);
      output[type] = fabric._.defaults(frame, {
        frame: false,
        shape: false,
        type: "photo",
        role: "frame"
      });
    });
    this.frames = output;
  },
  getFrame: function (value) {
    return fabric._.findWhere(this.frames,{"id": fabric.util.string.toDashed(value)});
  },
  getFramesList: function (el) {
    el = el || fabric.Image.prototype;
    let framesList = [];

    if(el.availableFrames){
      fabric._.forEach(el.availableFrames, frameId => {
        let frame = fabric.Image.frames[frameId];
        framesList.push({
          text: frame.title || frameId,
          id: frameId,
          data: fabric._.clone(frame)
        })
      });
    }else {
      fabric._.forEach(this.frames, frame => {
        framesList.push({
          text: frame.title || frame.id,
          id: frame.id,
          data: fabric._.clone(frame)
        })
      });
    }
    return framesList;
  }
});