fabric.util.object.extend(fabric.util,{

  Utf8ArrayToStr: function(array) {
    let out, i, len, c;
    let char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while(i < len) {
      c = array[i++];
      switch(c >> 4)
      {
        case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
        // 0xxxxxxx
        out += String.fromCharCode(c);
        break;
        case 12: case 13:
        // 110x xxxx   10xx xxxx
        char2 = array[i++];
        out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
        break;
        case 14:
          // 1110 xxxx  10xx xxxx  10xx xxxx
          char2 = array[i++];
          char3 = array[i++];
          out += String.fromCharCode(((c & 0x0F) << 12) |
            ((char2 & 0x3F) << 6) |
            ((char3 & 0x3F) << 0));
          break;
      }
    }
    return out;
  },
  dataURItoBlob: function (dataURI, dataTYPE) {
    let binary = atob(dataURI.split(',')[1]), array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: dataTYPE});
  },
  blobToDataURL: function (blob, callback) {
    return new Promise(function (resolve, fail) {
      let a = new FileReader();
      a.onload = function (e) {
        callback && callback();
        resolve(e.target.result);
      };
      a.readAsDataURL(blob);
    })
  }
})


fabric.saveAs  = require("./../plugins/saveAs.js").saveAs;

fabric.Editor.prototype.exportURL = "";

fabric.util.downloadUrl = function(url){
  let link = fabric.document.createElement('a');
  fabric.document.body.appendChild(link);
  link.href = url;
  link.setAttribute("download","download");
  link.click();
};

fabric._.extend(fabric.StaticCanvas.prototype, {
  getThumbnail: function (options,callback) {
    options = fabric._.extend({
      clipped_area: false,
      clipped_area_only: false,
      draw_background: true,
      format: 'blob',
      quality: 0.8
    }, options || {});

    let size;
    if (options.clipped_area) {
      size = options.clipped_area.getBoundingRect();
      let _zoom = this.getZoom();
      size.left   /= _zoom;
      size.top    /= _zoom;
      size.width  /= _zoom;
      size.height /= _zoom;
      fabric._.extend(options, size);
    } else {
      size = {
        width: options.width || this.originalWidth || this.width,
        height: options.height || this.originalHeight || this.height,
      };

      if (this.offsets && options.clipped_area_only) {
        size.width -= this.offsets.left + this.offsets.right;
        size.height -= this.offsets.top + this.offsets.bottom;
      }
    }
    if (options.zoom) {
      size.width *= options.zoom;
      size.height *= options.zoom;
    }

    let canvas = fabric.util.createCanvasElement();
    canvas.width = size.width;
    canvas.height = size.height;

    options.left = Math.floor(options.left);
    options.top = Math.floor(options.top);
    options.height = Math.ceil(options.height);
    options.width = Math.ceil(options.width);

    this.renderThumb(canvas, options);


    if(options.format === "data-url"){
      return canvas.toDataURL(options);
    }
    if(options.format === "blob") {
      return new Promise(resolve => {
        canvas.toBlob(function (blob) {
          callback && callback(blob);
          resolve(url);
        })
      });
    }
    if(options.format === "blob-url") {
      return new Promise(resolve => {
        canvas.toBlob(function(blob){
          let url = window.URL.createObjectURL(blob);
          callback && callback(blob);
          resolve(url);
        });
      });
    }
    if(options.format === "svg") {
      return canvas.toSVG();
    }
    if(options.format === "svg-url") {

      function b64EncodeUnicode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
          return String.fromCharCode(parseInt(p1, 16))
        }))
      }
      return "data:image/svg+xml;base64," + b64EncodeUnicode(this.toSVG());
    }
    return canvas;
    // let blob = fabric.util.dataURItoBlob(src, 'image/' + options.format);
    // return {
    //   dataURL: src,
    //   blob: blob,
    //   canvas: canvas
    // };
  },
  exportAction: "file",
  saveAsPNG(filename){
    const fs = require('fs');
    let pstream = this.createPNGStream(),
      pngOutputStream =  fs.createWriteStream(filename);

    pstream.on('data', (data) => {
      pngOutputStream.write(data);
    });

    pstream.on('end',  () => {
      pngOutputStream.end();
    });
  },
  exportBlob(blob){
    switch(this.exportAction){
      case "file":
        fabric.saveAs(blob, this.title);
        break;
      case "window":
        window.open(URL.createObjectURL(blob));
        break;
    }
  }
});

fabric._.extend(fabric.Canvas.prototype.actions, {
  export: {
    className: 'fa fa-file-download',
    title: "@canvas.export",
    menu: ["exportJSON","exportPNG","exportSVG","exportPDF"]
  },
  exportJSON: {
    title: "@canvas.exportJSON",
    className: 'fa fa-file-alt',
    action: function(){
      let jsonData = "," + window.btoa(JSON.stringify(this.toObject()));
      this.exportBlob(fabric.util.dataURItoBlob(jsonData, 'application/json'));
    }
  },
  exportPNG: {
    title: "@canvas.exportPNG",
    className: 'fa fa-file-image',
    action: function(){
      let imageBlob = this.getThumbnail({
        format: "blob",
        zoom: this.dotsPerUnit,
        clipped_area_only: false,
        draw_background: true
      });
      this.exportBlob(imageBlob);
    }
  },
  exportSVG: {
    title: "@canvas.exportSVG",
    className: 'fa fa-file-code',
    action: function () {
      let svgData =   "," + window.btoa(this.toSVG());
      this.exportBlob(fabric.util.dataURItoBlob(svgData, 'image/svg+xml'));
    }
  },
  exportPDF: {
    title: "@canvas.exportPDF",
    className: 'fa fa-file-pdf',
    action: function(){
      if(this.application.exportURL){
        let data = {
          format: "pdf",
          slide: JSON.stringify(this.toObject())
        };


        // window.open("/" + data.url, '_blank');

        $.post(this.application.exportURL,data).then(function(data){
          fabric.util.downloadUrl(data.url);
        });
      }
      else{
        this.makeDocument().toBlob(blob => {
          this.exportBlob(blob);
        })
      }
    }
  }
});


fabric.util.object.extend(fabric.Editor.prototype,{
  clientPdfClean: false,
  clientSvgClean: false,
  serverPdfClean: false,
  serverSvgClean: false,
  pdfRendered: false,
  pdfURL: "",
  svg_request: "/svg", /*location.origin + ":1337" */
  createFileOnServer: false,
  renderPDF: function(file, pdfCanvasContainer, callback) {
    while (pdfCanvasContainer.firstChild) {
      pdfCanvasContainer.removeChild(pdfCanvasContainer.firstChild);
    };

    let loadingTask = pdfjsLib.getDocument(file);
    loadingTask.promise.then((pdf) => {

      for(let pageNum =1; pageNum  <= pdf.numPages;pageNum ++){
        pdf.getPage(pageNum).then((page)=>{
          // let desiredWidth = demo.editor.canvas.width;
          let viewport = page.getViewport({scale: 1,});
          // let scale = desiredWidth / viewport.width;
          // let scaledViewport = page.getViewport({scale: scale,});

          let pdfcanvas = fabric.util.createCanvasElement();
          pdfcanvas.height = viewport.height;// scaledViewport.height;
          pdfcanvas.width = viewport.width;//scaledViewport.width;
          let pdfcontext = pdfcanvas.getContext('2d');
          pdfCanvasContainer.append(pdfcanvas);

          page.render({
            canvasContext: pdfcontext,
            viewport: viewport//scaledViewport
          }).then(function () {
            callback && callback();
          })
        });
      }
    }).catch((e)=> {
      if (e.name === "MissingPDFException") {
        console.error(e);
        // demo.requestPDF().then((data)=> {
        //   demo.pdf.renderPDF(data.url, callback);
        // });
      }
    });
  },
  _exportSvgClient: function( container, callback) {
    if(this.clientSvgClean)return callback();
    this.clientSvgClean = true;

    let data = fabric._.extend({
      slides: [{}],
      prototypes: demo.prototypes ,
      frames: demo.frames
    },demo.editor.storeObject());


    new fabric.Editor(data).then(function(e){
      fabric.util.svgMediaRoot = "./public/media/";
      let svg = e.editor.canvas.toSVG();
      $(document.getElementById(container)).html(svg);
      fabric.util.svgMediaRoot = "";
      callback();
    });
  },
  _exportPdfClient: function( container, callback) {
    if(this.clientPdfClean)return callback();
    this.clientPdfClean = true;
    // if(false){
    //   fabric.inlineSVG = true;
    //   demo.editor.makeDocument().blobURL(url => {
    //     fabric.inlineSVG = false;
    //     this.pdfURL = url;
    //     $("#download-form").attr("action",url);
    //     this.renderPDF(url, callback);
    //   });
    // }

    let data = fabric._.extend({
      slides: [{}],
      prototypes: demo.prototypes ,
      frames: demo.frames
    },demo.editor.storeObject());

    fabric.fonts.loadBinaryFonts(demo.editor._usedFonts)
      .then(() => {
        return new fabric.Editor(data).promise
      })
      .then((e)=>{
        fabric.inlineSVG = true;
        e.editor.makeDocument().blobURL((url)=> {
          fabric.inlineSVG = false;
          this.pdfURL = url;
          $("#download-form").attr("action",url);

          let pdfCanvasContainer = document.getElementById('pdf-canvas-container');
          this.renderPDF(url,pdfCanvasContainer, callback);
        });
      })
  },
  _exportSvgServer: function( container, callback) {
    if(this.serverSvgClean)return callback();
    this.serverSvgClean = true;
    let el = $(document.getElementById(container));
    el.empty();

    let data = demo.editor.storeObject();


    $.ajax({
      url: '/svg',
      type: 'post',
      contentType: 'application/json',
      cache: false,
      dataType: "xml",
      success: function (svgXmlDoc) {
        let pageSet = svgXmlDoc.documentElement.getElementsByTagName("pageSet")[0];

        if(pageSet){
          svgXmlDoc.documentElement.removeChild(pageSet);

          for(let i=0;i< pageSet.childElementCount; i++){
            let pagesvg = $(svgXmlDoc.documentElement.outerHTML).append(pageSet.children[i].children);
            el.append(pagesvg);
          }

        }else{
          el.append(svgXmlDoc.documentElement);
        }
        callback();
      },
      error: function (error) {
        console.log(error);
      },
      data: JSON.stringify(data)
    });
  },
  _exportPdfServer: function( container, callback) {
    if(this.serverPdfClean)return callback();
    this.serverPdfClean = true;

    let editor = this;
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/pdf', true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.responseType = 'blob';
    xhr.onload = function() {
      if (this.status === 200) {
        let blob = new Blob([this.response], {type: 'application/pdf'});
        editor.pdfURL = URL.createObjectURL(blob);

        $("#download-form").attr("action",editor.pdfURL);
        let pdfCanvasContainer = document.getElementById('pdf-canvas-container');
        editor.renderPDF(editor.pdfURL,pdfCanvasContainer, callback);
      }
    };
    let data = demo.editor.storeObject();
    xhr.send(JSON.stringify(data));
  },
  beforeExport(){
    return true;
  },
  exportSvgClient(){
    if(this.beforeExport()){
      this._exportSvgClient(this.exportContainer,this.onExport.bind(this,'client-svg'))
    }
  },
  exportSvgServer () {
    if(this.beforeExport()) {
      this._exportSvgServer(this.exportContainer, this.onExport.bind(this, 'server-svg'))
    }
  },
  exportPdfClient(){
    if(this.beforeExport()) {
      this._exportPdfClient(this.exportContainer, this.onExport.bind(this, 'client-pdf'))
    }
  },
  exportPdfServer () {
    if(this.beforeExport()) {
      this._exportPdfServer(this.exportContainer, this.onExport.bind(this, 'server-pdf'))
    }
  },
  downloadPDF: function(){
    fabric.saveAs(this.pdfURL, this.title);
    return;
    // demo.chooseView("loading");
    // document.getElementById("iframe").src = demo.pdfURL;

    if(this.pdfURL.startsWith("blob:")){
      window.open(this.pdfURL, '_blank');
    }else{
      window.open("/" + this.pdfURL, '_blank');
    }


    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "/pdf");
    form.setAttribute("style", "display: none;");

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("name", "slides[0]");
    hiddenField.setAttribute("value", JSON.stringify(data));
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
    // demo.chooseView('iframe');
  },
  exportContainer: "svg",
  onExport: function(type){

  },
  eventListeners: fabric.util.extendArraysObject(fabric.Editor.prototype.eventListeners, {
    "modified": function () {
      this.clientPdfClean = false;
      this.clientSvgClean = false;
      this.serverPdfClean = false;
      this.serverSvgClean = false;
    }
  }),
  actions: fabric.util.object.extend(fabric.Editor.prototype.actions, {
    export: {
      className: 'fas fa-file-export',
      title: "@editor.export",
      menu: [
        "exportSvgClient",
        "exportSvgServer",
        "exportPdfClient",
        "exportPdfServer"
      ]
    },
    exportSvgClient: {
      className: 'far fa-file-code',
      title: '@history.exportSvgClient'
    },
    exportSvgServer: {
      className: 'far fa-file-code',
      title: '@history.exportSvgServer'
    },
    exportPdfClient: {
      className: 'far fa-file-pdf',
      title: '@history.exportPdfClient'
    },
    exportPdfServer: {
      className: 'far fa-file-pdf',
      title: '@history.exportPdfServer'
    }
  })
});






// let xhr = new XMLHttpRequest();
// xhr.open('POST', '/pdf', true);
// xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
// xhr.responseType = 'blob';
// xhr.onload = function() {
//   if (this.status === 200) {
//     fabric.saveAs(this.response, "file.pdf");
//   }
// };
// let data = demo.editor.storeObject();
// xhr.send(JSON.stringify(data));

/**
 * download dpf with Jquery AJAX
 */
function downloadUsingPostRequestSaveAs(){
  let data = {slides: []};
  for(var i in canvasArray){
    data.slides.push(canvasArray[i].toObject(["width","height"]));
  }
  $.ajax({
    url: "/pdf",
    contentType: 'application/json',
    data: JSON.stringify(data),
    type: "POST",
    dataType: "binary",
    processData: false,
    success: function(result){
      fabric.saveAs(result, "new file.pdf");
    },
    error: function (error) {
      alert('There was an error! Error:' + error.name + ':' + error.status)
    }
  });
}

function downloadUsingGetRequest(){
  let data = {slides: []};
  for(var i in canvasArray){
    data.slides.push(canvasArray[i].toObject(["width","height"]));
  }
  window.open(`/pdf?data=${btoa(JSON.stringify(data))}`);
}

function downloadUsingFormSubmit(){
  var form = document.createElement("form");
  form.setAttribute("method", "post");
  form.setAttribute("action", "/pdf");
  form.setAttribute("style", "display: none;");

  for(var i in canvasArray){
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("name", canvasArray[i].canvasKey);
    hiddenField.setAttribute("value", JSON.stringify(canvasArray[i].toObject(["width","height"])));
    form.appendChild(hiddenField);
  }
  document.body.appendChild(form);
  form.submit();
  document.body.removeChild(form);
}
