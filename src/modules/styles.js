fabric.util.styles = {
  styleSheetContains: function (f) {
    var hasstyle = false;
    var fullstylesheets = document.styleSheets;
    for (var sx = 0; sx < fullstylesheets.length; sx++) {
      try {
        var sheetclasses = fullstylesheets[sx].rules || document.styleSheets[sx].cssRules;
      } catch (e) {
        continue;
      }
      if (!sheetclasses)continue;
      for (var cx = 0; cx < sheetclasses.length; cx++) {
        if (sheetclasses[cx].selectorText == f) {
          hasstyle = true;
          break;
          //return classes[x].style;
        }
      }
    }
    return hasstyle;
  },
  linkCSS: function (filename) {
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
    document.getElementsByTagName("head")[0].appendChild(fileref)
  }
};

fabric.Editor.prototype.linkCSS = function () {
  for(var i in this.css){
    fabric.util.styles.linkCSS(this.css[i])
  }
  return true;
};

// fabric.Editor.addPlugin("postloaders","linkCSS");
