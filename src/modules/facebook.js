

fabric._.extend(fabric.Editor.prototype,{
  setFacebook: function(data){
    this.facebook = data;

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);


    }(document, 'script', 'facebook-jssdk'));


  },
  facebookAuth: function(cb){


    FB.init({
      appId      : this.facebook.appId,
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.9' // use graph api version 2.8
    });
    FB.AppEvents.logPageView();


    FB.getLoginStatus(function(response) {

      /**
       * {
    status: 'connected',
    authResponse: {
        accessToken: '...',
        expiresIn:'...',
        signedRequest:'...',
        userID:'...'
    }
}
       */
      console.log(response);
      // statusChangeCallback(response);
    });
    //
    // if(this.facebook.accessToken){
    //   return cb.call(this)
    // }
  },
  getFacebookMedia: function(callback){
    this.facebookAuth(() => {
      this.facebook.api.user.media('self', this.facebook.accessToken, data => {
        callback(data.data);
      });
    });
  }
});

fabric._.extend(fabric.Canvas.prototype,{
  actions: fabric._.extend(fabric.Canvas.prototype.actions,{
    facebook: {
      className: "fa fa-facebook",
      type: 'effect',
      title: "facebook",
      effectClassName: "facebook-container",
      actionParameters: function ($el, data) {
        this.application.getFacebookMedia(data => {

          this.application.createElementsList($el,fabric._.map(data,el => {
            return {
              type: "image",
              src: el.images.thumbnail.url,
              title: el.caption && el.caption.text
            }
          }))
        });
      }
    }
  })
});
