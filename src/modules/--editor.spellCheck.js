
fabric._.extend(fabric.Editor.prototype, {
  spellCheck: false,
  actions: fabric._.extend(fabric.Editor.prototype.actions, {
    spellCheck: {
      title: "@editor.spellCheck",
      className: "fa fa-user-check broken",
      type: "checkbox",
      value: "spellCheck"
    }
  })
});