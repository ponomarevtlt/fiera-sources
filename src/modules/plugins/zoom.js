fabric.plugins["zoom"] = {
  prototypes: {
    Canvas: {
      zoomCtrlKey: true,
      mouseWheelZoom: false,
      changeDimensionOnZoom: true,
      zoomToPointEnabled: true,
      minZoomFactor: 1,
      maxZoom: 20,
      autoCenterAndZoomOut: false,
      zoomStep: 0.1,
      scaleFactor: 1.1,
      zoomOut() {
        let point = this.getOrignalCenter();
        let scaleValue = this.getZoom() / this.scaleFactor;
        this.zoomToPoint({x: point.left, y: point.top}, scaleValue);
      },
      zoomIn() {
        let point = this.getOrignalCenter();
        let scaleValue = this.getZoom() * this.scaleFactor;
        this.zoomToPoint({x: point.left, y: point.top}, scaleValue);
      },
      drawZoomArea: function (ctx, left, top, width, height) {
        ctx.save();
        ctx.translate(left || 0, top || 0);
        let _scale = this.getZoom();
        let _size = fabric.util.getProportions(this.backgroundImage, {width: width || 100, height: height || 100});
        ctx.globalAlpha = 0.5;
        ctx.fillRect(0, 0, _size.width, _size.height);
        ctx.strokeStyle = "red";

        let v = this.viewportTransform;

        let x1 = -v[4] * _size.scaleX / _scale;
        let y1 = -v[5] * _size.scaleY / _scale;
        let x2 = x1 + this.width * _size.scaleX / _scale;
        let y2 = y1 + this.height * _size.scaleY / _scale;

        x1 = Math.max(x1, 0);
        y1 = Math.max(y1, 0);
        x2 = Math.min(x2, _size.width);
        y2 = Math.min(y2, _size.height);

        ctx.globalAlpha = 1;
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y1);
        ctx.lineTo(x2, y2);
        ctx.lineTo(x1, y2);
        ctx.lineTo(x1, y1);

        ctx.fill();
        ctx.clip();
        // ctx.globalCompositeOperation = "source-in";
        ctx.drawImage(this.backgroundImage._element, 0, 0, _size.width, _size.height);
        // ctx.globalCompositeOperation = "source-over";
        // ctx.drawImage(this.backgroundImage._element,0,0,_size.width, _size.height)
        ctx.stroke();
        ctx.strokeRect(0, 0, _size.width, _size.height);
        ctx.restore();
      },
      setMouseWheelZoom: function (val) {
        this.mouseWheelZoom = val;
        this.on("mouse:wheel", this.wheelZoom);
      },
      setZoom: function (_scale, point) {

        if (!point) {
          point = new fabric.Point(0, 0);
        }

        if (this.changeDimensionOnZoom) {

          if (!this.originalWidth) {
            this.originalWidth = this.width;
          }
          if (!this.originalHeight) {
            this.originalHeight = this.height;
          }
          this.setDimensions({
            width: Math.round(this.originalWidth * _scale),
            height: Math.round(this.originalHeight * _scale)
          }, {
            // cssOnly: true
          });
        }

        let vpt = this.viewportTransform.slice(0);
        vpt[0] = _scale;
        vpt[3] = _scale;

        if (!this.changeDimensionOnZoom) {
          let vpt = this.viewportTransform.slice(0);
          point = fabric.util.transformPoint(point, fabric.util.invertTransform(this.viewportTransform));
          let after = fabric.util.transformPoint(point, vpt);
          vpt[4] = Math.round(vpt[4] + before.x - after.x);
          vpt[5] = Math.round(vpt[5] + before.y - after.y);
        }
        this.setViewportTransform(vpt);
        // this._zoomToPointNative( point, _scale);

        //  this._setZoomNative(_scale);
        this.fire('viewport:scaled', {scale: _scale});
        if (this.application) {
          this.application.fire("viewport:scaled", {scale: _scale});
        }
      },
      zoomToPoint: function (point, newZoom) {
        let _max = this.getMaxZoom();
        let _min = this.getMinZoom().scale;
        if (newZoom > _max) {
          newZoom = _max;
        }
        if (newZoom < _min) {
          newZoom = _min;
        }
        this.setZoom(newZoom, point);
      },
      resetViewport: function () {
        _canvas.viewportTransform[0] = 1;
        _canvas.viewportTransform[3] = 1;
        _canvas.viewportTransform[4] = 0;
        _canvas.viewportTransform[5] = 0;
        _canvas.renderAll();
        for (let i in this._objects) {
          this._objects[i].setCoords();
        }
      },
      getMaxZoom: function () {
        return this.maxZoom;
      },
      getMinZoom: function () {
        let scrollParent = $(this.wrapperEl).scrollParent();
        let $container = $(scrollParent[0] || this.wrapperEl);
        let _containerSize = {
          width: $container.width(),
          height: $container.height()
        };
        let _bgSize = {
          width: this.originalWidth || this.width,
          height: this.originalHeight || this.height
        };
        let _maxSize = {
          width: _containerSize.width * this.minZoomFactor,
          height: _containerSize.height * this.minZoomFactor
        };
        let size = fabric.util.getProportions(_bgSize, _maxSize, 'contain');


        if (size.scale > 1) {
          return {
            scale: 1,
            width: this.originalWidth,
            height: this.originalHeight,
          }
        }

        return size;
      },
      centerAndZoomOut: function () {

        // let _containerSize = {
        //   width: $(this.wrapperEl).width(),
        //   height: $(this.wrapperEl).height()
        // };

        // let vpt = this.viewportTransform.slice(0);
        // vpt[0] = options.scale;
        // vpt[3] = options.scale;
        // if(!this.changeDimensionOnZoom){
        //   vpt[4] = (_containerSize.width - options.width ) / 2;
        //   vpt[5] = (_containerSize.height - options.height) / 2;
        // }
        //
        // this.setViewportTransform(vpt);


        // this._update_background_overlay_image("background");
        // this._update_background_overlay_image("overlay");
        // // this.fire("viewport:scaled",{scale: options.scale})
        //this.renderAll();


        let options = this.getMinZoom();
        this.setZoom(options.scale);
        let scrollParent = $(this.wrapperEl).scrollParent()[0];

        if (scrollParent) {
          scrollParent.scrollTop = (scrollParent.scrollHeight - scrollParent.clientHeight) / 2;
          scrollParent.scrollLeft = (scrollParent.scrollWidth - scrollParent.clientWidth) / 2;
        }
      },
      centerOnObject: function (tag) {
        let br = tag.getBoundingRect();
        let ct = this.viewportTransform;
        br.width /= ct[0];
        br.height /= ct[3];
        let size = {
          width: br.width * 1.1,
          height: br.height * 1.1
        };

        let sizeOptions = fabric.util.getProportions(size, this);
        let _w = (this.width / sizeOptions.scaleX - size.width) / 2;
        let _h = (this.height / sizeOptions.scaleY - size.height) / 2;
        let _l = (br.left - ct[4]) / ct[0];
        let _t = (br.top - ct[5]) / ct[3];

        let x1 = [
          sizeOptions.scaleX,
          0, 0,
          sizeOptions.scaleY,
          -tag.left * sizeOptions.scaleX + (tag.width * 0.05 + _w) * sizeOptions.scaleX,
          -tag.top * sizeOptions.scaleY + (tag.height * 0.05 + _h) * sizeOptions.scaleY
        ];
        let x2 = [
          sizeOptions.scaleX,
          0, 0,
          sizeOptions.scaleY,
          -_l * sizeOptions.scaleX + (br.width * 0.05 + _w) * sizeOptions.scaleX,
          -_t * sizeOptions.scaleY + (br.height * 0.05 + _h) * sizeOptions.scaleY
        ];

        this.setViewportTransform(x2);
        this.fire("viewport:scaled", {scale: sizeOptions.scaleX});
        if (this.application) {
          this.application.fire("viewport:scaled", {scale: sizeOptions.scaleX});
        }
        this.renderAll();
      },
      wheelZoom: function (e) {
        let event = e.e;


        if (!this.mouseWheelZoom || this.zoomCtrlKey && !event.ctrlKey) {
          return;
        }
//Find nearest point, that is inside image END
        let zoomStep;// = 0.1 * event.deltaY;
        if (event.deltaY < 0) {
          zoomStep = 1 + this.zoomStep;
        } else {
          zoomStep = 1 - this.zoomStep;
        }

        let cZoom = this.getZoom();
        let newZoom = cZoom * zoomStep;
        let minZoom = this.getMinZoom().scale;


        if (this.zoomToPointEnabled) {
          let point = new fabric.Point(event.offsetX, event.offsetY);
          let _x = this.viewportTransform[4];
          let _y = this.viewportTransform[5];

          // Find nearest point, that is inside image BEGIN
          // It is needed to prevent canvas to zoom outside image
          let _w = this.originalWidth * cZoom + _x;
          let _h = this.originalHeight * cZoom + _y;


          if (point.x < _x) {
            point.x = _x;
          }
          if (point.y < _y) {
            point.y = _y;
          }
          if (point.x > _w) {
            point.x = _w;
          }
          if (point.y > _h) {
            point.y = _h;
          }
          if (minZoom > newZoom) {
            if (this.autoCenterAndZoomOut) {
              this.centerAndZoomOut();
            }
            else if (event.deltaY < 0) {
              this.zoomToPoint(point, newZoom);
            }
          } else {
            this.zoomToPoint(point, newZoom);
          }
        } else {
          this.setZoom(newZoom);
        }
        for (let i in this._objects) {
          this._objects[i].setCoords();
        }
        this.renderAll();
        event.stopPropagation();
        event.preventDefault();
        return false; //preventing scroll page
      },
      getOrignalCenter: function () {
        let point = this.getCenter();
        point.left += this.viewportTransform[4];
        point.top += this.viewportTransform[5];
        return point;
      },
      eventListeners: {
        "dimensions:modified": function () {
        },
        "background-image:loaded": function (event) {
          if (this.autoCenterAndZoomOut) {
            this.centerAndZoomOut();
          }
        },
        "loading:end": function (event) {
          if (this.autoCenterAndZoomOut && (this.originalHeight || this.originalWidth)) {
            this.centerAndZoomOut();
          }
        }
      }
    },
    Editor: {
      zoom: 1,
      _getScrollContainer: function () {
        let val = this.canvasContainer;
        let parents = $(val).parents();
        for (let i = 0; i < parents.length; i++) {
          let el = parents[i];
          if ($(el).css("overflow") !== "visible") {
            val = el;
            return el;
          }
        }
        return null;
      },
      dragScrollContainer: function (e) {
        this._scrollElement.scrollLeft = this._startScrollLeft - (e.clientX - this.dragCursorPosition.x);
        this._scrollElement.scrollTop = this._startScrollTop - (e.clientY - this.dragCursorPosition.y);
      },
      initScrollContainerDragging: function () {
        this._scrollElement = this._getScrollContainer();
        this._startScrollTop = this._scrollElement.scrollTop;
        this._startScrollLeft = this._scrollElement.scrollLeft;
      },
      setMouseDragging: function () {
        let editor = this;
        let scrollElement = this._getScrollContainer();
        scrollElement.addEventListener('mousedown', initDrag, false);

        function initDrag(e) {
          editor.dragCursorPosition = {x: e.clientX, y: e.clientY};
          editor.initScrollContainerDragging();
          document.addEventListener('mousemove', doDrag, false);
          document.addEventListener('mouseup', stopDrag, false);
          e.preventDefault();
          e.stopPropagation();
        }

        function doDrag(e) {
          editor.dragScrollContainer(e);
          e.preventDefault();
          e.stopPropagation();
        }

        function stopDrag(e) {
          document.removeEventListener('mousemove', doDrag, false);
          document.removeEventListener('mouseup', stopDrag, false);
          e.preventDefault();
          e.stopPropagation();
        }

        this.mouseMoveDragging = true;
      },
      actions: {
        zoomMenu: {
          className: 'fa fa-search',
          title: '@editor.zoom',
          menu: ["zoomOut", "zoom", "zoomIn"]
        },
        zoomOut: {
          className: 'fa fa-search-minus',
          title: '@editor.zoomOut',
          action: function () {
            this.canvas.zoomOut();
          }
        },
        zoom: {
          type: 'number',
          title: '@editor.zoom',
          min: 0.1,
          max: 10,
          fixed: 2,
          set: function (val) {
            let canvas = this.canvas;
            canvas.setZoom(val)
          },
          get: function (target) {
            if (!target.canvas) {
              return target.zoom;
            }
            return target.canvas.getZoom()
          },
          observe: "viewport:scaled"
        },
        zoomIn: {
          title: '@editor.zoomIn',
          className: 'fa fa-search-plus',
          action: function () {
            this.canvas.zoomIn();
          }
        }
      }
    }
  }
};