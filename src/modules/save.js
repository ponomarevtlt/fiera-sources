

fabric._.extend(fabric.Canvas.prototype, {
  eventListeners: fabric.util.extendArraysObject(fabric.Canvas.prototype.eventListeners, {
    'modified draw:after object:modified canvas:cleared object:added object:removed group:removed': function saveJsCallApplicationModifiedEvent(){
      // this.application.fire("modified");

      if(this.application && this.application.autoSave){
        this.application.save();
      }
    }
  })
});
fabric._.extend(fabric.Editor.prototype, {
  eventListeners: fabric.util.extendArraysObject(fabric.Editor.prototype.eventListeners, {
    modified: function(){
      if(this.autoSave){
        this.save();
      }
    }
  }),
  autoSave:false,
  productID: fabric.window.location.href,
  _get_storage_id(){
    return this.id + "_" + this.productID;
  },
  load: function(){
    let productData = localStorage.getItem(this._get_storage_id());
    if (productData) {
      productData = JSON.parse(productData);
      this.set(productData);
    }
    else{
      this.set({
        slides: {}
      });
    }
  },
  save: function(){
    localStorage.setItem(this._get_storage_id(), JSON.stringify(this.storeObject()));
  },
  actions: fabric._.extend(fabric.Editor.prototype.actions,{
    save: {
      title: "@editor.save",
      className: 'fa fa-save',
      action: "save"
    },
    load: {
      title: "@editor.load",
      className: 'fa fa-load',
      action: "save"
    },
  })
})