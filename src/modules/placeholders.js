
fabric.util.object.extend(fabric.Canvas.prototype, {
  placeholderMinSize: 250,
  placeholderMinAlpha: 200,
  createPlaceholders: function(){
    //possible to use "before:drop" event
    let xCanvas, xContext, xImgData, mask, contours, width = this.originalWidth, height = this.originalHeight;

    //detect all transparent zones on the overlay image using Fuzzy Selection Tool
    xCanvas = fabric.util.createCanvasElement(width, height);
    xContext = xCanvas.getContext("2d");
    xContext.drawImage(this.overlayImage._element, 0, 0, width, height);
    xImgData = xContext.getImageData(0, 0, xCanvas.width, xCanvas.height);
    mask = fabric.util.wand.selectColored(xImgData, {aMin: this.placeholderMinAlpha});
    //exclude little transparent areas (it is decorative elements, not photo frames)

    let invertedMask = fabric.util.wand.invertMask(mask);

    contours = fabric.util.wand.traceContours(invertedMask).filter((contour )=> {
      return (contour.points.length > this.placeholderMinSize)
    });
    // Undo/redo are not applicable
    this.processing = true;//todo processingPropertiees?
    this.photoFrames = [];
    this.activePhotoFrame = 0;

    let number = 1;
    //create invisible placeholder for photos
    contours.forEach((contour)=>{
      let photoFrame = this.createObject({type: "PhotoPlaceholder"});
      photoFrame.set({
        left: contour.bounds.minX - 2,
        top: contour.bounds.minY - 2,
        width: contour.bounds.maxX - contour.bounds.minX+4,
        height: contour.bounds.maxY - contour.bounds.minY + 4 ,
        text: number
      });
      this.add(photoFrame);
      photoFrame.setCoords();

      number++;

      this.photoFrames.push(photoFrame);
    });
    this.processing = false;//todo processingPropertiees?
  },
  getPhotoPlaceholder: function (data) {
    var size = fabric.util.getProportions(data, {width: 400, height: 400});
    if (this.photoFrames && this.photoFrames.length && this.activePhotoFrame < this.photoFrames.length) {
      var frame = this.photoFrames[this.activePhotoFrame];

      this.activePhotoFrame++;
      if (this.activePhotoFrame === this.photoFrames.length) {
        this.activePhotoFrame = 0;
      }
      return {
        left: frame.left,
        top: frame.top,
        width: frame.width,
        height: frame.height
      };
    }

    return {
      width: size.width,
      height: size.height
    }
  },
});



fabric.PhotoPlaceholder = fabric.util.createClass(fabric.SGroup, {
  selectable: false,
  stored: false,
  hoverCursor: "defaultCursor",
  controls: false,
  permanent: true,
  layer: "placeholders",
  accepts: {
    type: "frame"
  },
  setText: function(text){
    this._objects[0].setText(text);
    this.dirty = true;
  },
  setData: function (data) {
    this.canvas.setData(fabric.util.object.extend(data, {
      left: this.left,
      top: this.top,
      width: this.width,
      height: this.height
    }))
  }
});