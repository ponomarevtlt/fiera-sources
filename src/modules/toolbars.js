// import Toolbar from "../toolbar/toolbar.core";

require('./../jquery/util.js');

if(typeof FieraToolbar !== "undefined"){
  fabric.Toolbar = FieraToolbar;
}
// else{
//   fabric.Toolbar = require("./../toolbar/toolbar").Toolbar;
// }

fabric._.extend(fabric.Object.prototype,{
  optionsOrder: fabric.util.a.build(fabric.Object.prototype.optionsOrder).after("actions","tools").array
});

Object.defineProperty( fabric.Object.prototype, 'toolsIncludes', {
  configurable: false,
  enumerable: false,
  get: function(){
    let tools = {};
    for(var toolName of this.tools){
      tools[toolName] = toolName;
    }
    return tools;
  }
});

fabric._.extend(fabric.Editor.prototype ,{

  optionsOrder: fabric.util.a.build(fabric.Editor.prototype.optionsOrder)
    .find("prototypes").after("actions")
    .find("objects").before("toolbars","tools").array,
  objectsToolbarContainer: null,
  target: null,
  setToolbarCoords: function ($menu, target, options) {

    options = fabric._.extend({
      originX: "left",
      originY: "top",
      marginX: 0,
      marginY: 0
    }, options);

    target.setCoords();
    let r = target.getBoundingRect();

    let _left;
    switch (options.originX) {
      case "left":
        _left = r.left;
        break;
      case "right":
        _left = r.left + r.width;
        break;
      case "center":
        _left = r.left + r.width / 2;
        break;
    }

    let _top;
    switch (options.originY) {
      case "top":
        _top = r.top - $menu.height();
        break;
      case "bottom":
        _top = r.top + r.height;
        break;
      case "center":
        _top = r.top + r.height / 2 - $menu.height() / 2;
        break;
    }

    let $wrapper = $menu.relativeParent();// $(target.canvas.wrapperEl);
    let _menuContainerOffset = $($menu.parents()[0]).offset();
    let _canvasOffset = $(target.canvas.wrapperEl).offset();
    let _pos = $(target.canvas.wrapperEl).position();


    let _scale = $(target.canvas.wrapperEl).height() / target.canvas.height;
    _top *= _scale;
    _left *= _scale;

    _top += options.marginY + _canvasOffset.top - _menuContainerOffset.top;// + _pos.top;
    _left += options.marginX + _canvasOffset.left - _menuContainerOffset.left;// + _pos.left;

    _left = Math.max(3, _left);

    let documentsContainerWidth = $(target.canvas.wrapperEl).relativeParent();
    let _overflow = - documentsContainerWidth.width() + _left + $menu.width() + 3;

    if(_overflow > 0){
      _left -= _overflow;
    }

    let coords = {
      top: Math.max(3, _top),
      left: _left
    };

    $menu.css(coords);
    return coords;
  },

  _getActionValue: function(proto,property) {
    do {
      if(!proto.type)break;
      let klassname = fabric.util.string.capitalize(fabric.util.string.camelize(proto.type),true);
      let customProto = this.prototypes[klassname];
      if(customProto && customProto.actions && customProto.actions[property]){
        return customProto.actions[property];
      }
      if(proto.actions && proto.actions[property]){
        return proto.actions[property];
      }
    } while (proto = proto.__proto__ );
    return null;
  },

  _makeActions(proto,fullTools,force){
    let actions = [];
    for (let toolName of fullTools) {
      let _val;
      if(force){
        _val = proto.actions[toolName];
      }else{
        _val = this._getActionValue(proto, toolName);
      }
      if (!_val) {
        console.warn("tool " + toolName + " is undefined");
        continue;
      }
      _val.id = toolName;
      if(_val.staticOptions){
        _val.staticOptions = _val.staticOptions.call(_val,proto)
      }
      if (_val.menu) {
        for (let i in _val.menu) {
          let itemId = _val.menu[i];
          if(itemId.constructor === String){
            let item = this._getActionValue(proto, itemId);
            item.id = itemId;
            if(item.staticOptions){
              item.staticOptions = item.staticOptions.call(item,proto)
            }
            _val.menu[i] = item;
          }
        }
      }
      actions.push(_val);
    }

    for (let i in actions) {
      if (actions[i].constructor === Function) {
        actions[i] = actions[i].call(proto);
      }
    }
    return actions;
  },
  tools: {
    Object: [
      "order",
      "duplicate",
      "remove"
    ],
    Group: [
      "remove"
    ],
    ActiveSelection: [
      "remove"
    ],
    Editor: [
      "undo",
      "redo"
    ],
    Canvas: [
      "addText",
      "backgroundColor",
      "uploadBackgroundImage",
      "uploadOverlayImage",
    ],
    Image: [
      "*"
    ],
    Textbox: [
      "textAlign",
      "bold",
      "italic",
      "underline",
      "font",
      "fill",
      "*"
    ]
  },
  setTools: function(TOOLS) {
    this.toolbars = [];
    TOOLS = fabric._.defaults(TOOLS,this.tools );

    let self = this;
    /**
     *
     * @param tools ["x", "*", "y"]
     * @param proto
     * @returns ["x", "a", "b", "c", "y"]
     * @private
     */
    function _getTools( value) {
      let klassname, prototype;
      if(value.constructor === String){
        klassname = value;
        prototype = self.getKlass(klassname).prototype;
      }
      else{
        prototype = value;
        klassname = fabric.util.string.capitalize(fabric.util.string.camelize(prototype.type), true);
      }

      let tools = TOOLS[klassname];// || prototype.tools;
      let indexOfStar;
      if(tools){
        tools = tools.slice();
        indexOfStar = tools ? tools.indexOf("*") : -1;
        if (indexOfStar === -1) {
          return tools;
        }
      }
      let tools2 = _getTools(prototype.__proto__);

      if(tools){
        tools.splice.apply(tools, [indexOfStar, 1].concat(tools2));
      }else{
        tools = tools2;
      }

      return tools;
    }

    for(let klassname in TOOLS){
      TOOLS[klassname] =  _getTools( klassname);
    }

    for(var i in TOOLS){
      if(!this.prototypes[i]){
        this.prototypes[i] = {};
      }
      this.prototypes[i].tools = TOOLS[i];
    }

    // this.tools = TOOLS;

    // this._actions = {};
    // this._actions = this._makeActions(this, this.tools.Editor, true);

    let tb = this._toolbarOptions;
    let canvasTarget = new fabric.SStaticCanvas({application: this, element: false});

    if(tb.editor){
      let klassname = "Editor"
      options = tb.editor;
      target  = this;
      this.toolbars[klassname] = new fabric.Toolbar({
        id:       `fiera-${this.id}-${klassname}-toolbar`,
        tools:    this._makeActions(this.getKlass(klassname).prototype, TOOLS[klassname]),
        lang:     this.lang,
        target:   target,
        container: options.container,
        layout: $(this.canvasContainer).relativeParent(),
        doNotUpdate: true,
        options:  options
      });
      this.toolbars.Editor.setTarget(this);
      delete TOOLS.Editor;
    }
    if(tb.canvas){
      let klassname = "Canvas"
      options = tb.canvas;
      let target =  this.canvas || canvasTarget;
      this.toolbars[klassname] = new fabric.Toolbar({
        id:       `fiera-${this.id}-${klassname}-toolbar`,
        tools:    this._makeActions(this.getKlass(klassname).prototype, TOOLS[klassname]),
        lang:     this.lang,
        target:   target,
        container: options.container,
        layout: $(this.canvasContainer).relativeParent(),
        doNotUpdate: true,
        options:  options
      });

      if (this.canvas) {
        this.toolbars.Canvas.setTarget(this.canvas)
      }
      this.on("slide:changed", (e) => {
        this.toolbars.Canvas.setTarget(e.canvas)
      });

      delete TOOLS.Canvas;
    }

    if (tb.objects) {
      let options = tb.objects;


      let toolsNames = [];
      let tools = [];
      for(let klassname in TOOLS) {
        //get the pototype object
        let targetObject = new (this.getSyncKlass(klassname))({application: this,canvas: canvasTarget});
        let objecttools = this._makeActions(targetObject, TOOLS[klassname]);
        for(let tool of objecttools ){
          if(!toolsNames.includes(tool.id)){
            toolsNames.push(tool.id);
            tool.visible =  "toolsIncludes." + tool.id;
            // tool.visible =function(target){
            //   return target && target.tools.includes(this.id)
            // };
            tools.push(tool);
          }
        }
      }

      this.toolbars["Objects"] = new fabric.Toolbar({
        id:           `fiera-${this.id}-${"Objects"}-toolbar`,
        tools:        tools,
        lang:         this.lang,
        container:    options.container,
        layout:       $(this.canvasContainer).relativeParent(),
        doNotUpdate:  true,
        options:      options
      });
      // this.objectToolbar.moving_eventListener = this.setToolbarCoords.bind(this, $ctr, this.target, tb.objects.position);
      // this.target.on("moving",this.objectToolbar.moving_eventListener)

      let $ctr = $(document.getElementById(tb.objects.container));

      if(tb.objects.position){
        fabric._.defaults(tb.objects.position, {
          originX: "left",
          originY: "top",
          marginX: 0,
          marginY: 0
        });
        $ctr.addClass("floated-menu");
        $ctr.css({
          "position": "absolute",
          "z-index": 999
        });
      }

      $ctr.hide();
      this._activeObjectToolbar = null;

      this.on({
        'target:changed': event => {
          let toolbar = this.toolbars.Objects;
          if(event.selected){
            toolbar.setTarget(this.target);
            if (toolbar.options.position) {
              this.setToolbarCoords($ctr, this.target, toolbar.options.position);
            }
            $ctr.show();
          }else{
            toolbar.setTarget(null);
            $ctr.hide();
          }
        },
        'target:modified': event => {
          let toolbar = this.toolbars.Objects;
          if(toolbar.options.position){
            this.setToolbarCoords($ctr, this.target, toolbar.options.position);
          }
        }
      });
    }

  },
  setToolbars: function(options) {
    this._toolbarOptions = options;
  },
  eventListeners: fabric.util.extendArraysObject(fabric.Editor.prototype.eventListeners, {
  })
});
