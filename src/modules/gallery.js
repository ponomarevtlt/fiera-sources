
require('./../jquery/draggable.js');

fabric._.extend(fabric.Editor.prototype, {
  galleries: null,
  /**
   *
   *
     contentArea: ".main_container",
     photo: config.cardType === CARD_TYPES.PHOTO || config.cardType === CARD_TYPES.BLANK && {
          container: "photos-gallery",
          elements: config.photosGallery,
          mapper: function (val) {
            return {
              preview: "image",
              type: "photo",
              src: config.photosPath + val
            }
          },
          options: {
            itemClass: "prevClose",
            removeButtonTemplate: '<span class="fa fa-close closeIMG">',
            removable: true
          }
        },
   * @param selectedData
   */
  setGalleries: function (options) {
    // if(options.contentArea){
    //   this.contentArea = options.contentArea;
    //   delete options.contentArea;
    // }
    this.galleries = [];

    for(let i in options){
      if(options[i]){
        this.galleries[i] = new fabric.Gallery(fabric._.extend({
          application: this
        },options[i]));
      }
    }

  },

// setElementFromMenu: function (selectedData) {
//   this.canvas.createImageObject(selectedData.image, this.uploadClass);
// };
  /*createGallery: function (target, $el, options) {
    if ($el.constructor === String) {
      $el = $("#" + $el);
    }
    options = options || {};

    if (target.galleryCategory) {
      options.category = target.galleryCategory;
    }

    $el.empty();
    let library = this.getLibraryElements(options);


    this.createElementsList($el, library);

    _.each(library, function (libraryItem) {
      let img = new Image();
      img.src = fabric.util.mediaRoot + libraryItem.src;

      let $img = $(img).width(100).height(100)
        .click(function () {

          let selectedData = {
            image: img,
            data: libraryItem,
            options: options
          };
          target.setElementFromMenu(selectedData);
        });

      $el.append($img);
    });
  },*/
  getFontsLibrary: function () {
    return fabric._.map(this.fonts,function(fontName){
      return {
        fontFamily: fontName,
        fontSize: 25,
        role: "fontFamily",
        text: fontName,
        type: "textbox"
      }
    })
  }
});


fabric.Gallery = function(options){
  if(options.application){
    this.application = options.application;
    fabric.fire("entity:created", {target: this, options: options});
  }
  // this.draggableArea = options.contentArea || this.application.contentArea || "body";
  this.elements = [];

  fabric._.extend(this, options);
  if(options.container.constructor === String ){
    this.container = $("#" + options.container);
  }

  if(this.enabled){
    this.updateElements();
  }

  if(this.lazyLoad){
    let elem = this.container;

    let scrP = elem.scrollParent();
    let _parents = elem.parents();
    let sp;
    for(let i in _parents){
      if(_parents[i] === scrP[0]){
        sp = $(_parents[i]);
        break;
      }
    }

    this.loading = false;
    let completed = false;
    let wH = $(window).height();
    let elH = elem.height();
    let spH = sp.height();
    let sT = sp.scrollTop();

    $(sp).scroll(() =>{
      if(completed)return;
      sT = sp.scrollTop();
      updateList.call(this);
    });

    $(window).scroll(() =>{
      if(completed)return;
      sT =     $(window).scrollTop();
      updateList.call(this);
    });

    $(window).resize(() =>{
      if(completed)return;
      wH = $(window).height();
      updateList.call(this);
    });

    function updateList(){
      let _x =   elem.outerHeight() + elem.offset().top - spH - sp.offset().top ;
      if( _x < (parseInt(this.lazyLoadInterval) || 100) && !this.loading){
        this.loading = true;
        this.lazyLoad()(function(isCompleted){
          this.loading = false;
          completed = isCompleted;
        })
      }
    }
  }
};

fabric.Gallery.prototype = {
  type: "gallery",
  draggableArea: "body",
  lazyLoadInterval: 100,
  lazyLoad: false,
  enabled: true,
  removable: false,
  draggable: true,
  itemClass: "list-item",
  removeButtonTemplate: '<span class="fa fa-close">',
  activate: function (originalData) {
    let data = fabric._.clone(originalData);
    let aOptions = this.activateOptions;
    if(aOptions && aOptions.constructor === Function){
      aOptions = aOptions.call(this,data);
    }
    // if(aOptions && (aOptions.width || aOptions.height)){
    //   delete data.width;
    //   delete data.height;
    // }
    fabric._.extend(data,aOptions);
    // delete data.left;
    // delete data.top;
    let canvas = this.application.canvas;

    if (canvas.target &&  canvas.isAccepts(canvas.target.accepts, data) ) {
      canvas.target.setData(data);
    } else {
      canvas.setData(data);
    }

    this.fire("activated",{item: data, target: this});
    this.application && this.application.fire("gallery:activated",{item: data, target: this})
  },
  disable () {
    this._elements = [];
    this.container.empty()
  },
  enable () {
    this.enabled = true;
    this.updateElements();
  },
  updateElements () {
    let _this = this;

    function resolve(data){
      _this._elements = data;
      let els = fabric._.map(_this._elements, _this.mapper.bind(_this));
      _this.addElements(els);
    }

    function fail(){
      console.error("something goes wrong with setGalleries")
    }
    let value = this.elements || [];

    if(value.constructor === Function){
      if(value.length){
        value.call(this.application,resolve,fail);
      }
      else{
        resolve(value.call(this.application));
      }
    }
    else{
      fabric.util.data.loadJson(value,resolve,fail);
    }
  },
  imageWidth: 300,
  imageHeight: 150,
  _addCanvasEl: function (el, element, innerEl){

    let canvasEl  = fabric.util.createCanvasElement(this.imageWidth,this.imageHeight);
    innerEl.width = this.imageWidth;
    innerEl.height = this.imageHeight;
    let ctx = canvasEl.getContext("2d");
    element.position = "center";
    element.application = this.application

    fabric.util.createObject(element,_object => {
      let b = _object.getBoundingRect();
      /* _object.set({
         left : 80 - b.width/2 - _object.strokeWidth * 2,
         top : 45 - b.height/2 - _object.strokeWidth * 2
       });*/

      fabric._.extend(element,{
        scaleX: _object.scaleX,
        scaleY: _object.scaleY,
        width: _object.width,
        height: _object.height,
        left: _object.left,
        top: _object.top,
        strokeWidth: _object.strokeWidth
      });
      delete element.position;
      ctx.translate(this.imageWidth/2,this.imageHeight/2);
      _object.drawObject(ctx);


      innerEl.src = canvasEl.toDataURL();

      delete _object.canvas;
      el[0].data = element;
      el[0].element = _object;
    });
  },
  removeElement : function(element){
    element.element.remove();
    let index = this._elements.indexOf(element);
    this._elements.splice(index,1);
    this.fire("removed",{item: element, target: this,index: index});
    this.application && this.application.fire("gallery:removed",{item: element, target: this,index: index})
  },
  addElements : function(elements){
    elements.forEach(element => this.add(element))
  },
  add: function(element){

    let el = $("<li>")
      .addClass(this.itemClass);

    let insertedElement = {
      data: element,
      element: el
    };

    el[0].data = element;

    if (this.element) {
      element = this.element(element);
    }

    if (element.title) {
      let $span = $("<span>").text(element.title).addClass("element-title");
      el.append($span);
    }
    let $innerEl = null;

    if (this.render) {
      $innerEl = this.render(element);
    } else {
      if(element.role === "fontFamily"){
        element.preview = "fontFamily";
      }
      if(element.type === "image" || element.type === "video"){
        if(!element.preview){
          element.preview = element.type;
        }
        if(!element.role){
          element.role = element.type;
        }
      }


      switch (element.preview) {
        case "fontFamily":
          $innerEl = $("<span>").css({
            "color": element.fill || fabric.Text.prototype.fill,
            "font-family": element.fontFamily,
            "font-size": element.fontSize
          }).text(element.text);
          break;
        case "image":
          let img = element.image;

          if(!img){
            img = new Image();
            img.onload = function(){
              element.width = img.naturalWidth;
              element.height = img.naturalHeight;
              // element.src = img.src;
            };
            img.crossOrigin = 'anonymous';
            img.src = fabric.util.getURL(element.gallerySrc || element.thumbnail || element.src);
          }else{
            element.src = img.src;
            element.width = img.naturalWidth;
            element.height = img.naturalHeight;
            delete element.image;
          }
          delete element.preview;
            // el[0].element = img;
          el[0].element = element;
          $innerEl = $(img);
          break;
        case "video":
          $innerEl = $("<video>");
          $innerEl[0].crossOrigin = 'anonymous';
          for (let type in element.src) {
            $innerEl.append($("<source>").attr({
              src: fabric.util.mediaRoot + element.src[type],
              type: type
            }));
          }
          el[0].element = $innerEl[0];
          break;
        default:
          $innerEl = $("<img>");
          this._addCanvasEl(el, element, $innerEl[0]);
      }
    }
    if ($innerEl.constructor === Array) {
      let _span = $("<span>");
      _span.append($innerEl);
      $innerEl = _span;
    }
    $innerEl.addClass("gallery-item")
    el.append($innerEl);

    if (this.removable) {
      let _span = $(this.removeButtonTemplate);
      _span.click(this.removeElement.bind(this,insertedElement));
      el.append(_span);
    }

    if (this.draggable) {
      $innerEl.draggable({
        draggable: "helper",
        droppable: ".canvas-container",
        ngModel: element,
        onMove: data => {
          //todo может быть несколько канвасов
          this.application.canvas._onDragMove && this.application.canvas._onDragMove(data.$event)
        },
        onDrop: data => {
          //todo может быть несколько канвасов
          this.application.canvas._onDrop(data.$event)
        },
        draggableArea: this.draggableArea
      })
    }

    // element.activeElement = $innerEl;
    // element.width = $($innerEl).width();
    // element.height = $($innerEl).height();

    $innerEl.click(this.activate.bind(this, element));
    this.container.append(el);

    this._elements.push(insertedElement)
  }
};

fabric.util.observable(fabric.Gallery.prototype);
