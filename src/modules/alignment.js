// ⿰⿱⿲⿳⿴⿵⿶⿷⿸⿹⿺⿻
fabric.Object.ALIGNMENT_OPTIONS = {
  CENTER: "c",
  CENTER_HORIZONTAL: "h",
  CENTER_VERTICAL: "v",
  LEFT: "l",
  TOP: "t",
  RIGHT: "r",
  BOTTOM: "b",
};

fabric._.extend(fabric.Object.prototype,{
  getAlignment(){
    return ""
  },

  //center object
  _centerObject(hCenter, vCenter, boundingBox) {

    var cp = this.getCenterPoint(),
      left = cp.x,
      top = cp.y;

    if(hCenter) {

      if(boundingBox) {
        left = boundingBox.left + boundingBox.width * 0.5;
      }
      else {
        left = instance.options.stageWidth * 0.5;
      }

    }

    if(vCenter) {
      if(boundingBox) {
        top = boundingBox.top + boundingBox.height * 0.5;
      }
      else {
        top = instance.options.stageHeight * 0.5;
      }

    }

    object.setPositionByOrigin(new fabric.Point(left, top), 'center', 'center');

    instance.stage.renderAll();
    object.setCoords();
  },
  /**
   * Aligns an element.
   *
   * @method alignElement
   * @param {fabric.Object.ALIGNMENT_OPTIONS} option
   */
  setAlignment (option){

    let a = this.canvas._getMovementsLimitsRect(this.movementLimits || "canvas",true);

    let b = this.getBoundingRect(true, true);
    let mX = 0, mY = 0;

    let cX = (a.width)/2 + a.left  - (b.width)/2;
    let cY = (a.height)/2 + a.top  - (b.height)/2;

    let AO = fabric.Object.ALIGNMENT_OPTIONS;
    switch(option){
      case AO.CENTER:
        mX = b.left - cX;
        mY = b.top - cY;
        break;
      case AO.CENTER_HORIZONTAL: {
        mX = b.left - cX;
        break;
      }
      case AO.CENTER_VERTICAL: {
        mY = b.top - cY;
        break;
      }
      case AO.LEFT:
        mX = b.left - a.left;
        break;
      case AO.TOP:
        mY = b.top - a.top;
        break;
      case AO.RIGHT:
        mX = b.left + b.width - a.left - a.width;
        break;
      case AO.BOTTOM:
        mY = b.top + b.height - a.top - a.height;
        break;
    }

    this.set({
      left: this.left - mX,
      top:  this.top - mY
    });
    this.canvas.renderAll();
    this.setCoords();
  }
});

fabric._.extend(fabric.Object.prototype.actions, {
  alignment: {
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment.svg'),
    title: "@object.alignment",
    menu: ["alignmentC", "alignmentH", "alignmentV", "alignmentL", "alignmentT", "alignmentB", "alignmentR"]
  },
  alignmentH: {
    title: "@object.alignmentH",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-h.svg'),
    type: "button",
    value: "alignment",
    option: "h"
  },
  alignmentV: {
    title: "@object.alignmentV",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-v.svg'),
    type: "button",
    value: "alignment",
    option: "v"
  },
  alignmentL: {
    title: "@object.alignmentL",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-l.svg'),
    type: "button",
    value: "alignment",
    option: "l"
  },
  alignmentT: {
    title: "@object.alignmentT",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-t.svg'),
    type: "button",
    value: "alignment",
    option: "t"
  },
  alignmentB: {
    title: "@object.alignmentB",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-b.svg'),
    type: "button",
    value: "alignment",
    option: "b"
  },
  alignmentR: {
    title: "@object.alignmentR",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-r.svg'),
    type: "button",
    value: "alignment",
    option: "r"
  },
  alignmentC: {
    title: "@object.center",
    // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/alignment-c.svg'),
    type: "button",
    value: "alignment",
    option: "c"
  }
});