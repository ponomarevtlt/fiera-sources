/**
 * Привязать один объект к другому. Координаты привязанного объекта будут вычисляться относительно Выбранного объекта на канвасе.
 * Объект будет перемещаться ввемсте с объектом, к которому привязан.
 */
fabric._.extend(fabric.Object.prototype, {
  // specialProperties: fabric.Object.prototype.specialProperties.concat(["relativeObject","relativeTop","relativeLeft","rotatingStep"]),
  /**
   * ����������� �������� ������ 45 ��������
   */
  _step_Rotating: function (angle) {
    var _a = this.angle;

    var x = parseInt(_a / angle) * angle;
    if (_a - x < angle / angle) {
      this.setAngle(x);
    } else {
      this.setAngle(x + angle);
    }
  },
  setRotatingStep: function(value){
    this.rotatingStep = value;
    this.on("rotating", function(e){
      if(!e.e.shiftKey){
        this._step_Rotating(value)
      }
    });
  },
  updateRelativeCoordinates: function () {
    this.setCoords();
    var _c = this.relativeObject.getCenterPoint();
    this.relativeLeft = this.left - _c.x;
    this.relativeTop = this.top - _c.y;
  },
  setRelativeTop: function(val) {
    this.relativeTop = val;
    if(this.relativeObject){
      this.relativeObject.updateBindedObject(this);
    }
  },
  setRelativeLeft: function(val) {
    this.relativeLeft = val;
    if(this.relativeObject){
      this.relativeObject.updateBindedObject(this);
    }
  },
  updateBindedObject: function(tag) {
    var _c = this.getCenterPoint();
    tag.left = _c.x + tag.relativeLeft;
    tag.top = _c.y   + tag.relativeTop;
    tag.setCoords();
  },
  setRelative: function(obj){
    var tag = this;


    if (obj.constructor === String) {
      this.on("added",function(){
        obj = this.canvas.getObjectByID(obj.substring(1));
        this.setRelative(obj);
      });
      return;
    }

    var _foo = obj.updateBindedObject.bind(obj,tag);

    this.relativeObject = obj;


    var _boundRect = obj.getBoundingRect();
    if(this.relativeLeft === undefined) {
      this.relativeLeft = this.left ? obj.left - this.left : _boundRect.width/2 / obj.canvas.getZoom() + 20;
    }
    if(this.relativeTop === undefined){
      this.relativeTop = this.top ? obj.top - this.top : _boundRect.height/2 / obj.canvas.getZoom() + 20;
    };

    function onAdded(){
      var _proc = this.canvas.processing;
      if(!_proc)this.canvas.processing = true;
      this.canvas.add(tag);
      if(!_proc)this.canvas.processing = false;
    }
    function onRemoved() {
      var _proc = this.canvas.processing;
      if(!_proc)this.canvas.processing = true;
      tag.removeFromCanvas();
      if(!_proc)this.canvas.processing = false;
    }
    tag.on('removed', function () {
      // var _proc = this.canvas.processing;
      // if(!_proc)this.canvas.processing = true;
      // obj.removeFromCanvas();
      // if(!_proc)this.canvas.processing = false;
      obj.off('removed', onRemoved);
      obj.off('added', onAdded);
      obj.off('moving', _foo);
      obj.off('scaling', _foo);
      obj.off('rotating', _foo);
    });

    obj.on('added', onAdded );
    obj.on('removed', onRemoved);
    obj.on('moving rotating scaling modified', _foo);
    tag.on('moving rotating scaling modified', tag.updateRelativeCoordinates);

    obj.updateBindedObject(tag);
  }
});
