
function setActions(actions) {
  if(this.interactive === false){
    return;
  }
  for (let i in actions) {
    actions[i].id = i;
  }
  this.actions = fabric.util.deepExtend({}, this.__proto__.actions, actions);
}

fabric.plugins["actions"] = {
  prototypes: {
    Canvas: {
      setActions: setActions,
      deafultText: "You can add some text here",
      defaultTextType: "i-text",
      selectAll(){
        let selection = new fabric.ActiveSelection( this.getObjects(), {canvas: this } );
        this.setActiveObject(selection);
        this.renderAll();
      },
      addRect: function () {
        this.createObject({
          width: 100,
          height: 100,
          active: true,
          position: "center",
          type:   "rect",
          clipTo: this.activeArea,
          movementLimits : this.activeArea
        });
      },
      addCircle: function () {
        this.createObject({
          radius: 50,
          active: true,
          position: "center",
          type:   "circle",
          clipTo: this.activeArea,
          movementLimits : this.activeArea
        });
      },
      addTriangle: function () {
        this.createObject({
          width: 100,
          height: 100,
          active: true,
          position: "center",
          type:   "triangle",
          clipTo: this.activeArea,
          movementLimits : this.activeArea
        });
      },
      addText: function () {
        this.createObject({
          active: true,
          position: "center",
          type:   this.defaultTextType,
          clipTo: this.activeArea,
          text: this.deafultText,
          movementLimits : this.activeArea
        });
      },
      defaultImageURL: "http://",
      addImageByURL(){
        let url = window.prompt("enter image url",this.defaultImageURL);
        if(!url)return;
        this.createObject({
          active: true,
          position: "center",
          type: "image",
          clipTo: this.activeArea,
          movementLimits : this.activeArea,
          src: url,
        })
      },
      removeActive(){
        this._activeObject.removeFromCanvas()
      },
      actions: {
        clear: {
          title: "@canvas.clear",
          className: 'fa fa-trash',
        },
        clearObjects: {
          title: "@canvas.clearObjects",
          className: 'fa fa-trash'
        },
        selectAll: {
          keyCode: 'a',
          ctrlKey: true,
          title: 'Select All',
          type: 'key'
        },
        backgroundColor: {
          className: 'fas fa-fill',
          title: "@canvas.setBackgroundColor",
          observe: "loading:end",
          type: "color",
          colorpicker: {
            opacity:      false,
            text:         false
          }
        },
        add: {
          className: 'fa fa-plus',
          title: "@canvas.add",
          menu: [
            "uploadImage",
            "addText",
            "addRect",
            "addCircle",
            "addTriangle",
            "addImageByURL",
            "addQrCode",
          ]
        },
        addText: {
          className: 'fa fa-font',
          title: "@canvas.addText"
        },
        addRect: {
          className: 'fa fa-square-full'
        },
        addCircle: {
          className: 'fa fa-circle',
        },
        addTriangle: {
          className: 'fas fa-play',
        },
        addImageByURL: {
          className: 'fas fa-image'
        }
      }
    },
    Object: {
      setActions: setActions,
      actions: {
        origin: {
          type: "button",
          className: 'fa fa-th broken',
          title: "@object.origin",
          action: function(){

          }
        },
        movement: {
          className: 'fa fa-arrows',
          menu: [
            "moveUp",
            "moveDown",
            "moveLeft",
            "moveRight"
          ]
        },
        moveUp: {
          className: "fa fa-arrow-up",
          type: "key",
          keyCode: 38,
        },
        moveDown: {
          className: "fa fa-arrow-down",
          type: "key",
          keyCode: 40,
        },
        moveLeft: {
          className: "fa fa-arrow-left",
          type: "key",
          keyCode: 37,
        },
        moveRight: {
          className: "fa fa-arrow-right",
          type: "key",
          keyCode: 39,
        },
        boundingRect: {
          type: 'label',
          template: '<dt>L:</dt><dd class="{leftClass}" title="{left}">{roundLeft}</dd><dt>T:</dt><dd class="{topClass}"  title="{top}">{roundTop}</dd><dt>R:</dt><dd class="{rightClass}" title="{right}">{roundRight}</dd><dt>B:</dt><dd class="{bottomClass}"  title="{bottom}">{roundBottom}</dd>',
          observe: "modified scaling moving rotating",
          get: function () {
            var _rect = this.getBoundingRect();

            if (this.movementLimits) {

              if (this.movementLimits == this.canvas) {
                var _v = this.canvas.viewportTransform;
                var _mlr = {
                  left: _v[4],
                  top: _v[5],
                  width: (this.canvas.originalWidth || this.canvas.width) * _v[0],
                  height: (this.canvas.originalHeight || this.canvas.height) * _v[3],
                  right: 0,
                  bottom: 0
                }
              } else {
                _mlr = this.movementLimits.getBoundingRect();
              }


              _rect.bottom = this.movementLimits.height - _rect.height;
              var _t = _rect.top - _mlr.top;
              var _l = _rect.left - _mlr.left;
              var _r = _mlr.width - _rect.width - _l;
              var _b = _mlr.height - _rect.height - _t;
            } else {
              _t = _rect.top;
              _l = _rect.left;
              _b = this.canvas.height - _rect.height - _rect.top;
              _r = this.canvas.width - _rect.width - _rect.left;
            }

            return {
              topClass: _t > 0 ? "positive" : _t < 0 ? "negative" : "zero",
              bottomClass: _b > 0 ? "positive" : _b < 0 ? "negative" : "zero",
              leftClass: _l > 0 ? "positive" : _l < 0 ? "negative" : "zero",
              rightClass: _r > 0 ? "positive" : _r < 0 ? "negative" : "zero",
              top: _t,
              left: _l,
              bottom: _b,
              right: _r,
              roundTop: Math.round(_t),
              roundLeft: Math.round(_l),
              roundBottom: Math.round(_b),
              roundRight: Math.round(_r)
            }
          }
        },
        left: {
          type: 'number',
          title: '@object.left',
          set: function (val) {
            this.left = val;
            this.fire("modified");
            this.canvas.fire("object:modified", {target: this});
            this.canvas.renderAll();
          },
          get: function () {
            return this.left;
          },
          observe: "modified"
        },
        top: {
          type: 'number',
          title: '@object.top',
          set: function (val) {
            this.top = val;
            this.fire("modified");
            this.canvas.fire("object:modified", {target: this});
            this.canvas.renderAll();
          },
          get: function () {
            return this.top;
          },
          observe: "modified"
        },
        position: {
          className: 'fa fa-arrows-alt',
          title: '@object.position',
          type: 'menu',
          menu: ["left","top"]
        },
        opacity: {
          title: '@object.opacity',
          type: 'range',
          min: 0,
          max: 1,
          step: 0.05
        },
        dimensions: {
          // icon: 'data:image/svg+xml;base64,' + require('base64-loader!./../media/dimensions.svg'),
          title: '@object.dimensions',
          type: 'menu',
          menu: ["height","width"]
        },
        width: {
          type: 'number',
          className: 'fa fa-arrows-w',
          title: '@object.width',
          set: function (val) {
            this.saveState();
            this.dimensionsWidthValue = val;
            this.scaleToWidth(val * this.canvas.getZoom());
            // this.canvas.fireModifiedIfChanged(this);
            this.fire("modified", {});
            this.canvas.fire("object:modified", {target: this});
            this.canvas.renderAll();
            delete this.dimensionsWidthValue;
          },
          get: function () {
            if (this.dimensionsWidthValue) {
              return this.dimensionsWidthValue;
            }
            return Math.round(this.getBoundingRect().width / this.canvas.getZoom());
          },
          observe: "modified"
        },
        height: {
          className: 'fa fa-arrows-v',
          type: 'number',
          title: '@object.height',
          set: function (val) {
            this.saveState();
            this.scaleToHeight(val * this.canvas.getZoom());
            this.dimensionsHeightValue = val;
            this.fire("modified", {});
            this.canvas.fire("object:modified", {target: this});
            this.canvas.renderAll();
            delete this.dimensionsHeightValue;
          },
          get: function () {
            if (this.dimensionsHeightValue) {
              return this.dimensionsHeightValue;
            }
            return Math.round(this.getBoundingRect().height / this.canvas.getZoom());
          },
          observe: "modified"
        },
        centerAndZoomOut: {
          className: 'fa fa-search-plus',
          title: '@object.centerAndZoomOut'
        },
        flip: {
          className: 'fa fa-arrows-h',
          title: "@object.flip",
          action: 'flip'
        },
        flop: {
          className: 'fa fa-arrows-v',
          title: "@object.flop"
        },
        remove: {
          title: "@object.remove",
          className: 'fa fa-trash',
          key: "Delete",
          action: "removeFromCanvas"
        },
        duplicate: {
          title: "@object.duplicate",
          className: 'fa fa-clone'
        },
        fill: {
          className: 'fa fa-paint-brush',
          title: "@object.fill",
          type: 'color',
          observe: "modified"
        },
        fillGradient: {
          className: 'fas fa-palette',
          title: "@object.fillGradient",
          // type: 'effect',
          type: 'gradient',
          value: "fill",
          observe: "modified"
          // source: {
          //   type: 'effect',
          //   className: 'fa fa-file-image-o',
          //   title: "@image.source",
          //   actionParameters: function ($el, data) {
          //     data.target.application.createGallery(data.target, $el);
          //   }
          // }
        },
        stroke: {
          className: 'fa fa-pen-nib',
          title: "@object.stroke",
          type: 'color'
        },
        strokeWidth: {
          className: 'fa fa-pen-nib',
          title: "@object.strokeWidth",
          type: 'number'
        },
        rotateLeft: {
          title: "@object.rotateLeft",
          className: 'fa fa-chevron-circle-left',
          action: function () {
            this.saveState();
            this._normalizeAngle();
            let desiredAngle = (Math.floor(this.angle / 90) - 1) * 90;
            this.rotate(desiredAngle);
            this.canvas.renderAll();

            this.fire("modified",{});
            this.canvas.fire("object:modified",{target: this});
          }
        },
        rotateRight: {
          title: "@object.rotateRight",
          className: 'fa fa-chevron-circle-right',
          action: function () {
            this.saveState();
            this._normalizeAngle();
            let desiredAngle = (Math.floor(this.angle / 90) + 1) * 90;
            this.rotate(desiredAngle);
            this.canvas.renderAll();
            this.fire("modified",{});
            this.canvas.fire("object:modified",{target: this});
          }
        },
        stretch: {
          title: "@object.stretch",
          className: 'fa fa-arrows-alt',
          action: function () {
            this.canvas.saveState(["backgroundImage"]);
            this.application.history.processing = true;
            this.setAsBackgroundImage();

            this.application.history.processing = false;
            this.canvas.fire("modified");
          }
        }
      }
    },
    ActiveSelection: {
      removeFromCanvas: function(){
        this.canvas.processing = true;
        this.canvas.discardActiveObject();
        for(let o of this._objects){
          this.canvas.remove(o);
        }
        this.canvas.processing = false;
        this.canvas.fire("group:removed",{target: this});
        this.canvas.renderAll();
      },
      duplicate: function(){
        this.canvas._discardActiveObject();
        let group = [];
        for(let o of this._objects){
          let clone = o.duplicate();
          group.push(clone);
          this.canvas.add(clone);
        }

        let aGroup = new fabric.ActiveSelection(group, {
          canvas: this.canvas
        });
        // this.fire('selection:created', { target: group });
        this.canvas.setActiveObject(aGroup, e);
      }
    },
    Text: {
      fontSizeOptions: [6,7,8,9,10,12,14,18,24,36,48,64],
      actions: {
        fill: {
          className: 'fa fa-paint-brush',
          type: 'color',
          title: "@text.fill",
          get: function () {
            //texture pattern fill fix
            return typeof this.fill === "string" ? this.fill : "transparent";
          },
          // set: function (value) {
          //   this.setFill(value);
          // }
        },
        bgColor: {
          className: "fa fa-brush",
          title: "@text.backgroundColor",
          type: 'color',
          value: 'bgColor'
        },
        textBgColor: {
          title: "@text.textBackgroundColor",
          type: 'color',
          value: 'textBgColor'
        },
        bold: {
          type: "checkbox",
          title: "@text.bold",
          value: 'bold',
          className: 'fa fa-bold'
        },
        italic: {
          className: 'fa fa-italic',
          type: "checkbox",
          title: "@text.italic",
          value: 'italic',
        },
        underline: {
          type: "checkbox",
          title: "@text.underline",
          value: 'underline',
          className: 'fa fa-underline'
        },
        linethrough: {
          type: "checkbox",
          title: "@text.linethrough",
          value: 'linethrough',
          className: 'text-linethrough fa fa-strikethrough'
        },
        overline: {
          type: "checkbox",
          title: "@text.overline",
          value: 'overline',
          className: 'text-overline fa fa-overline'
        },
        textAlignCenter: {
          title: "@text.alignCenter",
          option: 'center',
          value: "textAlign",
          className: 'fa fa-align-center broken-feature'
        },
        textAlignLeft: {
          title: "@text.alignLeft",
          option: 'left',
          value: "textAlign",
          className: 'fa fa-align-left  broken-feature'
        },
        textAlignRight: {
          title: "@text.alignRight",
          option: 'right',
          value: "textAlign",
          className: 'fa fa-align-right  broken-feature'
        },
        textAlignJustify: {
          title: "@text.alignJustify",
          option: 'justify',
          value: "textAlign",
          className: 'fa fa-align-justify  broken-feature'
        },
        textAlign: {
          type: 'options',
          title: "@text.align",
          value: "textAlign",
          menu: ["textAlignCenter","textAlignLeft","textAlignRight","textAlignJustify"]
        },
        // fontFamily: {
        //   title: "@text.fontFamily",
        //   type: 'fontFamily',
        //   className: 'fa fa-font',
        //   value: 'fontFamily',
        //   data: function () {
        //     return this.application.fonts
        //   }
        // },
        font: {
          title: "@text.font",
          className: 'fa fa-font',
          menu: ['fontFamily','fontSize'],
          // subMenuClass: "fiera-toolbar-submenu-row"
        },
        // fontSize: {
        //   type: 'number',
        //   title: "@text.fontSize",
        //   value: 'fontSize'
        // },

        fontFamily: {
          title: "@text.fontFamily",
          // itemClassName: "images-selector",
          // className: "fa fa-filter",
          type: "select",
          inline: true,
          value: 'fontFamily',
          staticOptions: function(target){
            return target.application.fonts.map(font => ({id: font, text: font}))
          },
          // options (target) {
          //   return target.application.fonts.map(font => ({id: font, text: font}))
          // },
          dropdown: {
            // theme: "toolbar-fontfamily-selector",
            //minimumResultsForSearch: 1,
            templateSelection (state) {
              if (state.any) {
                return state.text;
              }
              return $(`<span style="font-family: ${state.id}">${state.text}</span>`);
            },
            templateResult (state) {
              return $(`<span style="font-family: ${state.id}">${state.text}</span>`);
            },
          }
        },
        fontSize: {
          type: "select",
          title: "@text.fontSize",
          itemClassName: "fontsize-selector",
          value: 'fontSize',
          inline: true,
          staticOptions: function (target) {
            return target.fontSizeOptions.map((el)=> {
              return {
                id: el,
                text: el
              }
            });
          },
          dropdown: {
            // theme: "toolbar-fontsize-selector select2-container--default",
            tags: true,
            width: 60,
            //minimumResultsForSearch: 1,
            templateSelection (state) {
              if (state.any) {
                return state.text;
              }
              return $(`<span>${state.text}</span>`);
            },
            templateResult (state, container, data) {
              let $el = $(`<span>${state.text}</span>`);
              return $el;
            },
          }
        },
        lineHeight: {
          type: 'number',
          title: "@text.lineHeight",
          value: 'lineHeight',
          step: 0.1
        },
        /*textFont: {
         insert: 'textFontSizeTools',
         type: 'menu',
         title: 'font',
         className: 'fa fa-font',
         menu: {
         textFontSizeDecrease: {
         title: 'decreaseFontSize',
         action: _TEX.decreaseFontSize,
         className: 'fa fa-font font-size-decrease'
         },
         textFontSizeIncrease: {
         title: 'increaseFontSize',
         action: _TEX.increaseFontSize,
         className: 'fa fa-font font-size-increase'
         }
         }
         }*/
        advancedColorsTools: {
          className: 'colors',
          type: 'menu',
          title: 'colors',
          toggled: true,
          menu: ["fill", "textBgcolor", "textTextbgcolor", "fill"]
        },
        textStyle: {
          type: 'menu',
          title: "@text.textStyle",
          toggled: true,
          className: 'fa fa-font',
          style: 'generateTextStyle',
          menu: [
            "bold", "italic", "underline", "linethrough",
            "overline", "textAlign", "fontFamily", "fontSize"
          ]
        }
      }
    },
    IText:{
      actions: {
        enterEditing: {
          insert: 'editTool',
          className: 'fa fa-pencil-square-o',
          title: '@text.enterEditing'
        }
      }
    },
    Image: {
      fitting: "manual",
      fittingProperty: {
        options: ["manual","cover","contain","fill"],
        optionsIcons: ["fa fa-bullseye","fa fa-dot-circle","far fa-dot-circle", "fa fa-circle"]
      },
      setAsBackgroundImage(){

        this._normalizeAngle();
        let _deviation = this.angle % 90;
        this.angle = Math.floor(this.angle / 90) * 90;
        if (_deviation > 45) {
          this.angle += 90;
        }
        let scaleX, scaleY;

        let canvas = this.canvas;
        if (this.angle === 90 || this.angle === 270) {
          scaleX = canvas.width / this.height;
          scaleY = canvas.height / this.width;
        } else {
          scaleX = canvas.width / this.width;
          scaleY = canvas.height / this.height;
        }
        let backgroundOptions = this.storeObject();
        delete backgroundOptions.type;
        backgroundOptions.element = this._element;
        backgroundOptions.src = this.src;

        canvas.setBackgroundImage(backgroundOptions);
        canvas.backgroundImage._setOriginToCenter();
        canvas.backgroundImage.set({
          scaleX: scaleX,
          scaleY: scaleY,
          left: canvas.width / 2,
          top: canvas.height / 2
        });
        canvas.backgroundImage._resetOrigin();
        this.canvas._history_removed_object = this;

        this.removeFromCanvas();
      },
      setFitting: function(value){
        this.fitting = value;
        this.dirty = true;
        this.canvas && this.canvas.renderAll();
      },
      actions: {
        fitManual: {
          title: "@image.fitManual",
          className: "fa fa-bullseye",
          value: "fitting",
          option: "manual"
        },
        fitCover: {
          title: "@image.fitCover",
          className: "fa fa-dot-circle",
          value: "fitting",
          option: "cover"
        },
        fitContain: {
          title: "@image.fitContain",
          className: "far fa-dot-circle",
          value: "fitting",
          option: "contain"
        },
        fitFill: {
          title: "@image.fitFill",
          className: "fa fa-circle",
          value: "fitting",
          option: "fill"
        },
        fitting: {
          title: "@image.fit",
          type: 'options',
          value: "fitting",
          menu: ["fitManual","fitCover","fitContain","fitFill"]
        },
        source: {
          type: 'effect',
          className: 'fa fa-file-image-o',
          title: "@image.source",
          actionParameters: function ($el, data) {
            data.target.application.createGallery(data.target, $el);
          }
        }
      }
    }
  }
};