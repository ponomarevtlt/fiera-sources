


fabric._.extend(fabric.Object.prototype, {
  setShape(shape, callback) {
    if (!shape) {
      shape = {};
    }
    if(!shape.offsets)delete shape.offsets;

    shape.width = this.width +2 ;
    shape.height = this.height +2;
    this.shape = shape;

    function createImageCropClipPath (path) {
      let el = new fabric.Path(path,{});
      el.left -= this.width/2 + 1;
      el.top -= this.height/2 + 1;
      this.dirty = true;
      this.canvas && this.canvas.renderAll();
      callback && callback(el);
    }

    if(Object.keys(shape).length === 2 && shape.width && shape.height){
      createImageCropClipPath.call(this,`M ${0} ${0} h ${shape.width} v ${shape.height} h ${-shape.width} z`);
    }
    else{
      fabric.util.shapes.makePath(shape,createImageCropClipPath.bind(this))
    }
  },
  eventListeners: fabric.util.extendArraysObject(fabric.Object.prototype.eventListeners, {
    "scaling":  function()  {
      if(this.shape){
        this.shape.width = this.width;
        this.shape.height = this.height;

        let path = fabric.util.shapes.makePath(this.shape);
        this.imageCropClipPath = new fabric.Path(path,{});
        this.imageCropClipPath.left -= this.width/2;
        this.imageCropClipPath.top -= this.height/2;



        // this.clipPath.scaleX = this.width /shape.width;
        // this.clipPath.scaleY = this.height  / shape.height;

        // this.clipPath.path = fabric.util.shapes.stretch(this.shape,this);
        // this.clipPath.width = this.width *this.scaleX;
        // this.clipPath.height = this.height *this.scaleY;
        // this.clipPath.scaleX = 1/this.scaleX;
        // this.clipPath.scaleY = 1/this.scaleY;
        // this.clipPath.pathOffset = {x: this.width/2, y: this.height/2};
        // this.clipPath.left =0;
        // this.clipPath.top =0;
        // this.clipPath.dirty = true;
        this.dirty = true;
      }
    },
    // "element:modified scaling": function () {
    //
    //   if(this.shape){
    //     this.clipPath.path = fabric.util.shapes.stretch(this.shape,this);
    //   }
    //   // this.clipPath.path = fabric.util.shapes.stretch(this.clipPath.originalPath,this.offsets);
    //   // this._setShapeOffsets();
    // }
  })
});



// fabric._.extend(fabric.Canvas.prototype, {
//   _setSVGObject: function(markup, instance, reviver) {
//     let result = instance.toSVG(reviver);
//     if(instance.clipTo && instance.clipTo.constructor !== Function) {
//       let id = instance.clipTo.id || ++fabric.Object.__uid;
//       const clipId = `Clipped${id}`;
//       if(!(id in this._clipPaths)){
//         markup.push(`<clipPath id="${clipId}">${instance.clipTo.toSVG(reviver)}</clipPath>`);
//         this._clipPaths.push(id);
//       }
//       markup.push(`<g clip-path="url(#${clipId})">${result}</g>`);
//     }else{
//       markup.push(result);
//     }
//   }
// });

// fabric._.extend(fabric.Object.prototype, {
//   /**
//    * get ClipTo value to srore object as JSON
//    * @returns {string|null}
//    */
//   store_clipTo () {
//     return this.clipTo && (this.clipTo.id && '#' + this.clipTo.id || this.clipTo.toString()) || null;
//   },
//   setClipTo: function (val) {
//     if(!val){
//       delete this.clipTo;
//       return;
//     }
//
//     /**
//      * val is String
//      */
//     this.clipTo = val;
//
//     /**
//      * val is FabricJS.Object
//      */
//     if (typeof val !== 'string') {
//       if(!val.id){
//         val.id = ++fabric.Object.__uid;
//       }
//     }
//
//     /**
//      * val is Function
//      */
//     let functionBody = fabric.util.getFunctionBody(val);
//     if (typeof functionBody !== 'undefined') {
//       this.clipTo = new Function('ctx', functionBody);
//     }
//   }
// });
//
// fabric.util.clipContext = function (receiver, ctx) {
//   let ct = receiver.clipTo;
//   if(!ct){
//     return;
//   }
//   if (ct.constructor === String) {
//     if (ct === "canvas") {
//       ct = receiver.clipTo = receiver.canvas;
//     }
//     else{
//       let id = ct.substring(1);
//       let object = receiver.canvas.getObjectByID(id);
//       if(object){
//         receiver.clipTo = object;
//       }else{
//         console.warn("error: clipTo not found" + ct);
//         delete receiver.clipTo;
//         return;
//       }
//     }
//   }
//   ctx.save();
//   if (ct.constructor === Function) {
//     ctx.beginPath();
//     receiver.clipTo(ctx);
//   }else if (ct.type === "canvas") {
//     receiver.clipTo = receiver.canvas;
//     let _w = ct.originalWidth || ct.width, _h = ct.originalHeight || ct.height, o = ct.offsets, _zoom = ct.getZoom();
//     ctx.rect(o.left, o.top, _w /*/_zoom */ - o.left - o.right, _h /*/_zoom*/ - o.top - o.bottom);
//   }else{
//     let fromLeft = false;
//     let center = fromLeft ? receiver._getLeftTopCoords() : receiver.getCenterPoint();
//     ctx.transform(1, Math.tan(fabric.util.degreesToRadians(-receiver.skewY)), 0, 1, 0, 0);
//     ctx.transform(1, 0, Math.tan(fabric.util.degreesToRadians(-receiver.skewX)), 1, 0, 0);
//     ctx.scale(
//       (1 / receiver.scaleX) * (receiver.flipX ? -1 : 1),
//       (1 / receiver.scaleY) * (receiver.flipY ? -1 : 1)
//     );
//     ctx.rotate(fabric.util.degreesToRadians(-receiver.angle));
//     ctx.translate(-center.x, -center.y);
//
//     if (ct.type === "slide" || ct.type === "canvas") {
//       let _w = ct.originalWidth || ct.width;
//       let _h = ct.originalHeight || ct.height;
//       let o = ct.offsets;
//       let _zoom = ct.getZoom();
//       ctx.rect(o.left,o.top, _w /*/_zoom */-  o.left - o.right, _h /*/_zoom*/ -  o.top - o.bottom);
//     } else {
//       ctx.globalAlpha   = 0;
//       ct.transform(ctx);
//       ctx.beginPath();
//       ct._render(ctx);
//       ctx.closePath();
//     }
//     ctx.restore();
//     ctx.clip();
//   }
// };