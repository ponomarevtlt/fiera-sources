'use strict';

fabric.objectsLibrary = {
  text: {
      title: "Text",
      type: "text",
      text: "Текст"
  },
  iText: {
    title: "IText",
    type: "i-text",
    text: "Текст"
  },
  textbox: {
    title: "Textbox",
    type: "textbox",
    text: "Текст",
    width: 100
  },
  line: {
    type: "line",
    strokeWidth: 5,
    stroke: "black",
    scaleX: 1,
    scaleY: 1,
    x1: 0,
    x2: 100,
    y1: 0,
    y2: 50
  },
  triangle: {
    type: "triangle",
    width: function(w,h){ return Math.min(w,h) - 4},
    height: function(w,h){ return Math.min(w,h) - 4}
  },
  rectangle: {
    type: "rect",
    width: function(w,h){ return Math.min(w,h) - 4},
    height: function(w,h){ return Math.min(w,h) - 4}
  },
  polygon: {
    scaleX: 1,
    scaleY: 1,
    type: "polygon",
    points: function(w,h) {
      return [
        {x: 25, y: 1},
        {x: 31, y: 18},
        {x: 49, y: 18},
        {x: 35, y: 29},
        {x: 40, y: 46},
        {x: 25, y: 36},
        {x: 10, y: 46},
        {x: 15, y: 29},
        {x:  1, y: 18},
        {x: 19, y: 18}
      ];
    }
  },
  path: {
    "type": "path",
    "path":  "m581.077942,2.537359c-2.053223,0.047071 -4.04071,0.188348 -6.108093,0.352907c-33.05542,2.663918 -62.235901,19.640541 -77.057678,44.925953l-7.8573,19.135319c1.698822,-6.633144 4.302979,-13.065384 7.8573,-19.135319c-26.430695,-22.16293 -63.531677,-32.388445 -100.192383,-27.574373c-36.661469,4.788353 -68.503082,24.041758 -85.901978,51.935225c-49.116486,-24.490013 -110.34288,-22.999454 -157.711807,3.860092c-47.369164,26.86068 -72.61673,74.40551 -64.941162,122.38308l5.021355,19.49968c-2.263329,-6.38501 -3.960793,-12.887695 -5.021355,-19.49968l-0.761948,1.798569c-41.179165,3.625244 -74.945375,29.465134 -83.716398,64.059235c-8.771805,34.597748 9.46701,70.085876 45.185621,87.96701l55.776558,10.973114c-19.480217,1.291962 -38.915543,-2.534515 -55.776558,-10.973114c-27.5478,24.96817 -33.888516,61.935303 -15.71492,92.467834c18.173733,30.524719 56.988899,48.110687 97.030457,44.11734l24.339722,-5.21109c-7.827499,2.651611 -15.960983,4.379059 -24.339722,5.21109c22.730042,33.857269 60.428192,58.556244 104.66893,68.383514c44.2491,9.81366 91.240952,4.014771 130.425949,-16.094604c31.96701,40.793823 88.707642,62.217468 145.596313,54.99707c56.902466,-7.219666 103.833984,-41.81427 120.501343,-88.770996l5.781433,-26.239532c-0.863708,8.909546 -2.742249,17.681366 -5.781433,26.239532c39.133301,20.753662 88.353333,21.927307 128.785095,3.049316c40.439819,-18.874084 65.665771,-54.869049 66.036133,-94.078247l-14.495605,-58.580597l-57.105713,-39.630768c44.163452,22.374573 71.992615,56.467255 71.601318,98.211365c52.49707,0.448181 97.103394,-35.956573 117.112427,-77.726288c20.011597,-41.769836 12.443604,-89.396759 -19.864929,-125.164642c13.401184,-26.637695 12.609985,-56.937332 -2.183472,-83.034088c-14.786194,-26.097893 -42.065491,-45.476891 -74.873047,-53.098335c-7.341431,-34.580929 -37.602661,-62.404482 -77.600708,-71.526293c-39.998474,-9.121368 -82.584839,2.123992 -109.364807,28.926123l-16.258179,22.19817c4.157959,-8.018612 9.583923,-15.495213 16.258179,-22.19817c-18.876953,-21.060713 -48.486023,-32.954061 -79.348938,-32.155401l0,0z",
    "width": function(w,h){return w - 4 },
    "height": function(w,h){return h - 4}
  },
  ellipse: {
    "type": "ellipse",
    "rx": function(w,h){return w /2 - 4},
    "ry": function(w,h){return h/ 2 - 4}
  },
  circle: {
    type: "circle",
    radius: function(w,h){ return Math.min(w,h)/2 - 4}
  }
};

fabric._.extend(fabric.Editor.prototype, {
  getLibraryElements: function(options){
    const category = fabric._.findWhere(this.library,{id: options.category});

    var category_items = fabric.util.deepClone(category.items);

    return fabric._.map(fabric.util.deepClone(category.items),el => fabric._.defaults(el,category.template))
  },
  /**
   *
   * @param value
   * @param callback
   *
   * @example
   [{
    "id": 5,
    "title": "video",
    "template": {
      "type": "video"
    },
    "items": [
      {
        "src": "video.mpg"
        "height": 360,
        "width": 480
      }]
    }]
   */
  setLibrary: function (data, callback) {
    data.forEach(category => {
      if(!category.id){
        category.id = ++fabric.util._idCounter
      }
    });
    this.library = data;
    callback && callback();
  },
  getObjectsList: function(w,h){
    var _lib = [];
    for(var i in fabric.objectsLibrary) {
      console.log(i);
      var o = fabric.util.deepClone(fabric.objectsLibrary[i]);
      o.id = i;
      _lib.push(o);
      o.title = o.title || o.type;
      if(o.width === 0 ) o.width = w;
      if(o.height === 0 ) o.height = h;
      for(var j in o){
        if(o[j].constructor === Function){
          o[j] = o[j].call(o,w,h);
        }
      }
    }
    return _lib;
  }
});

