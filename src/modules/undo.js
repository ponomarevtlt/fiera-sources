'use strict';

fabric.History = require('./../plugins/history');

fabric._.extend(fabric.Editor.prototype.actions, {
  undo: {
    key: 'z',
    observe: 'changed',
    className: 'fa fa-undo',
    title: '@history.undo',
    enabled: 'canUndo',
    action: function () {
      this.undo();
      this.application.fire("modified");
    },
    target: function () {
      return this.history;
    }
  },
  records: {
    title: '@history.list',
    itemClassName: "images-selector",
    className: "fa fa-history",
    type: "select",
    target: function () {
      return this.history;
    },
    dropdown: {
      width: 300,
      templateSelection: function (state) {
        if (state.any) {
          return state.text;
        }
        return $('<span>' + state.id + ":" + state.type + '</span>');
      },
      templateResult: function (state, container, data) {
        if (!state.type)return;
        let keys = state.originalState && Object.keys(state.originalState).join(',') || '';
        let otype = state.object && state.object.type || '';
        return $(`<span title='${state.id}:${state.type}(${otype} ${keys})'>${state.id}:${state.type}(${otype} ${keys})</span>`)
      },
    },
    value: {
      observe: 'changed',
      set: function (val, filtersData) {
        this.goto(val);
      },
      get: function () {
        return this.records[this.current].id;
      },
      options: function () {
        return this.records;
      }
    }
  },
  redo: {
    key: 'y',
    observe: 'changed',
    className: 'fa fa-redo',
    title: '@history.redo',
    enabled: 'canRedo',
    action: function () {
      this.redo();
      this.application.fire("modified");
    },
    target: function () {
      return this.history;
    }
  },
  history: {
    title: '@editor.history',
    type: 'menu',
    menu: ["redo", "undo"]
  }
});

fabric._.extend(fabric.Canvas.prototype, {
  _restoreGroup (object,objectsStates){
    if(this._activeObject !== object){
      this.setActiveObject(object);

      if(object._objects[0].group !== object){
        var objs = object._objects;
        object._objects = [];
        for(var i in objs){
          object.add(objs[i]);
          objs[i].set(objectsStates[i])
        }
      }
    }
  },
  onCanvasModified (e) {
    if (!this._isHistoryActive())return;
    let states = this.getModifiedStates();
    if(!states && !this._history_removed_object)return;
   this.application.fire("modified",e);
    
    this.history.add({
      canvas:  this,
      object: this._history_removed_object,
      originalState:  states.original,
      modifiedState:  states.modified,
      type: 'canvas:modified',
      undo: a => {
        if(a.object){
          this.undoDeleting(a)
        }
        this.undoChanges(a);
      },
      redo: a => {
        if(a.object){
          this.redoDeleting(a)
        }
        this.redoChanges(a);
      }
    });
    delete this._history_removed_object;
  },
  onCanvasCleared (e) {
    if (!this._isHistoryActive())return;
    let states = this.getModifiedStates();
    if(!e.objects.length && ! states){return;}
   this.application.fire("modified",e);

    this.history.add({
      originalState:  states && states.original,
      modifiedState:  states && states.modified,
      originalObjects: e.objects,
      type: 'canvas:cleared',
      redo: a => {
        this.discardActiveObject();
        this._clearObjects();
        if(a.originalState){
          this.set(a.modifiedState)
              .fire('modified')
        }
        this.renderAll();
      },
      undo: a => {
        Array.prototype.push.apply(this._objects, a.originalObjects);
        if(a.modifiedState){
          this.set(a.originalState)
              .fire('modified')
        }
        this.renderAll();
      }
    });
  },
  onObjectModified (e) {
    if (!this._isHistoryActive())return;
    let target = e.target, states = e.target.getModifiedStates();
    if(!states)return;
   this.application.fire("modified",e);


    if(target.type === "activeSelection"){


      target.includeDefaultValues = true;
      var objectsStates = fabric._.map(target.toObject().objects, function(currentObject) {
        return fabric._.pick(currentObject, "left", "top", "angle", "scaleX", "scaleY");
      });
      /**
       * была изменена группа объектов
       */
      this.history.add({
        canvas:   e.canvas || target.canvas,
        objectsStates: objectsStates,
        originalState:  states.original,
        modifiedState:  states.modified,
        object: target,
        type: 'group:modified',
        undo: function (a) {
          a.canvas._restoreGroup(a.object,a.objectsStates);

          a.object.set(a.originalState);
          a.object.setCoords();
          a.object.fire('modified');
          a.canvas.fire('object:modified', { target: a.object });
          a.canvas.renderAll();
        },
        redo: function (a) {
          a.canvas._restoreGroup(a.object,a.objectsStates);

          a.object.set(a.modifiedState);
          a.object.setCoords();
          a.canvas.fire('object:modified', { target: a.object });
          a.object.fire('modified');
          a.canvas.renderAll();
        }
      });

    }else{

      this.history.add({
        canvas:   e.canvas || target.canvas,
        originalState:  states.original,
        modifiedState:  states.modified,
        object: target,
        type: 'object:modified',
        undo: function (a) {
          a.canvas.setActiveObject(a.object);
          a.object.set(a.originalState);
          a.object.setCoords();
          a.object.fire('modified');
          a.canvas.fire('object:modified', { target: a.object });
          a.canvas.renderAll();
        },
        redo: function (a) {
          a.canvas.setActiveObject(a.object);
          a.object.set(a.modifiedState);
          a.object.setCoords();
          a.canvas.fire('object:modified', { target: a.object });
          a.object.fire('modified');
          a.canvas.renderAll();
        }
      });
    }
  },
  clearHistory () {
   this.application.fire("modified",e);
    this.history.clear();
  },
  disableHistory () {
    this.history.enabled = false;
  },
  undoChanges (a) {
    this.set(a.originalState)
      .fire('modified')
      .renderAll();
  },
  redoChanges (a) {
    this.set(a.modifiedState)
      .fire('modified')
      .renderAll();
  },
  undoDeleting (a) {
    a.canvas.add(a.object);
    a.canvas.setActiveObject(a.object);
    a.canvas.renderAll();
  },
  redoDeleting (a) {
    a.canvas.remove(a.object);
    a.canvas._discardActiveObject();
    a.canvas.renderAll();
  },
  _isHistoryActive(){
    return this.stateful && this.history.enabled && !this.processing && !this.history.processing;
  },
  onGroupRemoved (e) {
    if (!this._isHistoryActive())return;
   this.application.fire("modified",e);
    this.history.add({
      canvas:  e.target.canvas,
      object: e.target,
      type: 'group:removed',
      redo: function(a){
        a.object._objects.forEach((o) => {
          a.canvas.remove(o);
        });
        a.canvas.discardActiveObject();
        a.canvas.renderAll();
      },
      undo: function(a){
        a.object._objects.forEach((o) => {
          a.canvas.add(o);
        });
        a.canvas.setActiveObject(a.object);
        a.canvas.renderAll();
      }
    });
  },
  onObjectRemoved (e) {
    if (!this._isHistoryActive())return;
   this.application.fire("modified",e);
    this.history.add({
      canvas:  e.target.canvas,
      object: e.target,
      type: 'object:removed',
      redo: this.redoDeleting,
      undo: this.undoDeleting
    });
  },
  onDrawAfter (event){
    if (!e.target.canvas.stateful || !this.history.enabled || this.processing || this.history.processing) {
      return false;
    }
   this.application.fire("modified",e);
    this.history.add(this.freeDrawingBrush.getHistoryRecord(event))
  },
  onObjectAdded (e) {
    if (!this._isHistoryActive())return;
   this.application.fire("modified",e);
    this.history.add({
      canvas:  e.target.canvas,
      object: e.target,
      type: 'object:added',
      undo: this.redoDeleting,
      redo: this.undoDeleting
    });
  },
  initHistory (history) {
    if(!history){
      history = new fabric.History(this);
    }
    this.stateful = true;
    this.history = history;

    this.on({
      'modified':         this.onCanvasModified,
      'loading:begin':    this.clearHistory,
      'draw:after':       this.onDrawAfter,
      'object:modified':  this.onObjectModified,
      'canvas:cleared':   this.onCanvasCleared,
      'object:added':     this.onObjectAdded,
      'object:removed':   this.onObjectRemoved,
      'group:removed':    this.onGroupRemoved
    });

    // let _this = this;
    // this.history.on('changed', function(e){
    // });
    let proto = this.application.prototypes && this.application.prototypes.History;
    if(proto){
      if(proto.eventListeners){
        history.on(proto.eventListeners);
      }
    }
  },
  enableHistory () {
    this.history.enabled = true;
  }
});

fabric.HISTORY_MODE = {
  INDIVIDUAL: "slide",
  SHARED: "global"
};

fabric._.extend(fabric.Editor.prototype, {
  optionsOrder: fabric.util.a.build(fabric.Editor.prototype.optionsOrder).find("*").before("history").array,
  history: null,
  // _default_event_listeners : {
  //   "slide:change:begin" : function(){
  //     this.processing = true ;
  //     if(this.history){
  //       this.history.processing = true ;
  //     }
  //   },
  //   "slide:changed" : function(){
  //     this.processing = false;
  //     if(this.history){
  //       this.history.processing = false;
  //     }
  //   }
  // },
  setHistory (historyOption){
    if(historyOption === true){
      historyOption = fabric.HISTORY_MODE.SHARED;
    }
    if(historyOption === fabric.HISTORY_MODE.SHARED){
      this.history = new fabric.History(this);
      this.enableHistory();

      this.on("slide:created",(e) => {
        e.target.initHistory(this.history);
      });
      // this.on("ready",() => {
      //   if (this.slides) {
      //     this.slides.forEach(slide => {
      //       slide.initHistory(this.history);
      //     })
      //   }
      // })
    }
   /* if(historyOption === fabric.HISTORY_MODE.INDIVIDUAL){
      if(this.canvas){
        this.history = new fabric.History(this);
        this.enableHistory();
        this.canvas.initHistory(this.history);
      }
      //todo unsupported
    }*/
  },
  enableHistory () {
    this.history.enabled = true;
  }
});
