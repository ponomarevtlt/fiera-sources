let InstagramApi = require("../plugins/instagram-api"),
    Cookies = require("../plugins/cookie");

fabric._.extend(fabric.Editor.prototype,{
  setInstagram (data){
    this.instagram = data;
    this.instagram.api = new InstagramApi(this.instagram.clientID, this.instagram.redirectURI);

    this.instagram.accessToken = Cookies.get('instagramAcessToken');
  },
  instagramLogout () {
    delete this.instagram.accessToken;
    Cookies.remove('instagramAcessToken');
  },
  instagramAuth (cb){

    if(this.instagram.accessToken){
      cb();
      return;
    }

    let authUrl = this.instagram.api.getAuthUrl();
    //the pop-up window size, change if you want
    let popupWidth = 700,
      popupHeight = 500,
      popupLeft = (window.screen.width - popupWidth) / 2,
      popupTop = (window.screen.height - popupHeight) / 2;

    //the url needs to point to instagram_auth.php
    let popup = window.open(authUrl, '', 'width='+popupWidth+',height='+popupHeight+',left='+popupLeft+',top='+popupTop+'');
    if(!popup){
      console.error("popup is blocked");
      return;
    }

    let redirectHandler = function (event) {
      let data = JSON.parse(event.data);
      if (data.type === 'access_token_instagram') {
        this.instagram.accessToken =  data.access_token;
        if(typeof Cookies !== "undefined") {
          Cookies.set('instagramAcessToken', this.instagram.accessToken);
        }
      }
      cb();
      window.removeEventListener('message',redirectHandler, false );
    }.bind(this);

    popup.addEventListener("beforeunload", function(){
      window.removeEventListener('message',redirectHandler, false );
    });

    window.addEventListener("message", redirectHandler , false);
  },
  getInstagramMedia (callback){
    this.instagramAuth(() => {
      let ts = Math.round((new Date()).getTime() / 1000);

      this.instagram.api.user.media('self', {
        count: 200,
        min_timestamp: 0 ,
        max_timestamp:ts,
        access_token: this.instagram.accessToken
      }, data => {
        callback(data.data);
        // let images = _.pluck(data.data,"images");
        // let thumbnails = _.pluck(images,"thumbnail");
        // let thumbnails_urls = _.pluck(thumbnails,"url");
        // console.log(thumbnails_urls);
      });
    });
  }
});

fabric._.extend(fabric.Canvas.prototype,{
  actions: fabric._.extend(fabric.Canvas.prototype.actions,{
    instagramConnect: {
      className: "fa fa fa-chain",

    },
    instagramDisconnect: {
      className: "fa fa fa-chain-broken",

    },
    instagram: {
      className: "fa fa-instagram",
      type: 'effect',
      title: "instagram",
      effectClassName: "instagram-container",
      actionParameters: function ($el, data) {
        this.application.getInstagramMedia(data => {
          this.application.createGallery($el,fabric._.map(data,el => {
            return {
              type: "image",
              src: el.images.thumbnail.url,
              title: el.caption && el.caption.text
            }
          }))
        });
      }
    }
  })
});
