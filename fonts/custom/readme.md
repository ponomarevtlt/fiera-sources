Custom fonts should be placed here.

example 1
---

For single file font Families ,like Kedage you could add the following line to "fonts.json" file
```
    "Kedage":   "custom/Kedage.ttf"  //Custom Kannada font
```

Custom fonts should beadded to the end of the file:

example 2
---
"Times New Roman" 
To Add "Times New Roman" FontFamily  has 4 variations . the following files should be copied
 to the fonts directory:
 
"times.ttf", "timesbd.ttf", "timesi.ttf","timesbi.ttf"

Then add folowing lines to "fonts.json" in the beginning of the file
(for fallbacks fonts such as Times New Roman)
app willdetect all fonts by itself 
```
  "Times New Roman": "standart/times*.ttf"
```

There is full Font Notation Object: 
```
  "Times New Roman": {
    "variations": {
      "400":    "standart/times.ttf",
      "700":    "standart/timesbd.ttf",
      "400i":  "standart/timesi.ttf",
      "700i":  "standart/timesbi.ttf"
    }
  },
```